In sanctum Andream (homilia 7)
Ησυχίου, πρεσβυτέρου Ἱεροσολύμων, ἐγκώμιον εἰς τὸν ἅγιον Ἀνδρέαν
τὸν ἀπόστολον.
1 Σάλπιγξ ἡμᾶς ἀποστολικὴ πρὸς πανήγυριν ἤθροισεν, σάλπιγξ ἣν
ἐχάλκευσεν ὁ Χριστὸς σφύρᾳ τῷ σταυρῷ, ἄκμονι τῷ εὐαγγελίῳ, πυρὶ τῷ πνεύματι,
ἀσκοῖς εἰπεῖν θαρρῶ παλαιᾷ καὶ νέᾳ διαθήκῃ χαλκευσάμενος, σάλπιγξ ἣν εἰπεῖν οὐκ
ἄτοπον, προφητικόν τε ὁμοῦ καὶ ψαλμικὸν αὐτῇ δανεισάμενος ἐγκώμιον·
«Σαλπίσατε». Πᾶσι γὰρ Ἀνδρέας τῆς χάριτος μεταδίδωσι, πᾶσιν δανείζει τὴν
χορηγίαν τοῦ Πνεύματος, πάντας εἰς τὸν ἀγῶνα στρατολογεῖ ὁ πρὸ πάντων κληθεὶς
μετὰ πάντων στεφανωθῆναι βου λόμενος. «Σαλπίσατε», ἐπειδὴ σάλπιγγος ἄξια τοῦ
Χριστοῦ τὰ μυστήρια πάσῃ διαδοθῆναι τῇ γῇ, πᾶσιν ὀφείλονται κηρυ χθῆναι τοῖς
ἔθνεσιν. «Σαλπίσατε», ἐπειδὴ σάλπιγγος νοητῆς τεκμήριον ἡ πανήγυρις· πολέμου
γὰρ χρεία καὶ μάχης, ἀγῶνος καὶ πάλης, ὅπλων δεξιῶν καὶ ἀριστερῶν εἰς τὴν τοῦ
προκειμένου κατόρθωσιν. «Σαλπίσατε», ἵνα τῇ φωνῇ τὸν ἐχθρὸν καταλάβητε καὶ
τῆς νοητῆς Ἰεριχὼ τὰ τείχη καταστρέψητε. Ὁ ἦχος εἰς ὦτα κωφῶν εἰσέλθοι, ἐκ
νεκρῶν ἐγείροι τοὺς ἴσα θανάτῳ τὸν βαρὺν ὕπνον τῆς ἁμαρτίας καθεύδοντας.
«Σαλπίσατε ἐν νεομη νίᾳ σάλπιγγι»· νῦν γὰρ ὁ βίος ἡμῶν νεομηνία γέγονεν, τὸ γῆ
ρας ἀπέθετο, τὰς ῥυτίδας ἀπέσμηξεν, νέος ἐκ παλαιοῦ διὰ τὸν ἐγκαινισμὸν τοῦ
μυστηρίου γεγένηται «ἐν εὐσήμῳ ἡμέρᾳ ἑορτῆς ἡμῶν». Τί γὰρ τῆς παρούσης ἑορτῆς
εὐσημότερον ἐν ᾗ πάντα τὰ ἔθνη κροτήσει χεῖρας; Πᾶσα ἡ γῆ πρὸς εὐωχίαν
συνάγεται, βασιλεῖς τιμῆσαι τὸν ἁλιέα συντρέχουσιν, οἱ ζῶντες τροφὴν αἰ τοῦσιν
τὸν εἶναι δοκοῦντα νεκρόν, οἱ σοφοὶ πρὸς τὸν ἀγράμματον ὡς πρὸς διδάσκαλον
σπεύδουσιν· κοινὸν τοῦ κόσμου τὸ τοῦ πτω χοῦ δεῖπνον, κοινὴ πρὸς πάσας τὰς
πόλεις ἡ πανήγυρις.
2 Σάλπιγξ ἡμᾶς ἀποστολικὴ πρὸς πανήγυριν ἤθροισεν, Ἀν δρέας ὁ τοῦ χοροῦ
τῶν ἀποστόλων πρωτότοκος, ὁ πρωτοπαγὴς τῆς ἐκκλησίας στῦλος, ὁ πρὸ Πέτρου
Πέτρος, ὁ τοῦ θεμελίου θεμέλιος, ὁ τῆς ἀπαρχῆς ἀρχή, ὁ πρὶν κληθῆναι καλῶν, ὁ
πρὶν σφραγισθῆναι σφραγίζων, ὁ πρὶν προσαχθῆναι προσάγων, εὐαγ γέλιον
κηρύττων ὃ μηδέπω πεπίστευται, θύραν ἀνοίγων ἣν τέως οὐκ ἦν εἰσελθών, πρὶν
μαθεῖν ἀποκαλύπτων τὴν ζωήν, ἄρτον ἐπαγ γελλόμενος ὅσον μετὰ χεῖρας οὐκ
ἔλαβεν. Μὴ γάρ τι μέγα ἢ μικρὸν τῶν εἰς μάθησιν ἀκηκοὼς ἐτύγχανεν; «Ποῦ
μένεις;» αὐτὸς τῷ Ἰησοῦ ἔλεγεν. Ὁ δέ· «Ἔρχεσθε καὶ ὄψεσθε.» Καὶτί τοιοῦτον ἡ λέξις
εἶχεν οἷον Ἀνδρέας προλαμβάνει καὶ φθέγ γεται; Μὴ γὰρ τὴν ἑαυτοῦ δόξαν ἐν
τούτοις ὁ Χριστὸς ἀπεκάλυ ψεν; Μὴ ὅσης ἂν εἶχεν δυνάμεως ἑαυτὸν ἔδειξεν; Μὴ
δημιουρ γὸν ἑαυτὸν καὶ ποιητὴν ἀπήγγειλεν; Μὴ χωλῶν ἔταξε δρόμον; Μὴ τυφλῶν
ἀνῆψεν ὀφθαλμούς; Μὴ ὑπὲρ φύσιν ἠνάγκασεν; Πό θεν σοι τοίνυν τοιαῦτα περὶ
αὐτοῦ λέγειν; Πῶς γέγονας προφή της, πῶς θεοφόρος ἀθρόον; Τί θορυβεῖς τοῦ
Πέτρου τὰς ἀκοάς; Τί φθάσαι σπεύδεις ὃν οὐ δύνῃ φθάσαι; Ἀλλ' Ἰωάννης, ἐρεῖς,πρὸς
τούτους σε τοὺς λόγους ἐκίνησεν. Καὶ μὴν ταῦτα μόνα σου παρόντος ἐφθέγξατο·
«Ἴδε ὁ ἀμνὸς τοῦ θεοῦ ὁ αἴρων τὴν ἁμαρτίαν τοῦ κόσμου.» Σὺ δὲ ἀμνὸν ἀκούσας
εὐαγγελίζει θεόν, ἀφῆκας τὰ κάτω καὶ τὰ ἄνω πολυπραγμονεῖς, εἴασας τὴν λί μνην
καὶ τὸ δίκτυον εἰς τὸν οὐρανὸν ἔπεμψας· μὴ γὰρ ταῦτα σαγήνη θηρᾶν οἶδεν, μὴ
κάλαμος ἁλιευτικὸς ἕλκει; Σοῦ τῆς χει ρὸς ἡ γῆ καὶ ὁ οὐρανὸς κρέμαται. Τί λέγεις·
«Εὑρήκαμεν τὸν Μεσσίαν τὸν λεγόμενον Χριστόν»; Ποῦ γὰρ αὐτὸν καὶ πῶς καὶ πότε
ἀπώλεσας; Εὑρίσκει τις τὸ ἀπολωλός· ὁ δὲ πάντας σῴζων πῶς ἀπολέσθαι δύναται; Ὁ
1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

πανταχοῦ παρὼν πῶς εὑρε θῆναι δύναται ἢ λέγεται; Ἀλλὰ γὰρ οἶδεν ὁ Ἀνδρέας ὃ
λέγει· Εὑρήκαμεν ὃν ἀπώλεσεν ὁ Ἀδάμ, ὃν ἐζημιώθη ἡ Εὔα, ὃν ἡ νεφέλη τῆς
ἁμαρτίας ἀπὸ τῶν ὀφθαλμῶν τῶν ἡμετέρων ἔκρυψεν, ὃν ἐχώρισεν ἡ παρακοὴ καὶ
μακρὰν ἐγγὺς ὄντα καὶ παρόντα νομισθῆναι πεποίηκεν. Εὑρήκαμεν ὃν ἐκζητῆσαι καὶ
∆αυὶδ ἐσε μνύνετο· «Ἐν ἡμέρᾳ», φησίν, «θλίψεώς μου τὸν θεὸν ἐξεζήτησα.» Ἀλλὰ
καὶ Σοφονίας βοᾷ· «Ζητήσατε τὸν κύριον, πάντες ταπεινοὶ γῆς»· εὑρεθεὶς γὰρ τοὺς
ταπεινοὺς ἀνυψοῖ, ἐγείρει τῆς γῆς, ἀπαλλάττει τῆς φθορᾶς, τῶν ὑψηλῶν
ὑψηλοτέρους δείκνυσιν.
3 Εὗρεν οὗ τὴν εὕρεσιν ἡμῖν προφήτης ἄριστος Ὡσηὲ διελέ γετο· «Ἐν ἡμέρᾳ
θλίψεως αὐτῶν, ὀρθριοῦσι πρός με λέγοντες· πορευθῶμεν καὶ ἐπιστρέψωμεν πρὸς
κύριον τὸν θεὸν ἡμῶν, ὅτι αὐτὸς πέπαικεν καὶ ἰάσατο ἡμᾶς· πατάξει καὶ μοτώσει
ἡμᾶς· ὑγιώσει ἡμᾶς μετὰ δύο ἡμέρας, καὶ ἐν τῇ ἡμέρᾳ τῇ τρίτῃ ἀναστη σόμεθα
ἐναντίον αὐτοῦ, καὶ διώξομεν τοῦ γνῶναι τὸν κύριον· ὡς ὄρθρον ἕτοιμον
εὑρήκαμεν αὐτόν, καὶ ἥξει ἡμῖν ὡς ὑετὸς πρώ ϊμος καὶ ὄψιμος τῇ γῇ.» Ἐνταῦθά μοι
κάλει τὸν Ἰουδαῖον εἰς κρίσιν, εἰς δικαστήριον ἄγαγε τὸν ἀγνώμονα. Ἀνάγνωθι τὸν
προφήτην καὶ τὴν βίβλον περίπτυξον. Προφερέσθω κατ' αὐτοῦ τὰ παρ' αὐτοῦ· παρ'
αὐτῷ γὰρ γέγραπται, εἰ καὶ παρ' ἐμοὶ πέ πρακται. Λέγω τοίνυν τὴν ἑρμηνείαν τῶν
προφητικῶν. Σύστηθί πως ἐπὶ σοὶ πῶς καὶ πότε γεγένηται· ὅτι μὲν γὰρ ἐπατάχθητε,
καὶ ὑμεῖς ὁμολογεῖτε καὶ ἡμεῖς αἰσθανόμεθα· ἀλλ' οὐ βλέπω σου θεραπείαν, οὐ
θεωρῶ τὴν ἴασιν. Τοὐναντίον δὲ τὸ τραῦμα ἐν φλεγμονῇ, ἡ νόσος χαλεπωτέρα
γέγονεν, ἡ ἀρρωστία ἀπογνωσ θεῖσα πρὸς θάνατον ἔδραμεν. Ἐπειδὴ τοίνυν οὐκ ἔχεις
τῆς προ φητείας παρασχεῖν τὴν ἀπόδειξιν, ἐγὼ καὶ τὸν λόγον τὸν προφη τικὸν
ἑρμηνεύσω· τὸν καιρὸν παριστῶ καὶ τὸν τόπον ἐπιδείκνυμι καὶ πᾶσαν καλῶ τὴν
κτίσιν, οὐ τοῖς ἐμοῖς ῥήμασιν ἀλλὰ τοῖς θείοις μαρτυρῆσαι δεσποτικοῖς θαύμασι.
4 Πέπαικε τοίνυν ἡμᾶς ὁ θεός, ἡνίκα τοῦ Ἀδὰμ κατεδίκασε θάνατον, ὅτε τὸν
ἄρτον ἡμᾶς ἐσθίειν ἐν ἱδρῶτι τοῦ προσώπου κατέκρινεν, ὅτε τὴν γῆν ἡμῖν
ἀνατέλλειν ἀκάνθας καὶ τριβό λους ἐκέλευσεν, ὅταν διὰ τοῦ πρωτοπλάστου ὅλον τὸ
γένος <ἐκ> «τοῦ παραδείσου τῆς τρυφῆς» ἐξέβαλεν. Ἀλλὰ παίσας ἰάσατο καὶ πατάξας
ἐμότωσεν. Πῶς καὶ τίνα τρόπον; Ἄκουε τοῦ Ἠσαΐου λέγοντος· «Παιδεία εἰρήνης
ἡμῶν ἐπ' αὐτῷ· τῷ μώ λωπι αὐτοῦ ἡμεῖς πάντες ἰάθημεν», ὥστε οὐδὲν τῶν
εἰρημένων τῷ προφήτῃ διέψευσται. ∆είξει γὰρ τὰ ἐχόμενα μάλιστα· «Πα τάξει ἡμᾶς
καὶ ὑγιώσει ἡμᾶς μετὰ δύο ἡμέρας καὶ ἐν τῇ τρίτῃ ἡμέρᾳ ἀναστησόμεθα.» Ἆρα
μέμνησαι τὸν καιρὸν ἐν ᾧ τὸν Χριστὸν ἐσταύρωσας, ἐν ᾧ τὴν ζωὴν ἐπὶ ξύλου
θεωρῶν κρεμαμέ νην οὐκ ἐπίστευσας; Ἐπιγινώσκεις τὸ παρὰ σοῦ πραχθὲν δρᾶμα καὶ
τὰ τοῦ δράματος θαύματα, ὅτε γυμνὸς ἐπὶ τοῦ σταυροῦ ὁ Χριστὸς ὡς ἐπὶ γάμον
ἀνήγετο. Ἁμαρτίαν γὰρ οὐκ ἔχων, ἱμάτιον οὐκ ἔχρῃζεν· ἄμωμος ἦν καὶ λανθάνειν
οὐκ ὤφειλεν· οὐδὲν ἦν ἄσχημον αὐτῷ· αὐτὸς γὰρ τὰ εὐσχήμονα δείκνυσιν. Ἡ κτίσις
δ' ὅμως τοῦ δεσποτικοῦ κάλλους τὰς ἀστραπὰς οὐκ ἔφερεν, ἀλλὰ φῶς ἐνίκα τὸ φῶς,
ἥλιος ἡλίῳ παρεχώρει, ὁ αἰσθητὸς τῷ νοητῷ, ὁ τρέχων τῷ πανταχοῦ ὄντι, ὁ ἐν τόπῳ
τῷ ὑπὲρ τόπον, ὁ δύνων τῷ μὴ δύνοντι, ὁ τὴν νύκτα κινῶν τῷ θεμένῳ τὰ τῆς νυκτὸς
καὶ τῆς ἡμέρας ὅρια. Ἦν δὲ τότε νύξ· τὸ γὰρ σκότος πᾶσαν κατέσχε τὴν γῆν, ἀλλ'
ἡμέρα πάλιν ἀπὸ ἐνάτης ὥρας μέχρι ἑσπέρας ἔδραμεν, καὶ πάλιν τῇ ἡμέρᾳ νὺξ κατὰ
τὸν νόμον τὸν ἐπ' αὐτῆς ἠκολούθησεν, καὶ πάλιν ἡμέρα τὴν νύκτα διαδέχεται. Ὁ δὲ
ἡμερῶν δύο δρόμος κατὰ τάξιν γεγένηται, καὶ πέρας ἔσχεν ὁ τῆς προφητείας ὅρος·
«Ὑγιώσει ἡμᾶς μετὰ δύο ἡμέρας.»
5 Πέρας τότε καὶ τὸ ἑξῆς ἐλάμβανεν· «Ἐν τῇ ἡμέρᾳ τῇ τρί τῃ», φησίν,
«ἀναστησόμεθα.» Ἀνέστη γὰρ ὁ Χριστὸς καὶ ἡμεῖς συνανέστημεν. Ἀνῆλθεν ἐκ τῶν
νεκρῶν ὁ τῶν νεκρῶν ἐλευ θερωτὴς καὶ τὸ γένος ἀνεβίου τὸ ἡμέτερον, ἐπειδὴ δὲ
2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

καθ' ἅ φη σιν ὁ θεσπέσιος Παῦλος· «Ὥσπερ ἐν τῷ Ἀδὰμ πάντες ἀποθνῄσκουσιν,
οὕτως ἐν τῷ Χριστῷ πάντες ζωοποιηθήσονται.» Μάρ τυς δὲ ὁ τάφος οὗτος, ἡ τῶν
ζώντων χαρά, ἡ τῶν νεκρῶν παρά κλησις. Μάρτυς ὁ λίθος οὗτος ὁ κυλισθεὶς καὶ
κυλίσας ὅλα τὰ τῶν δαιμόνων εἴδωλα, οὐ μόνον δὲ κυλίσας, ἀλλὰ καὶ συντρίψας καὶ
λεπτύνας καὶ τῷ πτύῳ λικμήσας τοῦ σταυροῦ καὶ <ἀπὸ> τῆς γῆς «ἐκφυσήσας ὥσπερ
χοῦν ἀφ' ἅλωνος καὶ ὥσπερ ἀπὸ καπνοδόχης ἀτμίδα» καὶ τῇ ῥιπῇ καὶ τῷ ῥοίζῳ τοῦ
πνεύματος. Μάρτυρες αἱ γυναῖκες αὐταὶ ἀντὶ τῶν ποτε γυναικῶν ἡμέρας ἑκάστης
καὶ νυκτὸς αἰσθητὰ καὶ νοητὰ μύρα τῷ νυμφίῳ εἰσφέρουσαι. Μάρτυς ὁ λευχείμων
ἱερεὺς ὁ τῆς ἐκκλησίας ἄγγελος βοῶν πρὸς αὐτάς· «Μὴ φοβεῖσθε· οἶδα γὰρ ὅτι
Ἰησοῦν τὸν Ναζαρηνὸν τὸν ἐσταυ ρωμένον ζητεῖτε.» Ἀλλὰ τὴν ὥραν ἐπιζητεῖς; Φησὶ
γάρ· «ὡς ὄρθρον ἕτοιμον εὑρήσομεν αὐτόν.» Λουκᾶς σοι καὶ τοῦτο ἀπαγγεῖλαι
δυνήσεται· «Τῇ δὲ μιᾷ τῶν σαββάτων ὄρθρου βα θέως ἦλθον ἐπὶ τὸ μνῆμα, φέρουσαι
ἃ ἡτοίμασαν ἀρώματα. Εὗρον δὲ τὸν λίθον ἀποκεκυλισμένον ἐκ τῆς θύρας τοῦ
μνημείου· εἰσελ θοῦσαι δὲ οὐχ εὗρον τὸ σῶμα τοῦ κυρίου Ἰησοῦ. Καὶ ἐγένετο ἐν τῷ
διαπορεῖσθαι αὐτὰς περὶ τούτου καὶ ἰδοὺ δύο ἄνδρες ἐπέστη σαν αὐταῖς ἐν ἐσθήσεσιν
ἀστραπτούσαις. Ἐμφόβων δὲ γενομέ νων αὐτῶν καὶ κλινουσῶν τὰ πρόσωπα εἰς τὴν
γῆν, εἶπον πρὸς αὐτάς· "3Τί ζητεῖτε τὸν ζῶντα μετὰ τῶν νεκρῶν;"3 Τί λογί ζεσθε
περὶ τῆς ζωῆς τὰ τῆς ζωῆς ἀλλότρια; Ὁ παρ' ὑμῶν ὧδε ζητούμενος ἐν οὐρανοῖς
καθέζεται, ὁ ἐνταῦθα ψηλαφώμενος ἐν τοῖς κόλποις τοῦ Πατρὸς ἀναπαύεται, ὁ
γενόμενος νεκρὸς ἵνα νεκρῶν καὶ ζώντων κυριεύσῃ τὴν ζωὴν οὐ κατέλιπεν· αὐτὸς
γὰρ πᾶσι δανείζει τὴν ζωήν, ἐξ αὐτοῦ πᾶσα ἡ φύσις ἐμπνεῖται, ἐν αὐτῷ κινεῖται τὰ
κινούμενα, τὰ συνεστῶτα συνίσταται, ἐνερ γεῖ τὰ ἐνεργούμενα.» Ἐκεῖθεν αἱ
γυναῖκες ἐξελθοῦσαι τῷ Ἰησοῦ συνήντησαν, καὶ τὸν ὄρθρον εἶδον ὡς ὄρθρον, καὶ
εἶπον πρὸς αὐ τὸν ὅπερ ὁ ∆αυὶδ ἐν προφητείαις ἔψαλλεν· «Ὅτι παρὰ σοὶ πηγὴ ζωῆς,
ἐν τῷ φωτί σου ὀψόμεθα φῶς.» Εἰ δὲ λέγεις· «Αἱ μὲν γυ ναῖκες εὗρον, οὐχ εὗρον δὲ
οἱ ἀπόστολοι», καὶ «Πῶς ἐστι κέρδος κοινόν; Πῶς τὸ εὑρεθὲν ὅλης τῆς φύσεως
γίνεται σωτηρία;» περιττὸν ζητεῖς, μάτην ἐγκαλεῖς. Εὗρον γὰρ αἱ γυναῖκες ὅπερ διὰ
τῆς Εὔας ἀπώλεσαν πρότερον. Εὗρεν τὸ κέρδος ἡ παρασχοῦσα τῆς ζημίας τὴν
ὑπόθεσιν. Πλὴν ἀνάγνωθι τῆς προφητείας τὰ λεί ποντα· «Ὡς ὄρθρον ἕτοιμον
εὑρήσομεν αὐτόν, καὶ ἥξει ἡμῖν ὡς ὑετὸς πρώϊμος καὶ ὄψιμος τῇ γῇ.» Ταῦτα σαφῶς
κατὰ λέξιν πεπλήρωται. Οὐ χρῄζει θεωρίας ὁ λόγος, ἀλληγορεῖν οὐ δεόμεθα τοῦ
προφήτου τὴν ἔννοιαν· «Ἥξει ἡμῖν ὡς ὑετὸς πρώϊμος καὶ ὄψιμος τῇ γῇ.» Ὅρα
τοίνυν αὐτὸν πρώϊμον ταῖς γυναιξίν, ὄψιμον δὲ τοῖς ἀποστόλοις φαινόμενον· πρωῒ
μὲν γὰρ ἐνταῦθα ταῖς γυ ναιξὶν «Χαίρετε», ὀψὲ δὲ ἐν τῇ Σιὼν «Εἰρήνην» τοῖς
μαθηταῖς προσεφώνησεν· «Χαίρετε» ταῖς γυναιξὶν ἵνα λύσῃ τῆς λύπης τὴν
ἀπόφασιν, «Εἰρήνην» τοῖς μαθηταῖς, ἵνα καταλύσῃ τὴν ἔχθραν ἣν ὁ δράκων
ἐφύτευσεν. ∆ιὸ πρὸς αὐτοὺς μετὰ ταύτης τῆς διαστολῆς ἔλεγεν· «Εἰρήνη ὑμῖν· τῷ
γὰρ διαβόλῳ ὁ πό λεμος, ἡ πρὸς ἐκεῖνον ἔχθρα μένει· δεῖ γὰρ αὐτοῦ συντριβῆναι τὸ
κέρας, δεῖ αὐτοῦ τὸν ἰὸν ἐκχυθῆναι, δεῖ πατηθῆναι αὐτοῦ τὸ κέντρον. Νῦν ὅπλα
χαλκεύσω οἷς κατ' αὐτοῦ συγκροτηθήσεται πόλεμος.»
6 Οὐ μάτην τοίνυν ὁ Ἀνδρέας τῷ Πέτρῳ ἀπήγγειλεν· «Εὑ ρήκαμεν τὸν
Μεσσίαν τὸν λεγόμενον Χριστόν», ἐπειδὴ καὶ ὁ θεὸς τοιαῦτα διὰ τῆς Ἠσαΐου
προφητείας ἐφθέγγετο· «Ἐὰν ἐπιζητήσῃς, ζήτει καὶ παρ' ἐμοὶ οἴκει.» Ὅθεν τὸν
Χριστὸν ὁ Ἀνδρέας ἐπερωτᾶν ἐτόλμησεν· «Οἶδα γὰρ ὡς οἰκίαν ἐπὶ τῆς γῆς οὐκ ἔχεις,
οὐκ οἰκοδομεῖς ἐν τῇ φθορᾷ, οὐ σκηνοποιεῖς ἐν πλίνθῳ. "3Ποῦ μένεις;"3 ἵνα
θυρωρὸς τῆς αὐλῆς τῆς δεσποτικῆς γένωμαι. "3Ἐξελεξάμην γὰρ παραριπτεῖσθαι ἐν
τῷ οἴκῳ τοῦ θεοῦ μου μᾶλλον ἢ οἰκεῖν με ἐν σκηνώμασιν ἁμαρτωλῶν."3 "3Ποῦ
μένεις;"3 Ἐὰν μάθω, τῆς θύρας οὐκ ἀφίσταμαι.» Ἀλλ' αὕτη μὲν ἡ τῆς ἐκκλησίας νῦν
3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἐκ τῶν εὐαγγελίων παρατεθεῖσα τρά πεζα· ἐγὼ δὲ αὐτῆς τὰ ὄψα ὡς οἰκέτης μόνον
παρέθηκα, διόπερ ἐφ' ἑτέραν, εἴπερ ὑμῖν δοκεῖ, μεταβήσομαι τράπεζαν. Ἀνδρέαν ἡ
κλῆσις στεφανοῖ· δείκνυσι τῷ στρατιώτῃ τὴν ἐκλογήν. Παρέ στησεν τοῦ
ἀμπελουργοῦ τὴν ἀξίαν ἡ μίσθωσις ἣν οὕτως ἡμῖν ὁ Μαθθαῖος παρέθετο· «Ἀπὸ τότε
ὁ Ἰησοῦς ἤρξατο κηρύττειν καὶ λέγειν· Μετανοεῖτε· ἤγγικεν γὰρ ἡ βασιλεία τῶν
οὐρανῶν.» «Ἀπὸ τότε.» Πότε; –Ἀφ' οὗ τὸν Ἰορδάνην ἡγίασεν καὶ τοῦβαπτίσματος
τὴν δωρεὰν ἐφύτευσεν καὶ τὴν κεφαλὴν τῶν δρακόν των συνέτριψεν ἐν τοῖς ὕδασιν
καὶ τοῦ πνεύματος τὴν δωρεὰν ἐξ οὐρανοῦ κατήγαγεν, ἤνοιξεν ὁδὸν ζωῆς, τῆς
σωτηρίας τὴν πύ λην ἀνεπέτασεν· «Ἀπὸ τότε κηρύττειν ἤρξατο καὶ λέγειν· Μετα
νοεῖτε· ἤγγικεν γὰρ ἡ βασιλεία τῶν οὐρανῶν.» Ἦλθεν γὰρ ὁ δανειστὴς ὁ πᾶσιν
ἀπαλείφων τὰ ἐγκλήματα· ὁ ἄφθονος βα σιλεὺς ἐπεδήμησεν· πάρεστιν ὁ δικαστὴς
τῆς φυλακῆς τοὺς ὑπευ θύνους ἐλευθερῶν, ὁ λύων τοὺς κλοιούς, ὁ ἀπαλείφων
πᾶσιν ἐγ κλήματα, «ὁ καμμύων τοὺς ὀφθαλμοὺς αὐτοῦ ἵνα μὴ ἴδῃ ἀδικίαν, ὁ
βαρύνων τὰ ὦτα αὐτοῦ ἵνα μὴ ἀκούσῃ κρίσιν αἵματος.» «Με τανοεῖτε»· πλύνατε τῷ
λόγῳ τὰς πράξεις, γλώσσῃ τὸ στόμα ἀποσμήξατε, εὐχῇ τὸν βίον καθάρατε,
προσενέγκατε θυσίαν ἥτις πολλῶν ἄρχει. «Μετανοεῖτε»· οὐκοῦν ὁ μετανοῶν οὐ
μόνον τῶν δεινῶν ἀπαλλάττεται ἀλλὰ καὶ τῶν ἀγαθῶν κληρονομίαν δέχεται, οὐ
μόνον κακίας ἐλεύθερος ἀλλὰ καὶ γεωργὸς δικαιοσύ νης γίνεται. «Μετανοεῖτε·
ἤγγικεν γὰρ ἡ βασιλεία τῶν οὐρα νῶν.» Οἱ δυνατοὶ βιάσασθε, οἱ ἀσθενεῖς
δυναμώθητε, οἱ χωλοὶ τρέχετε καὶ οἱ τρέχοντες σπεύσατε καὶ οἱ σπεύδοντες ἑαυτοῖς
μὴ θαρρήσητε, ἀλλὰ τῷ θεῷ τῷ τὸν δρόμον ἰθύνοντι· «Οὐ γὰρ τοῦ θέλοντος οὐδὲ τοῦ
τρέχοντος ἀλλὰ τοῦ ἐλεοῦντος θεοῦ.» «Μετανοεῖτε»· οὐκ ἔστι γὰρ κακία νικῶσα τὴν
μετάνοιαν, οὐκ ἔστιν ἰὸς ὅστις δύναται τούτου τοῦ μέλιτος ἀποσβέσαι τὴν δύνα μιν·
πᾶσιν ὁ διάβολος παλαίει καὶ πᾶσιν ἀνθίσταται καὶ πᾶσιν μάχεται· πρὸς μόνην οὐ
τολμᾷ παλαῖσαι τὴν μετάνοιαν· αὐτὴ γὰρ αὐτοῦ ἐκ τῶν φαρύγγων ἐκβάλλει τὸ
πρόβατον, ἐξ αὐτῆς ἀπο σπᾷ τῆς χειρὸς τοῦ τυράννου τὸν αἰχμάλωτον.
7 «Καὶ περιπατῶν ὁ Ἰησοῦς εἰς τὴν θάλασσαν τῆς Γαλιλαίας εἶδεν δύο
ἀδελφούς, Σίμωνα τὸν καλούμενον Πέτρον καὶ Ἀν δρέαν τὸν ἀδελφὸν αὐτοῦ.»
Πολλοὺς ἰδὼν παρεῖδεν, ἐπειδὴ τῶν δεσποτικῶν ὀφθαλμῶν ἦσαν ἀνάξιοι. «Εἶδεν
δύο ἀδελφούς», οὐ σώματι μόνον ἀλλὰ καὶ τὴν ψυχήν, τὴν φύσιν καὶ τὴν χάριν, τὴν
πολιτείαν καὶ τὴν συγγένειαν. «Εἶδεν δύο ἀδελφούς, Σίμωνα τὸν καλούμενον
Πέτρον», οὐ τὸν κληθησόμενον· περὶ γὰρ τῆς κλήσεως ἠξιωμένος ἐτύγχανεν. Ἤδει
γὰρ ὅτι ὁ Χριστὸς πρὸς αὐτὸν εἰρηκὼς ἦν· «Σὺ εἶ Σίμων, σὺ κληθήσῃ Κηφᾶς ὁ λεγόμε
νος Πέτρος.» Ἰδοὺ πρὸ τῆς ὁμολογίας τὸν μισθὸν ἔχεις καὶ πρὶν ἐργάσῃ τὸν
ἀμπελῶνα τὸ δηνάριον ἔλαβες, πρὶν ἀνάψῃς τὸν βωμὸν τὴν θυσίαν προσδέδεξαι,
πρὶν κηρύξῃς ἐστεφανώθης, πρὶν εὐαγγελίσῃ τὸ τῆς θεολογίας σοι βραβεῖον
ὑπήντησεν. Αἴτιος δὲ αὐτοῦ τούτου ὁ Ἀνδρέας ἐγένετο καὶ τούτου χάριν ὁ Μαθθαῖος
προσέθηκεν· «Καὶ Ἀνδρέαν τὸν ἀδελφὸν αὐτοῦ», καίτοι ἦν ἅπαξ εἰρηκώς· «Εἶδεν δύο
ἀδελφούς», ἀλλ' ὅμως ἰδοὺ νῦν ἀνα λαμβάνει καὶ δεύτερον, εὐεργέτην κηρύξαι τὸν
Ἀνδρέαν τοῦ Πέτρου βουλόμενος. ∆ιὰ τοῦτό φησιν· «Σίμωνα τὸν λεγόμενον Πέτρον
καὶ Ἀνδρέαν τὸν ἀδελφὸν αὐτοῦ», τὸν πληρώσαντα τὴν ἐντολὴν ἣν ἐν Παροιμίαις ὁ
Σολομὼν προϋπέγραψεν· «Ἀδελφοὶ ἐν ἀνάγκαις ἔστωσαν χρήσιμοι», ἐν τοῖς ἄγαν
συμφέρουσιν, ἐν τοῖς λίαν σπουδαίοις, ἐν τοῖς σφόδρα κατεπείγουσιν· τοῦτο γὰρ ὡς
ἀναγκαίας καὶ ἀπαραιτήτους αἰτίας ἡ σοφία ἐκάλεσεν· «Ἀδελ φοὶ δὲ ἐν ἀνάγκαις
ἔστωσαν χρήσιμοι.» Οὐ τοίνυν παρεῖδεν ὁ Ἀνδρέας τὸν Πέτρον κατεπειγούσης τῆς
κλήσεως· ἀκήκοας γὰρ ὅπως πρὸς αὐτὸν ἔλεγεν· «Εὑρήκαμεν τὸν Μεσσίαν τὸν λεγό
μενον Χριστόν.» ∆ράμε, Πέτρε, φησίν, πρὶν προλάβωσι Πέ τροι, πρὶν ἄλλοι τῶν
πρωτείων ἐπιλάβωνται· οὗτος γὰρ ὅθεν θέλει στρατολογεῖ, οὗτός ἐστιν ὁ ἀπὸ «τῶν
4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ὁδῶν καὶ φραγμῶν» ἀθροίζων, ὁ πόρνους καὶ τελῶνας σφραγίζων. Ἔνθα ἂν ῥίψῃ
τὸν σπόρον, ἐκεῖ ἡ γῆ καρποφορεῖ, ὅπου τὸ κλῆμα φυτεύσει ἡ ἄμπελος παραχρῆμα
ἀνέρχεται, ἔνθα ἂν ῥίψῃ τὸ δίκτυον, πλῆρες ἰχθύων εὑρίσκεται. «Εἶδεν δύο
ἀδελφούς, Σίμωνα τὸν λεγόμενον Πέτρον καὶ Ἀνδρέαν τὸν ἀδελφὸν αὐτοῦ,
βάλλοντας ἀμφίβληστρον εἰς τὴν θάλασσαν· ἦσαν γὰρ ἁλιεῖς.» Θαύμασον τοῦ
Χριστοῦ τὴν φιλοτιμίαν ὅτι τοὺς πτωχοὺς καὶ ἀπαιδεύτους καὶ ἀγραμ μάτους καὶ
ἁλιεῖς ἐκλέλεκται. Ἡ σοφία γὰρ οὐ χρῄζει συνηγόρων ἀλλ' ὑπουργῶν· οὐδεὶς γὰρ
αὐτῇ παρέχει, παρ' αὐτῆς δὲ πάντες τὰ πάντα λαμβάνομεν· οὐδεὶς γὰρ δανείζει τῷ
πλουσίῳ, πάντες γὰρ παρ' αὐτοῦ δανειζόμεθα. Τί τοίνυν χρεία σοφῶν ἔσται ἐν τῷ
κηρύγματι; ∆εῖ γὰρ γενέσθαι μωρὸν τὸν τῆς ἀληθοῦς σοφίας μεταλαμβάνειν
μέλλοντα.
8 Θαύμασον τοῦ Χριστοῦ τὴν σοφίαν καὶ τῶν ἀποστόλων τὴν ἐκλογὴν
μακάρισον. Οἷα ἀνθ' οἵων ἠλλάξαντο; ∆ηλοῖ γὰρ τὰ ἐχόμενα τοῦ λέγειν αὐτοῖς·
«∆εῦτε ὀπίσω μου καὶ ποιήσω ὑμᾶς ἁλιεῖς ἀνθρώπων.» Τῶν ἀρχαίων ἁπάντων
ἐπιλάθεσθε, ἀρνήσασθε τοὺς γονεῖς τῆς σαρκὸς ἵνα τὸν πατέρα τὸν ἀληθινὸν ἀπολάβητε. Μικρὸς ὑμῖν ὁ κόσμος ἔστω ὅλος, πρὸς τὸν ποιητὴν τὸν ἑαυτοῦ κρινόμενος.
«∆εῦτε ὀπίσω μου»· πρὸς γὰρ ζωὴν ὑμᾶς ἀνάγω, πρὸς τὸ φῶς χειραγωγῶ. «∆εῦτε
ὀπίσω μου», ἵνα πατριάρχας προλάβητε, ἵνα πρὸ τῶν προφητῶν τὸν κλῆρον ὑποδέξησθε· καὶ ἔσχατοι ὄντες γενήσεσθε πρῶτοι, ἵνα οἱ περὶ τὴν ἑνδεκάτην τῶν πρωῒ
ἅμα μισθωθέντων ἀνώτεροι γένησθε. «∆εῦτε ὀπίσω μου»· ἐμοὶ γὰρ ἀκολουθοῦντες
βίον ἕξετε ἄπονον· χωρὶς γὰρ ἀργυρίου πλουτήσετε, χωρὶς φαρμάκων ἰατροὶ τῆς
οἰκουμένης ἔσεσθε, χωρὶς ὅπλων εὑρεθήσεσθε τῶν ἐχθρῶν δυνατώτεροι. «∆εῦτε
ὀπίσω μου»· «Ἐγὼ γάρ εἰμι τὸ φῶς τοῦ κόσμου», «ἐγώ εἰμι ἡ ὁδὸς καὶ ἡ ζωὴ καὶ ἡ
ἀλήθεια», ἐγὼ πρὸς τὸν Πατέρα χειραγωγῶ, «ἐπὶ ἀσπίδα καὶ βασιλίσκον ἐπιβαίνειν
καὶ καταπατεῖν λέοντα καὶ δράκοντα» τοῖς ὀπίσω μου πορευομένοις δίδωμι. «Καὶ
εὐθέως ἀφέντες τὰ δίκτυα ἠκολούθησαν αὐτῷ.» Μακαρίσαι τοίνυν προσῆκεν τὴν
τῶν ἀποστόλων εὐγνωμοσύνην· ὅπερ γὰρ εἶχον ἀφῆκαν, ἣν εἶχον ἀφορμὴν τοῦ βίου
ταύτην ὑπερεῖδον. Ὅλος ἦν αὐτοῖς ὁ θησαυρὸς καὶ ὅλος ὁ πλοῦτος τὰ δίκτυα· τὴν
γὰρ ζωὴν ἐν σαγήναις εἶχον· ἡ τῶν ἰχθύων ἄγρα τὰς ἐλπίδας τοῦ βίου ἔτρεφεν. Οἱ δὲ
ἀφῆκαν καὶ ταῦτα, οὐκ εἶπον πρὸς ἑαυτούς· λήρους ἡμῖν οὗτος καὶ μύθους περιττοὺς
διαλέγεται, ἁλιεῖς ἀνθρώπων καταστῆσαι ἐπαγγέλλεται. Καὶ ποῦ τοιοῦτον δί κτυον;
Ποῦ σαγήνη τοιαῦτα δυναμένη θηρᾶν; Τίς δὲ λίμνη, τίς δὲ θάλασσα ἀνθρώπους ἡμῖν
τοῖς δικτύοις προσφέρουσα; Τί δὲ τὸ κέρδος τῆς θήρας ἔσται; Ποῦ τὴν θήραν μετὰ τὴν
θήραν χρησόμεθα; Ἀλλὰ τῷ καλοῦντι προσέχοντες καὶ τῇ πίστει βεβαιωθέντες καὶ
τὸν λόγον ἔχειν τι νενοηκότες ἀπόκρυφον, ἠκολούθησαν αὐτῷ· καὶ παραχρῆμα τῆς
γῆς ἄνω γεγόνασι καὶ βασιλέων ὤφθησαν ἐνδοξότεροι, ἀγγέλων ὅμοιοι, λειτουργοὶ
θεοῦ, σωτηρίας κήρυκες, ἀθανασίας διάκονοι, ἡλίου καὶ σελήνης καὶ ἀστέρων
ἐκλαμπρότεροι· τούτων γὰρ ἡ δόξα οὐχ ὡς ὑπὸ τῶν οὐρανῶν ἀλλ' ὑπὲρ τῶν
οὐρανῶν ἐστιν, τὸ φῶς ἔχουσιν ἄσβεστον, τὸν δρόμον ἀκατάπαυστον, τὰς ἀκτῖνας
ἀειφανεῖς, ἃς ἡμεῖς ἐλλαμφθῆναι ταῖς ἡμετέραις διανοίαις εὐξώμεθα. Καὶ αὐτὸ τὸ
σκότος ἐκφύγωμεν καὶ τοῦ βίου τὴν ἅλμην ἐκκλίνωμεν καὶ τὴν ἄστατον
πορευόμενοι θάλασσαν τὸν κλύδωνα τῶν ψυχοφθόρων κυμάτων ἐκφύγωμεν καὶ
γενώμεθα Χριστοῦ ἀγραμμάτων ἀποστόλων ἰχθύες τῆς ἄνω σαγήνης, ὄψα τῆς
δεσποτικῆς τραπέζης ἣν ὁ βασιλεὺς τῶν βασιλευόντων δειπνεῖ βρῶσιν τὴν ἡμετέραν
σωτηρίαν ἡγούμενος· αὐτῷ ἡ δόξα καὶ τὸ κράτος εἰς τοὺς αἰῶνας τῶν αἰώνων.
Ἀμήν.

5
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

