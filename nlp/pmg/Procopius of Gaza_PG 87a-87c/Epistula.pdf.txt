Epistula (e cod. Barocc. gr. 131)
Εὖ γέ σοι τῆς θαυμασίας ἐπιστολῆς· ὡς ἔαρος ἡμῖν προσέβαλε λογικοῦ, καὶ μουσικόν
τι λίαν ἀντὶ τῶν χελιδόνων ἠχεῖ. ὡς ἔνθους εἰμί τις ὑφ' ἡδονῆς, πάντα θαυμάζων,
τὴν λέξιν, τὸν νοῦν, τὴν ἁρμονίαν, τὸν γράψαντα. καί μοι δοκῶ Λυδὸς ἐξαίφνης
εἶναι, τὰ Λυδῶν μιμησάμενος, οἳ τῷ ∆ιονύσῳ προσκείμενοι μόνον ἔαρ ἦγον ἡνίκα δὴ
καὶ τύχοι παρ' αὐτοῖς ὁ ∆ιόνυσος. ἀλλὰ μὲν ἴσως ταῦτα γελάσεις, ὡς αὐτόχρημα τὸ
σοφιστικὸν ἐπανθεῖ αὐτοῖς, καί με τυχὸν ὡς ἐν ὀνείδει καλέσειας σοφιστήν· ἐγὼ δὲ
τὰ καλὰ μὴ θαυμάζειν οὐκ ἔχω, κἂν σιωπήσῃς, ὦ λῷστε, τἀναντία φθεγγόμεθα·
ἀλλὰ μή μοι διδόναι πρᾶγμα ζητοῦντι τὴν ἐκ τῆς σιωπῆς λύπην παραστῆσαι τοῖς
γράμμασιν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

