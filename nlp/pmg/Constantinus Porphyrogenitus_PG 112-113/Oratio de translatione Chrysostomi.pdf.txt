Oratio de translatione Chrysostomi
Κωνσταντίνου ἐν αὐτῷ τῷ Χριστῷ, τῷ αἰωνίῳ βασιλεῖ, βασιλέως, υἱοῦ Λέοντος τοῦ
σοφωτάτου καὶ ἀειμνήστου βασιλέως, λόγος, ἡνίκα τὸ τοῦ σοφοῦ Χρυσοστόμου
ἱερὸν καὶ ἅγιον σκῆνος ἐκ τῆς ὑπερορίας ἀνακομισθὲν ὥσπερ τις πολύολβος καὶ
πολυέραστος ἐναπετέθη θησαυρὸς τῇ βασιλίδι ταύτῃ καὶ ὑπερλάμπρῳ τῶν πόλεων.
Εὐλόγησον πάτερ.
Τὶ τερπνότερον τοῦ νῦν ὁρωμένου θεάματος ἢ τί λαμπρότερον τῆς παρούσης
ἡμέρας; Ἰδοὺ λαὸς ἔκκριτος, θεῷ μόνῳ καὶ ἀρετῇ ἀνακείμενος καὶ τοσαύτην τῆς
οἰκείας εὐταξίας καὶ κοσμιότητος τὴν περιουσίαν ἐπιδεικνύμενος, ὡς μή τινα χωρὶς
ἀποδοχῆς καὶ θαύματος καὶ ἐπαίνου τοῦτον παραδραμεῖν. Ποῖος γὰρ λειμὼν
εὐανθὴς παντοίοις κομῶν φυτοῖς καὶ ἄνθεσί τε ποικίλοις πάντοθεν
καταγλαϊζόμενος οὕτω δύναται τὸν ὀφθαλμὸν ἑστιᾶσαι καὶ μὴ ἀλλαχοῦ πλανᾶσθαι
παραχωρῆσαι, ἀλλ' ἐφ' ἑαυτὸν ὅλην τὴν ὅρασιν οἱονεὶ δέσμιον κρατῆσαι διὰ τὸ ἐν
αὐτῷ κάλλος, ὡς τοῦτο τὸ ἱερὸν σύνταγμα, τὸ ἅγιον ἔθνος, τὸ βασίλειον ἱεράτευμα;
Καὶ τίς ὁ τρόπος τῆς συνελεύσεως καὶ τίς ἡμᾶς ἐπὶ τὸ αὐτὸ οὕτως συνήγαγεν ἢ
πρόδηλον, ὡς ἐπανιόντι τῷ Χρυσοστόμῳ πατρὶ ὁ λαὸς Κυρίου θρίαμβον συγκροτεῖ
καὶ προευτρεπίζεται τὴν πομπὴν καὶ διὰ τοῦτο πολὺ τὸ χαῖρον ἐν τῇ ἑκάστου ψυχῇ
καὶ διαπρύσιαι τῶν λόγων αἱ σάλπιγγες ὡς ἄξιον τῆς παρούσης ἡμέρας τὸ σύνταγμα,
ὡς πρέπον τῇ λαμπρᾷ ταύτῃ καὶ ἐρασμίῳ πανηγύρει τὸ τοιοῦτον συνάθροισμα; ∆ιὸ
δεῦτε χαρμονικῶς μεγάλῳ καὶ ὑψηλῷ τῷ κηρύγματι κράζωμεν· εὐλογητὸς ὁ θεός, ὁ
καταξιώσας ἡμᾶς εἰς τήνδε τὴν ἱερὰν καὶ σεβάσμιον καταντῆσαι πανήγυριν, ἐν ᾗ ὁ
λαὸς θεοῦ ἱερός τε καὶ ἅγιος, ὃν ἑαυτῷ Κύριος ἐξελέξατο, ὁ ἐπὶ τῶν ἀΰλων Κυρίου
χειρῶν καλῶς ἐζωγραφημένος. Ἄξιον γὰρ ἀπὸ τῶν προφητικῶν προρρήσεων πρὸς
τοὺς παρόντας οὑτωσί πως καὶ προσφυῶς ἐλλαμπρύνεσθαι. Τίς ὑμᾶς ὡς ἐξ ἱερᾶς
προθεσμίας καὶ ὡς ἄν τις εἴποι ἐξ ἐπιτάγματος ἤθροισε; Πόθεν λαβόντες τὰς
ἀφορμὰς τῆς συνελεύσεως τῆς ἐνθάδε σπουδαίαν οὕτω καὶ πρόθυμον τὴν ἄφιξιν
ἀπειργάσασθε; Τίς ὁ τὴν πάνδημον ταύτην συναθροίσας πανήγυριν καὶ ὥσπερ ἔκ
τινος σίμβλου τοῦ ἱεροῦ τούτου οἴκου ἐκχυθῆναι τὸν τοσοῦτον ἐσμὸν τῶν ἱερῶν
μελισσῶν κατηνάγκασε καὶ ταῦτα ἐν οὕτω σφοδρᾷ τοῦ χειμῶνος δριμύτητι; Τίς
ὥσπερ ἐν στρατοπέδου κινήματι στενοχωρουμένας ἔδειξεν ἐκ τῆς τῶν συρρεόντων
πληθύος τὰς ἐνταῦθα φερούσας ὁδοὺς καὶ ταῦτα ἐν κατηφείᾳ χειμερινῇ, ἡνίκα καὶ
Σκύθης καὶ βάρβαρος καὶ ὅσα ἄλλα πολεμικώτατα καὶ μάχιμα τῶν ἐθνῶν ἠρεμεῖν
οἶδε διὰ τὴν ἀπὸ τοῦ χειμῶνος δριμύτητα τῆς ἀκαίρου πορείας ἀνακοπτό 307 μενα,
ὅτε καὶ θάλασσα πλωτήρων ἔρημος καὶ ναυτιλίας καθίσταται καὶ γεωργὸς ἄροτρον
καὶ τὰ πρὸς γεωργίαν ἀφεὶς ἐπιτήδεια τοὺς ἀροτῆρας βοῦς δαψιλῶς ἐπὶ φάτνης
ἐκτρέφει καὶ θεραπεύει, ἢ πρόδηλον ὅτι λαμπρῷ καὶ περιχαρεῖ τῷ κηρύγματι πάντας
τοὺς φιλοθέους καὶ φιλοχρυσοστόμους ὡς φιλοπάτορας ἡ θεία αὕτη καὶ σεβάσμιος
συγκαλεῖται πανήγυρις καὶ δαψιλεῖ τῷ φωτὶ τὸ τῆς νυκτὸς ζοφῶδες πρὸς τὴν
ἡμερινὴν λαμπρότητα προσφόρως πάνυ μεταλλαττόμενον καὶ ἐν τούτῳ ποιητικῶς
εἰπεῖν τῷ ἀστερόεντι οὐρανῷ ἐξομοιοῦται καὶ ἀπεικάζεται; ∆ιὸ δεῦτε λαὸς ἱερός,
μερὶς ἐκλεκτή, τίμιον ἔθνος καὶ ἅγιον, σχοίνισμα Κυρίου τὸ κράτιστον, τὰ καλὰ καὶ
τριπόθητα πρόβατα, ἃ τῆς τοῦ διδασκάλου φωνῆς καλῶς καὶ σωτηρίως ἀκούοντα ἐπὶ
νομὰς ζωηφόρους καὶ σωτηριώδεις συνήθως ἐκτρέφεσθε, τὴν εὐφημίαν μοι
ἐνυφαίνοντες τῶν ἀγοραίων ταραχῶν καὶ ἀκοὴν ἀνακαθάρατε καὶ διάνοιαν, ἵνα τῇ
τῶν ἐκείνου καλῶν διδαχῶν ἀναμνήσει κατατρυφήσωμεν καὶ τὴν ὥσπερ ἐκ μαστῶν
διαύλων πρὸς τὴν τῆς νίκης νύσσαν ἐπάνοδον τοῦ ἀθλητοῦ κατοπτεύσωμεν· εἰ γὰρ
καὶ μὴ πρὸς τὸ πᾶν τῆς ὑποθέσεως ἀποδύσασθαι διὰ τὸ ἀνέφικτον τεθαρρήκαμεν,
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

ἀλλ' οὖν καὶ ἡ τοῦ μέρους ἀφήγησις εἰς τὴν τοῦ λόγου χειραγωγήσει ἀνάμνησιν καὶ
ἀπὸ τῶν τελευταίων τὰ τῆς ἀρχῆς τῷ νῷ θεωρήσομεν, ὥσπερ ἐκ τῶν ὀνύχων τὸν
λέοντα τεκμαιρομένοι καὶ ἀπὸ τῆς κρόκης τὸ ὕφασμα ἢ καὶ τὸ πᾶσι δῆλον τὸ
δένδρον ἐκ τοῦ καρποῦ. Τὰ μὲν οὖν ἄλλα λεγόντων τε καὶ γραφόντων ἐστὶν ἀκούειν
πολλῶν καὶ ὅσοι τῆς λογικῆς παιδείας καὶ τῶν ταύτης ὀργίων οὐ πάντῃ ἀμύητοι καὶ
τοῦ μουσικοῦ κρατῆρος ἀπώναντο. Τοσοῦτοι γὰρ τὰ ἐκείνου θαυμάζουσιν, ὅσοι
σχεδὸν καὶ λόγου μετέχουσι. Τὰ δ' ὅσα πρὸς τὴν παροῦσαν ἡμέραν τείνει καὶ ἃ
διεξιὼν οὐ περιττὸς ἴσως δόξω καὶ ὀχληρός, ταῦτα δὴ διεξέρχομαι. Ἦν ὅτε καλῶς
εἶχε τῷ λαῷ Κυρίου τὰ πράγματα καὶ ταῖς ἐκκλησίαις Θεοῦ τὸ ἀστασίαστον
ἐνεπολιτεύετο καὶ ἡ πρὸς ἀλλήλους ἀγάπη ὥσπερ τις κλῆρος πατρῷος ἐτηρεῖτο καὶ
ἐφυλάττετο καὶ τὸ Χριστοῦ γεώργιον ἐπληθύνετο καὶ πάντες ἔσπευδον ἐκζητεῖν τὸν
θεὸν καὶ ταῖς τρίβοις τῶν θείων αὐτοῦ πορεύεσθαι ἐντολῶν, ὅταν ὁ μέγας οὗτος τῆς
ὀρθοδοξίας κανὼν, ἡ ποτιμωτάτη τῶν λόγων καὶ ἀέναος θάλασσα, ἡ τῆς σοφίας
ἀκρόπολις, ἡ ὡς ἀληθῶς οὐρανία φωνή, τὸ τοῦ θείου Πνεύματος καταγώγιον, ἡ
σάλπιγξ τοῦ σωτηρίου κηρύγματος, ὁ τῆς οἰκουμένης ἄδυτος ἥλιος, τὸ τῆς
ἐκκλησίας ἐμπρέπον ἱερῷ στερεώματι τὴν περίγειον πᾶσαν οἰκουμένης διηύγαζε καὶ
ταῖς ἀστραπηφόροις ἀκτίσιν αὐτοῦ τῶν λόγων ὁ κόσμος κατεφαιδρύνετο καὶ πάντες
ἐκ τοῦ καταρρύτου καὶ εὐώδους αὐτοῦ λειμῶνος τὰ παντοδαπὰ καὶ ποικίλα ἄνθη
308 τῶν ἀρετῶν ἀπεδρέποντο καὶ τὰ τῆς διανοίας ἀπεπλήρουν προκόλπια καὶ
πάντας εἶχε παιδευομένους καλῶς καὶ ἐκτρεφομένους ἐπὶ ζωηφόρους νομὰς καὶ ἐφ'
ὕδωρ πνευματικῆς ἀναπαύσεως. Ἐπεὶ δὲ τὴν τοσαύτην τῶν ἀγαθῶν φορὰν οὐκ
ἔμελλε πράως οἴσειν ὁ φθόνος, ἐκ τῶν ἀνακτορικῶν θαλάμων αἱ συσκευαὶ καὶ ἡ
ἄδικος ἐξορία συνερράπτετό τε καὶ ἐξηρτύετο. Τὸ γὰρ βασιλέως γύναιον
χρυσομανίας κάτοχον ὂν καὶ τοῖς ἑτέρων καὶ μὴ προσήκουσι χρήμασιν ἐποφθαλμιῶν
ἐμίσησε τὸν ἐν πύλαις ἐλέγχοντα. Ὡς ἀπόλοιτο ἐξ ἀνθρώπων ἡ τοῦ πλείονος ἔφεσις
καὶ ἡ σύντροφος αὐτῇ ἀπληστία καὶ ἀσωτία καὶ ὁ ταύτην κακῶς παραδείξας ἀπ'
ἀρχῆς ἀνθρωποκτόνος διάβολος. Ὅσας γὰρ καὶ οἵας ὁ δίκαιος οὗτος καὶ τοῦ θεοῦ
ὄντως ἄνθρωπος ψυχωφελεῖς παραινέσεις ἐκ θείων νόμων καὶ διδαχῶν καθ'
ἑκάστην τῇ ἀπλήστῳ ταύτῃ καὶ μιαρᾷ ἐπῇδε βασιλίσσῃ ἀνελλιπῶς ὑμῖν ἐστι τῷ
τούτου περιτυχοῦσι βίῳ μαθεῖν. Ἀλλ' αὕτη πᾶσαν ἀπωσαμένη καὶ σύνεσιν καὶ αἰδῶ
πρὸς τὰς θείας καὶ σωτηριώδεις ἐπῳδὰς δυσχερῶς εἶχε καὶ βαρέως καὶ ἐπαχθῶς
μᾶλλον διετίθετο τῷ ἁγίῳ κακῶς καθ' ἑαυτὴν πρὸ τοῦ ἀνοίᾳ βουλευσαμένη καὶ
κακῶς φρενὸς ἐπαφῆκε κατὰ τῆς μακαρίας ἐκείνης καὶ ἁγίας ψυχῆς. Τοῖς οὖν
τοιούτοις παναισχίστοις συνεργοῖς καὶ συμπράκτορσιν ἡ μαινὰς ἐκείνη χρωμένη καὶ
δαιμονῶσα καὶ κακῶν προαχθεῖσα γεννητόρων καὶ διδαγμάτων κινεῖ κατὰ τῆς
ἀλήπτου ψυχῆς ἐκείνης καὶ σεβασμίας, ἃς ἤδη συνήγαγε μαγγανείας καὶ τερατείας.
Καὶ μέντοι ἡ σολομώντιος βδέλλα καὶ ῥυπαρὰ συνεργὸν πλουτοῦσα τὸν κοινωνὸν
τοῦ λέχους καὶ τῆς ἀνοίας, ἴστε τὰ τολμηθέντα, τὴν ὑπερορίαν καὶ τὰ μέχρι τοῦ
δεῦρο κροτούμενα. Ἀλλ' ἐπανακτέον τὸν λόγον πρὸς τὴν νύσσαν τῆς ὑποθέσεως. Καὶ
πρῶτα μὲν τὴν βασιλέως ὑποποιεῖται ἁπλότητα ἢ τό γε ἀληθὲς εἰπεῖν μᾶλλον
κουφότητα, καὶ γὰρ ἥμερος ἦν τῷ ὄντι καὶ γυναικώδης ὁ βασιλεύς, ἔπειτα τοὺς τὴν
ἱερατικὴν ἀνελευθέρως ὑποκρινομένους σκηνὴν καὶ τὸ πρᾶγμα κακῶς
καπηλεύοντας, οὐ μόνον δέ, ἀλλὰ καὶ τοὺς πεπαυμένους τοῦ ἱερᾶσθαι τῇ Ἰωάννου
κρίσει διὰ τὴν οἰκείαν τῶν τρόπων φαυλότητα κοινωνοὺς ποιησαμένη τοσούτου
τολμήματος τὴν ἄδικον ἐξορίαν κυροῖ κατ' αὐτοῦ καὶ τὴν τῆς ἐκκλησίας
περιαιρεῖται εὐπρέπειαν καὶ πενθίμῳ ταύτην ὑποβάλλει κουρᾷ ἡ νέα Ἰεζάβελ καὶ
∆αλιδᾶ τὸν κόσμον αὐτῆς λυμηναμένη καὶ διαρρήξασα ὡς οἷα γρομφὶς ἀναιδὴς καὶ
βορβόρῳ καλινδουμένη καὶ ἀποσπαίρουσα. Τὶς ἡ ὑπερορία; Κουκουσός, πολίχνη τις
ἔρημος καὶ θείας πίστεως καὶ φόβου θείου ἐπιδεής, καὶ μετὰ ταύτην Ἀραβισσὸς καὶ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

μετ' ἐκείνην ἄλλο βάρβαρον χωρίον ἄνικμόν τε καὶ ἄχαρι καθ' ἑκάστην ὑπὸ τῶν
γειτνιώντων πολιορκούμενον Ἰσαύ 309 ρων τοὔνομα Πιτυοῦς. Ταῦτα τῆς ἡμῶν
ἐσχατιᾶς τέρματα ἀκαλλῆ καὶ ὀλέθρια ἀπιστίᾳ τῇ πρὸς Θεὸν κρατούμενα καὶ
ματαίαις τισι δόξαις περιπλανώμενα, ἡλίῳ τε καὶ πυρὶ τὸ σέβας ἀμαθίᾳ ἀπονέμοντα
καὶ ἄλλοις τισὶ κιβδήλοις σεβάσμασιν ὡς θεοῖς λατρεύοντα. Οὗτος ὁ μέγας καὶ τίμιος
ὄντως τοῦ Θεοῦ ἄνθρωπος πλήρης θεογνωσίας καὶ πίστεως ἐκ τῶν οἰκείων διδαχῶν
πάντας τοὺς ἐκεῖσε υἱοὺς φωτὸς διὰ τῆς ἐξ ὕδατος καὶ πνεύματος ἀναγεννήσεως
συντόμως ἀπέδειξε καὶ τῇ τροφίμῳ καὶ ποτιμωτάτῃ τῶν λόγων αὑτοῦ διδασκαλίᾳ
χρηστοὺς καὶ θεοσεβεῖς τοὺς πρὶν ἀχρήστους καὶ ἀσεβεῖς ἀπειργάσατο. Ταῦτα τοῦ
μεγάλου ὄντως καὶ θαυμασίου τῆς ἀποικίας καὶ ἀδίκου ὑπερορίας τὰ κατορθώματα,
ταῦτα τοῦ δευτέρου Ἰὼβ τὰ παλαίσματα καὶ τῆς περὶ αὐτὸν πραγματείας ἡ ἔκβασις.
Ἀγὼν γὰρ ἦν ἀρετῆς τοῦ μὲν ὅπως τὴν ὑπέρφρονα ταύτην καὶ μαινομένην τῆς
δευτέρας εἰδωλολατρείας ἐκσπάσῃ, εἴτουν φιλαργυρίας, τῆς δὲ ὅπως τὸν ἅγιον
πονηρᾷ ἀνταποδόσει ἀμείψηται. Καὶ γὰρ οὐδὲ τοῦτο συνιδεῖν ἠδυνήθη τῷ καιρῷ τῆς
μανίας οἱονεὶ κύφωνί τινι παρὰ τοῦ πονηροῦ βαρυνθεῖσα καὶ δυσανάκλητον ἔχουσα
τὴν διόρθωσιν. Ἐκράτει τοίνυν τὸ ἀσεβές, τὸ εὐσεβὲς ἀπηλαύνετο, καὶ ἐν
νυκτομαχίᾳ δεινῇ τὰ τῶν ἐκκλησιῶν ἦν, ὁ φθόνος ἱστᾶν ἐδόκει τῆς οἰκείας
μοχθηρίας τὰ τρόπαια καὶ ἀλαλάζειν τὰ ἐπινίκια. Ἐπεὶ δὲ τὸν δρόμον ἀποστολικῶς
εἰπεῖν τελέσας καὶ τὴν πίστιν τηρήσας μετὰ τοὺς πολλοὺς ἐκείνους ἱδρῶτας καὶ
κόπους καὶ τὴν πολλὴν ταλαιπωρίαν τῶν ἀλλεπαλλήλων ἐξοριῶν ἐκάλει τοῦτον
πρὸς ἑαυτὸν ὁ βασιλεὺς καὶ στεφοδότης Ἰησοῦς, ὁ ἐμὸς Χριστὸς καὶ θεός, τὸν οἰκεῖον
ἀθλητὴν καὶ νικηφόρον τὰ βραβεῖα τῶν πολλῶν καμάτων ἀποληψόμενον, ἐνταῦθα
τὶ γίνεται καὶ τὶ θαυματουργεῖται δίκαιον ταῖς φιλοθέοις ἀκοαῖς εἰσηγήσασθαι.
Πέτρον καὶ Ἰωάννην τοὺς προκρίτους καὶ τιμίους τῶν ἀποστόλων αὐτῷ πάλιν
ἐξαποστέλλει εὐαγγελιζομένους τῶν πολλῶν καὶ μακρῶν πόνων καὶ ἄθλων τὴν
ἀνάπαυλαν, ἐπιρρωνύντας τὸν ἀριστέα, ἐπισφραγίζοντας τὴν ἀνάρρησιν,
κροτοῦντας τὰ νικητήρια, τὰ γέρα τῆς νίκης, τὰ βραβεῖα, τοὺς ἀμαράντους
στεφάνους, τὴν δόξαν, ἣν κληρονομήσειε σὺν αὐτοῖς, τὸν πικρὸν θάνατον τοῦ
καταράτου γυναίου διὰ τῆς τῶν σκωλήκων βρώσεως προσημαίνοντας ὡς ἐχθίστου,
ὡς ἀποπτύστου, ὡς κυνῶν ἀξίου καὶ τῆς τούτων περιπλοκῆς, ἀνθ' ὧν εἰς αὐτὸν
ἐπλημμέλησεν, ἀμβροσίαν σιτίζοντας. Συντηροῦσι τοῦτον μέχρι τοῦ καιροῦ τῆς πρὸς
θεὸν ἐκδημίας καὶ ἀναλύσεως ἄσιτον. Καὶ πάλιν μετὰ ταῦτα ἕτερον θαῦμα καὶ ἄξιον
ἱστορίας· τριῶν γάρ που παρεληλυθότων μηνῶν οἱ καλλίνικοι μάρτυρες Βασιλίσκος
τε καὶ Λουκιανὸς τὸ βασίλειον κλέος τῆς νίκης εὐαγγελίζονται 310 καὶ τὴν πρὸς τὸν
στεφοδότην Χριστὸν καὶ θεὸν ἐκδημίαν καὶ τὴν ἐκείνου μεθ' ἑαυτῶν ἐν τῷ οἰκείῳ
μνήματι συγκατάταξιν. Ἀπαίρει τοίνυν ὁ ἀθλητὴς τῶν κάτω χειρὶ θεοῦ τὸ πνεῦμα
παραθεὶς καὶ τὸ καρτερικὸν αὑτοῦ καὶ πολλοῖς ἱδρῶσι συντακὲν ἅγιον σῶμα τῷ
τάφῳ τῶν μαρτύρων συνθάπτεται καὶ τὸ ἑαυτοῦ τὸ μακάριον διὰ τῆς τῶν
ἐπελθόντων πόνων ὑπομονῆς οὐ πόρρω μαρτυρικῆς τιμῆς ἀποφήνας. Ὅτι δὲ καὶ τῆς
ἄνω χορείας ὁ ἐπαινούμενος ἄξιος καὶ ἀνθάμιλλον τῶν ἑαυτοῦ πόνων τε καὶ
ἀγώνων ἡ μακαρία ἐκείνη καὶ ἁγία ψυχή, τὸ παγκόσμιον ἐκεῖνο καὶ τιλμαφέστατον
ἀγαθόν, ἡ τοῦ παντὸς κόσμου ὁδηγία πρὸς τὸ καλὸν καὶ οἷόν τις φωταγωγία καὶ
ἔλλαμψις, τὴν διαγωγὴν ἐκληρώσατο καὶ τῆς ἐγγυτάτω στάσεώς τε καὶ τάξεως
ἔτυχε, τῆς μετ' ἀγγέλων φημι καὶ ὅπου τῶν ἐξ ἀρετῆς κεκαθαρμένων ἡ κατοικία,
ἔξεστιν οὐκ ἀπεικότως ἀπό τινων ἐν σαρκὶ περιόντων ἔτι καὶ τὴν ἀλήθειαν
φαντασθέντων τοῦ μὴ τὸν τῆς ἀρετῆς λύχνον ὑπὸ τὸ μόδιον τῆς τινων κιβδήλου καὶ
ὡσάν τις εἴποι οὐκ ἀληθοῦς ὑπολήψεως κρύπτεσθαι λαθεῖν καὶ τοῖς ὑποσκάζουσι καὶ
εἴπερ ἀμφισβητησίμοις λαμπρότερον ἡλίου ἀποφῆναι τὴν ὄντως περὶ τοῦ ἀμαθῶς
ὑπειλημμένου ἀλήθειαν. Ἤσχαλλέ τις τῶν φιλίων καὶ μάλιστα τοῦτον ἠγαπηκότων,
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

Ἀδέλφιος ὄνομα τῷ ἀνδρί, ὃς καὶ ὑπερόριον ἀπενηνεγμένον τρόπῳ συμπαθείας
ἐδέξατο καὶ τῶν ἐπισκοπῇ τιμωμένων περιφανείᾳ ἐπίσημος ὁ ἀνήρ. Καὶ ἔτρυχε
τοῦτον λογισμός τις καὶ ἐδαπάνα οὐκ ἔξω τοῦ πρέποντος ὑπονύττων τοῦτο
ὑποτιθεμένου τοῦ δάκνοντος λογισμοῦ καὶ σαλεύοντος καὶ μαθεῖν ἀναζητοῦντος
πάσῃ σπουδῇ ποίας ἄρα δόξης τε καὶ τιμῆς ὁ φίλτατός μοι καὶ τίμιος Ἰωάννης, καὶ
τίνος τάξεως ἔλαχεν, ᾧ πολλοί τινες διὰ τῶν λόγων πόνοι καὶ συγγραφαὶ καὶ
κόσμος ὅλος πεφωτισμένος καὶ πρὸς ἀρετὴν καὶ θεῖον ζῆλον ἐπεστραμμένος. Οὕτως
αὐτὸν τὸν εἰρημένον Ἀδέλφιον τοῦ λογισμοῦ ἐκθλίβοντός τε καὶ κατατρύχοντος,
ἵνα καί τινος θειοτέρας ἀντιλήψεως αἴσθηται, συντονώτερόν τε καὶ προσκειμένου τῇ
ἱκεσίᾳ καὶ ταῖς εὐχαῖς ἐγκαρτεροῦντος καὶ δεομένου ἔδοξέ πως ἐξεστηκέναι καὶ
προβῆναι τοῖς οὐρανίοις τῆς τε χειρὸς λαβομένου τινὸς αὐτὸν νεανίου λευχείμονος
πρὸς χῶρον ἀγαγεῖν τοῦτον φωτὸς λαμπροτάτου ὑπόπλεων, οὗ πᾶς ὁ τῶν θεῷ
ᾠκειωμένων καὶ ἀληθῶν ἀρχιερέων ἐνεσκήνου χορός. Καὶ πάλιν ἡ αὐτὴ ἀπορία καὶ
σύγχυσις μὴ συνορωμένου τοῦ ποθουμένου. Ὡς δὲ καὶ ἐξ ὀνόματος ἐμέμνητο καὶ οὐκ
ἦν καὶ λοιπὸν στυγνάζων ἐπέμπετο πλήρης ἀνίας καὶ κατηφείας τὸ τριπόθητον
χρῆμα μὴ συνορῶν, οἷς ἐπήλπιζε, τὸν χρυσοῦν ὄντως καὶ λόγοις καὶ πράξεσιν,
ἕτερός τις τῆς αὐτῆς ὄψεως νεανίας τῆς προοφθείσης τῇ εἰρημένῃ ταύτῃ πολυφώτῳ
οἰκίᾳ ὑπηρετῶν 311 τὸ κατηφὲς τῆς ὄψεως θεασάμενος ἠρώτα τὸν τρόπον
ἀναμαθεῖν. Ὁ δὲ οὐκ ἀποκρίσεως ἠξίου τῇ λύπῃ κατεσχημένος. Τοῦ δὲ σφοδρότερον
ἐγκειμένου καὶ μαθεῖν ζητοῦντος τὶ τὸ τὴν τοσαύτην ποιοῦν ἀνίαν καὶ ἅμα οὐδὲ
δίκαιον ἀπιέναι λυπούμενον τοῦτο μόνον εἰπεῖν ἔτι ἀλύοντα καὶ ποσῶς ἐπιστένοντα
ὅτι φησὶ τὸν Κωνσταντινουπόλεως ἐπίσκοπον Ἰωάννην οὐχ ἑώρακα
συνδιαιτώμενον τοῖς παροῦσι. Καὶ τὸν εὐθέως ὑπολαβόντα εἰπεῖν· καὶ οἴει τοῦτόν
τινα τῶν ἐν σώματι ὅπου ποτ' ἔστιν ἐκεῖνος ἰδεῖν καὶ ἧς μετέσχηκε τάξεως; Καὶ οὐ
παρέστηκεν ἡ μακαριωτάτη φύσις ἐκείνη, καὶ τολμηρὸν εἰπεῖν ὑπὲρ ἄνθρωπον.
Ταύτην λαβὼν τοῦ τέως ἐνδοιαζομένου καὶ τυραννοῦντος τὸν λογισμὸν ὁ ἱερὸς
οὗτος Ἀδέλφιος ἀληθεστάτην ἀπόκρισιν δοξάζων ἦν τὸν Θεόν, τὸν πολλαπλασίως
ἀποδιδόντα τῶν πιστῶς ἐλπιζομένων τὰς ἀμοιβὰς καὶ χάρισι τιμῶντα ταῖς ἀνωτάτω
καὶ νοῦ καταλήψεως. Ἐπεὶ δὲ ἡ θεία δίκη τὴν μαινάδα ἐκείνην καὶ διώκτριαν
δικαίως ἐδίκασε καὶ σκωλήκων βρώσει τὸν βίον οἰκτρῶς ἐδικαίωσε καταστρέψαι
ὥστε μηδὲ θρήνοις γενέσθαι χώραν διὰ τὸ συνεχὲς τῶν οἰκείων αὐτῆς κακῶν, ἀλλὰ
καὶ τὸ σῶμα τελευταῖον παρὰ θεοῦ πλήττεται πληγὴν ἀνίατόν τε καὶ δυσθεώρητον
αὐτῆς ἀποτινούσης ἃ ἥρπασε καὶ θριαμβευούσης ἃ εἰς τὸν ἅγιον ἔδρασε Θεοφίλου τε
λιθώδει καὶ οἷα τὰ ἐκ νεφρῶν πάθη περιπεσόντος καὶ βιαίως τὸ ζῆν ἀπορρήξαντος
τῶν τε λοιπῶν κατηγόρων, τῶν τὴν ἐξορίαν ἐξηρτυκότων τοῦ ἁγίου, αὖθις
διαφόροις πληγαῖς καὶ νόσοις θεηλάτοις τῆς ζωῆς τὸν θάνατον ἀνταλλαττομένων,
αὐτοῦ δὲ τοῦ βασιλέως Ἀρκαδίου τὸ κοινὸν καὶ πᾶσιν ὀφειλόμενον πιόντος τοῦ
θανάτου ποτήριον καὶ Θεοδοσίου, τοῦ ἐκείνου παιδός, τὰ πατρῷα σκῆπτρα
κληρωσαμένου, ὁ μεταξὺ χρόνος εἰς ἀρχιερέα θεοῦ θείᾳ ψήφῳ προὐβάλλετο ἄνδρα
ἱερόν τε καὶ ἅγιον τῷ διδασκαλικῷ τοῦ μεγάλου Ἰωάννου γάλακτι ἐκτραφέντα.
Πρόκλος δὲ ἦν ὁ πάλαι περιόντι τὰ μυστικώτατα τούτῳ διακονησάμενος. Τῷ τοίνυν
τετάρτῳ ἔτει τῆς αὐτοῦ ἀρχιερατείας τὸ καταλειφθὲν τῆς ἐκκλησίας ἐγκάρδιον, οἷόν
τι μνήμης ἐμπύρευμα, θείῳ πνεύματι κινηθέν τε καὶ ἀναζέσαν ὡς εἶδεν ὥσπερ
αἰθρίαν τὸν κόσμον ἐπιλαβοῦσαν καὶ ἀναλάμψασαν καὶ τὰ τοῦ φθόνου
διασκεδασθέντα νέφη, ὃς πρὸς μὲν τὸ ἀντιβαῖνον εἴωθε διεγείρεσθαι, πρὸς δὲ τὸ
ἐκποδὼν ὃν ὑπολωφᾶν καὶ μαραίνεσθαι, ἡνίκα ἡ ἐπαινετὴ πρᾶξις ἐφ' ἑαυτῆς
θεωρουμένη θαυμάζεσθαι τὸν ἐργάτην ποιεῖ. Τοῦτον οὖν τὸν καιρὸν παρρησίας
ἑαυτοῖς καὶ δυσχερῶν ἀπολύσεως νομίσαντες ἐπιτήδειον, πάνδημον ἐποιοῦντο
πρεσβείαν πρὸς τὸν τῶν ἱερῶν προεστῶτα πραγμάτων πεῖσαι δεόμενοι τὴν βασιλέως
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

γνώμην μαλάξαι τοῦτον καὶ τεθνηκότι τὴν ἐξορίαν τῷ κοινῷ πάντων πατρί τε καὶ
διδασκάλῳ 312 κἂν ὀψέ ποτε διαλύσασθαι ἀπαλλάξαι τε τῆς ὀρφανίας αὐτοὺς τὴν
ἱερὰν αὐτοῦ κόνιν καὶ τὸ λείψανον τῆς ἐναρέτου μορφῆς ὥς τινα χρυσοφόρον
αὐτοῖς χαρίσασθαι θησαυρόν, ἵνα ἀντὶ τοῦ παρ' ἐκείνου βλύσαντος αὐτοῖς τῆς
διδασκαλίας γάλακτος τοὺς τῶν δακρύων αὐτῷ ποταμοὺς ἐπαφήσωσι καὶ γίνηταί τις
αὐτοῖς κἀντεῦθεν ἁγιασμὸς καὶ τῆς καλῆς ἐφέσεως πλήρωσις. Ἦν οὖν ὁ ἱερεὺς κατ'
αὐτὰ τὰ τῶν βασιλείων ἀνάκτορα καὶ τὰ τῆς πρεσβείας ἠνύετο καὶ ἡ πάνδημος τῶν
τέκνων ἐδηλοῦτο παράκλησις διὰ τοῦ ἀρχιερέως ἐκεῖνα λέγοντος τῷ βασιλεῖ καὶ
ὑποτιθεμένου ἃ καὶ τὰς ἀκροτόμους τῶν πετρῶν ἐπικλᾶν οἶδε καὶ παροτρύνειν καὶ
πρὸς ἔρωτα θεῖον ἀνακινεῖν καὶ ἀναρριπίζειν τῆς τοῦ ἁγίου σκήνους τιμίαν
ἐπάνοδον καὶ σεβάσμιον. Τούτων τοίνυν ἀκούσας ὁ βασιλεὺς θερμαίνεται τὴν
καρδίαν καὶ πρὸς τὴν αἴτησιν οὐκ ἀντιβαίνει, ἀλλ' εὐδοκιμήσεως αὑτῷ νομίσας
τοῦτον καιρὸν καὶ θείας ἀποδοχῆς καὶ εὐφημίας τῆς ἐξ ἀνθρώπων καὶ φίλτρου
γνησιωτάτου πρὸς τὴν ἀνάληψιν ηὐτρεπίζετο καὶ πάντα ἦν τῷ εὐσεβεῖ σκοπῷ
συντελούμενα. Ὡς δ' οὖν τοῦ βασιλέως ἐφοδιασθέντες γράμμασιν, οἷς προσετέτακτο,
τῇ τὸν πολύτιμον θησαυρὸν ἐχούσῃ πόλει ἐπέστησαν καὶ οὐδὲν ἦν τὸ ἀντιστῆναι
δύνασθαι νομιζόμενον, οἱ γὰρ κληρονόμοι τοῦ χρήματος βασιλικῆς ἡττῶντο χειρὸς
καὶ ἠνιῶντο μὲν καὶ ἠθύμουν καὶ ἤσχαλλον, ἀντιβῆναι δὲ καὶ μὴ ὑπεῖξαι οὐκ
ἔσθενον, ἐνταῦθα ἡ τοῦ μεγάλου δύναμις ἐγνωρίζετο· τούς τε γὰρ λαχόντας καὶ
περιστείλαντας τιμῶν καὶ τοῖς κατ' ἐξουσίαν μετάγειν πειρωμένοις δεικνὺς ὡς οὐ
πάντων ἐξουσιάζουσιν οὐχ ὥσπερ ἐν τῷ διώκεσθαι πειθήνιος ἦν τὸν οὐκ ἐρίζοντα
Χριστὸν οὐδὲ κραυγάζοντα πάντως μιμούμενος, οὕτως καὶ πρὸς τὴν ἀνάκλησιν κατὰ
τὸ ἀκούσιον βιάζεσθαι καθίστατο δυνατός, ἀλλ' ἦν ὥσπερ προσπεφυκὼς τῇ ἀκινήτῳ
σορῷ καὶ δυσαπόσπαστος ἢ δυσανάγωγος πάντῃ πρὸς τὴν μετάθεσιν· ἀπολιθοῦσθαι
γὰρ λέγεται τοῦ ἁγίου τὸ σῶμα καὶ τῇ δυσκινησίᾳ εἰς βάρος λίθου μεταπεποιεῖσθαι
ἐδόκει. Ἠπόρουν οὖν οἱ τοῦ βασιλέως θεράποντες, ἠθύμουν οἱ διάκονοι τοῦ
προστάγματος, ἐνευθυμίων οἱ κληρονόμοι τὸ χαῖρον τῆς ψυχῆς ἐπίδηλον ἔχοντες.
Ἀναφέρεται ταῦτα τῷ βασιλεῖ, γνωρίζεται τῷ ἀρχιερεῖ, τῇ τῶν ἐν τέλει βουλῇ παρ'
αὐτοῦ τοῦ κρατοῦντος ἡ τῆς ἀναβολῆς αἰτία ἐπιγινώσκεται. Ὑπόνοιαν ἐδίδου πρὸς
τὴν ἐκ προστάγματος ἀντιβαίνειν ἀνάκλησιν, οὐ μνησικακεῖν τοῖς λυπήσασιν.
Ἀλλάττεται τὸ σχῆμα τῆς δοξάσης ἐναντίας ἀποστολῆς, ἀμείβεται πρὸς ἱκεσίαν τὰ
γράμματα. Αὐτὸς ἐκεῖνος ὁ βασιλεὺς οἰκείᾳ χειρὶ διαχαράττει τὴν δέησιν. Τῷ
οἰκουμενικῷ διδασκάλῳ καὶ πνευματικῷ πατρὶ Ἰωάννῃ, τῷ ἁγίῳ καὶ χρυσοστόμῳ
πατρὶ καὶ πατριάρχῃ. Ἱκετεύομεν, πάτερ, δουλο 313 πρεπῶς ὡς εὐγνώμονες, οὐ
προστάσσομεν βασιλικῶς ὡς καὶ σοῦ κυριεύοντες, ὡς δεσπότην μετακαλούμεθα, οὐχ
ὡς ὑποχείριον βιαζόμεθα. Γνησίων τέκνων φίλτρῳ κινούμεθα πατρικὴν ἐπίδειξαι
τὴν συμπάθειαν. Ἀπόδος σου τὸν νεκρόν, οἷς καὶ πρότερον ἐπέδωκας σεαυτόν.
Ἀδικήσεις ἐπὶ πλέον ἀναβαλλόμενος. Εἴπερ τοῖς ἀγνωμονήσειν πρός σε μέλλουσιν
ἐπεδίδους σαυτόν, οὐ γὰρ ἠγνόεις ὁ διορατικώτατος νοῦς τὰ καταληψόμενα, τοῖς
ἀξιοχρέους σοι τὰς τιμὰς ἀποδιδόναι προθυμουμένοις ζημιώσεις τὴν προστασίαν
σου; Ὡς δὲ μετὰ τῆς τοῦ βασιλέως δεήσεως καὶ τῶν παννύχων ὕμνων καὶ
προσευχῶν τὴν προτέραν ἀπόπειραν προσήγαγον τῇ σορῷ, ἦν ἰδεῖν, ὢ τοῦ θαύματος,
πάλιν τὴν φυσικὴν ἀπολαβὸν τὸ ἅγιον ἐκεῖνο σῶμα σύνταξιν καὶ κουφότητα, τὴν
μετάθεσιν εὐχερῆ καὶ ῥᾴστην τὴν μετακίνησιν, ὥσπερ τινὸς ἐν προαιρετικῇ κινήσει
τὸν οἶκον ἀμείβοντος. Καὶ τοῦτο γὰρ λέγεται τὸ θαυμάσιον, ἑαυτὸν ἐπικουφίσαι τὸν
ἅγιον τοῖς μετακομίζουσι τοῦτον πρὸς τὴν σορὸν τὴν βασίλειον. Ὥσπερ οὖν οἱ
μυρίανδρον ἑλόντες πόλιν καὶ μέγαν κατάγοντες θρίαμβον πολλοὺς τοὺς πρὸς τὴν
θέαν συρρέοντας ἔχουσιν, οὕτω δὴ καὶ οἱ τὸ θεῖον ἐκεῖνο σκῆνος κατάγοντες μυρίον
εἶχον ὄχλον τὸν παραπέμποντα. Εἴκασεν ἄν τις πόρρωθεν ὢν τὸ τῶν λαμπάδων
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

πλῆθος ἰδὼν ὡς ἀστέρες ἐν ἀνεφέλῳ τῷ καταστήματι τὴν ὑπερκειμένην θέσιν
ἀμείψαντες καὶ πρόσγειοι χρηματίζοντες σελήνην ὄντως ἐγκλείουσι καὶ σχῆμα
σώζουσιν εὐτάκτως κινουμένου χοροῦ οἷόν τινα κορυφαῖον μέσον τὸν δίκαιον
ἔχοντες. Ὡς δ' ἐγγυτέρω προσελθόντος ταῖς ἀκοαῖς ἡ ψαλμῳδία προσέπεσεν, εἰκόνα
ἂν ἔδοξε θεωρεῖν τῆς ἐπὶ γῆς τοῦ θεοῦ λόγου ἐνσωματώσεως, καθ' ἣν ὁ τῶν
ἀγγέλων χορὸς τὸ δόξα ἐν ὑψίστοις ἐκραύγαζε. Τοσαύτη τις ἦν ἡ περὶ τὸν μέγαν
Ἰωάννην δορυφορία τε καὶ πομπή, τοσοῦτος ὄχλος πανταχόθεν συνέρρεε. Τίνος οὖν
βασιλέως ἐπάνοδος ἔθνη χειρωσαμένου μυρία καὶ πόλεις τειχήρεις κατὰ κράτος
ἑλόντος τοιαύτη προεπέμφθη τιμή; Ἢ τὶς τοσούτους ἔσχε τοὺς πρὸς τὴν θέαν
συντρέχοντας; Ὡς δ' οὖν πρὸς τὴν ἀντιπέρας τῆς βασιλείου ταύτης τῶν πόλεων
ἀκρόπολιν ἔφθασαν, ἦν ἰδεῖν τὸν ἐν μέσῳ τούτων πορθμὸν ὥσπερ τινὶ στόλῳ ταῖς
μικραῖς ὁλκάσι καὶ λέμβοις καὶ ἀκατίοις πρὸς τὴν ὑπάντησιν ἐπειγομένοις
καταπυκνούμενον. Οὐδὲ πορθμεῦσιν ἄλλη τις ἦν ἐμπορία οὐδὲ ἑωρᾶτο τὸ πλοῖον,
ἄλλοσέ ποι φερόμενον. Ἐπεραιοῦτο γὰρ σὺν τῷ βασιλεῖ καὶ γερουσία βουλὴ καὶ τῇ
ἀρχιερατικῇ πολιτείᾳ τὸ ἀποκληρωθὲν τὸ πᾶν συνεξήρχετο. Οὐδὲ γυναῖκας ἐπεῖχεν
ἡ διηνεκὴς οἰκουρία τὸ τηνικαῦτα, ἀλλὰ πολλαὶ καὶ ὄψεως ἄρρενος τότε πρῶτον
κατατολμήσασαι ἐξῄεσαν εἰς ὑπάντησιν ἁγιασμοῦ πιστεύουσαι μετασχεῖν καὶ ἀπὸ
μόνης τῆς ὄψεως. 314 Τριήρης τῶν βασιλέων ἦν εὐτρεπής, ἡ δεξομένη τὸν ἤδη παρὰ
θεῷ βασιλεύοντα. Καὶ πάλιν κἀνταῦθα τὴν εἴσοδον τοῖς θαύμασι θεοῦ ἐμεγάλυνεν·
ἐξαίφνης γὰρ βιαίας ζάλης διεγερθείσης καὶ τοῦ τῆς θαλάσσης κύτους ἐν ἀνέμῳ
νότῳ βιαίῳ διαστάντος καὶ κλύδωνος διεγηγερμένου μεγάλου καὶ τῶν κυμάτων
συγκρουομένων ἀτάκτως καὶ πολὺν ἐχόντων σάλον καὶ τάραχον, τὰ μὲν ἄλλα τῶν
πλοίων τῇδε κἀκεῖσε διερρίπτοντο ὡς ἕκαστον ἔτυχεν ἔν τισι κοιλώμασι
προσοκεῖλαν ἢ καὶ πρὸς λιμένας διασωθέν. Τὸ δὲ φέρον τὸν δίκαιον τῶν κάλων
ἀπορραγέντων οἱονεὶ χειρὶ τῇ δυνάμει τοῦ φερομένου κάλου διακυβερνώμενον
προσωρμίσθη τῷ τῆς χήρας ἀγρῷ, ὃν κακῶς ἡ τύραννος βασιλὶς τοῦ πενομένου
γυναίου ἀφήρπασεν ὥσπερ οἶμαι καὶ τοῖς ἧττον μετέχουσι ἀγχινοίας τε καὶ φρενῶν
δεικνύοντος τοῦ θεοῦ τῆς ἐξορίας τούτου τὸ αἴτιον. Μᾶλλον δὲ οὐδὲ θανὼν ὁ
δίκαιος τῆς συνηγορίας καὶ τοῦ ἐκδικεῖν τὸ ἐπηρεασθὲν ἐπαύετο γύναιον, ἀλλ'
ὥσπερ τις μάρτυς ἀψευδὴς πόρρωθεν εἰς ὁρίων ὑπόδειξιν μετακληθεὶς τὴν οἰκείαν
ἔθετο ψῆφον καὶ κρείττονι γλώσσης ἐμαρτύρει φωνῇ, ἧς τὸ χωρίον ἐτύγχανεν. Ἐπεὶ
δὲ τὸ περὶ τούτου αὐτοῦ διήνυστο βούλημα, τὸ παραυτίκα ἀποδοθῆναι τῇ χήρᾳ τὸν
ταύτης ἀγρόν, κατηυνάσθη μὲν εὐθέως τὸ πέλαγος, ἐστορέσθη δὲ τῷ κλύδωνι καὶ
οἷον φιλεῖ γίνεσθαι ἐν τούτοις, ὅταν γαλήνη πορφύρηται καὶ προσπαίζῃ ταῖς ἀκταῖς
ἡδύ τι καὶ ἥμερον. Καὶ ὡς ἔκ τινος ναυαγίου διασωθέντα πανταχόθεν τὰ πλοιάρια
συνηθροίζετο καὶ ὡς στρατηγίδα ναῦν τὴν φέρουσαν τὴν τιμίαν ἐκείνην σορὸν
δορυφοροῦντα προέπεμπον. Καὶ οὕτως ἀπεδόθῃ τῇ βασιλίδι πόλει ὁ εὐπρεπὴς αὐτῆς
κόσμος καὶ τὸ βασίλειον ὄντως διάδημα. Καὶ ταῦτα μὲν ἐν τούτοις. Τὰ δὲ νῦν
ἄλληλα λαμπρότης καὶ φῶς ἀπρόσιτον καὶ χαρὰ διαχέουσα τῶν πιστῶν τὰς καρδίας.
Ἡμέρας γὰρ ἄρτι διαγελώσης καὶ τοῦ χρυσοστομικοῦ καὶ ἡλιολαμποῦς σκήνους τῇ
ἡμετέρᾳ πόλει λαμπρὰς ἀκτίνας ἐπαφιέντος ὁ τοῦ ἀοιδίμου ἀποστόλου Θωμᾶ ναός, ὁ
πρὸς θάλασσαν κείμενος, ὃν Ἀμαντίου οἶδεν ὀνομάζειν ἡ πόλις, τοσοῦτον ἀγαθὸν
δεξιοῦται. Καὶ ταῖς παννύχοις ὑμνῳδίαις καὶ εὐφημίαις δι' ὅλης νυκτὸς
γεραιρόμενος καὶ πολλοὺς δεξάμενος σὺν δάκρυσιν ἀσπασμοὺς ὥσπερ τις
ἀγνωμονηθεὶς τῳόντι καὶ παρὰ τὸ εἰκὸς ἐπηρεασθεὶς τοὺς ἠδικῆσαι δοκοῦντας καὶ
διαλλαττομένους προσίεται μετ' εὐλαβείας καί τινος προσιόντας αἰδοῦς, οὕτω δὴ καὶ
ὁ ἀρχιερεὺς τοῦ θεοῦ πρῶτα μὲν αὐτὸν τὸν τὸ κράτος ἰθύνοντα προσεδέχετο, τοῦτο
μὲν ὡς πατέρα καὶ ἀνάδοχον ἐκ τοῦ θείου γεγονότα λουτροῦ τιμῶντα καὶ
μεγαλύνοντα, τοῦτο δὲ τῆς κατὰ σάρκα μητρὸς παραιτούμενον τὴν δικαίαν ὀργὴν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

καὶ πολυειδεῖς δεήσεις προσάγοντα λυθῆναι αὐτῇ τῶν 315 εἰς αὐτὸν ἐπταισμένων
τὴν εἴσπραξιν καὶ στῆσαι τὸν τῆς λάρνακος αὐτῆς κλόνον ἐπὶ τριάκοντα καὶ πέντε
ἔτεσιν ἀνενδότως ὡς ἐν συσσεισμῶ σαλευόμενον, ὃν ὡς συσσείουσα τὴν ἐκκλησίαν
τοσούτοις ἤδη χρόνοις κατὰ τὴν τοῦ Κάϊν ἐκλονήθη καὶ μεταστᾶσα ὁμοίωσιν. Καὶ
μέντοι καὶ τυγχάνει τῶν δεήσεων, ὡς ἐκ τοῦ φανεροῦ τὸ ἀφανές ἐστι
συλλογίσασθαι. Ὅρα δ' ἐνταῦθα τὴν τοῦ φιλοχρίστου βασιλέως θερμοτάτην
προαίρεσιν· καὶ γὰρ ὡς ζῶντι τῷ ἁγίῳ ἐκείνῳ καὶ τριποθήτῳ λειψάνῳ προσομιλεῖ.
Ἦν γὰρ ἰδεῖν ὄντως θαῦμα πᾶσαν ὑπερεκπίπτον ἔννοιαν. Ὡς ἄρτι γὰρ τῶν
αἰσθήσεων ἠρεμουσῶν ἡδέως καθεύδειν τὸν ἅγιον ἐλογίζετο. Καὶ δὴ καὶ τὴν
βασίλειον χλαῖναν προσεφαπλοῖ ἄνωθεν τοῦ ἱεροῦ ἐκείνου σκήνους ὡς ὑπεξούσιον
αὐτῷ δῆθεν ποιῶν τὴν αὐτοκρατορικὴν ὄντως τῶν ῥωμαίων ἀρχὴν ὁμοῦ τε δι'
αὐτῆς ἁγιασθῆναι καὶ τὸ ῥωμαίων κράτος ἐπηλπικώς. Ἔπειτα μετ' ἐκεῖνον καὶ τὸ
ἔκκριτον ἱεράτευμα. Πολὺς ἦν ὠθισμὸς καὶ τῶν ἐν τέλει καὶ τοῦ λαοῦ πρὸς τὸ
πλησίον γενέσθαι καὶ ψαῦσαι μόνον τοῦ ἱεροφόρου σοροῦ. Εἶτα δαψιλεῖ τῷ φωτὶ καὶ
ἀφθόνοις εὐωδίαις θυμιαμάτων καὶ ἀρωμάτων ἐπηρμέναις τε τῶν ὕμνων ᾠδαῖς καὶ
διαπρυσίοις φωναῖς διὰ μέσης οἷον θριαμβεύων τῆς πόλεως ἐπὶ τὴν τῆς Εἰρήνης
μεγάλην καὶ καθολικὴν ἀπάγεται. Ἐπὶ δὲ τὸν ἴδιον θρόνον παρὰ τοῦ ἱεροῦ Πρόκλου
ἀνατεθείς, πάσης δὲ τῆς πόλεως μετὰ πολλῆς θυμηδίας καὶ χαρμονῆς ἐκβοησάσης
ἀπόλαυέ σου καὶ θρόνον καὶ τὸν λαόν, ὃν ταῖς σαῖς διδαχαῖς ἐφώτισας, ἀρχιερεῦ
θεοπρόβλητε, εἶθ' οὕτως τῇ βασιλικῇ κελεύσει τοῦ αὐτοκράτορος τὸ ἱερὸν ἐκεῖνο
σκῆνος βασιλείῳ ἅρματι εὐλαβῶς ἅμα καὶ αἰσίως ἐπετέθη καθάπερ ἥλιος τῷ πυρίνῳ
ἅρματι διφρευόμενος. Εἰ δὲ καὶ Πρόκλος συνέποχος ἦν τῷ σοφῷ Χρυσοστόμῳ περὶ
τὸ ἅγιον αὐτοῦ σκῆνος πονῶν καὶ διακονῶν, χάρις τῷ καλῷ διδασκάλῳ τῷ καὶ
προελομένῳ εἰς μαθητείαν καὶ ἀναδείξαντι καὶ θρόνου καὶ ἀρετῆς κύριον διάδοχον.
Ὁ μὲν οὖν ἀοίδιμος Πρόκλος πρὸ ἑαυτοῦ θεὶς τὴν πολύολκον ἐκείνην τοῦ ἁγίου
λειψάνου θήκην πρὸς τῷ τῶν θείων ἀποστόλων περιωνύμῳ καὶ περικαλλεῖ ναῷ
ἀποικίζεται. Καὶ οὕτως ἐπὶ τῆς ἱερᾶς καθέδρας ἀνυψωθεὶς καὶ τῷ θείῳ κἀνταῦθα
θρόνῳ ἐνιδρυθεὶς μυστικῶς τε τὰ μεμυκότα κινήσας χείλη καὶ εἰρηνεύσας τὸ
ποίμνιον ὡς παρὰ τῶν διαυγέστερον ἠξιωμένων τὰ τοιαῦτα ὁρᾶν ἐλέγετό τε καὶ
ἐπιστεύετο τῇ τῆς ἀναπαύσεως ἐναπετέθη σορῷ, κἀνταῦθα ἡμῖν τὸ ἄσυλον τοῦτο
ἀποτεθησαύρισται ἀγαθὸν καὶ νῦν ἐστι βασιλεῦσι καὶ ἄρχουσιν, ἀρχιερεῦσι καὶ
κλήρῳ καὶ τῷ λαῷ πολύτιμος ὄλβος, κειμήλιον ἄπαυστον, θησαυρὸς ἀναφαίρετος,
ἰαμάτων πηγή, διδαγμάτων ἀέναος ποταμός, ὀρθοδοξίας κρηπίς, στύλος ἐκκλησιῶν,
ὀχύρωμα 316 καὶ κόσμος καὶ καυχήσεως στέφανος τῆς ὑποδεξαμένης
μεγαλοπόλεως. Οὐ γὰρ ἔδει τὴν τοσούτοις εὐθυνουμένην καλοῖς καὶ τοσούτοις
ἐναγλαϊζομένην ἀγάλμασιν ἀρετῆς οἷον ἠκρωτηριᾶσθαι περὶ τὸ καιριώτερον καὶ τὸν
οἰκεῖον περιῃρεῖσθαι κόσμον, πιναρὰν δὲ καὶ πένθιμον στολὴν ἐνδιδύσκεσθαι τὸν τῷ
κάλλει τῶν λόγων ὡς νύμφην ταύτην καταστολίσαντα κατὰ τὴν ὑπερόριον ὁρῶσαν
παροραθέντα αὐλίζεσθαι. Τῆς δὲ θείας καὶ ἀναιμάκτου θυσίας κατὰ τὸ εἰωθὸς
τελεσθείσης Πρόκλος ὁ παναοίδιμος μετὰ τοῦ ἱερατικοῦ καταλόγου καὶ τοῦ
βασιλέως παρόντος Θεοδοσίου τοῖς τελουμένοις καὶ πάσης τῆς συγκλήτου βουλῆς
εἰς ταὐτὸ συγκεκροτημένης πρὸς τὴν κηδείαν τρέπονται τοῦ θεοφόρου ἐκείνου καὶ
ὄντως ἀκηράτου λειψάνου. Καὶ πάλιν θαῦμα ἐν θαύματι καὶ λογισμοὺς ἐκβαῖνον καὶ
οἷον ἐξαίσιον· ἄνθρωπον γάρ τινα νόσῳ ἀθρίτιδι κατεσχημένον καὶ ἐπὶ πολλοῖς
ἔτεσιν ἐξ αὐτῆς χεῖράς τε καὶ πόδας παρειμένον καὶ ὡς ἄλλον τινὰ παραλελυμένον
ἐπὶ κλίνης βεβλημένον καὶ σωτηρίας ἁπάσης ἀπειρηκότα καὶ μικροῦ ἀποψύχοντα
ἐνεγκόντες τῷ τιμίῳ τούτῳ λειψάνῳ κατέψων τοῦ μακαρίου. Αὐτοῦ δὲ τοῦ
παρειμένου σὺν δάκρυσι τὸν ἅγιον ἱκετεύοντος τὰ εἰκότα ἐγένετο μὴ ἀποτυχεῖν τῆς
ἰάσεως ὥστε πάντας τοὺς θεασαμένους δόξαν ἀναπέμψαι θεῷ τῷ τοιαύτην χάριν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

7

παρασχόντι τῷ σεβασμίῳ τούτῳ καὶ ἁγίῳ Χρυσοστόμῳ πατρί. Χερσὶ δὲ ταῖς ἰδίαις ὁ
πατριάρχης τὸν ἑαυτοῦ ἐν πνεύματι πατέρα τε καὶ διδάσκαλον ἐν τῷ τῆς
ἀναπαύσεως κατατίθησι μνήματι, ἐν τοῖς ἀδύτοις τοῦ ἱεροῦ καὶ ἀποστολικοῦ
τεμενίσματος πάντως συνεῖναι τοῖς Χριστοῦ μαθηταῖς καὶ ἀποστόλοις τὸν τοῦ
Χριστοῦ μαθητὴν καὶ ἀπόστολον. ∆ιὰ τοῦτο τοῖς ποθοῦσιν ὁ ποθούμενος ἀποδίδοται,
τοῖς πιστοτάτοις τέκνοις ὁ πατὴρ ὁ φιλότεκνος, τοῖς ὑπηκόοις ὁ πανεπιστήμων
διδάσκαλος, τοῖς ὀρφανοῖς ὁ προστάτης, τοῖς ἀδικουμένοις ὁ προῖκα συνηγορῶν,
τοῖς ἐπηρεαζομένοις ὁ μέγας ἐπίκουρος, τοῖς ἀνδραποδιζομένοις πολλάκις ταῖς
ἡδοναῖς ὁ τὴν ἐπιθυμίαν πτερῶν πρὸς θεόν, τοῖς εἰς κακίας ὀλισθήσασι βάραθρον ὁ
τῆς μετανοίας ὑφηγητής, τοῖς ἡττωμένοις κόρου τε καὶ τρυφῆς ὁ τῆς ἐγκρατείας
ὑπογραμμός, ὁ πανταχοῦ τῶν πάντων κρατῶν, ὁ παρὰ παντὸς τὸ ἐν πᾶσιν
ἀποφερόμενος συγκεχωρηκώς, ὁ μηδενὸς τῶν πρὸ αὐτοῦ κατὰ πᾶν εἶδος ἀρετῆς
ἀπολειπόμενος καὶ παιδεύσεως, ὁ μηδενὶ τῶν μετ' αὐτὸν ἐγγὺς αὑτοῦ συγχωρήσας
ἐλθεῖν, εἰ μὴ καὶ πλεῖστον ἀπολειφθῆναι τῷ τῆς ἀρετῆς ὑπερβάλλοντι, ὁ μηδένα
βυθὸν τῶν ἀποκρύφων μυστηρίων θεοῦ ἀδιερεύνητον καταλιπών, ὁ τῆς Γραφῆς τὸ
βάθος ἅπαν θαυμασίως ἀναστομώσας καὶ ἀναπτύξας καὶ οἷον εἰς φῶς ἀγαγὼν καὶ
τρισσῶς ὄντως ἀπογραψάμενος. Θάλλει μου ἡ καρδία καὶ πηγάζει ὁ νοῦς 317 καὶ
πανηγυρίζειν ἐφίεται καὶ τοῦ λέγειν μᾶλλον ὀρέγεται. ∆εῦτε δὲ συμπληροῦτέ μοι
τὴν ἐπιτάφιον τούτῳ τῶν ὕμνων ᾠδὴν ὅσοι τῶν ἐκείνου καλῶν μετειλήφατε καὶ
καὶ τὰ ἐκείνου θαυμάζετε. Οἱ βασιλεῖς τὸν λαμπρύνοντα μᾶλλον ὑμῶν τὰς ψυχὰς
τοῖς ὁλοχρύσοις ῥείθροις τῆς γλώσσης αὐτοῦ ἢ τὸ σῶμα τὸ τῆς πορφύρας ἄνθος καὶ
τῶν μαργάρων τὸ κάλλος καὶ τὸ ἐκ λίθων τιμίων διάδημα. Οἱ ἱερεῖς τὸν εὐθὺν
κανόνα τῆς ἀρχιερατικῆς καταστάσεως, οἱ περὶ λόγους τὸν ἐν πᾶσι λόγων ἡδέα
πάντα ἀποκρυψάμενον. Οἱ πλούσιοι τὸν ἄριστον οἰκονόμον τῶν ἀποθέτων ὑμῖν
χρημάτων καὶ περιττῶν. Οἱ πένητες τὸν ποριστὴν τῶν ἀναγκαίων καὶ τὴν ἔνδειαν
λύοντα. Οἱ ἐν πένθει τὴν παραμυθίαν, τὴν παράκλησιν οἱ ἐν συμφοραῖς, τὴν
ἀνάκλησιν οἱ τῇ ἀπογνώσει πρὸς ἀπωλείας κατερραγμένοι βυθόν. Τὸν πάντα
γεγονότα τοῖς πᾶσι κατὰ Παῦλον εἰπεῖν, τὸν μέγαν ἀπόστολον, ἵνα κερδήσῃ τοὺς
πάντας ἢ πλείονας. Τοίνυν ἀπὸ τοῦ καταρρύτου τούτου λειμῶνος τῶν τῆς ψυχῆς
ἀγαθῶν τὰ ῥόδα τῆς ἀρετῆς καὶ ἀγάπης τρυγήσωμεν ὡς ἥδιστα ἐκ ψυχῆς καὶ
φίλτρου περικαοῦς ἐπιχορηγοῦντες τὰς εὐφημίας καὶ λέγοντες ἅμα καὶ οὕτω
κατονομάζοντες σκέπην κραταιὰν καὶ ὠχυρωμένων βασίλειον, φιλίας κειμήλιον
ἔμψυχον, κῆπον κεκλεισμένον, πηγὴν ἐσφραγισμένην παράδεισόν τε τρυφῆς,
εὐκαίρως ἀνοιγόμενον τε καὶ συγκλειόμενον, λιμένα γαλήνης τοῖς ὑπὸ καταιγίδων
ταραττομένοις καὶ ἀναψύξεως, τῶν χρυσῶν δογμάτων καὶ ἀκηράτων τὸν ἀέναον
χορηγόν. Ὁρᾶτε τοῦ Χρυσοστόμου τὰ χάριτας, ὁρᾶτε τῶν αὐτοῦ πόνων τὰ γέρα.
Μέμνησθε πάντων τῶν γλυκυτάτων αὐτοῦ καὶ ἡδίστων διδαχῶν. Οἶδα τοῦτο καὶ
σύνοιδα μεμνησομένους ἀεὶ καὶ οἷον ὑμῖν τὸ φίλτρον περὶ τὸν κοινὸν πατέρα καὶ
εὐεργέτην καὶ ὡς ἐξήρτησθε τοῦ φίλτρου τούτου καὶ τῆς ἀγάπης καὶ τοῦτο μόνον
πνεῖτε καὶ ἀναψύχεσθε. Ὁρᾶτε οἷος ἐγὼ κοινωνῶν καὶ τοῦ ζήλου καὶ τῆς ἀγάπης τοῦ
θαυμασίου τούτου καὶ ἀξιεπαίνου πατρὸς καὶ διδασκάλου, εἴθε δὲ καὶ τῶν ἱερῶν
πόνων ὧν ἡμῖν διὰ τῶν αὑτοῦ θεοπνεύστων λογίων ὑποτίθεται καὶ τῆς
σωφρονεστέρας διαγωγῆς ὑπηκόους γενέσθαι. Καὶ γένοιτο, δέσποτα τοῦ ἐλέους καὶ
πάσης παρακλήσεως Κύριε, καὶ πάντα τὸν κόσμον καὶ πρὸ πάντων ἡμᾶς τοῦτο μόνον
κεκτῆσθαι χαράν, τὴν κοινὴν ἐλπίδα, τὸ ἀναφαίρετον ἀγαλλίαμα, τὴν ἀνεπίληστον
μνήμην, τὴν ψυχοτρόφον, ὃν εἰ μὴ τιμῶμεν καὶ ἀγαπῶμεν καὶ τῆς σωζούσης
διδασκαλίας τούτου ἐχώμεθα, τίνα ἄλλον στέρξομεν ἢ τίνι τῶν κρειττόνων καὶ εἰς
ζῆλον ἀρετῆς ἀναγόντων ἑψόμεθα ἢ διὰ τίνος εὑρήσομεν τὰ ἐλπιζόμενα ἀγαθά; Ναὶ
τέκνα πολιτείας εὐγενοῦς τε καὶ φιλοσέμνου τῆς εὐδαίμονος πόλεως εὐτυχέστατα
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

8

καὶ θρέμματα τῆς τιμώσης τὰ τίμια λόγοις καὶ πανδήμοις ἑορταῖς τε καὶ πανηγύρεσι,
φιλόχριστοι, 318 φιλαπόστολοι, φιλομάρτυρες, φιλοχρυσόστομοι, τὸ πάντων τῶν
τιμιωτάτων κεφάλαιον, τὰς τούτων τιμάς τε καὶ πανηγύρεις ἐμπνέοντες, ταύτας
αἰσίως διατιθέντες, ἐν τούτοις στρεφόμενοι. Οἶδα γὰρ ὑμῶν τὰς παννύχους στάσεις
καὶ ἀγρυπνίας, τὰς ψαλμῳδίας καὶ τοὺς ὕμνους τοὺς ἱερούς, τὴν ἀκρόασιν τῶν
λογίων τοῦ πνεύματος καὶ πρός γε τούτοις καὶ τοῦ ἡμετέρου θείου καὶ σοφοῦ
Χρυσοστόμου καὶ τὴν πρὸς θεὸν ἀνάβασιν ἢ ἐκδημίαν καὶ μεταχώρησιν, ἐκείνου τοῦ
μεγάλου νοῦ τὰ θεῖα καὶ γεννήματα καὶ προβλήματα. ∆ιὰ τοῦτο πειστέον ταῖς αὐτοῦ
εἰσηγήσεσιν· οὗτος γὰρ ἀληθῶς ὁ τῆς οἰκουμένης φωστήρ, ζωῆς ἐν κόσμῳ λόγον
ἐπέχων, ὁ τῶν ἐκκλησιῶν στύλος, τῶν πιστῶν τὸ προπύργιον, ὁ ἀπλανὴς ὁδηγὸς ὁ
καλὸς γεωργὸς τῶν ἡμετέρων ψυχῶν, ὁ μόνον καὶ μνημονευόμενος ἡδονὴ καὶ
γλυκύτης, ὃν οὕτως διαφερόντως ἀγάμεθα καὶ οὕτως αὐτοῦ ἐξηρτήμεθα καὶ οὕτως
τῇ περὶ αὐτοῦ μνήμῃ συνεδήσαμεν τὰς ψυχάς, ὡς ἀπό τινος χρυσῆς σειρᾶς ἀδόλου
καὶ πιστῆς διαθέσεως ἐκκρεμάμενοι. Ὑπέρευγε τοσούτου καλοῦ χαρισθέντος ἡμῖν
εἰς ἀπόλαυσιν. Χρυσοστόμου ὁ μνησθεὶς πάντων τῶν φαύλων ἀπέσχετο· ἐκείνου γὰρ
ὁ ἄμεμπτος καὶ θεάρεστος βίος ἡγίασε καὶ ἐτίμησε, διὸ καὶ παρ' ἡμῶν ἀεὶ φιλεῖται
καὶ τιμᾶται καὶ μακαρίζεται. Εἴ τι καλὸν τῷ βίῳ τῶν ἀνθρώπων ἐμπολιτεύεται, εἴ τι
σεμνότητος ἄξιον, εἴ τι πολιτείας θεοφιλεστάτης ἐχόμενον ἐκ τούτου εὕρηται καὶ
κατόρθωται. Κενὸς ἂν ἦν ὁ κόσμος παντὸς ἀγαθοῦ τοῦ πρὸς θεὸν βλέποντος, εἰ μὴ
τῆς ἀγαθῆς ῥίζης ἐτύχομεν τοῦ σοφοῦ κήρυκος, τοῦ ἀπλανοῦς ὁδηγοῦ, τοῦ
ἀνεσπέρου φωτὸς καὶ λαμπροτάτου. Ὀργᾷ τὸ πνεῦμα καὶ γέγηθε καὶ ὥσπερ περιζέει
τὸ φίλτρον καὶ διανίσταται καὶ οὐκ ἐθέλει τῆς περὶ τούτου μνήμης ὁ λόγος
ἀφέλκεσθαι. Αἴθε πως ἦν καὶ διαβῆναι μόνον δι' ἀρετῆς πρὸς αὐτόν. Αἴθε συνεῖναι
τῷ ποθουμένῳ καὶ τοῦτο μόνον ἡγεῖσθαι χαρὰν καὶ ἀπόλαυσιν· τὶ γὰρ καὶ τοῦ μόνον
ἐκείνου μεμνῆσθαι γλυκύτερον; τὶ δὲ τοῦ διὰ γλώσσης τοῦτον ἔχειν ἐπιτερπέστερον;
Οὕτω σοφῶς ἑαυτοὺς οἰκονομοῦντες καὶ σωτηρίας χαριέστερον ἐντευξόμεθα τῆς
προσδοκωμένης. Ἀλλ' ὦ στόμα πάγχρυσον τοῦ Χριστοῦ, ὦ σάλπιγξ οὐρανία τοῦ
πνεύματος, ὦ θεωρὲ καὶ κῆρυξ τοῦ ἀνάρχου πατρός, ὁ τῆς ἁγίας Τριάδος ὑποφήτης
καὶ εὐαγγελιστὴς ἀληθής, ὁ ἀμέσως ὑπὲρ ἡμῶν ἐντυγχάνων καὶ πρεσβεύων τῇ ἁγίᾳ
καὶ τρισηλίῳ θεότητι, αἴτησαι παρὰ θεοῦ εἰρήνην ταῖς ἐκκλησίαις, ἃς ταῖς σαῖς
διδαχαῖς ἐφώτισας, ἵνα ἡμῖν αἱ πανηγύρεις αὗται φιλοτιμότερον ἄγωνται. Μὴ
πλεονεξία, ἥτις ἐστὶ δευτέρα εἰδωλολατρεία, ἄρῃ καθ' ἡμῶν κεφαλήν. Μὴ ζιζάνιον
χρυσομανίας ἐγερθὲν ἀποπνίξῃ τὸν σῖτον τῆς σῆς μεταδοτικῆς διδασκαλίας τῆς εἰς
τοὺς δεομένους. Μή τις πέτρα γένηται καθ' 319 ἡμῶν τὸ στερρὸν ὑποφαίνουσα τῆς
κακίας λειπομένη τῆς πιότητος τῆς σῆς ταπεινοφροσύνης καὶ ἄρριζον ἀποδειχθῇ
τῶν σῶν σπερμάτων τὸ καλὸν καὶ πίον γεώργιον. Χρήζομεν τῆς ἀπὸ σοῦ ὠφελείας
καὶ τῶν πολλῶν καὶ μεγάλων σου εὐεργεσιῶν τὴν ἄφθονον δαψίλειαν ἐπιζητοῦμεν.
Πρέσβευσον ὑπὲρ τῆς σῆς ἁγίας ποίμνης πρὸς τὸν κοινὸν θεὸν καὶ βασιλέα ταῖς σαῖς
εὐχαῖς τίμιε καὶ παντὸς τοῦ κόσμου ἰσοστάσιε ἄνθρωπε λυθῆναι τὰ ἡμῶν
πλημμελήματα. Ἔχεις πολλοὺς τῇ σῇ μεγαλειότητι συμπρεσβεύοντας, τούτους
συμπρεσβευτὰς συμπαράλαβε. Ἄθροισον τὸν χορὸν τῶν σοφῶν ἀποστόλων καὶ μετὰ
τούτων δεήθητι ὡς τούτων ὑπάρχων ὁμότροπος. Ὑπόμνησον αὐτοὺς ἃ ὑπὲρ τῶν
ἐκκλησιῶν ἐκακοπάθησαν, ὑπὲρ ὧν τοὺς κινδύνους ὑπήνεγκαν καὶ τὰς ἁλύσεις
ἐφόρεσαν, ὑπὲρ ὧν καὶ μελῶν ἠκρωτηριάσθησαν καὶ τοῦ ζῆν βιαίως ἀφῄρηνται,
ὑπὲρ ὧν διὰ πυρὸς καὶ ὕδατος προφητικῶς εἰπεῖν διήλθωσαν. Ὑφορώμεθα τὰς ἀπὸ
τοῦ πονηροῦ εἰσαγομένας ἡμῖν παγίδας. Προσδοκῶμέν τι τῶν ὀχληρῶν τοῦτο τῆς
πονηρᾶς ἐννοίας ἀποτικτούσης, μήπως εἴδωλον ἁμαρτίας ἐν ἡμῖν μορφωθὲν πόρρω
τῆς σῆς διδασκαλίας ἀπώσηται. Εἰ γὰρ καὶ ὑπερέβης τὸν βίον καὶ μετὰ ἀγγέλων τῷ
θεῷ ὡς ἄϋλός τις καὶ οὐράνιος λειτουργεῖς καὶ δοξάζεις, ἀλλ' οἶδας, ὡς ὀλισθηρὰ καὶ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

9

εὐθύδρομος πρὸς τὸ ἐναντίον ἡ ἀνθρωπίνη πέφυκε φύσις. Μὴ ἐπιλάθῃ τῶν σῶν
τέκνων, οὓς τῷ γάλακτι τῶν σῶν διδαχῶν φιλοστόργως ἐξέθρεψας. Ἡμεῖς γὰρ εἰ καί
τι τῶν κρειττόνων καὶ θεοφιλῶν ἀρετῶν μετερχόμεθα, σοὶ λογιζόμεθα τὴν
εὐεργεσίαν. Αἰτοῦμεν δὲ καὶ πρὸς τὸ μέλλον καλῶς εὐθύνειν ἡμᾶς καὶ διεξάγειν ἐπὶ
νομὰς ζωηφόρους καὶ σωτηριώδεις τῶν θείων ἐντολῶν. Αἴτησαι δὲ παρὰ θεοῦ
βασιλεῦσι κράτος γαλήνιον ἐν ὑποπτώσει τῶν δυσμενῶν, ἀρχιερεῦσιν εἰρηναίαν
κατάστασιν τῇ τῶν σκανδάλων ἀνατροπῇ, τῷ ὑπηκόῳ τὴν εὐθηνίαν τῶν ἐντὸς
ἀγαθῶν καὶ τῶν ἔξωθεν, ἵνα τῆς ἡμετέρας ψυχῆς λαμπάδες, ἃς ὁ τῆς σοφίας ἀνῆψεν
ἔρως, τὰς ἐκκλησίας καταφαιδρύνωσιν, ἵν' οἱ περικαλλεῖς οὗτοι ναοὶ τοῦ
προσήκοντος κόσμου μεταλαγχάνωσι τῷ τῶν συρρεόντων ἐσμῷ καὶ πάντες ἐν τοῖς
τῆς ἀρετῆς λειμῶσι κατασκηνούμενοι καὶ περιηνθισμένοι τοῖς ἐντεῦθεν
πλεκομένοις ἀμαραντίνοις στεφάνοις ὑπὲρ λίθον καὶ σάπφειρον ὡραϊσθέντες ὑπὲρ
λυχνίτην καὶ σμάραγδον αὐγασθέντες καὶ τῇ λευκότητι τῶν ἀρετῶν ὡς μάργαροι
γαλακτοειδῶς λευκαθέντες καὶ στίλψαντες λαμπροὶ λαμπρῶς ἐποφθείημεν τῷ καλῷ
νυμφαγωγῷ τῶν καθαρῶν ψυχῶν Χριστῷ, τῷ θεῷ ἡμῶν, ὅτι δι' αὐτοῦ ἡ σωτηρία
πᾶσιν ἡμῖν ἐξήνθησε νῦν καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

10

