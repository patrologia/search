Fragmenta in Matthaeum
1 Mt 1, 1 Ὁ μὲν μακάριος Ματθαῖος εὐθὺς καὶ ἐκ προοιμίων διδάξαι πάντα
σκοπὸν ἐσχηκώς, ὅτι κατὰ τὰς προφητικὰς ὤφθη φωνὰς ὁ Χριστὸς ταῖς μὲν
γεγενημέναις ἐπαγγελίαις τὸ ἀψευδὲς παρεχόμενος οὐδὲ μίαν δ' ἀγνωμοσύνης
πρόφασιν τοῖς ἀπιστοῦσιν καταλιμπάνων, βίβλος, φησί, γενέσεως Ἰησοῦ Χριστοῦ
υἱοῦ ∆αβὶδ υἱοῦ Ἀβραὰμ ἀντὶ τοῦ· τοῦτον ἐκήρυττον αἱ προλαβοῦσαι φωναί, τοῦτον
ηὔδων αἱ πρὸς τοὺς δικαίους ἐπαγγελίαι. 2 mt 1, 1 Περιττὸν μὲν τὸ τοῦτο ζητεῖν· τί
γὰρ διαλλάττει τὸ πρῶτον τοῦτον εἰπεῖν ἢ τὸν ἕτερον; δι' ἑκατέρου γὰρ ἀπόγονος
ὁμοίως ἀμφοτέρων ἐδείκνυτο. ὁ δὲ εὐαγγελιστὴς οὐδὲ οὕτως φησίν· οὐ γὰρ τὸν
Χριστὸν ἔφη υἱὸν τοῦ ∆αβὶδ καὶ υἱὸν Ἀβραὰμ, ἀλλὰ τὸν μὲν Χριστὸν τοῦ ∆αβίδ, τοῦ
δὲ Ἀβραὰμ τὸν ∆αβίδ, τὸ αὐτὸ μὲν καὶ οὕτως σημαίνων, οὐ τοῦτο δὲ τέως ἐν τῷ
προχείρῳ λέγων τῆς λέξεως· τὸν γὰρ υἱὸν δῆλον ὡς οὐ τὸν γεννηθέντα λέγει, ἀλλὰ
τὸν ἀπόγονον. ἀλλὰ φαίη τις ἂν πρὸς αὐτόν, ὡς οὐκ ἀπὸ τοῦ ∆αβὶδ ἀνάγειν ἐπὶ τὸν
Ἀβραὰμ ἐβουλήθη τὸ γένος ὁ Ματθαῖος· τί δήποτε γάρ, εἰ τοῦτο ἤθελεν, μὴ κατὰ τὸν
Λουκᾶν αὐτὸν ἐγενεαλόγησεν; ἀλλ' ἰδικῶς δι' ἃς ἔφαμεν αἰτίας τοῦτό φησιν. ἦν ἀπὸ
τῶν δώδεκα τούτων μνημονεῦσαι δι' ὧν τὸ γένος ἐπὶ τὸν Χριστὸν κατάγεσθαι
συνέβαινεν. 4 mt 1, 16 Ἐπειδὴ τοίνυν ἐδόκει τι καινὸν ἐμφαίνειν τῇ μεταβάσει εἰπὼν
Ἰακὼβ δὲ ἐγέννησεν τὸν Ἰωσὴφ τὸν ἄνδρα Μαρίας, ἐξ ἧς ἐγεννήθη Ἰησοῦς ὁ
λεγόμενος Χριστός, ἐπηγγείλατο καὶ τὸν τρόπον ἡμῖν ἐκθέσθαι, καθ' ὃν γεγέννηται
καί φησιν· «τοῦ δὲ Ἰησοῦ Χριστοῦ ἡ γέννησις οὕτως ἦν»· ἀντὶ τοῦ οὐχ ὁμοίως
ἐτέχθη τοῖς λοι ποῖς, ἀλλ' ἑτέρως. 5 mt 1, ∆υνατὸν δὲ νοῆσαι τὴν δύναμιν τοῦ θεοῦ·
καὶ γὰρ πνεῦμα κα λεῖται. 7
mt 2, 7 Ὁ Ἡρώδης λάθρα ἠβουλήθη ἐρωτῆσαι τοὺς μάγους δεδιὼς μή πως οἱ
Ἰουδαῖοι ὡς οἰκεῖον βασιλέα σώσωσιν αὐτόν. 8 mt 2, 23 Ἕτεροι δέ φασιν ταῦτα
διαφωνεῖν πρὸς τὰ παρὰ τῷ Λουκᾷ κεί μενα· εἰ γὰρ ἀπὸ τῆς Βηθλεὲμ κατὰ τὸν
Λουκᾶν ἀνῆλθεν εἰς Ἰερου σαλὴμ κἀκεῖθεν εἰς τὰ Νάζαρα, πῶς κατὰ τὸν Ματθαῖον
ἀπὸ τῆς Βηθ λεὲμ χρηματισθεὶς ὁ Ἰωσὴφ ἀπήρθη εἰς Αἴγυπτον; Λουκᾶς μὲν γὰρ τὸν
καιρὸν ἱστορῶν τῆς τοῦ σωτῆρος ἡμῶν γεννήσεως τῆς «Αὐγούστου» βασιλείας
μνημονεύει καὶ τῆς κατ' αὐτὸν «ἀπογραφῆς» καὶ τὰ συμ βάντα ἐπὶ τῇ τοῦ σωτῆρος
γεννήσει, ἀλλὰ καὶ «ὅτε», φησίν, «ἐπλήσθησαν αἱ ἡμέραι τοῦ περιτεμεῖν αὐτόν,
ἀνήγαγον» τὸ παιδίον «εἰς Ἱεροσόλυμα» καὶ τελέσαντες ἐπ' αὐτῷ τὰ νενομισμένα
ἀπίασιν εἰς «Ναζαρέθ». ὁ δὲ Ματθαῖος οὐδ' ὅλως τῶν παρὰ τῷ Λουκᾷ μέμνηται,
τίθησι δὲ τὰ παρὰ τούτῳ σεσιγημένα, λέγω δὴ τὰ κατὰ τοὺς μάγους, κινησάντων μὲν
ἴσως ἀπὸ τῆς οἰκείας γῆς ἅμα τῷ γεννηθῆναι τὸν Ἰησοῦν οὔπω δὲ τὴν τοσαύ την
στειλαμένων πορείαν ἐν καιρῷ βραχεῖ. πόσος δὲ ἦν οὗτος ὁ μεταξὺ χρόνος τοῦ τε
φανέντος ἀστέρος καὶ τῆς τῶν μάγων ἀφίξεως, αὐτὸς ἐδίδαξεν ὁ εὐαγγελιστὴς
λέγων «ἀπὸ διετοῦς» καὶ «κατὰ τὸν χρόνον ὃν ἠκρίβωσεν παρὰ τῶν μάγων»·
οὐκοῦν διετὴς ἤδη χρόνος παρελήλυθεν ἀπὸ τῆς τοῦ κυρίου γεννήσεως καὶ ἐπὶ τὴν
ἄφιξιν τῶν εἰρημένων. οὔκουν διαφωνεῖ τὰ παρὰ τοῖς ἱεροῖς εὐαγγελισταῖς, εἰ μὲν ὁ
Λουκᾶς ὀγδόῃ τῆς γεννήσεως ἡμέρᾳ ἀνάγει αὐτὸν ἅμα τοῖς γονεῦσιν εἰς Ἰερουσαλὴμ
κἀκεῖθεν ἀπάγει ἐπὶ τὴν Ναζαρέθ, Ματθαῖος δὲ μετὰ διετῆ χρόνον γενόμενον πάλιν
ἐν Βηθλεὲμ ἀναγράφει, ἐντεῦθέν τε εἰς Αἴγυπτον ἀπεληλυθέναι διὰ τὴν τοῦ
βασιλέως ἐπιβουλήν. καὶ ἦν εἰκὸς οὐ μόνον δεύτερον, ἀλλὰ καὶ πλειστάκις
ἐπιφοιτᾶν αὐτοὺς τῷ τόπῳ μνήμης τοῦ παραδόξου χάριν. ὅτι δὲ μὴ εἷς ἦν παρ'
ἀμφοτέροις χρόνος, καὶ ἄλλως ἔστι συλλογίσασθαι. Λουκᾶς φησι μὴ εὐπορῆσαι
1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

αὐτοὺς καταγωγείου ἐν τῇ Βηθλεὲμ διὰ τὸ πλῆθος τῶν ἐπιξενουμένων αὐτόθι
προφάσει τῆς ἀπογραφῆς, ὁ δὲ Ματ θαῖος, ὅτι «ἐλθόντες εἰς τὴν οἰκίαν εἶδον τὸ
παιδίον μετὰ Μαρίας τῆς μητρὸς αὐτοῦ, καὶ πεσόντες προσεκύνησαν αὐτῷ». ἀλλ'
οὐκ ἐν «φάτνῃ» κείμενον αὐτοὶ καταλαμβάνουσι τὸ παιδίον ὁμοίως τοῖς ποιμέσιν,
ἀλλ' ἔνδον ἐν «οἰκίᾳ μετὰ τῆς αὐτοῦ μητρὸς» θεωροῦσιν. ἐκεῖ μὲν γὰρ διὰ τὸ μὴ
εἶναι «τόπον ἐν τῷ καταλύματι» ἐν «φάτνῃ» τίθεται, ἐνταῦθα δὲ μετὰ διετῆ χρόνον
ἐν «οἰκίᾳ» εὑρίσκεται ὑπὸ τῶν μάγων «μετὰ τῆς μη τρὸς αὐτοῦ» σχολῆς οὔσης ἐν τῇ
Βηθλεὲμ «καὶ πεσόντες προσεκύνησαν αὐτῷ». 9
Mt 3, 2 Βασιλεία ἐστὶν οὐρανῶν ἡ παρουσία τοῦ Χριστοῦ· αὕτη γὰρ ἡμῖν
χαρίζεται τὴν τοῦ πατρὸς μετάληψιν καὶ τὴν εἰς οὐρανοὺς ἄνοδον καὶ τὸ εἰς ἀγαθὸν
τοῖς ἁγίοις ἐν τῷ ἐσομένῳ αἰῶνι ἄτρεπτον. 10 mt 3, 5 Ἰώσηπος ἐν τῷ
πεντεκαιδεκάτῳ λόγῳ τῆς Ἰουδαϊκῆς ἱστορίας μαρτυρεῖ Ἰωάννῃ βαπτιστῇ γενομένῳ
καὶ καθάρσιον τοῖς βαπτιζομένοις ἐπαγγειλαμένῳ. 11 mt 3, 7–9 μηδὲ τοῖς προγόνοις
σεμνυνόμενοι τῆς κατὰ ψυχὴν ἀρετῆς ἀμελήσητε· 14
mt 4, 12 Ἔοικεν Ἰωάννης πληρώσας ἐν τῷ Ἰησοῦ τὴν τοῦ βαπτίσματος
διακονίαν εἰς αὐτὸν καταλήξασαν μετὰ τοῦτο παραδεδόσθαι καὶ ἐν φυ λακῇ
κατακεκλεῖσθαι· μετὰ γὰρ τὸ βαπτίσασθαι τὸν Ἰησοῦν οὐδὲν εὕρομεν
ἀναγεγραμμένον περὶ Ἰωάννου ἢ ὅτι μετὰ τὸν πρὸς τὸν διάβολον πει ρασμὸν εὐθέως
καὶ ἅμα τῷ βαπτίσματι ὑπὸ τοῦ πνεύματος ἐκβληθεὶς εἰς τὴν ἔρημον, ὡς ὁ Μᾶρκος
ἔγραψεν, ἀκούσας Ἰησοῦς, ὅτι Ἰωάννης παρεδόθη ἀνεχώρησεν εἰς τὴν Γαλιλαίαν.
ἀλλὰ καὶ ὁ Λουκᾶς τῇ περὶ τοῦ βαπτίζειν τὸν Ἰωάννην διηγήσει συνάπτει τὰ περὶ
τοῦ κεκλεῖσθαι αὐτὸν ὑπὸ Ἡρώδου. δῆλον δὲ ὅτι καὶ κατὰ τὸν εὐαγγε λιστὴν
Ἰωάννην οὗτος ἦν ὁ καιρὸς αὐτῷ τοῦ κεκλεῖσθαι εἰς φυλακήν· λέλεκται γὰρ παρ'
αὐτῷ τὸ «ἦν δὲ καὶ Ἰωάννης βαπτίζων ἐν Αἰνὼν ἐγγὺς Σαλείμ. οὔπω γὰρ ἦν
Ἰωάννης βεβλημένος εἰς φυλακήν». κατὰ τοὺς τέσσαρας τοίνυν εὐαγγελιστὰς οὔτε
πρὶν βαπτίσασθαι Ἰησοῦν ἀπήντα τι κώλυμα βαπτίζοντι Ἰωάννῃ οὔτε μετὰ ταῦτα ἔτι
ἐβάπτισεν, ἀλλ' ἔδει καταλῆξαι τὰ Ἰωάννου ἐν ἀρχῇ τοῦ οἰκονομεῖσθαι τὸ εὐαγ
γέλιον ὑπὸ τοῦ Ἰησοῦ. τάχα δὲ ἡ φρουροῦσα δύναμις Ἰωάννην, ἕως ἐν Ἰησοῦ
τελειώσῃ τὴν τοῦ βαπτίσματος οἰκονομίαν, κατέλιπεν αὐτὸν τῷ τε ἐν φυλακῇ
πειρασμῷ καὶ τῷ ἀποκεφαλισμῷ. 15
mt 5, 5 τοῦτον δὲ μακαρισμόν τινες τῶν ἀντιγράφων τρίτον περιέχουσιν,
δεύτερον δὲ τὸν ἑξῆς τούτων κείμενον. 16 mt 5, 6 Ἄλλος δέ φησιν, ὅτι τῆς τοιαύτης
ἐπιθυμίας μισθὸν αὐτῷ τὴν τῶν μελλόντων ἀπόλαυσιν ἐπαγγέλλεται. 17 mt 5, 12
Ἐντεῦθεν αὐτοῖς παρέχεται τὴν παραμυθίαν ἀπὸ τῆς κοινωνίας τῶν πρὸ αὐτῶν
πεπονθότων κακῶς, ὥστε καὶ ἀπ' ἐκείνων εἰδέναι προσ ῆκεν ἡμᾶς, ὅτι τοὺς ἀρετῆς
ἐφιεμένους ἔξω καθεστάναι τῶν λυπηρῶν οὐ δυνατόν. 18 mt 5, 16 Οὐ λέγει·
λαμψάτω τὸ φῶς ὑμῶν ἔμπροσθεν τοῦ θεοῦ· οὐδὲ γὰρ λύχνων φῶς λάμπει
ἔμπροσθεν ἡλίου. 19 mt 5, 15–16 Φῶς ἐστιν ἡ ἐνάρετος πολιτεία. οὐ δεῖ ἐν κρυφῇ
λαλεῖν τὸν εὐαγ γελικὸν λόγον, ἀλλὰ παρρησίᾳ, ἵνα οἱ ἐπὶ γῆς ἄνθρωποι
φωτισθῶσιν. 20 mt 5, 22 Ῥακὰ καλεῖ τὸν κατάπτυστον· οὕτω γὰρ καὶ παρὰ Σύροις ὁ
πτύε λος καλεῖται. 21 mt 5, 35 Ἡ γῆ ὑποπόδιον, ὅτι ὑπὸ τὴν ἐξουσίαν, εἰ καὶ μὴ τῆς
ἁγιό τητος αὐτοῦ δεκτικὴ νῦν διὰ τὰς ἁμαρτίας. τὸ δὲ Ἱεροσόλυμα πόλις ἐστὶ τοῦ
μεγάλου βασιλέως. τοῦτο δὲ πρὸς τὴν ὑπόληψιν τῶν Ἰου δαίων φησίν. 22 Mt 5, 40
Οὐκ εἶπε τῷ θέλοντί σοι λαβεῖν τὸ ἱμάτιον, δὸς καὶ τὸ ἕτερον, ἀλλὰ τῷ θέλοντί σοι
κριθῆναι, δεικνύς, ὅτι τὴν πρὸς τοὺς ἐναν τίους φιλονεικίαν μὴ φεύγειν προσήκει,
2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ὅταν ὑπὲρ τῆς εὐσεβείας ταύτην ἡμῖν ἐπάγωσι, καὶ πάθη καὶ ζημίαν ὑπομένοντας
ἡδέως, ὥστε τὴν εὐσέβειαν μὴ παριδεῖν. τοῦτο δὲ φυλακτέον ἂν πρὸς ἀρετὴν
βλέπειν ἑλώμεθα. 23
mt 6, 11 Ὡς τρέφεται εἰς ζωὴν ὁ ἄνθρωπος, οὕτω καί φασί τινες τὸ ἐπ
ιούσιον παρὰ τὸ ἐπιέναι καὶ ἐπέρχεσθαι, ἵν' ᾖ τοιοῦτον τὸ λεγόμενον· τὸν μὴ τοῦ
ἐνεστῶτος αἰῶνος οἰκεῖον ἄρτον, ἀλλὰ τοῦ μέλλοντος καὶ μετὰ ταῦτα
ἀποδοθησόμενον ὑπό σου τοῖς ἀξίοις λαβεῖν ἤδη δὸς ἡμῖν σήμερον καὶ «καθ'
ἡμέραν». 24 mt 6, 22–23 Τινὲς δὲ τὸ ὁ λύχνος τοῦ σώματός ἐστιν ὀφθαλμὸς περὶ τοῦ
διδασκάλου τῆς ἐκκλησίας εἰρῆσθαι ἔφασαν, ἀλλά μοι δοκοῦσιν οὐκ ἐπεσκέφθαι
πᾶσαν τῶν εἰρημένων τὴν ἀκολουθίαν ἄνωθεν. ἤρξατο γὰρ «μὴ θησαυρίζετε», εἶτα
παρενθεὶς τὸ ὁ λύχνος τοῦ σώματος καὶ τὸ «οὐδεὶς δύναται δυσὶ κυρίοις δουλεύειν»
ἐπήγαγεν· «οὐ δύνασθε θεῷ δουλεύειν καὶ μαμωνᾷ», δι' ὅλου δεικνύων, ὅτι σκοπὸς
ἦν αὐτῷ τοὺς ἀκούοντας ἀποστῆσαι τῆς περὶ τὰ παρόντα σπουδῆς. 25
mt 7, 21 Οὗτος γὰρ τῆς βασιλείας ἀπολαύει τῶν οὐρανῶν, ὅστις ἂν ἐπὶ τῶν
ἔργων δείξῃ τὴν οἰκειότητα ταῦτα διδάσκων τε καὶ ποιῶν, ἃ καὶ παρὼν αὐτὸς
παρέδωκε τοῖς ἀνθρώποις. 26
mt 8, 11–12 Ὥσπερ τότε πρὸς σύγκρισιν τῶν ἀπολλυμένων ὀλίγοι ἦσαν οἱ
σῳζό μενοι, οὕτως νόει μοι τοὺς σῳζομένους πολλοὺς εἶναι πρὸς σύγκρισιν τῶν
ἀπολλυμένων ὀλίγων νῦν. 27
mt 9, 12–13 Εἰ γὰρ ἐπὶ τῶν σωμάτων οὐ μυσαττόμεθα τοὺς πάσχοντας, ἦπού
γε πάντα προσῆκε ποιεῖν ὑπὲρ τῶν κακῶς τὰς ψυχὰς διακειμένων, ὅπως ἂν ταύτας
δυνηθεῖεν ἄριστα ἔχειν. ἐχρῆν δὲ ὑμᾶς, φησίν, ἀπὸ τῆς θείας γραφῆς μαθεῖν, ὡς ὁ
περὶ τοὺς πλησίον οἶκτος πάσης ἐστὶν θυσίας προτιμότερος. τίνας δὲ οἰκτείρειν
δίκαιον ἢ τούτους, οἳ τὰς ψυχὰς οὐ κάλλιστα ἔχοντες, ὅπως ἂν ταύτας βέλτιον
ἔχοιεν, δέονται διορθώσεως. ὥστε κἀμοί, φησίν, τῆς παρουσίας σκοπός, οὐχ ἵνα
δικαίους ὄντας ὠφε λήσω τοὺς ἀνθρώπους (οὐδὲ γὰρ ἂν ἐδέοντο τοιούτου τινὸς
δίκαιοι τὸν τρόπον ὄντες), ἀλλ' ἵνα ἁμαρτίαις τε κακῶς κείμενοι πολλαῖς οἰκείᾳ
χάριτι μεταστήσονται πρὸς τὸ κρεῖττον· οὐ χρείαν γὰρ ἔχουσιν, φησίν, οἱ ἰσχύοντες
ἰατροῦ, ἀλλ' οἱ κακῶς ἔχοντες. 28 mt 9, 37–38 Ὁρᾶτε, φησίν, ὅση μὲν τῶν
πιστευόντων ἡ ἐπίδοσις, ὅπως δὲ οὔκ εἰσιν οἱ τὰ προσήκοντα διδάξοντες αὐτούς, ὡς
ἐντεῦθεν ἀναγκάζεσθαι ἐμοὶ κατακολουθοῦντας τοσοῦτον ὑπομένειν κάματον. 29
mt 9, 37–38 Θεριστὰς δὲ αὐτοὺς καλεῖ δεικνύων, ὅτι καὶ τὰ διὰ τῶν προφητῶν
γεγονότα διὰ τῆς αὐτοῦ δυνάμεως καὶ ἐπιχορηγίας ἐγένετο, καὶ ὅτι προ φητῶν καὶ
ἀποστόλων αὐτὸς κύριος καὶ πάντων ἀνθρώπων ὡς δημιουργός, καὶ ἐπὶ τὸν ἑαυτοῦ
σπόρον, οὐκ ἐπ' ἀλλότριον ἀποστέλλει καὶ πέμπει τοὺς μαθητάς. 31
mt 10, 8–10 ∆ωρεὰν ἐλάβετε, δωρεὰν δότε. «τοὺς κατεσθίοντας τὰς οἰκίας
τῶν χηρῶν» Φαρισαίους ἀποτρέπει μιμήσασθαι, καινὸν τρόπον διδασκα λίας τὸν
καθαρὸν καὶ ἀδωροδόκητον εἰσάγων καὶ τὴν πρὸς αὐτὸν ὑπο δεικνὺς ἐν τῷ
πράγματι μίμησιν· καθὸ τοὺς λαβόντας δωρεὰν παρ' αὐτοῦ τὰ χαρίσματα δωρεὰν
κελεύει καὶ μεταδοῦναι. μήτε οὖν χρυσὸν μήτε ἄργυρον ἢ χαλκὸν τῆς κατὰ τὴν ὁδὸν
ἕνεκεν λαβεῖν ἀνάσχησθε, φησίν, χρείας, ἀλλὰ μηδὲ πήραν ἔχειν ὑμᾶς βούλομαι,
ὥστε καὶ ἀπὸ τοῦ σχήματος δεικνύναι πᾶσιν ὑμᾶς, ὅσον ἀφεστήκατε χρημάτων
ἐπιθυ μίας. 33 mt 10, 34 Αὐτάρκως αὐτοὺς πρὸς τὰς λοιδορίας ὁπλίσας, ἀλλὰ μὴν καὶ
3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

τὰ πάθη φέρειν γενναίως παιδεύσας αὐτούς, ἐπειδὴ ἡ τῆς συγγενείας ὑπελιμπάνετο
διάθεσις σφόδρα χαυνοῦν λογισμὸν ἱκανὴ οὖσα (τίς γὰρ ἂν ἕλοιτο ῥᾳδίως πρὸς τοὺς
μάλιστα οἰκείους σφόδρα ἔχειν ἐχθρῶς;), καλῶς φησι τὸ μὴ νομίσητε· τοσοῦτον γὰρ
ὑμᾶς ἀφεστάναι βούλομαι τοῦ τὴν σωματικὴν αἰδεῖσθαι συγγένειαν, ὅτι ἐκεῖνο
πλέον εἰδέναι προσ ῆκεν ὑμᾶς ὡς τὸ κήρυγμα τὸ ἐμὸν πολλὴν τῶν οἰκείων
ποιήσεται τὴν διαίρεσιν· τοὺς γὰρ τὸ πρὸς τὴν εὐσέβειαν ἅπαξ ἰδεῖν ἑλομένους ἐν
δευ τέρῳ πᾶσαν καὶ τὴν ἐγγύτατα βούλομαι τίθεσθαι συγγένειαν. 36
Mt 12, 30 Πῶς δυνατόν; φησίν, ἐμέ τε συνελθεῖν καὶ τοὺς δαίμονας οὕτως
σαφῶς ἐμοῦ μὲν τἀναντία ἐκείνοις διαπραττομένου, ἐκείνων δὲ πάντων πρὸς
τοὐναντίον ἐπιτηδευόντων ποιεῖν; ἐγὼ μὲν γὰρ καὶ οἱ ἐμοὶ ἅπαντας ἐπὶ τὸν θεὸν
ἀνάγειν πειρῶμεν καὶ τοῦτο βούλεταί μοι τῶν οἰκείων ὁ σύλλογος, ἐκεῖνοι δὲ
ἅπαντας ἀφέλκειν ἐσπουδάκασιν. 37 mt 12, 33 Ἐπειδὴ πονηρός ἐστιν ὁ Βεελζεβούλ,
τὰ δὲ παρ' ἐμοῦ γινόμενα ἀγαθότητος ἔχεται ἀμέτρου, ἀσύμφωνά ἐστι τὰ παρὰ ὑμῶν·
οὔτε γὰρ ὁ πονηρὸς δύναται ἀγαθόν τι ποιεῖν οὔτε ὁ ἀγαθὸς πονηρόν, ὥσπερ οὐδὲ τὰ
δένδρα ἀνομοίους ἑαυτῶν καρπούς. 38 mt 12, 38 Ὡς ἂν τοῦ Μωσέως διὰ θαυμάτων
αὐτοὺς πεπεικότος καὶ οὕτως δεδωκότος τὸν νόμον τοιοῦτόν τι βούλονται καὶ αὐτὸν
ποιεῖν, ἵν' ὡς δῆθεν μεταθησόμενοι τῆς γνώμης, εἰ τοῦτο ποιήσειεν. 39
mt 13, 24–30 Μετὰ τὴν ἐπίδοσιν τοῦ Χριστιανισμοῦ καὶ ὅταν ἤδη κατὰ πάσης
ἄρξηται φαίνεσθαι τῆς οἰκουμένης, ἀναδειχθήσονται αἱρέσεις παρα τρεπόντων
τινῶν τὴν ἀλήθειαν καὶ Χριστιανῶν ἐπαγομένων ὄνομα καὶ ταύτῃ τοὺς πολλοὺς
δελεαζόντων· τοιοῦτον γὰρ καὶ τὸ ζιζάνιον ἀπὸ τοῦ σίτου κατὰ διαφθορὰν
γινόμενον καί τι φέρον τῆς ἀληθείας τοῦ σίτου κατὰ τὴν ὄψιν· τοῦτο λυπήσει,
φησίν, καὶ τὰς ἀοράτους δυνάμεις εὐνοίᾳ τῇ πρὸς ἐμὲ ὡς ἐθελῆσαι καὶ ἀφανίσαι τοὺς
εὑρετὰς ἤτοι τοὺς πειθο μένους αὐτοῖς, ἀλλ' ἐφέξω τοῦτο γενέσθαι δι' αὐτοὺς τοὺς
ἀλλοτρίους τῆς πίστεως, μήπου τι τοῦτο πασχόντων διὰ τὸ τῆς προσηγορίας κοινὸν
οἰηθέντες οἱ ἐναντίοι τοὺς Χριστιανοὺς παρὰ τῶν δαιμόνων κολάζεσθαι τοῦ
τολμήματος ἕνεκεν καὶ ὅτι πάντας ἀφίστασθαι τῶν εἰδώλων διδά σκουσιν,
ἀλλοτριωθῶσι τῆς εὐσεβείας πάντη ὡς λοιπὸν αὐτοῖς μειοῦσθαι τῶν εὐσεβῶν τὸ
συνάθροισμα τῶν πολλῶν φευγόντων προσιέναι τῷ Χριστιανισμῷ ἀγνοίᾳ τῶν
γινομένων. τί δέ, ὅτε προσελθόντες οἱ δοῦλοι διαποροῦσιν, ἀποκρίνεται αὐτοῖς; καὶ
τῶν μὲν λέγει τὴν διά θεσιν, τοῦ δὲ τὴν αἰτίαν τῆς συγχωρήσεως· καὶ γὰρ ὅταν ἐπὶ
τῶν παρα βολῶν ὁμιλίας ἀνατυποῖ, ταῖς μὲν παραβολαῖς τὸ εἰκὸς ἀναδίδωσιν, ἵνα
καὶ δοκῶσιν εἶναι ἐντελεῖς ἐν διηγήματος τάξει γινόμεναι. ἡμᾶς δὲ προσ ῆκεν τὸν
σκοπὸν ἐξετάζειν, ὃν βούλεται διὰ τῆς παραβολῆς σημαίνειν. 40
mt 21, 12 σφοδρότερον δὲ τὸ ἔργον οὐκ ἐν νηπιότητι λόγου γινόμενον, ὁπότε
καὶ τοὺς πιπράσκοντας ἐξέβαλεν καὶ τὰς τραπέζας κατέστρεψεν· καίτοι κατὰ τὸ
προφητικὸν οὐδὲν βίαιον πεποίηκεν κατὰ τὸ «οὐκ ἐρίσει οὐδὲ κραυγάσει» «οὐδὲ
ἀκουσθήσεται ἡ φωνὴ αὐτοῦ». καὶ ὁ Ἰωάννης «φραγέλλιον ἐκ σχοινίων» εἶπεν
αὐτὸν «πεποιηκέναι»· καὶ τοῦτο ση μεῖον ἐπιτιμήσεως καὶ σφοδρότητος. ἐν
συμβόλοις οὖν καὶ τοῦτο ἀρι θμητέον· οὐδὲ γὰρ ἔτυπτέν τινα φραγελλίῳ, ἀλλὰ
σημεῖον ἦν τῆς ἐπι φερομένης μάστιγος τοῖς τὰ τοῦ θεοῦ χραίνουσιν ἀγοραίοις
πράγμασιν. 42
mt 22, 16 Ἡρωδιανούς φασιν εἶναι τοὺς Ἡρώδου στρατιώτας ἢ τοὺς
ἀπαιτητὰς τῶν τελῶν τῶν ἐπιβληθέντων τοῖς Ἰουδαίοις ὑπὸ Ῥωμαίων. 43
4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

mt 23, 25 Πολὺ γὰρ κάλλιον καθαίρει τὸ σκεῦος ἡ δικαιοσύνη παρὰ τὸ ὕδωρ,
ὥσπερ καὶ ἐπ' αὐτῶν τῶν ἀνθρώπων εἶχε. καθαροὶ τὰ σώματα ἦσαν οἱ δίκαιοι παρὰ
τοὺς τοῖς ὕδασι καθαιρομένους, ὥς φησιν ὁ ∆αβίδ· «νίψομαι ἐν ἀθῴοις τὰς χεῖράς
μου», ὥστε σύμφωνα τοῖς τοῦ νόμου καὶ τὰ τοῦ κυρίου. 46
mt 25, 14–18 Ἀποδημίαν καλεῖ τὸν ἀπὸ τῆς εἰς οὐρανοὺς ἀναλήψεως ἐν μέσῳ
ἄχρι τῆς αὐτοῦ παρουσίας καιρόν, ἐν ᾧ ἀπεῖναι ἡμῶν δοκεῖ. τὸ δὲ εὐθέως τὸ
ἀνυπέρθετον δηλοῖ τῆς σπουδῆς, ὅπως δεῖ τὰ τοῦ θεοῦ ἐργάζεσθαι. πέντε δὲ καὶ δύο
τάλαντα καὶ ἓν τῆς λειτουργίας τὴν διαφοράν, ἐπισκόπου οἷον εἰπεῖν, πρεσβυτέρου,
διακόνου· τῇ γὰρ ποσότητι τοῦ ταλάντου τὸ τῆς λειτουργίας πλέον ἢ ἔλαττον
ἐμφαίνει. ὅθεν καλῶς εἶπεν ἑκάστῳ κατὰ τὴν ἰδίαν δύναμιν, ἵνα εἴπῃ· καθὸ ἕκαστος
ἀναλόγως τοῖς ἐγχειρισθεῖσι δύναται τὴν λειτουργίαν ἐκπληροῦν συνέβαινε·
βούλεται γὰρ δεῖξαι, ὅτι καὶ τὴν προθυμίαν παρὰ πάντων ἀπαιτεῖ καὶ ἀμείβεται
ὁμοίως. 47 mt 25, 20–25 ∆ιπλασιασμὸν μὲν τῶν δοθέντων καλεῖ τὸ ἐπιδείξασθαι
μετὰ σπουδῆς τὰ προσήκοντα καὶ ἄλλοις γενέσθαι ὠφελείας αἴτιον, ἀνθ' ὧν
μεγάλων αὐτὸν τεύξεσθαί φησιν μισθῶν εἰκότως· τοῦτο γάρ ἐστι τὸ ἐπὶ πολλῶν σε
καταστήσω. τὸ δὲ κατακρύψαι εἶπεν τὸ μηδὲν ἐπιδείξασθαι τῶν δεόντων, ἀλλ'
ἀφανῆ τὴν δοθεῖσαν αὐτῷ χάριν ἐργάσασθαι. 48
mt 26, 29 Βασιλείαν τοῦ πατρὸς ὠνόμασεν τὴν ἀνάστασιν, ἀφ' ἧς αὐτῷ τε ἡ
βασιλεία προσεγίνετο καὶ τοῖς λοιποῖς τῶν ἀνθρώπων ἡ ταύτης κοινωνία. τί δέ ἐστιν
καινὸν ἀντὶ τοῦ καινῶς–καὶ ἄφθαρτον καὶ μὴ δεόμενον τροφῆς. 51 mt 26, 37 Οὐδὲ
ἐκεῖνο δεῖ θαυμάζειν, ὅπως ἀγωνιῶν ὁ κύριος κατά γε τὸ ἀνθρώπινον φαίνεται
καίτοι τῶν μαθητῶν χαιρόντων ἐφ' οἷς ἔπασχον πολλάκις. ὥσπερ δυνάμενος ἑαυτῷ
ἐξελέσθαι τὸν θάνατον παθεῖν συνε χώρησεν ὑπὲρ τῆς τοῦ κόσμου σωτηρίας, οὕτως
καὶ κατὰ τὴν ἀνθρωπίνην φύσιν ἀγωνιάσαι συνεχώρησεν πείθων αὐτοὺς μὴ δόκησίν
τινα τὸ πάθος νομίζειν, ἀλλ' ἀλήθειαν καὶ μάλιστα τοῦ συντόμου τῆς ἀναστάσεως
εὐκόλως αὐτοὺς εἰς ταύτην καταγαγεῖν δυναμένου τὴν ὑπόνοιαν.

5
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

