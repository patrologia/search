Ad eos qui in Euphratesia et Osrhoena regione, Syria, Phoeni
ΡΝΑʹ. – Θεοδωρήτου πρὸς τοὺς ἐν τῇ Εὐ φρατησίᾳ, καὶ Ὀσροηνῇ, καὶ Συρίᾳ,
καὶ Φοι νίκῃ καὶ Κιλικίᾳ μονάζοντας.
Ὁρῶν τὴν ἐν τῷ παρόντι καιρῷ τῆς Ἐκκλησίας κατάστασιν καὶ τὴν ἔναγχος
ἐπαναστᾶσαν τῇ ἱερᾷ νηῒ ζάλην, καὶ τὰς σφοδρὰς καταιγίδας καὶ τῶν κυμά των τὴν
προσβολὴν καὶ τὴν βαθεῖαν σκοτομήνην, καὶ πρὸς τούτοις τῶν πλωτήρων τὴν ἔριν, καὶ
τῶν ἐρέτ τειν λαχόντων τὴν μάχην, καὶ τὴν τῶν κυβερνητῶν μέθην, καὶ ἁπαξαπλῶς
τὴν τῶν κακῶν ἀωρίαν, τῶν Ἰερεμίου θρήνων ἀναμιμνήσκομαι, καὶ μετ' ἐκείνου βοῶ·»
Τὴν κοιλίαν μου, τὴν κοιλίαν μου ἐγὼ ἀλγῶ, καὶ τὰ αἰσθητήρια τῆς καρδίας μου
μαιμάσσει ἡ ψυχή μου, σπαράσσεται καὶ ἡ καρδία μου,» καὶ πηγὰς δακρύων ἐπιζητῶ, ἵνα
ταῖς λιβάσι τῶν ὀφθαλ μῶν, τὸ πολὺ τῆς ἀθυμίας ἀποσκευάσωμαι νέ φος. ∆έον γὰρ ἐν
οὕτως ἀγρίῳ χειμῶνι καὶ τοὺς κυ βερνήτας ἐγρηγορέναι, καὶ τῷ κλύδωνι μάχεσθαι, καὶ
τῆς τοῦ σκάφους σωτηρίας φροντίζειν καὶ τοὺς ναύτας τῆς κατ' ἀλλήλων ἔριδος
ἀποστάντας εὐχῇ καὶ τέχνῃ τὰ δεινὰ διαλύειν· καὶ τοὺς πλωτῆρας ἡσυχῆ καθῆσθαι· καὶ
μήτε ἀλλήλοις μήτε τοῖς κυβερνήταις ζυγομαχεῖν· τὸν δὲ τῆς θαλάττης ἱκετεύειν
∆εσπότην, ἵνα νεύματι μεταβάλῃ τὰ σκυθρωπά. Τούτων μὲν οὐδεὶς οὐδὲν ἐθέλει ποιεῖν·
ὡς ἐν νυκτομαχίᾳ δὲ ἀλ λήλους ἀγνοήσαντες, καὶ τοὺς ἐναντίους καταλιπόν τες, καθ'
ἡμῶν αὐτῶν πάντα δαπανῶμεν τὰ βέλη, καὶ τοὺς ὁμοφύλους ὡς πολεμίους
τιτρώσκομεν, οἱ δὲ πλησίον ἑστῶτες γελῶσιν ἡμῶν τὴν μέθην, καὶ τοῖς ἡμετέροις
ἐπεντρυφῶσι κακοῖς, καὶ χαίρουσιν 83.1417 ὑπ' ἀλλήλων ἡμᾶς ὁρῶντες δαπανωμένους·
αἴτιοι δὲ τούτων οἱ τὴν ἀποστολικὴν διαφθεῖραι πίστιν φιλονει κήσαντες καὶ τοῖς
εὐαγγελικοῖς δόγμασιν ἀλλόκοτον διδασκαλίαν ἐπιθεῖναι τολμήσαντες· καὶ τὰ δυσσεβῆ
κεφάλαια, ἃ μετὰ ἀναθεματισμῶν εἰς τὴν βασιλίδα πόλιν ἐξέπεμψαν, δεξάμενοι, καὶ
ταῖς οἰκείαις ὑπο γραφαῖς, ὡς ᾠήθησαν, βεβαιώσαντες, ἃ σαφῶς ἐκ τῆς πικρᾶς
Ἀπολιναρίου βεβλάστηκε ῥίζης. Μετ έχει δὲ καὶ τῆς Ἀρείου καὶ Εὐνομίου δυσσεβείας· εἰ
δέ τις ἀκριβῶς κατιδεῖν ἐθελήσειεν, οὐδὲ τῆς Οὐαλεντίνου καὶ Μαρκίωνος ἄμοιρα
δυσσεβείας τυγ χάνει. Ἐν μὲν γὰρ τῷ πρώτῳ κεφαλαίῳ τὴν ὑπὲρ ἡμῶν γεγενημένην
οἰκονομίαν ἐκβάλλει· οὐκ ἀνειληφέναι τὸν Θεὸν Λόγον φύσιν ἀνθρωπείαν, ἀλλ' αὐτὸν
εἰς σάρκα μεταβληθῆναι διδάσκων· καὶ δοκήσει καὶ φαν τασίᾳ τὴν τοῦ Σωτῆρος ἡμῶν
ἐνανθρώπησιν, ἀλλ' οὐκ ἀληθείᾳ γεγενῆσθαι δογματίζων.
Ταῦτα δὲ τῆς Μαρκίωνος καὶ τοῦ Μάνεντος καὶ Οὐαλεντίνου δυσσεβείας
ὑπάρχει γεννήματα. Ἐν δὲ τῷ δευτέρῳ καὶ τρίτῳ κεφαλαίῳ, ὥσπερ ἐπιλαθόμενος ὧν ἐν
προοιμίοις ἐξ έθετο, τὴν καθ' ὑπόστασιν ἕνωσιν εἰσάγει, καὶ σύν οδον καθ' ἕνωσιν
φυσικὴν, κρᾶσίν τινα καὶ σύγχυσιν διὰ τούτων τῶν ὀνομάτων γεγενῆσθαι διδάσκων τῆς
τε θείας φύσεως καὶ τῆς τοῦ δούλου μορφῆς. Τοῦτο τῆς αἱρετικῆς Ἀπολιναρίου
καινοτομίας ἐστὶ κύημα. Ἐν δὲ τῷ τετάρτῳ κεφαλαίῳ ἀπαγορεύει τῶν εὐαγγελικῶν καὶ
ἀποστολικῶν φωνῶν τὴν διαίρεσιν, καὶ οὐκ ἐᾷ κατὰ τὰς τῶν ὀρθοδόξων Πατέρων διδα
σκαλίας τὰς μὲν θεοπρεπεῖς φωνὰς περὶ τῆς θείας ἐκλαμβάνεσθαι φύσεως τὰς δὲ
ταπεινὰς καὶ ἀνθρω πίνως εἰρημένας τῇ ἀναληφθείσῃ προσάπτειν ἀνθρω πότητι· καὶ
ἐντεῦθεν τοίνυν ἔστιν εὑρεῖν τοὺς εὐφρο νοῦντας τὴν τῆς ἀσεβείας συγγένειαν. Ἄρειος
γὰρ καὶ Εὐνόμιος, κτίσμα καὶ ἐξ οὐκ ὄντων, καὶ δοῦλον τὸν μονογενῆ Υἱὸν τοῦ Θεοῦ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

εἶναι φάσκοντες, τὰ τα πεινῶς ὑπὸ τοῦ ∆εσπότου Χριστοῦ καὶ ἀνθρωπίνως εἰρημένα τῇ
θεότητι αὐτοῦ προσάψαι τετολμήκασι· τὸ ἑτεροούσιον ἐντεῦθεν καὶ τὸ ἀνόμοιον
κατασκευάζον τες. Πρὸς τούτοις, ἵνα συνελὼν εἴπω, αὐτὴν τὴν ἀπαθῆ καὶ ἄτρεπτον τοῦ
Χριστοῦ θεότητα καὶ πα θεῖν, καὶ σταυρωθῆναι καὶ ἀποθανεῖν καὶ ταφῆναι δι αγορεύει.
Τοῦτο δὲ καὶ τῆς Ἀρείου καὶ Εὐνομίου μανίας ἐπέκεινα· οὐδὲ γὰρ οἱ κτίσμα τολμῶντες
ἀπο καλεῖν τὸν Ποιητὴν τῶν ὅλων καὶ ∆ημιουργὸν, εἰς ταύτην ἐξώκειλαν τὴν ἀσέβειαν.
Βλασφημεῖ δὲ καὶ εἰς τὸ ἅγιον Πνεῦμα· οὐκ ἐκ τοῦ Πατρὸς αὐτὸ λέγων ἐκπορεύεσθαι,
κατὰ τὴν τοῦ Κυρίου φωνὴν, ἀλλ' ἐξ Υἱοῦ τὴν ὕπαρξιν ἔχειν. Καὶ οὗτος δὲ τῶν
Ἀπολιναρίου σπερμάτων ὁ καρπός· γειτνιάζει δὲ καὶ τῇ Μα κεδονίου πονηρᾷ γεωργίᾳ.
Τοιαῦτα τοῦ Αἰγυπτίου τὰ κυήματα, πονηροῦ πατρὸς ἀληθῶς ἔγγονα πονηρότε ρα.
Ταῦτα δὲ δέον ἢ ἀμβλωθρίδια ποιῆσαι κυοφο ρούμενα, ἢ εὐθὺς τεχθέντα διαφθεῖραι
τοὺς τῶν ψυ χῶν τὴν ἰατρείαν ἐγκεχειρισμένους, ὡς ὀλέθρια καὶ τῆς ἡμετέρας φύσεως
δηλητήρια, ἐκτρέφουσιν οἱ γεν νάδαι, καὶ πολλῆς ἀξιοῦσι σπουδῆς ἐπ' ὀλέθρῳ σφῶν
αὐτῶν καὶ τῶν τὰς ἀκοὰς αὐτοῖς ὑπέχειν ἀνεχομένων.
Ἡμεῖς δὲ τὸν πατρῷον κλῆρον ἄσυλον φυλάττειν σπουδάζομεν, καὶ ἣν
παρελάβομεν πίστιν μεθ' ἧς καὶ ἐβαπτίσθημεν καὶ βαπτίζομεν, ἀνέπαφον καὶ ἀκήρατον
διατηροῦμεν· καὶ ὁμολογοῦμεν τὸν Κύριον ἡμῶν Ἰησοῦν Χριστὸν, Θεὸν τέλειον καὶ
ἄνθρωπον τέλειον, ἐκ ψυχῆς λογικῆς καὶ σώματος πρὸ αἰώνων μὲν ἐκ τοῦ Πατρὸς
γεννηθέντα κατὰ τὴν θεότητα· ἐπ' ἐσχάτων δὲ τῶν ἡμερῶν δι' ἡμᾶς καὶ διὰ τὴν
ἡμετέραν σωτηρίαν ἐκ Μαρίας τῆς Παρθένου· τὸν αὐτὸν ὁμοούσιον τῷ Πατρὶ κατὰ τὴν
θεότητα, καὶ ὁμοούσιον ἡμῖν κατὰ τὴν ἀνθρωπότητα ∆ύο γὰρ φύσεων ἕνωσις γέγονε.
∆ιὸ ἕνα Χριστὸν, ἕνα Υἱὸν, ἕνα Κύριον ὁμολογοῦμεν· οὔτε γὰρ τὴν ἕνωσιν λύομεν, καὶ
ἀσύγχυτον αὐτὴν γεγενῆσθαι πιστεύο μεν, τῷ Κυρίῳ πειθόμενοι λέγοντι τοῖς Ἰουδαίοις·
«Λύσατε τὸν ναὸν τοῦτον, καὶ ἐν τρισὶν ἡμέραις ἐγερῶ αὐτόν.» Εἰ δὲ κρᾶσις ἐγεγόνει καὶ
σύγχυσις, καὶ μία φύσις ἐξ ἀμφοῖν ἀπετελέσθη, ἐχρῆν εἰπεῖν· Λύσατέ με, καὶ ἐν τρισὶν
ἡμέραις ἐγερθήσομαι. Νῦν δὲ δεικνὺς ὡς ἄλλο μὲν ὁ Θεὸς κατὰ τὴν φύσιν. ἄλλο δὲ ὁ
ναὸς, εἷς δὲ Χριστὸς ἀμφότερα, «Λύσατε, φησὶ, τὸν ναὸν τοῦτον, καὶ ἐν τρισὶν ἡμέραις
ἐγερῶ αὐτὸν,» σαφῶς διδάσκων, ὡς ὅτι οὐχ ὁ Θεὸς ἦν ὁ λυόμενος, ἀλλ' ὁ ναός. Καὶ τοῦ
μὲν ἡ φύσις τὴν λύσιν ὑπεδέχετο, τοῦ δὲ ἡ δύναμις ἤγειρε τὸ λυόμε νον. Θεὸν δὲ καὶ
ἄνθρωπον τὸν Χριστὸν ὁμολογοῦμεν ταῖς θείαις ἀκολουθοῦντες Γραφαῖς. Ὅτι μὲν γὰρ
Θεὸς Κύριος ἡμῶν Ἰησοῦς Χριστὸς, ὁ μακάριος Ἰωάννης ὁ εὐαγγελιστὴς βοᾷ· «Ἐν ἀρχῇ
ἦν ὁ Λόγος, καὶ ὁ Λόγος ἦν πρὸς τὸν Θεὸν, καὶ Θεὸς ἦν ὁ Λόγος· οὗτος ἦν ἐν ἀρχῇ πρὸς
τὸν Θεόν· πάντα δι' αὐτοῦ ἐγένετο, καὶ χωρὶς αὐτοῦ ἐγένετο οὐδὲ ἓν ὃ γέγονε.» Καὶ
πάλιν· «Ἦν τὸ φῶς τὸ ἀληθινὸν, ὃ φωτίζει πάντα ἄνθρωπον ἐρχόμενον εἰς τὸν
κόσμον.» Καὶ αὐτὸς δὲ ὁ Κύριος διαῤῥήδην διδάσκει λέ γων· «Ὁ ἑωρακὼς ἐμὲ, ἑώρακε
τὸν Πατέρα μου.» Καί· «Ἐγὼ καὶ ὁ Πατὴρ ἕν ἐσμεν· κἀγὼ ἐν τῷ Πατρὶ, καὶ ὁ Πατὴρ ἐν
ἐμοί.» Καὶ ὁ μακάριος Παῦλος ἐν μὲν τῇ πρὸς Ἑβραίους φησίν· «Ὃς ὢνἀπαύγασμα τῆς
δόξης καὶ χαρακτὴρ τῆς ὑποστάσεως αὐτοῦ, φέρων τε τὰ πάντα τῷ ῥήματι τῆς δυ
νάμεως αὑτοῦ.» Ἐν δὲ τῇ πρὸς Φιλιππησίους· «Τοῦτο φρονείσθω ἐν ὑμῖν, φησὶν, ὃ καὶ
ἐν Χριστῷ Ἰησοῦ, ὃς ἐν μορφῇ Θεοῦ ὑπάρχων, οὐχ ἁρπαγὴν ἡγήσατο τὸ εἶναι ἴσα Θεῷ·
ἀλλ' ἑαυτὸν ἐκένωσε μορφὴν δούλου λαβών.» Ἐν δὲ τῇ πρὸς Ῥωμαίους· «Ὧν οἱ πατέρες
καὶ ἐξ ὧν ὁ Χριστὸς τὸ κατὰ σάρκα, ὁ ὢν ἐπὶ πάντων Θεὸς εὐλογητὸς εἰς τοὺς αἰῶνας.
Ἀμήν.»
Ἐν δὲ τῇ πρὸς Τίτον· «Προσδεχόμενοι τὴν μακαρίαν ἐλπίδα καὶ ἐπιφάνειαν τῆς δόξης
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

τοῦ μεγάλου Θεοῦ καὶ Σωτῆρος ἡμῶν Ἰησοῦ Χριστοῦ.»
Καὶ Ἡσαΐας δὲ βοᾷ, ὅτι «Παιδίον ἐγεννήθη ἡμῖν, υἱὸς καὶ ἐδόθη ἡμῖν, οὗ ἡ ἀρχὴ
ἐπὶ τοῦ 83.1421 ὤμου αὐτοῦ, καὶ καλεῖται τὸ ὄνομα αὐτοῦ μεγάλης βουλῆς Ἄγγελος,
θαυμαστὸς, σύμβουλος, Θεὸς ἰσχυ ρὸς, ἐξουσιαστὴς, ἄρχων εἰρήνης, Πατὴρ τοῦ μέλλον
τος αἰῶνος.» Καὶ πάλιν· «Ὀπίσω σου, φησὶν, ἀκο λουθήσουσι δεδεμένοι χειροπέδαις, καὶ
ἐν σοὶ προσ εύξονται, ὅτι ἐν σοὶ Θεός ἐστι, καὶ οὐκ ἔστι Θεὸς πλὴν σοῦ. Σὺ γὰρ εἶ Θεὸς,
καὶ οὐκ ᾔδεισαν, Θεὸς τοῦ Ἰσραὴλ Σωτήρ. Καὶ τὸ Ἐμμανουὴλ δὲ ὄνομα, Θεοῦ καὶ
ἀνθρώπου τυγχάνει σημαντικὸν, ἑρμηνεύε ται γὰρ, κατὰ τὴν τοῦ Εὐαγγελίου
διδασκαλίαν, Μεθ' ἡμῶν ὁ Θεὸς, τουτέστιν, ἐν ἀνθρώπῳ Θεὸς, ἐν τῇ ἡμετέρᾳ φύσει
Θεός. Καὶ ὁ θεῖος δὲ Ἱερεμίας προ θεσπίζει λέγων· «Οὗτος ὁ Θεὸς ἡμῶν, οὐ λογισθή
σεται ἕτερος πρὸς αὐτόν· ἐξεῦρε πᾶσαν ὁδὸν ἐπιστή μης, καὶ ἔδωκεν αὐτὴν Ἰακὼβ τῷ
παιδὶ αὑτοῦ, καὶ Ἰσραὴλ τῷ ἠγαπημένῳ ὑπ' αὐτοῦ. Μετὰ ταῦτα ἐπὶ τῆς γῆς ὤφθη, καὶ
τοῖς ἀνθρώποις συνανεστράφη.» Καὶ ἄλλας δ' ἄν τις μυρίας εὕροι φωνὰς ἔκ τε τῶν
θείων Εὐαγγελίων καὶ τῶν ἀποστολικῶν συγγραμ μάτων, καὶ ἐκ τῶν προφητικῶν
θεσπισμάτων, δει κνυούσας, ὅτι Θεὸς ἀληθινὸς ὁ Κύριος ἡμῶν Ἰησοῦς Χριστός.
Ὅτι δὲ καὶ ἄνθρωπος μετὰ τὴν ἐνανθρώπησιν προσαγορεύεται, διδάσκει μὲν
αὐτὸς ὁ Κύριος, Ἰου δαίοις διαλεγόμενος καὶ βοῶν· «Τί με ζητεῖτε ἀπο κτεῖναι,
ἄνθρωπον, ὅστις τὴν ἀλήθειαν ὑμῖν λελά ληκα;» καὶ ὁ μακάριος Παῦλος ἐν τῇ πρὸς
Κοριν θίους προτέρᾳ λέγων· «Ἐπειδὴ γὰρ δι' ἀνθρώπου ὁ θάνατος, καὶ δι' ἀνθρώπου
ἀνάστασις νεκρῶν.» Καὶ δεικνὺς περὶ τίνος λέγει, ἑρμηνεύει τὸ εἰρημένον, οὑτωσὶ
λέγων· «Ὥσπερ γὰρ ἐν τῷ Ἀδὰμ ἀποθνήσκουσιν, οὕτω καὶ ἐν τῷ Χριστῷ πάντες
ζωοποιη θήσονται.» Καὶ Τιμοθέῳ δὲ γράφων ὁμοίως φησίν· «Εἷς Θεὸς, εἷς καὶ μεσίτης
Θεοῦ καὶ ἀνθρώπων, ἄνθρωπος Χριστὸς Ἰησοῦς.» Καὶ ἐν ταῖς Πράξεσιν, ἐν Ἀθήναις
δημηγορῶν· «Τοὺς μὲν οὖν χρόνους τῆς ἀγνοίας ὑπεριδὼν ὁ Θεὸς, φησὶ, τὰ νῦν
παραγγέλλει πᾶσι πανταχοῦ μετανοεῖν· καθότι ἔστησεν ἡμέραν, ἐν ᾗ μέλλει κρίνειν τὴν
οἰκουμένην ἐν δικαιοσύνῃ, ἐν ἀνδρὶ ᾧ ὥρισε, πίστιν παρασχὼν πᾶσιν, ἀναστή σας αὐτὸν
ἐκ νεκρῶν.» Καὶ ὁ μακάριος Πέτρος Ἰουδαίοις διαλεγόμενος, «Ἄνδρες, φησὶν,
Ἰσραηλῖται, ἀκούσατε τοὺς λόγους τούτους· Ἰησοῦν τὸν Να ζωραῖον, ἄνδρα ἀπὸ τοῦ
Θεοῦ ἀποδεδειγμένον εἰς ὑμᾶς σημείοις καὶ τέρασι, καὶ δυνάμεσιν οἷς, ἐποίη σεν ὁ Θεὸς
δι' αὐτοῦ.» Καὶ ὁ προφήτης δὲ Ἡσαΐας τοῦ ∆εσπότου Χριστοῦ τὰ πάθη προαγορεύων, ὃν
πρὸ βραχέων ὠνόμασε Θεὸν, τοῦτον ἄνθρωπον ἀπο καλεῖ λέγων· «Ἄνθρωπος ἐν πληγῇ
ὢν, καὶ εἰδὼς φέρειν μαλακίαν· οὗτος τὰς ἀνομίας ἡμῶν φέρει, καὶ ὑπὲρ ἡμῶν
ὀδυνᾶται.»
Καὶ ἄλλας δ' ἂν ὁμοφώνους μαρτυρίας συλλέξας ἐκ τῆς ἁγίας Γρα φῆς, ἐνέθηκα
ἂν τῇ ἐπιστολῇ, εἰ μὴ τὴν ὑμετέραν θεοσέβειαν ἠπιστάμην βίον ἔχειν τὴν τῶν θείων λο
83.1424 γίων μελέτην, κατὰ τὸν ἐν Ψαλμοῖς μακαριζόμενον ἄνθρωπον. Τῇ ὑμετέρᾳ
τοίνυν φιλοπονίᾳ καταλιπὼν τὴν τῶν μαρτυριῶν συλλογὴν, ἐπὶ τὰ προκείμενα
βαδιοῦμαι. Θεὸν τοίνυν ἀληθινὸν καὶ ἄνθρωπον ἀληθινὸν τὸν Κύριον ἡμῶν Ἰησοῦν
Χριστὸν ὁμολογοῦμεν, οὐκ εἰς δύο πρόσωπα διαιροῦντες τὸν ἕνα, ἀλλὰ δύο φύσεις
ἀσυγχύτως ἡνῶσθαι πιστεύομεν. Οὕτω γὰρ καὶ τὴν πολυσχεδῆ τῶν αἱρετικῶν
βλασφημίαν ῥᾳδίως διελέγξαι δυνησόμεθα· πολλὴ καὶ ποικίλη τῶν ἐπαναστάντων τῇ
ἀληθείᾳ ἡ πλάνη, ὡς αὐτίκα δη λώσομεν. Μαρκίων μὲν γὰρ καὶ Μάνης οὔτε ἀν
ειληφέναι ἀνθρωπείαν φύσιν τὸν Θεὸν Λόγον φασὶν, οὔτε ἐκ Παρθένου τὸν Κύριον
ἡμῶν Ἰησοῦν Χριστὸν γεγεννῆσθαι πεπιστεύκασιν· ἀλλ' αὐτὸν τὸν Θεὸν Λόγον
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

σχηματισθῆναι εἰς εἶδος ἀνθρώπειον καὶ φα νῆναι ὡς ἄνθρωπον, φαντασίᾳ μᾶλλον ἢ
ἀληθείᾳ χρησάμενον. Βαλεντῖνος δὲ καὶ Βαρδισάνης τὴν μὲν γέννησιν δέχονται· τὴν δὲ
ἀνάληψιν ἀρνοῦνται τῆς ἡμετέρας φύσεως, οἷόν τινι σωλῆνι χρήσασθαι τῇ Παρθένῳ
λέγοντες τὸν Υἱὸν τοῦ Θεοῦ. Σαβέλλιος δὲ ὁ Λίβυς, καὶ Φωτεινὸς, καὶ Μάρκελλος ὁ
Γαλάτης, καὶ Παῦλος ὁ Σαμοσατεὺς, ἄνθρωπον ψιλὸν ἐκ τῆς Παρ θένου γεννηθῆναι
λέγουσι· τὸ δὲ καὶ Θεὸν εἶναι τὸν προαιώνιον Χριστὸν, διαῤῥήδην ἀρνοῦνται· Ἄρειος δὲ
καὶ Εὐνόμιος σῶμα μόνον ἀνειληφέναι ἐκ τῆς Παρθένου τὸν Θεὸν Λόγον φασίν·
Ἀπολινάριος δὲ τῷ σώματι προστίθησι καὶ ψυχὴν ἄλογον, ὡς τῆς ἐνανθρωπήσεως τοῦ
Θεοῦ Λόγου ὑπὲρ ἀλόγων, οὐχ ὑπὲρ λογικῶν γεγενημένης· ἡ δὲ τῶν ἀποστόλων
διδασκαλία τέλειον ἄνθρωπον ὑπὸ τελείου Θεοῦ ἀνει λῆφθαι διδάσκει· τὸ γὰρ, «Ὃς ἐν
μορφῇ Θεοῦ ὑπάρ χων μορφὴν δούλου ἔλαβε,» τοῦτο δηλοῖ· ἀντὶ φύσεως γὰρ καὶ
οὐσίας ἡ μορφὴ πρόκειται· δηλοῖ γὰρ ὅτι φύσιν ἔχων Θεοῦ, φύσιν ἔλαβε δούλου. ∆ιὸ
τοῖς μὲν πρώτοις τῆς ἀσεβείας εὑρεταῖς Μαρκίωνι καὶ τῷ Μάνεντι καὶ Βαλεντίνῳ
διαλεγόμενοι ἀποδεικνύ ναι σπουδάζομεν ἐκ τῶν θείων Γραφῶν, ὅτι οὐ μό νον Θεὸς,
ἀλλὰ καὶ ἄνθρωπος ὁ ∆εσπότης Χριστός. Ἀρείου δὲ καὶ Εὐνομίου καὶ Ἀπολιναρίου τὸ
περὶ τὴν οἰκονομίαν ἀτελὲς δῆλον ποιοῦντες τοῖς ἀγνοοῦσι, τελείαν εἶναι τὴν
ληφθεῖσαν ἀποφαινόμεθα φύσιν ἐκ τῶν θείων λογίων τοῦ Πνεύματος· Σαβελλίου δὲ καὶ
Φωτεινοῦ καὶ Μαρκέλλου καὶ Παύλου τὴν ἀσέβειαν ἐλέγχομεν, μάρτυρι τῇ θείᾳ Γραφῇ
κεχρημένοι καὶ δεικνύντες, ὡς οὐκ ἄνθρωπος μόνον, ἀλλὰ καὶ Θεὸς προαιώνιος καὶ τῷ
Πατρὶ ὁμοούσιος ὁ ∆εσπότης Χρι στός. Ὅτι γὰρ ψυχὴν ἀνέλαβε λογικὴν, αὐτὸς ὁ Κύριος
διδάσκει λέγων· «Νῦν ἡ ψυχή μου τετάρακται, καὶ τί εἴπω; Πάτερ, σῶσόν με ἐκ τῆς ὥρας
ταύτης· ἀλλὰ διὰ τοῦτο ἦλθον εἰς τὴν ὥραν ταύτην.» Καὶ πάλιν· «Περίλυπός ἐστιν ἡ
ψυχή μου ἕως θανάτου.» Καὶ ἑτέρωθι· «Ἐξουσίαν ἔχω θεῖναι τὴν ψυχήν μου, καὶ
ἐξουσίαν ἔχω πάλιν λαβεῖν αὐτήν· οὐδεὶς αἴρει αὐτὴν ἀπ' ἐμοῦ.» Καὶ ὁ ἄγγελος πρὸς τὸν
83.1425 Ἰωσήφ· «Παράλαβε τὸ παιδίον καὶ τὴν μητέρα αὐτοῦ, καὶ πορεύου εἰς γῆν
Ἰσραήλ· τεθνήκασι γὰρ πάντες οἱ ζητοῦντες τὴν ψυχὴν τοῦ παιδίου.» Καὶ ὁ
εὐαγγελιστής· «Ἰησοῦς δὲ προέκοπτεν ἡλικίᾳ, καὶ σοφίᾳ, καὶ χάριτι παρὰ Θεῷ καὶ
ἀνθρώποις.» Προ κόπτει δὲ ἡλικίᾳ καὶ σοφίᾳ οὐ θεότης ἡ ἀεὶ τελεία, ἀλλ' ἡ ἀνθρωπεία
φύσις, ἡ χρόνῳ καὶ γινομένη, καὶ αὐξομένη καὶ τελειουμένη.
Οὗ χάριν τὰ μὲν ἀνθρώπινα πάντα τοῦ ∆εσπότου Χριστοῦ, πεῖνάν φημι καὶ
δίψαν καὶ κόπον, καὶ ὕπνον, καὶ δειλίαν, καὶ ἱδρῶτας, καὶ προσευχὴν, καὶ ἄγνοιαν, καὶ
ὅσα τοιαῦτα τῆς ἡμετέρας ἀπαρχῆς εἶναί φαμεν, ἣν ἀναλαβὼν ὁ Θεὸς Λόγος ἥνωσεν
ἑαυτῷ, τὴν ἡμετέ ραν πραγματευόμενος σωτηρίαν. Τὸν δὲ τῶν χωλῶν δρόμον, καὶ τῶν
νεκρῶν τὴν ἀνάστασιν, καὶ τὰς τῶν ἄρτων πηγὰς, καὶ τὴν τοῦ ὕδατος εἰς οἶνον μεταβο
λὴν, καὶ πάσας τὰς ἄλλας θαυματουργίας τῆς θείας εἶναι δυνάμεως ἔργα πιστεύομεν. Ὡς
τὸν αὐτόν φημι δὴ τὸν ∆εσπότην Χριστὸν, καὶ πάσχειν, καὶ πάθη λύειν· πάσχειν μὲν
κατὰ τὸ ὁρώμενον, λύειν δὲ τὰ πάθη κατὰ τὴν ἀῤῥήτως οἰκοῦσαν θεότητα. ∆ηλοῖ δὲ
τοῦτο σαφῶς καὶ τῶν ἱερῶν Εὐαγγελίων ἡ ἱστορία. Καὶ μανθάνομεν ἐκεῖθεν, ὡς ἐν
φάτνῃ κείμενος καὶ σπάρ γανα περιβεβλημένος, ὑπὸ ἀστέρος ἐκηρύττετο, καὶ ὑπὸ
μάγων προσεκυνεῖτο, καὶ ὑπὸ ἀγγέλων ὑμνεῖτο, καὶ διακρίνομεν εὐσεβῶς, ὅτι τὰ ῥάκη,
καὶ σπάργανα, καὶ τῆς κλίνης ἡ ἀπορία, καὶ ἡ πολλὴ εὐτέλεια, τῆς ἀνθρωπότητος ἴδια.
Ὁ δὲ τῶν Μάγων δρόμος καὶ τοῦ ἀστέρος ἡ ποδηγία καὶ ἡ τῶν ἀγγέλων χορεία κηρύττει
τὴν τοῦ κρυπτομένου θεότητα. Οὕτως ἀποδιδράσκει μὲν εἰς Αἴγυπτον, καὶ τῇ φυγῇ τῆς
Ἡρώδου μανίας ἀπαλλάττεται· καὶ γὰρ ἄνθρωπος ἦν. Συσσείει δὲ, κατὰ τὸν Προφήτην,
τὰ χειροποίητα Αἰγύπτου· Θεὸς γὰρ ὑπῆρχε. Περιτέμνεται καὶ φυλάττει τὸν νόμον καὶ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

καθαρσίους προσφέρει θυσίας· ἐκ γὰρ τῆς Ἰεσ σαὶ βεβλάστηκε ῥίζης. Καὶ ὑπὸ νόμον ὡς
ἄν θρωπος ἦν· καὶ ἔλυσε τὸν νόμον μετὰ ταῦτα, καὶ δέδωκε τὴν καινὴν διαθήκην·
νομοθέτης γὰρ ἦν, καὶ ταύτην αὐτὸς δώσειν διὰ τῶν προφητῶν ἐπηγγείλατο.
Ἐβαπτίσθη ὑπὸ Ἰωάννου· τοῦτο δείκνυσι τὸ ἡμέτερον. Μαρτυρεῖται ἄνωθεν ὑπὸ τοῦ
Πατρὸς, καὶ ὑπὸ τοῦ Πνεύματος δείκνυται· τοῦτο κηρύττει τὸν προαιώ νιον. Ἐπείνησεν,
ἀλλὰ καὶ πολλὰς χιλιάδας ἐκ πέντε ἄρτων ἐκόρεσε· τοῦτο θεῖον, ἐκεῖνο ἀνθρώπι νον.
Ἐδίψησε καὶ ᾔτησεν ὕδωρ, ἀλλὰ πηγὴ ἦν ζωῆς· καὶ τὸ μὲν ἦν τῆς ἀνθρωπίνης
ἀσθενείας, τὸ δὲ τῆς θείας δυνάμεως. Ἐκαθεύδησεν ἐν τῷ πλοίῳ, ἀλλὰ καὶ τῆς θαλάττης
τὴν ζάλην ἐκοίμησε· τοῦτο τῆς παθητῆς φύσεως, ἐκεῖνο τῆς ποιητικῆς καὶ δημι
ουργικῆς καὶ τῆς τοῖς πᾶσι τὸ εἶναι δωρησαμένης. Ἐκοπίασε βαδίσας, ἀλλὰ καὶ χωλοὺς
ἀρτίποδας εἰρ γάσατο, καὶ νεκροὺς ἐκ τῶν τάφων ἀνέστησε· καὶ τὸ μὲν ἦν τῆς
ὑπερκοσμίου δυνάμεως, τὸ δὲ τῆς ἡμετέ ρας ἀσθενείας. Ἐδειλίασε θάνατον, καὶ ἔλυσε
83.1428 θάνατον· καὶ τὸ μὲν τοῦ θνητοῦ δηλωτικὸν, τὸ δὲ τοῦ ἀθανάτου, μᾶλλον δὲ
ζωοποιοῦ τυγχάνει σημαντικόν. Ἐσταυρώθη, κατὰ τὸν μακάριον Παῦλον, ἐξ ἀσθε νείας·
ἀλλὰ ζῇ ἐκ δυνάμεως Θεοῦ, κατὰ τὸν αὐτόν. Τὸ τῆς ἀσθενείας ὄνομα διδασκέτω οὐχ ὡς
ὁ παντο δύναμος, καὶ ἀπερίγραφος, καὶ ἄτρεπτος καὶ ἀναλ λοίωτος προσηλώθη, ἀλλ' ἡ
ἐκ δυνάμεως Θεοῦ ζωο ποιηθεῖσα φύσις, κατὰ τὴν τοῦ Ἀποστόλου διδασκα λίαν,
ἀπέθανε καὶ ἐτάφη· ἀμφότερα τῆς τοῦ δούλου μορφῆς. Πύλας χαλκᾶς συνέτριψε καὶ
μοχλοὺς σι δηροῦς συνέθλασε, καὶ κατέλυσε τοῦ θανάτου τὸ κράτος, καὶ ἐν τρισὶν
ἡμέραις ἀνέστησε τὸν οἰκεῖον ναόν· ταῦτα τῆς τοῦ Θεοῦ μορφῆς τὰ γνωρίσματα, κατὰ
τὴν τοῦ Κυρίου φωνήν· «Λύσατε τὸν ναὸν τοῦτον, καὶ ἐν τρισὶν ἡμέραις ἐγερῶ αὐτόν.»
Οὕτως ἐν τῷ ἑνὶ Χριστῷ διὰ μὲν τῶν παθῶν θεωροῦμεν τὴν ἀνθρωπότητα, διὰ δὲ τῶν
θαυμάτων νοοῦμεν αὐτοῦ τὴν θεότητα. Οὐ γὰρ εἰς δύο Χριστοὺς τὰς δύο φύσεις
μερίζομεν. Καὶ ἴσμεν, ὅτι μὲν ἐκ τοῦ Πατρὸς ὁ Θεὸς Λόγος ἐγεννήθη, καὶ ἐκ σπέρματος
Ἀβραὰμ καὶ ∆αβὶδ ἡ ἡμετέρα ἀπαρχὴ προσελήφθη. ∆ιὸ καὶ ὁ μακάριος Παῦλός φησι,
περὶ τοῦ Ἀβραὰμ διαλε γόμενος· «Οὐκ εἶπε, Καὶ τοῖς σπέρμασί σου, ὡς ἐπὶ πολλῶν, ἀλλ'
ὡς ἐφ' ἑνὸς, Καὶ τῷ σπέρματί σου, ὅς ἐστι Χριστός.» Καὶ Τιμοθέῳ δὲ γράφων, «Μνη
μόνευε, φησὶν, Ἰησοῦν Χριστὸν ἐγηγερμένον ἐκ νε κρῶν ἐκ σπέρματος ∆αβὶδ, κατὰ τὸ
Εὐαγγέλιόν μου.» Καὶ Ῥωμαίοις ἐπιστέλλων· «Περὶ τοῦ Υἱοῦ αὐτοῦ, φησὶ, τοῦ
γενομένου ἐκ σπέρματος ∆αβὶδ κατὰ σάρκα.» Καὶ πάλιν· «Ὧν οἱ πατέρες καὶ ἐξ ὧν ὁ
Χριστὸς τὸ κατὰ σάρκα.» Καὶ ὁ εὐαγγελιστής· «Βίβλος γενέσεως Ἰησοῦ Χριστοῦ, υἱοῦ
∆αβὶδ, υἱοῦ Ἀβραάμ.» Καὶ ὁ μακάριος Πέτρος ἐν ταῖς Πράξεσι, «Προφήτης, φησὶν,
ὑπάρχων ὁ ∆αβὶδ, καὶ εἰδὼς ὅτι ὅρκῳ ὤμοσεν αὐτῷ ὁ Θεὸς ἐκ καρποῦ τῆς ὀσφύος αὐτοῦ
ἀναστήσειν τὸν Χριστὸν καὶ καθίσαι ἐπὶ τοῦ θρόνου αὐτοῦ· προειδὼς ἐλάλησε περὶ τῆς
ἀναστά σεως αὐτοῦ.» Καὶ ὁ Θεὸς τῷ Ἀβραάμ φησι· «Ἐν τῷ σπέρματί σου
ἐνευλογηθήσονται πάντα τὰ ἔθνη τῆς γῆς.» Καὶ ὁ Ἡσαΐας δέ· «Ἐξελεύσεται ῥάβδος ἐκ
τῆς ῥίζης Ἰεσσαὶ, καὶ ἄνθος ἐκ τῆς ῥίζης ἀναβήσεται, καὶ ἐπαναπαύσεται ἐπ' αὐτὸν
πνεῦμα σοφίας καὶ συνέσεως, πνεῦμα βουλῆς καὶ ἰσχύος, πνεῦμα γνώσεως καὶ
εὐσεβείας, πνεῦμα φόβου Θεοῦ ἐμπλήσει αὐτόν.» Καὶ μετ' ὀλίγα, «Καὶ ἔσται, φησὶν, ἡ
ῥίζα τοῦ Ἰεσσαὶ καὶ ὁ ἀνιστάμενος ἄρχειν ἐθνῶν· ἐπ' αὐτῷ ἔθνη ἐλπιοῦσι· καὶ ἔσται ἡ
ἀνά παυσις αὐτοῦ τιμή.» ∆ῆλον τοίνυν ἐκ τῶν εἰρημένων, ὡς τὸ μὲν κατὰ σάρκα ὁ
Χριστὸς τοῦ Ἀβραὰμ καὶ ∆αβὶδ ὑπῆρχεν ἀπόγονος, καὶ τὴν αὐτὴν αὐτοῖς περιέκειτο
φύσιν, κατὰ δὲ τὴν θεότητα τοῦ Θεοῦ προαιώνιός ἐστιν Υἱὸς καὶ Λόγος, ἀφράστως τε καὶ
ὑπὲρ ἄνθρωπον ἐκ τοῦ Πατρὸς γεννηθεὶς, καὶ συναΐδιος ὑπάρχων ὡς ἀπαύ γασμα καὶ
χαρακτὴρ καὶ Λόγος. Ὡς γὰρ λόγος πρὸς 83.1429 νοῦν καὶ ἀπαύγασμα πρὸς τὸ φῶς
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

ἀχωρίστως ἔχει, οὕτως ὁ μονογενὴς Υἱὸς πρὸς τὸν ἑαυτοῦ Πατέρα.
Φαμὲν τοίνυν τὸν Κύριον ἡμῶν Ἰησοῦν Χριστὸν Υἱὸν εἶναι μονογενῆ τοῦ Θεοῦ
καὶ πρωτότοκον· μο νογενῆ μὲν καὶ πρὸ τῆς ἐνανθρωπήσεως, καὶ μετὰ τὴν
ἐνανθρώπησιν· πρωτότοκον δὲ μετὰ τὴν ἐκ Παρθένου γέννησιν· τῷ γὰρ Μονογενεῖ τὸ
Πρωτό τοκος ὄνομα ἐναντίον μὲν εἶναί πως δοκεῖ, διότι μονογενὴς μὲν ὁ μόνος ἔκ τινος
γεννηθεὶς προσαγο ρεύεται, πρωτότοκος δὲ ὁ πολλῶν ἀδελφῶν πρῶτος. Τὸν δὲ Θεὸν
Λόγον μόνον ἐκ τοῦ Πατρὸς αἱ θεῖαι Γραφαὶ γεννηθῆναι λέγουσιν· γίνεται δὲ καὶ πρωτό
τοκος ὁ Μονογενὴς, τὴν ἡμετέραν φύσιν εἰληφὼς ἐκ τῆς Παρθένου, καὶ ἀδελφοὺς τοὺς
εἰς αὐτὸν πεπιστευ κότας προσαγορεῦσαι καταξιώσας· ὡς εἶναι τὸν αὐ τὸν μονογενῆ μὲν
καθὸ Θεὸς, πρωτότοκον δὲ καθὸ ἄνθρωπος. Οὕτως ἡμεῖς τὰς δύο φύσεις ὁμολογοῦν τες
τὸν ἕνα Χριστὸν προσκυνοῦμεν, καὶ μίαν αὐτῷ προσφέρομεν τὴν προσκύνησιν. Τὴν γὰρ
ἕνωσιν ἐξ αὐτῆς τῆς συλλήψεως ἐν τῇ ἁγίᾳ τῆς Παρθένου νηδύϊ γεγενῆσθαι πιστεύομεν.
∆ιὸ καὶ Θεοτόκον καὶ ἀν θρωποτόκον τὴν ἁγίαν Παρθένον προσαγορεύομεν. Ἐπειδὴ καὶ
αὐτὸς ὁ ∆εσπότης Χριστὸς, Θεὸς καὶ ἄνθρωπος ὑπὸ τῆς θείας καλεῖται Γραφῆς. Καὶ ὁ
Ἐμμανουὴλ δὲ, τῶν δύο φύσεων κηρύττει τὴν ἕνω σιν. Εἰ δὲ τὸν Χριστὸν Θεὸν καὶ
ἄνθρωπον ὁμολο γοῦμεν καὶ λέγομεν, τίς οὕτως εὐήθης, ὡς φυγεῖν τὴν ἀνθρωποτόκος
φωνὴν, μετὰ τῆς Θεοτόκου τιθε μένην; Ἐν γὰρ τῷ ∆εσπότῃ Χριστῷ τὰς δύο τίθε μεν
προσηγορίας· διὸ ἡ Παρθένος τετίμηται καὶ κε χαριτωμένη προσηγορεύθη· τίς οὖν εὖ
φρονῶν παραιτήσαιτο, ἀπὸ τῶν τοῦ Σωτῆρος ὀνομάτων ἀπο καλέσαι τὴν Παρθένον, ἣ
δι' ἐκεῖνον παρὰ τῶν πιστῶν γεραίρεται; Οὐ γὰρ ὁ ἐξ αὐτῆς δι' αὐτὴν σεβάσμιος, ἀλλ'
αὐτὴ διὰ τὸν ἐξ αὐτῆς ταῖς μεγίσταις προσηγορίαις καλλύνεται. Εἰ μὲν οὖν Θεὸς μόνον ὁ
Χριστὸς, καὶ ἐκ τῆς Παρθένου τοῦ εἶναι τὴν ἀρχὴν εἴληφεν, ἐντεῦθεν μόνον ἡ Παρθένος
ὀνομαζέσθω καὶ καλείσθω Θεοτόκος ὡς Θεὸν φύσει γεννήσασα. Εἰ δὲ Θεὸς καὶ
ἄνθρωπος ὁ Χριστὸς, καὶ τὸ μὲν ἦν ἀεὶ (οὔτε γὰρ ἤρξατο τοῦ εἶναι· συναΐδιος γὰρ τῷ
γεννήσαντι), τὸ δὲ ἐπ' ἐσχά των τῶν καιρῶν ἐκ τῆς ἀνθρωπείας ἐβλάστησε φύσεως,
ἑκατέρωθεν ὁ δογματίζειν ἐθέλων, πλεκέτω τῇ Παρθένῳ τὰς προσηγορίας, δηλῶν ποῖα
μὲν τῇ φύσει, ποῖα δὲ τῇ ἑνώσει προσήκει. Εἰ δὲ πανηγυρικῶς τις λέγειν ἐθέλοι, καὶ
ὕμνους ὑφαίνειν, καὶ ἐπαίνους διεξιέναι, καὶ βούλεται τοῖς σεμνοτέροις ὀνόμασιν
ἀναγκαίως κεχρῆσθαι, οὐ δογματίζων ὡς ἔφην, ἀλλὰ πανηγυρίζων καὶ θαυμάζων ὡς
οἷόν τε τοῦ μυστηρίου τὸ μέγεθος· ἀπολαυέτω τοῦ πόθου, καὶ τοῖς μεγάλοις ὀνόμασι
κεχρήσθω, καὶ ἐπαινείτω καὶ θαυμαζέτω. Πολλὰ γὰρ τοιαῦτα παρὰ τοῖς ὀρθοδόξοις
διδασκάλοις εὑρίσκομεν· πανταχοῦ δὲ τὸ μέτριον τιμάσθω. Ἐπαινῶ γὰρ τὸν εἰρηκότα,
ἄριστον εἶναι τὸ μέτριον, εἰ καὶ τῆς ἡμετέρας ἀγέλης οὐκ 83.1432 ἐστιν.
Αὕτη τῆς ἐκκλησιαστικῆς πίστεως ἡ ὁμολογία· τοῦτο τῆς εὐαγγελικῆς καὶ
ἀποστολικῆς διδασκαλίας τὸ δόγμα. Ὑπὲρ τούτου τρὶς καὶ πολλάκις ἀποθανεῖν τῆς τοῦ
Θεοῦ δηλονότι χάριτος συνεργούσης οὐ παραιτησόμεθα. Ταῦτα καὶ τοὺς νῦν
πλανωμένους διδάξαι προεθυμήθημεν· καὶ πολλάκις αὐτοὺς εἰς διάλεξιν
προὐκαλεσάμεθα, ὑποδεῖξαι αὐτοῖς σπουδάζοντες τὴν ἀλήθειαν, καὶ οὐ πεπείκαμεν.
Ὑφορώμενοι γὰρ τῶν ἐλέγχων τὸ προφανὲς ἔφυγον τοὺς ἀγῶνας· σαθρὸν γὰρ ὡς
ἀληθῶς τὸ ψεῦδος, καὶ τῷ σκότει συνεζευγμένον. «Πᾶς γὰρ, φησὶν, ὁ φαῦλα πράσσων,
οὐκ ἔρχεται πρὸς τὸ φῶς, ἵνα μὴ φανερωθῇ ὑπὸ τοῦ φωτὸς τὰ ἔργα αὐτοῦ.» Ἐπειδὴ
τοίνυν πολλὰ πεπονηκότες, οὐ πεπείκαμεν αὐτοὺς ἐπιγνῶναι τὴν ἀλήθειαν, εἰς τὰς
οἰκείας ἐπανήλθομεν Ἐκκλησίας ἀθυμοῦντες καὶ χαίροντες· τὸ μὲν, διὰ τὸ ἡμέτερον
ἀπλανὲς, τὸ δὲ, διὰ τὴν τῶν μελῶν ἡμῶν σηπεδόνα. ∆ιὸ τὴν ὑμετέραν ἁγιωσύνην
παρακαλῶ, ἐκθύμως τὸν φιλάνθρωπον ἡμῶν ἱκετεῦσαι ∆εσπότην, καὶ πρὸς αὐτὸν
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

βοῆσαι· Φεῖσαι, Κύριε, τοῦ λαοῦ σου, καὶ μὴ δῷς τὴν κληρονομίαν σου εἰς ὄνειδος.
Ποίμανον ἡμᾶς, Κύριε, ἵνα μὴ γενώμεθα ὡς τὸ ἀπαρχῆς ὅτε οὐκ ἦρχες ἡμῶν, οὐδὲ
ἐπεκέκλητο τὸ ὄνομά σου ἐφ' ἡμᾶς. Ἴδε, Κύριε, ὅτι ἐγενήθημεν ὄνειδος τοῖς γείτοσιν
ἡμῶν, μυκτηρισμὸς καὶ χλευασμὸς τοῖς κύκλῳ ἡμῶν· ὅτι εἰσῆλθε δόγματα πονηρὰ εἰς
τὴν κληρονομίαν. Ἐμίαναν τὸν ναὸν τὸν ἅγιόν σου, ὅτι εὐφράνθησαν θυγατέρες
ἀλλοφύλων ἐπὶ τοῖς ἡμετέροις κακοῖς. Ὅτι ἐμερίσθημεν εἰς γλώσσας πολλὰς οἱ πρώην
ὁμοφρονοῦντές τε καὶ ὁμοφωνοῦντες. Κύριε ὁ Θεὸς ἡμῶν, εἰρήνην δὸς ἡμῖν ἣν
ἀπωλέσαμεν, τῶν σῶν ἐντολῶν ἀμελήσαντες. Κύριε, ἐκτὸς σοῦ ἄλλον οὐκ οἴδαμεν· τὸ
ὄνομά σου ὀνομάζομεν· ποίησον τὰ ἀμφότερα ἓν, καὶ τὸ μεσότοιχον τοῦ φραγμοῦ
λῦσον, τὴν ἀναφυεῖσαν ἀσέβειαν. Συνάγαγε ἡμᾶς ἕνα καθ' ἕνα τὸν νέον σου Ἰσραὴλ,
οἰκοδομῶν Ἱερουσαλὴμ καὶ τὰς διασπορὰς Ἰσραὴλ ἐπισυνάγων. Γενώμεθα πάλιν μία
ποίμνη, καὶ πάντες ὑπὸ σοῦ ποιμανθείημεν· σὺ γὰρ εἶ ὁ ποιμὴν ὁ καλὸς, ὁ τὴν ψυχὴν
αὑτοῦ τεθεικὼς ὑπὲρ τῶν προβάτων. Ἐξεγέρθητι, ἱνατί ὑπνοῖς, Κύριε; ἀνάστηθι καὶ μὴ
ἀπώσῃ εἰς τέλος· ἐπιτίμησον τοῖς ἀνέμοις καὶ τῇ θαλάττῃ· καὶ δὸς γαλήνην τῇ Ἐκκλησίᾳ
σου καὶ κυμάτων ἀπαλλαγήν. Ταῦτα καὶ ὅσα τοιαῦτα παρακαλῶ τὴν ὑμετέραν
θεοσέβειαν βοᾷν πρὸς τὸν τῶν ὅλων Θεόν· ἀγαθὸς γὰρ ὢν καὶ φιλάνθρωπος, καὶ τὸ
θέλημα τῶν φοβουμένων αὐτὸν ποιῶν ἀεὶ, τῆς ὑμετέρας δεήσεως ἐπακούσεται, καὶ τὸν
παρόντα. ζόφον ἀποσκεδάσει τὸν τῆς Αἰγυπτιακῆς πληγῆς ζοφωδέστερον, καὶ τὴν αὑτοῦ
φίλην χαριεῖται γαλήνην, καὶ συνάξει τοὺς διεσκορπισμένους, καὶ τοὺς ἀπωσμένους
εἰσδέξεται. Καὶ ἀκουσθήσεται πά 83.1433 λιν φωνὴ ἀγαλλιάσεως καὶ σωτηρίας ἐν
σκηναῖς δικαίων. Τότε καὶ ἡμεῖς βοήσομεν πρὸς αὐτόν· «Εὐφράνθημεν ἀνθ' ὧν ἡμερῶν
ἐταπείνωσας ἡμᾶς, ἐτῶν ὧν εἴδομεν κακά.» Καὶ ὑμεῖς δὲ τῆς αἰτήσεως τυχόντες,
ἀνυμνοῦντες αὐτὸν ἐρεῖτε· «Εὐλογητὸς ὁ Θεὸς ὃς οὐκ ἀπέστησε τὴν προσευχὴν ἡμῶν,
καὶ τὸ ἔλεος αὑτοῦ ἀφ' ἡμῶν. Αὐτῷ ἡ δόξα εἰς τοὺς αἰῶνας. Ἀμήν.»

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

7

