Ad cives Nazianzenos
ΛΟΓΟΣ ΙΖʹ.
Πρὸς τοὺς πολιτευομένους Ναζιανζοῦ ἀγωνιῶντας, καὶ τὸν ἄρχοντα
ὀργιζόμενον.
Αʹ. Τὴν κοιλίαν μου, τὴν κοιλίαν μου ἀλγῶ, 35.965 καὶ τὰ αἰσθητήρια τῆς
καρδίας μου μαιμάσ σει, φησί που τῶν ἑαυτοῦ λόγων Ἱερεμίας, ὁ τῶν προφητῶν
συμπαθέστατος, ἀποκλαιόμενος τὸν Ἰσραὴλ ἀπειθοῦντα, καὶ τῆς τοῦ Θεοῦ φιλανθρω
πίας ἀλλοτριούμενον, κοιλίαν μὲν τὴν ἑαυτοῦ ψυχὴν ὀνομάζων, κατὰ τοὺς τῆς
τροπῆς νόμους. Οὕ τω γὰρ εὑρίσκω πολλαχοῦ τῆς Γραφῆς, εἴτε ὡς ἀπόκρυφον καὶ
ἀόρατον (τὸ γὰρ αὐτὸ καὶ ψυχῆς καὶ γαστρὸς τὸ κρύπτεσθαι), εἴτε ὡς χωρητικὴν καὶ
ἀνα δοτικὴν, ἵν' οὕτως εἴπω, τῶν ἐκ τοῦ λόγου τροφῶν· ὃ γάρ ἐστιν ἐν σώματι
τροφὴ, τοῦτο ἐν ψυχῇ λό γος. Αἰσθητήρια δὲ τάχα μὲν τὰ τῆς ψυχῆς κινήματα καὶ
διανοήματα, καὶ τούτων μάλιστα, ὅσα ἐκ τῆς αἰ σθήσεως, οἷς ὁ δίκαιος σπαράσσεται,
καὶ θερμαίνε ται, καὶ ὁρμητικῶς ἔχει, καὶ οὐδὲ καθεκτός ἐστι διὰ τὴν ζέσιν τοῦ
Πνεύματος· τοῦτο γὰρ τὸ μαιμάσσειν, οἷον προθυμία τις θυμῷ σύγκρατος. Εἰ δὲ καὶ
τῶν σωματικῶν ἔστιν ἃ λαμβάνοι τις αἰσθητηρίων, οὐκ ἀπὸ σκοποῦ λήψεται· καὶ
ὀφθαλμῶν, καὶ ὤτων, οὐ μόνον ἀχθομένων τοῖς χείροσι καὶ θεάμασι καὶ
ἀκούσμασιν, ἀλλὰ καὶ τὰ βελτίω ποθούντων καὶ ἰδεῖν καὶ ἀκοῦσαι διὰ τὸ
εὔσπλαγχνον. Πλὴν ὅπως ἂν νοῆ ται ταῦτα, ὁ δίκαιος ἀλγεῖ καὶ μαιμάσσει, καὶ οὐ με
τρίως φέρει τὰ πάθη τοῦ Ἰσραὴλ, ὁποτέρως βούλει ταῦτα νοεῖν· εἴτε τὰ σωματικὰ,
καὶ τοῦ σωματικῶς ὁρωμένου, εἴτε τὰ πνευματικὰ, καὶ τοῦ νοουμέ νου πνευματικῶς.
Ἐπεὶ καὶ πηγὴν δακρύων ὁ αὐτὸς προφήτης ἐπιζητεῖ, καὶ σταθμὸν ἔσχατον ἐπι ποθεῖ,
καὶ ἐρημίαν ἀσπάζεται, ἵνα κενώσῃ τὸ πολὺ τῆς λύπης, καὶ κουφισθῇ πως τῆς ἔνδον
πληγῆς, καθ' ἡσυχίαν θρηνήσας τὸν Ἰσραήλ.
Βʹ. Τὸ δ' αὐτὸ καὶ ὁ θεῖος ∆αβὶδ, οὐ μικρὸν ἔμπρο σθεν ἐπὶ τοῖς καθ' ἑαυτὸν
δυσχεραίνων, Τίς δώσει μοι, φησὶ, πτέρυγας ὡσεὶ περιστερᾶς, καὶ πετα σθήσομαι, καὶ
καταπαύσω; Περιστερᾶς ἐπιζητεῖ πτέρυγας, εἴτε ὡς κουφοτέρας καὶ ταχυτέρας· τοι
οῦτος γὰρ ἅπας δίκαιος· εἴτε ὡς τὸ Πνεῦμα σκιαγρα φούσας, ᾧ μόνῳ τὰ δεινὰ
φεύγομεν, ἵν' ὡς ποῤ ῥωτάτω γένηται ἀπὸ παρόντων κακῶν. Εἶτα τὸ ἐν τοῖς στενοῖς
φάρμακον, ἡ ἐλπίς· Προσεδεχόμην, φησὶ, τὸν Θεὸν τὸν σώζοντά με ἀπὸ ὀλιγοψυχίας
καὶ ἀπὸ καταιγίδος. Ὅπερ οὖν καὶ ἀλλαχοῦ ποιῶν φαίνεται, τάχιστα τῷ λυποῦντι τὸ
ἴαμα προστιθεὶς, καὶ ἀγαθὸν ἡμῖν παρέχων παίδευμα καὶ λόγῳ καὶ ἔρ γῳ τῆς πρὸς τὰ
δεινὰ μεγαλοψυχίας· Ἀπηνήνατο, φησὶ, παρακληθῆναι ἡ ψυχή μου. Ὁρᾷς ἀκη 35.968
δίας ῥήματα καὶ ἀπογνώσεως; Ἆρ' οὐκ ἔδεισας ὡς περὶ ἀθεραπεύτου τοῦ ∆αβίδ; Τί
λέγεις; Οὐ δέχῃ παράκλησιν; ἀπελπίζεις παραμυθίαν; οὐδεὶς θερα πεύσει σε; οὐ
λόγος, οὐ φίλος, οὐ συγγενὴς, οὐ συμβουλεύων, οὐ συναλγῶν, οὐ τὰ καθ' ἑαυτὸν δι
ηγούμενος, οὐ τὰ ἀρχαῖα λέγων, οὐ τὰ νῦν παρατι θεὶς, ὅσοι καὶ ἀπὸ χαλεπωτέρων
κακῶν διεσώθησαν; Ἔῤῥιπται πάντα; οἴχεται; περικέκοπται; ἀπόλωλε πᾶσα ἐλπίς;
κεῖσθαι δεῖ μόνον, τὸ τέλος ἐκδεχομέ νους; Ταῦτα ὁ μέγας ∆αβὶδ, ὁ ἐν θλίψεσι
πλατυνόμε νος· ὁ καὶ σκιᾶς θανάτου κυκλούσης μετὰ τοῦ Θεοῦ κατεξανιστάμενος. Τί
οὖν ἐγὼ πάθω, ὁ μικρὸς, ὁ ἀσθενὴς, ὁ γήϊνος, ὁ μὴ τοσοῦτος τῷ πνεύματι; ∆α βὶδ
ἰλιγγιᾷ, καὶ τίς σώζεται; Τίνα βοήθειαν εὕροι μι κακοπαθῶν, ἢ τίνα παράκλησιν;
πρὸς τίνα καταφύγω στενοχωρούμενος; Ἀποκρίνεταί σοι ∆α βὶδ ὁ μέγας θεραπευτὴς,
 
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ὁ καὶ πνευμάτων πονηρῶν κατεπᾴδων διὰ τοῦ ἐν αὐτῷ Πνεύματος. Πρὸς τίνα
καταφεύξῃ παρ' ἐμοῦ βούλει μαθεῖν, αὐτὸς δὲ οὐκ ἐπίστασαι; Τίς ὁ τὰς παρειμένας
ἐνισχύων χεῖ ρας, καὶ γόνατα παραλελυμένα παρακαλῶν, καὶ διὰ πυρὸς ἄγων, καὶ
σώζων δι' ὕδατος; Οὐδὲν δεῖ σοι παρατάξεως, φησὶν, οὐχ ὅπλων, οὐ τοξοτῶν, οὐχ
ἱππέων, οὐ συμβούλων, οὐ φίλων, οὐκ ἐπικουρίας τῆς ἔξωθεν. Ἔχεις ἐν σεαυτῷ τὴν
συμμαχίαν, ἣν κἀγὼ καὶ πᾶς ὁ βουλόμενος. Θελῆσαι δεῖ μόνον, ὁρμῆσαι μόνον.
Ἐγγὺς ἡ παράκλησις, ἐν τῷ στόματί σου, καὶ ἐν τῇ καρδίᾳ σου· Ἐμνήσθην τοῦ Θεοῦ,
φησὶ, καὶ εὐφράνθην. Τί μνήμης ἑτοιμότερον; Μνήσθητι καὶ σὺ, καὶ εὐφράνθητι. Ὢ
τοῦ εὐπορίστου τῆς ἰατρείας! ὢ τοῦ τάχους τῆς θεραπείας! ὢ τοῦ μεγέθους τῆς
δωρεᾶς! Οὐ κοιμίζει μόνον ὀλιγοψυχίαν καὶ λύπην μνημονευθεὶς ὁ Θεὸς, ἀλλὰ καὶ
εὐφροσύνην ἐρ γάζεται.
Γʹ. Βούλει καὶ ἄλλων ἀκοῦσαι φιλανθρωπίας ῥημά των; Ἐὰν ἐπιστραφεὶς,
φησὶ, πρὸς Κύριον στενάξῃς, τότε σωθήσῃ. Ὁρᾶτε τῷ στεναγμῷ τὴν σωτηρίαν
συνεζευγμένην; Καὶ ἔτι λαλοῦντός σου, ἐρεῖ· Ἰδοὺ πάρειμι· καὶ φθέγξεται τῇ ψυχῇ
σου· Σωτηρία σου εἰμὶ ἐγώ. Οὐδὲν μέσον τῆς αἰτήσεως καὶ τῆς ἐπιτεύξεως· οὐ
χρυσὸς, οὐκ ἄργυρος, οὐ λί θοι τῶν διαφανῶν καὶ τιμίων, οὐχ ὅσοις ἄνθρωποι
κάμπτονται πρὸς τὸ ἥμερον. Ὅταν εἴπῃ Σοφο νίας, ὡς παρὰ τοῦ Θεοῦ λέγων
ὠργισμένου καὶ χαλε παίνοντος· Ἐξερημώσω τὰς ὁδοὺς αὐτῶν τὸ πα 35.969 ράπαν,
τοῦ μὴ διοδεύειν. Ἐξέλιπον αἱ πό λεις αὐτῶν διὰ τὸ μὴ ὑπάρχειν, μηδὲ κατοικεῖ σθαι·
ὅταν ἀπειλήσῃ τὰ φρικωδέστατα, ὅταν κατα βάλῃ τῇ ἀθυμίᾳ, ὅταν ζόφον ἐργάσηται
διὰ τῆς ἀπειλῆς, εὐθὺς ἐπάγει καὶ τὸ τῆς χρηστοτέρας ἐλπίδος φῶς, καὶ ἀνακτᾶταί με
τούτοις τοῖς ῥή μασι· Πλὴν εἶπα· Φοβεῖσθέ με, καὶ δέξασθε παι δείαν, καὶ οὐ μὴ
ἐξολοθρευθῆτε ἐξ ὀφθαλμῶν αὐ τῆς· καὶ ἔτι φαιδροτέροις μικρὸν προϊὼν καὶ φιλαν
θρωποτέροις· Ἐν τῷ καιρῷ ἐκείνῳ ἐρεῖ Κύριος· Θάρσει, Σιὼν, μὴ παρείσθωσαν αἱ
χεῖρές σου. Κύριος ὁ Θεός σου ἐν σοὶ, δυνατὸς σῶσαί σε· καὶ ἐπάξει ἐπὶ σὲ
εὐφροσύνην, καὶ καινιεῖ σε ἐν τῇ ἀγαπήσει αὐτοῦ, καὶ συνάξει τοὺς συν
τετριμμένους, καὶ σώσει τοὺς ἐκπεπιεσμένους, καὶ τοὺς ἀπωσμένους εἰσδέξεται.
Ταῦτα καὶ οἱ ἅγιοι καὶ οἱ λογισμοὶ βούλονται, καὶ ὁ ἐμὸς ἀπαιτεῖ λόγος. Ἀλλὰ
δέξασθε λόγους φρονήσεως, ὥς φη σιν ὁ θεῖος Σολομὼν, ἵνα μὴ ἐμπεσόντες εἰς
κακῶν βάθος καταφρονήσητε, μηδὲ ὑπὸ τῆς ἰδίας ἀμα θίας μᾶλλον ἢ τῆς κατεχούσης
δυσχερείας κατα ποθῆτε.
∆ʹ. Κύκλος τίς ἐστιν, ἀδελφοὶ, τῶν ἀνθρωπί νων πραγμάτων, καὶ παιδεύει διὰ
τῶν ἐναντίων ἡμᾶς ὁ Θεός· ὥσπερ συνεστήσατο τὰ πάντα σοφῶς καὶ συνέδησεν,
οὕτω σοφῶς τὸ πᾶν διεξάγων καὶ κυ βερνῶν τοῖς ἀνεφίκτοις αὐτοῦ καὶ
ἀνεξιχνιάστοις κρίμασιν, ἐν δὲ τοῖς πᾶσι καὶ τὰ ἡμέτερα. Κινεῖται γὰρ, ἵν' οὕτως
εἴπω, τὸ πᾶν περὶ τὸν ἀκίνητον, καὶ σαλεύεται, λόγῳ μὲν οὐδαμῶς (ὁ γὰρ αὐτὸς ἐν
πᾶσιν ἑστὼς καὶ ἀκίνητος, κἂν διαφεύγῃ τὴν ἡμε τέραν ἀσθένειαν, τοῖς δὲ καθ'
ἑκάστην ἡμέραν ἀπαντῶσί τε καὶ συμπίπτουσιν. Καὶ τοῦτό ἐστι δό γμα Θεοῦ παλαιόν
τε καὶ πάγιον, σκότος εἶναι ἀποκρυφὴν αὐτοῦ πρὸ τῶν ἡμετέρων ὀφθαλμῶν
κεχυμένον· καὶ τὰ πολλὰ μὴ καθορᾶσθαι τῆς αὐτοῦ διοικήσεως, πλὴν ὅσον ἐν
ἀμυδροῖς αἰνίγμασι καὶ φαντάσμασιν· εἴτε τὸν τῦφον ἡμῶν συστέλλοντος, ἵν'
εἰδῶμεν τὸ μηδὲν ὄντες πρὸς τὴν ἀληθινὴν σοφίαν καὶ πρώτην, ἀλλὰ πρὸς αὐτὸν
νεύωμεν μόνον, καὶ ζη τῶμεν ἀεὶ ταῖς ἐκεῖθεν αὐγαῖς ἐναστράπτεσθαι· εἴτε διὰ τῆς
ἀνωμαλίας τῶν ὁρωμένων καὶ περιτρεπομέ νων μετάγοντος ἡμᾶς ἐπὶ τὰ ἑστῶτα καὶ
μένοντα. Πλὴν οὐδὲν ἀκίνητον, ὅπερ ἔφην, οὐδὲ ὁμαλὸν, οὐδὲ αὔταρκες, οὐδὲ εἰς
τέλος ὅμοιον, οὔτε τὸ εὐθυμεῖν, οὔτε τὸ ἀθυμεῖν, οὐ πλοῦτος, οὐ πενία, οὐ δύναμις,
35.972 οὐκ ἀσθένεια, οὐ ταπεινότης, οὐ δυναστεία, οὐ τὸ παρὸν, οὐ τὸ μέλλον, οὐ τὸ
ἡμέτερον, οὐ τὸ ἀλλό τριον, οὐ μικρὸν, οὐ μεῖζον, οὐχ ὅ τι ἂν εἴποι τις. Καὶ τοῦτο
 
 
 

2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἴσον ἡ ἀνισότης ἔχει, τὴν περὶ πάντα μεταβο λήν. Περιχωρεῖ γὰρ τὰ πάντα ῥᾳδίως,
καὶ μεταχω ρεῖ, καὶ ἀντικαθίσταται· ὡς αὔραις εἶναι μᾶλλον πι στεύειν καὶ γράμμασι
τοῖς καθ' ὕδατος, ἢ ἀνθρώπων εὐημερίᾳ. Καὶ φθόνος μὲν εὐπραξίαν, συμφορὰν δὲ
ἔλεος ἵστησιν· καὶ τοῦτο σοφῶς, κατά γε τὸν ἐμὸν λόγον, καὶ θαυμασίως, ἵνα μήτε τὸ
ἀλγεῖν ἀπαραμύ θητον ᾖ, μήτε τὸ εὖ πράττειν ἀπαιδαγώγητον.
Εʹ. Σωφρόνων δὲ ἀνδρῶν, οἵτινες παιδεύονται ταῖς συμφοραῖς, καὶ ὡς χρυσὸς
πυρὶ καθαιρόμενοι λέγουσιν· Ἀγαθόν μοι, ὅτι ἐταπείνωσάς με, ὅπως ἂν μάθω τὰ
δικαιώματά σου· ὡς τικτούσης τῆς ταπεινώσεως τὸ γινώσκειν τὰ δικαιώματα. Καὶ τὸ
τοῦ Πέτρου πάσχουσιν, ἐπικαλεσαμένου τὴν σωτηρίαν ἐν τῷ μέλλειν καταδύεσθαι·
καὶ μᾶλλόν τι προσχωροῦσι Θεῷ διὰ τὸ ἀλγεῖν, καὶ τὸν εὐεργέτην οἰκειοῦνται διὰ
τῆς θλίψεως. Ἐπειδὴ κάμνουσα ψυχὴ ἐγγύς ἐστι Θεοῦ, καὶ τὸ χρῄζειν ἐπιστρέφει
πρὸς τὸν δοῦναι δυνάμενον, τῷ ἀφθόνῳ τῆς δωρεᾶς τυχὸν καὶ καταφρονούμενον.
∆ιὰ τοῦτο ἄνω νεύω μεν, ἀδελφοὶ, ἐν παντὶ καιρῷ καὶ περιστάσει πάσῃ
προβαλώμεθα τὴν καλὴν ἐλπίδα, μήτε ἐν ταῖς εὐθυμίαις ἀποβάλωμεν τὸν φόβον,
μήτε ἐν τοῖς λυπηροῖς τὸ εὔελπι. Μνησθῶμεν καὶ ἐν εὐδίᾳ τῆς ζά λης, καὶ ἐν χειμῶνι
τοῦ κυβερνήτου· ἀλλὰ μὴ ταῖς θλίψεσιν ἐκκακήσωμεν, μηδὲ γενώμεθα δοῦλοι κα
κοὶ, ἀγαθύνοντι μὲν ἐξομολογούμενοι τῷ ∆εσπότῃ, παιδεύοντι δὲ μὴ προστιθέμενοι·
καίτοι ποτὲ κρείσ σων ὑγιείας πόνος, καὶ ὑπομονὴ ἀνέσεως, καὶ ἐπίσκεψις ἀμελείας,
καὶ ἐπιστροφὴ συγχωρή σεως. Εἴπω συντόμως, μήτε διὰ τὰς συμφορὰς πίπτω μεν,
μήτε διὰ τὸν κόρον ὑβρίζωμεν.
ςʹ. Ὑποτασσώμεθα καὶ Θεῷ καὶ ἀλλήλοις, καὶ τοῖς ἐπὶ γῆς ἄρχουσιν· Θεῷ διὰ
πάντα, διὰ τὴν φιλ αδελφίαν ἀλλήλοις, δι' εὐταξίαν τοῖς ἄρχουσιν· καὶ 35.975
τοσούτῳ μᾶλλον, ὅσῳπερ ἂν ὦσιν ἡμερώτεροι καὶ χρηστότεροι. ∆εινὸν ἐπιείκειαν
δαπανῆσαι τῷ συνεχεῖ τῆς συγγνώμης, ἵνα μὴ καὶ τῆς ἐκείνων τρα χύτητος αὐτοὶ
δίκας ἀπαιτηθῶμεν, γαλήνην πνεύματι λύσαντες, καὶ φωτὶ ζόφον ἐπάγοντες, καὶ
μέλιτι καταμιγνύντες ἀψίνθιον. Ἔστι καὶ οὗτος εἷς τῶν ἡμετέρων νόμων, καὶ οὗτος
τῶν ἐπαινουμένων, καὶ κάλλιστα διατεταγμένων τῷ Πνεύματι, τὸ δυνα τὸν μετὰ
τοῦ καλοῦ δοκιμάσαντι, καὶ νομοθετήσαντι, ὥσπερ δούλους ὑπακούειν δεσπόταις,
καὶ γυναῖκας ἀνδράσι, καὶ Κυρίῳ τὴν Ἐκκλησίαν, καὶ μαθητὰς ποιμέσι καὶ
διδασκάλοις· οὕτω δὲ καὶ πάσαις ἐξου σίαις ὑπερεχούσαις ὑποτάσσεσθαι, οὐ μόνον
διὰ τὴν ὀργὴν, ἀλλὰ καὶ διὰ τὴν συνείδησιν, ὡς ὑποτελεῖς φόρου τυγχάνοντας· καὶ
μὴ κακοποιοῦντας μισεῖν τὸν νόμον, μηδὲ ἀναμένειν τὴν μάχαιραν, ἀλλὰ τῷ φόβῳ
καθαιρομένους ἐπαινεῖσθαι ὑπὸ τῆς ἐξουσίας.
Ζʹ. Μία καὶ ἡ αὐτὴ στάθμη· ἀλλὰ φείδεται μὲν τοῦ ὀρθοῦ, περικόπτει δὲ τὸ
περιττόν. Εἷς ἥλιος· ἀλλὰ φωτίζει μὲν τὴν ὑγιαίνουσαν ὄψιν, ἀμαυροῖ δὲ τὴν
ἀσθενοῦσαν. Βούλει τι καὶ τῶν ἡμετέρων εἴπω θαῤ ῥήσας; Εἷς Χριστός· ἀλλ' εἰς
πτῶσιν κεῖται καὶ ἀνά στασιν· πτῶσιν μὲν τοῖς ἀπίστοις, ἀνάστασιν δὲ τοῖς
πιστεύουσι· καὶ τοῖς μὲν ἔστι πέτρα προσ κόμματος, καὶ πέτρα σκανδάλου, ὅσοι μὴ
ἔγνωσαν, οὐδὲ συνῆκαν, ἀλλ' ἐν σκότει διαπορεύον ται, εἴτε εἰδώλοις θρησκεύοντες,
εἴτε μέχρι τοῦ γράμματος διαβλέποντες, καὶ ὑπὲρ τοῦτο αὐγασθῆναι μήτε θέλοντες,
μήτε ἰσχύοντες· τοῖς δὲ λίθος ἀκρογωνιαῖος, καὶ πέτρα ἐπαινουμένη, ὅσοι συν
δεσμοῦνται τῷ λόγῳ, καὶ ἐπ' αὐτῆς βεβήκασιν· εἰ βούλει δὲ, ὁ μαργαρίτης ἐκεῖνος,
ὃν πάντων ὧν ἔχει ὠνεῖται ὁ καλὸς ἔμπορος. Ἡμεῖς δὲ τοιοῦτόν τι πά σχομεν,
ἀδελφοὶ, μὴ τὰ ἑαυτῶν κατορθοῦντες, ἀλλὰ πρὸς τὴν ἐξουσίαν δυσχεραίνοντες·
ὥσπερ ἂν εἴ τις αἰτιῷτο τὸν ἀγωνοθέτην ὡς μοχθηρὸν, αὐτὸς ἐκ παλαίων· ἢ τὸν
ἰατρὸν ὡς ἀμαθῆ καὶ θρασὺν ταῖς τομαῖς καὶ ταῖς καύσεσι κεχρημένον, αὐτὸς
πονήρως διακείμενος, καὶ τῶν αὐστηροτέρων δεόμε νος φαρμάκων. Ταῦτά μοι πρὸς
τοὺς ἀρχομένους πα ραμυθία τε ἅμα καὶ νουθεσία· ταῦτα ὁ πενιχρὸς ποι μὴν
 
 
 

3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

καταρτίζων τὸ μικρὸν ποίμνιον, ᾧ χαίροντι συν ευθυμεῖν, καὶ σκυθρωπάζοντι
συστενάζειν, τῆς ἐμῆς ποιμαντικῆς ὁ νόμος. 35.976
Ηʹ. Τί δὲ ὑμεῖς, οἱ δυνάσται καὶ ἄρχοντες; Ἤδη γὰρ πρὸς ὑμᾶς ὁ λόγος
μέτεισιν, ἵνα μὴ δόξωμεν πάντη τυγχάνειν ἄνισοι, καὶ τοῖς μὲν τὰ εἰκότα παραινεῖν,
ὑμῶν δὲ τῇ δυναστείᾳ παραχωρεῖν, ὥσπερ αἰδοῖ τὴν κατὰ Χριστὸν ἡμῶν ἐλευθερίαν,
ἢ δέει ἐκ κλίνοντες· ἢ τῶν μὲν κήδεσθαι μᾶλλον, ὑμῶν δ' ἀμελεῖν, ὧν καὶ μᾶλλον
φροντίζειν ἄξιον, ὅσῳ καὶ εἰς μείζω φέρουσα ἡ ῥοπὴ ὁποτέρως ἂν ἔχῃ, καὶ πλεῖον τὸ
κατορθούμενον· τὸ γὰρ ἐναντίον ἀπείη, καὶ ἡμῶν, καὶ τοῦ λόγου. Τί οὖν φατε; καὶ τί
διομολο γούμεθα πρὸς ἀλλήλους; Ἆρα δέξεσθε σὺν παῤῥη σίᾳ τὸν λόγον; Καὶ ὁ τοῦ
Χριστοῦ νόμος ὑποτίθησιν ὑμᾶς τῇ ἐμῇ δυναστείᾳ καὶ τῷ ἐμῷ βήματι. Ἄρχο μεν γὰρ
καὶ αὐτοί· προσθήσω δὲ, ὅτι καὶ τὴν μείζονα καὶ τελεωτέραν ἀρχήν· ἢ δεῖ τὸ πνεῦμα
ὑποχω ρῆσαι τῇ σαρκὶ, καὶ τοῖς γηίνοις τὰ ἐπουράνια. ∆έ ξῃ τὴν παῤῥησίαν, οἶδ', ὅτι
πρόβατον εἶ τῆς ἐμῆς ποίμνης, τῆς ἱερᾶς ἱερὸν, καὶ θρέμμα τοῦ με γάλου Ποιμένος,
καὶ καλῶς ἄνωθεν ἠγμένον ὑπὸ τοῦ Πνεύματος, καὶ τῷ φωτὶ τῆς ἁγίας καὶ μακα
ρίας Τριάδος ὁμοίως ἡμῖν ἐλλαμπόμενον. ∆ιὰ τοῦτό μοι καὶ βραχὺς ὁ πρὸς σὲ λόγος
καὶ σύντομος.
Θʹ. Χριστῷ συνάρχεις, Χριστῷ καὶ συνδιοικεῖς· παρ' ἐκείνου σοι τὸ ξίφος, οὐκ
εἰς ἔργον, ἀλλ' εἰς ἀπειλήν· ὃ καθαρὸν ἀνάθημα τῷ δεδωκότι φυλάσσοι το. Εἰκὼν εἶ
τοῦ Θεοῦ· καὶ εἰκόνα διὰ χειρὸς ἄγεις, ἐνταῦθα οἰκονομουμένην, καὶ πρὸς βίον
ἄλλον μεθισταμένην, εἰς ὃν πάντες μεταβησόμεθα, μικρὰ παίξαντες ἐν τῷ τῆς ζωῆς
τούτῳ, εἴτε δεσμω τηρίῳ, εἴτε σταδίῳ, εἴτε προχαράγματι, εἴτε σκιά σματι. Τίμησον
τὴν συμφυΐαν· αἰδέσθητι τὸ ἀρχέτυ πον· γενοῦ μετὰ τοῦ Θεοῦ, μὴ τοῦ
κοσμοκράτορος· μετὰ τοῦ χρηστοῦ ∆εσπότου, μὴ τοῦ πικροῦ τυράννου. Ἐκεῖνος
ἀνθρωποκτόνος ἦν ἀπ' ἀρχῆς· ἐκεῖνος καὶ τὸν πρῶτον ἄνθρωπον διὰ τῆς παρακοῆς
ἔπληξε, καὶ τὴν ἐπίμοχθον ζωὴν ἐπεισήγαγε, καὶ τὸ κολάζειν, καὶ τὸ κολάζεσθαι διὰ
τὴν ἁμαρτίαν ἐνομοθέτησε. Σὺ δὲ, ἄνθρωπε τοῦ Θεοῦ, μνήσθητι τίνος εἶ ποίημα, καὶ
ποῦ καλῇ, καὶ πόσα ἔχεις, καὶ πόσον ὀφείλεις, παρὰ τίνος σοι λόγος, νόμος,
προφῆται, αὐτὸ τὸ εἰδέναι Θεὸν, τὸ μὴ ἀπελπίζειν τὰ προσδοκώμενα. Μίμησαι διὰ
ταῦτα Θεοῦ φιλαν θρωπίαν. Τοῦτο ἔχει μάλιστα θεῖον ἄνθρωπος, τὸ εὖ ποιεῖν.
Ἔξεστί σοι Θεὸν γενέσθαι μηδὲν πο νήσαντι· μὴ πρόῃ τὸν καιρὸν τῆς θεώσεως.
35.977
Ιʹ. Ἄλλοι χρήματα κενοῦσιν, ἄλλοι σάρκας τῷ πνεύματι, καὶ νεκροῦνται
Χριστῷ, καὶ κόσμου παντελῶς ἀπανίστανται· ἄλλοι καθιεροῦσι Θεῷ τὰ φίλτατα. Καὶ
τὴν Ἀβραὰμ θυσίαν πάντως ἀκήκοας· ὃς προθυμότερον ἔδωκε τῷ Θεῷ τὸν
μονογενῆ, τὸν ἐκ τῆς ἐπαγγελίας, καὶ εἰς ὃν ἡ ἐπαγγελία, ἢ παρὰ Θεοῦ τὸ ἀπ' ἀρχῆς
ἐκομίσατο. Σὲ δὲ τούτων οὐδὲν αἰτοῦμεν· ἓν ἀντὶ πάντων εἰσένεγκε, τὴν φιλ
ανθρωπίαν̣ ᾗ χαίρει Θεὸς μᾶλλον, ἢ πᾶσιν ὁμοῦ τοῖς ἄλλοις· δῶρον ἴδιον, δῶρον
ἄμωμον, δῶρον φι λοτιμίαν Θεοῦ προκαλούμενον. Μῖξον τῷ φόβῳ τὴν ἐπιείκειαν·
κέρασον τῇ ἀπειλῇ τὴν ἐλπίδα. Πολλὰ καὶ χρηστότητα κατορθοῦσαν οἶδα,
δυσωποῦσαν εἰς ὀφειλομένην ἀντίδοσιν, ὅταν βιάζεσθαι παρὸν συγχωρήσαντες,
δυσωπήσωμεν εὐνοίᾳ τὸν ἐλεούμε νον. Μηδὲν εἶναί σε πειθέτω τῆς ἀρχῆς ἀνάξιον·
μηδὲν ἀποκλειέτω τὸν οἶκτον καὶ τὴν ἡμερότητα, μὴ καιρὸς, μὴ δυνάστης, μὴ
φόβος, μὴ μειζόνων ἀρχῶν ἐλπὶς, μὴ θρασύτης τὸ πλέον ἔχουσα. Κτῆσαι τὴν εὔνοιαν
ἐν τοῖς ἀναγκαίοις καιροῖς, τὴν ἄνω· χρῆσον Θεῷ τὸν ἔλεον. Οὐδεὶς μετέγνω τῶν
Θεῷ τι προεισενεγκάντων. Πολύς ἐστιν εἰς ἀντίδοσιν, μάλιστα μὲν τοῖς ἐκεῖθεν
ἀγαθοῖς ἀμειβόμενος τοὺς προκαταβαλόντας τι, καὶ προχρήσαντας· ἔστι δὲ ὅτε καὶ
τοῖς ἐνταῦθα πρὸς πίστιν τῶν ὕστερον.
 
 
 

4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ΙΑʹ. Ἔτι μικρὸν, καὶ ὁ κόσμος παρέρχεται, καὶ ἡ σκηνὴ καταλύεται.
Πραγματευσώμεθα τὸν καιρόν· τοῖς οὐχ ἑστῶσι τὰ μένοντα ὠνησώμεθα. Ἕκαστος
ἡμῶν ἐστιν ἐν ἐπιτιμίοις· καὶ ὁ χοῦς πολλὰ φέρει τὰ ὀφλήματα. Συγχωρήσωμεν, ἵνα
συγχωρηθῶ μεν· ἀφῶμεν, ἵνα αἰτήσωμεν ἄφεσιν. Ὁρᾷς ἐν τῷ Εὐαγγελίῳ·
Χρεωφειλέτης εἰσάγεταί τις πολλῶν ταλάντων, καὶ ἀφίεται τοῦ χρέους· προσάγεται
γὰρ ἀγαθῷ δεσπότῃ. Καὶ συγχωρεῖται μὲν, οὐ συγχωρεῖ δέ· δοῦλος γάρ ἐστι καὶ τὴν
προαίρεσιν· ἀλλ' ἧς ἔτυχε φιλανθρωπίας περὶ τὸ πλεῖον, ταύτης οὐ μεταδίδωσι τῷ
ὁμοδούλῳ περὶ τὸ ἔλαττον, οὐδὲ, εἰ μή τινι ἄλλῳ, τῷ τῆς μεγαλοψυχίας ἑπόμενος
ὑπο δείγματι περὶ τὸ ἀλλότριον· καὶ ὁ δεσπότης ἀγανα κτεῖ. Καὶ τὸ ἑξῆς σιωπήσομαι·
πλὴν ἄμεινον τῷ παντὶ, 35.980 προεισφέρειν τοῦ ἐκεῖσε λόγου τὴν ἐντεῦθεν χρη
στότητα.
ΙΒʹ. Τί φῄς; Ἔχομέν σε τοῖς λόγοις τούτοις, ὧν ἐραστὴς εἶναι πολλάκις
καθωμολόγησας, ὦ κάλλιστε ἀρχόντων, εἴη δὲ προσθεῖναι, καὶ ἡμερώτατε; Ἢ δεῖ,
καὶ τὴν πολιάν σοι ταύτην ἀνθ' ἱκετηρίας προσαγαγεῖν, καὶ τὸν τῶν ἐτῶν ἀριθμὸν,
καὶ τὴν μακρὰν ἱερωσύνην καὶ ἄσπιλον ταύτην, ἣν αἰδοῦνται τυχὸν καὶ ἄγγελοι,
καθαρῶς τῷ καθαρωτάτῳ λατρεύοντες, ὡς τῆς ἑαυτῶν λατρείας ἀξίαν. Πείθει
ταῦτα; ἤ τι τολμήσω μεῖζον; τολμηρὸν δέ με ποιεῖ τὸ ἀλγεῖν. Χριστὸν προσάγω σοι,
καὶ τὴν Χριστοῦ κένωσιν τὴν ὑπὲρ ἡμῶν, καὶ τὰ τοῦ ἀπαθοῦς πάθη. καὶ τὸν σταυρὸν,
καὶ τοὺς ἥλους, οἷς ἐλύθην ἐγὼ τῆς ἁμαρτίας· καὶ τὸ αἷμα, καὶ τὴν ταφὴν, καὶ τὴν
ἀνάστασιν, καὶ τὴν ἄνοδον, ἢ καὶ τὴν τράπεζαν ταύτην, ᾗ κοινῇ πρόσιμεν· καὶ τοὺς
τύπους τῆς ἐμῆς σωτηρίας, οὓς ἀπὸ τοῦ αὐτοῦ τελῶ στόματος, ἀφ' οὗ ταῦτα πρὸς σὲ
πρεσβεύω, τὴν ἱερὰν καὶ ἄνω φέρουσαν ἡμᾶς μυσταγωγίαν.
ΙΓʹ. Εἰ καὶ μὴ δι' ἓν τούτοιν, ἀλλ' οὖν διὰ πάντα, δὸς καὶ ἡμῖν καὶ σεαυτῷ τὴν
χάριν, καὶ τῇ κατ' οἶκόν σου Ἐκκλησίᾳ, καὶ τῷ καλῷ τούτῳ Χριστοῦ πληρώματι· ὃ
νόμισον ἡμῖν συμπρεσβεύειν, εἰ καὶ ἡμῖν παραχωρεῖ τῆς πρεσβείας, ὡς
αἰδεσιμωτέροις διὰ τὸν τιμήσαντα, καὶ ἅμα τῷ τῆς ἀρχῆς νόμῳ κατείργεται. Ἓν
τοῦτο καλῶς ἡττήθητι· νίκησον ἡμᾶς φιλανθρωπίᾳ. Ἰδοὺ προσάγω σοι τοὺς ἐμοὺς
ἱκέτας ἐναντίον Θεοῦ, καὶ ἀγγέλων, καὶ βασιλείας οὐρανῶν, καὶ τῆς ἐκεῖθεν
ἀνταποδόσεως. Τίμησον τὴν ἐμὴν πίστιν, ἣν ἐπιστεύθην τε καὶ ἐπίστευσα· ὡς ἂν καὶ
ἡ σὴ τιμηθείη περὶ τὰ μείζω καὶ τελεώτερα. Κεφάλαιον 35.981 δὲ τοῦ λόγου, ἔχεις
καὶ αὐτὸς ἐν οὐρανῷ Κύριον· τοιούτου τύχοις περὶ σεαυτὸν τοῦ κριτοῦ, οἷόσπερ ἂν
αὐτὸς γένῃ τοῖς ἀρχομένοις. Τύχοιμεν δὲ πάντες, καὶ τῶν ἐντεῦθεν χρηστῶν, καὶ
τῶν ἐκεῖθεν ἀνεκτοτέρων, ἐν Χριστῷ Ἰησοῦ τῷ Κυρίῳ ἡμῶν, ᾧ ἡ δόξα καὶ τὸ
κράτος, ᾧ τιμὴ καὶ βασιλεία, σὺν τῷ Πατρὶ καὶ τῷ ἁγίῳ Πνεύματι, ὥσπερ ἦν, καὶ
προῆν, καὶ ἔσται, καὶ νῦν, καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 
 

5
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

