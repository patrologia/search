Fragmenta in epistulam ad Philippenses
452 phil 1,19 Σωτηρίαν δέ φησι τὸ ὅσον οὐδέπω μαρτύριον. phil 1,24 Καὶ κατ' ἄλλο
δὲ νοήσεις τὸ ἀναγκαιότερον· ὅτι τὸ προσάγειν Χριστῷ διὰ τῆς πίστεως πλείονας
πλεῖον αὐτὸν ᾠκείου τῷ Χριστῷ. phil 2,2 Ἢ καὶ περὶ τοῦ δόγματος λέγει. phil 3,8
∆ύνῃ καὶ κατ' ἄλλο σκύβαλα νοῆσαι τὸν νόμον· τὸ γὰρ σκύβαλον κτηνώδης ἐστὶ
τροφή, καὶ ὁ νόμος τοίνυν μετὰ τὴν πίστιν ἀλογωτέροις ἀνθρώποις ἐστὶν
παραφυλακή. διὰ τί δὲ ὅλως ἥγησαι αὐτὸν ζημίαν καὶ σκύβαλα, ὦ Παῦλε; ὅτι ἐπὶ
πολὺν χρόνον, φησίν, ἐζημίωσέ με Χριστοῦ τῷ γράμματι παρακαθήμενον, οὐχ ὅτι
νῦν ἐζημιοῦτο ἐξ αὐτοῦ ᾔδει γὰρ αὐτῷ πνευματικῶς χρήσασθαιἀλλὰ περὶ τῆς ζημίας
453 ἐκείνης φησὶ τῆς πρὸ τῆς πίστεως αὐτοῦ· καὶ γὰρ καὶ μετὰ τὸ παρελθεῖν τὴν
ζημίαν ζημιώσασαν ἡμᾶς, ἔτι αὐτῆς μεμνημένοι ζημίαν αὐτὴν καλοῦμεν. ὅρα γὰρ
πῶς ἀσφαλῶς εἶπεν· ἡγοῦμαι αὐτὸν ζημίαν διὰ τὸ ὑπερβάλλον τῆς γνώσεως
Χριστοῦ, ὡσεὶ εἶπεν· φύσει μὲν οὐκ ἔστι ζημία, ἀλλ' ἐπειδὴ δῆθεν φυλάττων αὐτὸν
οὐκ ἐπεγίνωσκον τὸν Χριστόν, εἰς ζημίαν μοι ἐγένετο. Εἰ γάρ, φησίν, ἀντιπαραθείη
τις τὴν τοῦ νόμου σωματικὴν παραφυλακὴν πρὸς τὴν τοῦ Χριστοῦ γνῶσιν, ζημίαν
εὑρήσει τὸν νόμον· τοσαύτη διαφορὰ τῆς Χριστοῦ, φησί, γνώσεως. phil 3,14
Ἀνέγνωμεν δὲ ἔν τισιν ἀντιγράφοις· εἰς τὸ βραβεῖον τῆς ἀνεγκλησίας τοῦ θεοῦ ἐν
Χριστῷ Ἰησοῦ. καὶ εἴη ἂν ἐκεῖ τῆς λέξεως ὁ νοῦς τοιοῦτος· τὸ βραβεῖόν ἐστι βραβεῖον
ἀνεγκλησίας, ὥστε μηδαμῶς ἐγκλήματι τῷ ἀπὸ θεοῦ ἐμπίπτειν τὸν καταλαβόντα τὸ
διωκόμενον καὶ τυχόντα τοῦ σκοποῦ καὶ ἐπιλαβόμενον τοῦ βραβείου. phil 3,19
Καλῶς τὸ τέλος εἶπεν ἀπώλειαν· ἐὰν γάρ, φησίν, ἄχρι τέλους μείνωσι τοιοῦτοι· εἰκὸς
γὰρ ἦν καὶ μεταβληθῆναι αὐτούς. phil 3, 21 Ὅρα δὲ πόσην οἶδεν ὁμογνωμοσύνην τῆς
ἁγίας τριάδος, ὅτι ἄνω τὸν υἱὸν ὑποθέμενος τὴν ἐναλλαγὴν τοῦ σώματος ἡμῶν
ποιεῖσθαι, νῦν τὸν πατέρα τοῦτο ποιεῖν λέγει. ἆρα δέ, ἐὰν οὕτω νοήσωμεν, μὴ
τολμηρόν ἐστιν ὅτι ὁ υἱὸς τοῦτο ποιήσει κατὰ τὴν δύναμιν αὐτοῦ, ἐκείνην ἐν ᾗ
ὑπέταξε τῷ ἰδίῳ σώματι τὰ πάντα; ἀλλ' εἰ οὕτω νοήσομεν, δέος ἐστὶ μὴ δόξωμεν
διαιρεῖν μετὰ τὴν ἕνωσιν, τὴν μίαν σεσαρκωμένην τοῦ θεοῦ λόγου φύσιν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

