Commentarii in Joannem (additamenta)
1 Jo 10, 17 Ἐπειδὴ ὡς θεὸς εἰδὼς τὰς ἐνθυμήσεις τῶν Ἰουδαίων ἑώρα δὲ
αὐτοὺς καταγελῶντας ἐφ' οἷς ἔλεγεν ὑπεραποθνῄσκειν τῶν προβάτων, δείκνυσιν
αὐτοὺς ἐκ τῶν λεγομένων τοσοῦτον ὄντας ἀσεβεῖς ὡς καὶ καταγελᾶν ἐκείνων ἅπερ
πάνυ ἀγαπᾷ ὁ θεὸς καὶ πατήρ, ἐπειδὴ καὶ οὕτως ἑώρα ἀθόλωτον μένουσαν ἐν τῷ
σαρκωθέντι ἑαυτοῦ υἱῷ καὶ ἐκλάμπουσαν τῆς ἑαυτοῦ φύσεως τὴν εἰκόνα διὰ τὸ
ἄτρεπτον. ἕως ὧδε τὸ ϛʹ βιβλίον τοῦ ἁγίου Κυρίλλου.
2 Jo 10, 20–21 Ἄλλοι ἔλεγον· ταῦτα τὰ ῥήματα οὐκ ἔστι δαιμονιζομένου καὶ
τὰ ἑξῆς. ἀπὸ μὲν τῆς τῶν λόγων ποιότητος ὀρθὴν ὁρίζονται τὴν ψῆφον· καὶ γὰρ ἦν
μανίας εἰπεῖν τοῦτον λέγοντα, ὅτι τεθνήξεται μὲν ἀόκνως καὶ προκινδυνεύσει
προθύμως τῶν ἰδίων προβάτων ὁ ποιμὴν ὁ καλός, φεύξεται δὲ καὶ πρὸς μόνην
ἀποπήξας τοῦ λύκου τὴν θέαν ὁ μισθωτὸς καὶ οὐκ ὢν ποιμήν, ὅτι δαιμόνιον ἔχει καὶ
μαίνεται. τί αὐτοῦ ἀκούετε;
3 Jo 10, 26 Ἀρχὴ εὐπειθείας ἡ οἰκειότης· οἰκειοῖ γὰρ ἡμᾶς ἡ πίστις τῷ θεῷ
καὶ σοφοὺς ἀποτελεῖ ἡ οἰκείωσις, οὗτοι δὲ σοφοὶ οὐ τὰ λυσιτελοῦντα πρὸς σωτηρίαν
δεχόμενοι μαθήματα. ἀλλοτρίους δὲ αὐτοὺς λέγων σημαίνει, ὅτι πρόβατα αὐτοῦ
ἦσαν καὶ οἱ ἐξ ἐθνῶν μέλλοντες πιστεύειν καὶ ἠπείλει αὐτοῖς τὴν μετανάστασιν ἣν
ἐπὶ τὰ ἔθνη, ἵνα κἂν διὰ τῆς ἀπειλῆς φοβήσῃ αὐτοὺς τὰ δέοντα φρονεῖν. 4 Jo 10, 29
∆ιδοὺς ἃ εἶχεν ὡς φύσει θεὸς ὡς ἄνθρωπος λαμβάνων καὶ εἰς τὴν ἰδίαν ἀνακομίζων
δόξαν αὐτὸς ὑφ' ἑαυτοῦ καὶ πρὸς τοῦτο ἰών.
5 Jo 10, 31 3 Εἰ γὰρ μὴ ἐμαρτύρουν τὰ ἔργα τοῖς ἐμοῖς λόγοις, οἷς εἶπον λέγων
ἐμαυτὸν ἴσον εἶναι τῷ πατρί, οὐκ ἂν ἠμέλλετε ἀγανακτεῖν ἐφ' οἷς εἶπον ἐμαυτὸν
φύσει θεόν, εἰ μὴ πάντα τὰ ἔργα μου ἐδείκνυ με θεὸν φύσει· εἰ γὰρ ἐβλασφήμησα,
φησίν, εἰρηκὼς ἐμαυτὸν θεόν, εἰκὸς ὑμᾶς ἐννοεῖν, ὅτι δι' ὧν ἐποίησα μειζόνως
φαίνομαι θεὸς καὶ διὰ τοῦτο φονεῦσαί με ζητεῖτε.
6 Jo 10, 36 Ὁ μονογενὴς καὶ τῶν ἄλλων ἁπάντων ἁγιαστικός, ὅσα τῇ τοῦ
ἁγιάζεσθαι μετουσίᾳ πρὸς ἁγιασμὸν ἀναβαίνει. ἁγιάζεσθαι δὲ λέγεταί τι παρὰ τῇ θείᾳ
γραφῇ κατὰ πολλοὺς τρόπους. ἡγιάσθη τοίνυν τοῦτ' ἔστι προεχειρίσθη πρὸς
ἀποστολὴν ἐν κόσμῳ διὰ τὸ σῶσαι τοὺς ἐν αὐτῷ ἢ τὸ ἡγιάσθαι τι τὸ πρὸς θυσίαν
ἀφωρίσθαι νοήσεις· εἷς γὰρ ὑπὲρ πάντων ἀπέθανεν εὐδοκίᾳ τοῦ θεοῦ καὶ πατρός. ἢ
διὰ ἅγιον τὸν ἑαυτοῦ ἀποτελέσαι ναόν· οὐ γὰρ ἦν κατὰ φύσιν ἰδίαν ἡ σὰρξ ἁγία
, ἀλλὰ τοῦ ἑνωθέντος αὐτῇ λόγου δεξαμένη τὸν ἁγιασμὸν ἡγιάσθη καὶ κατὰ τοῦτο
ὡς οἶμαι τοῦτό φησιν.
7 Jo 10, 37–38 Λέγει δὲ καὶ κατὰ Ἀρειανῶν καὶ ὅτι οὐκ ἔστιν ὅμοιον τοῦτο τῷ
πρὸς τοὺς μαθητὰς εἰρημένῳ· καὶ γὰρ ὁ κύριος εἰπὼν πρὸς αὐτοὺς ἐὰν ἐντολάς μου
τηρήσητε, μενεῖτε ἐν τῇ ἀγάπῃ μου, καθὼς κἀγὼ τὰς ἐντολὰς τοῦ πατρὸς τετήρηκα
καὶ μένω αὐτοῦ ἐν τῇ ἀγάπῃ ἑρμηνεύων, ὅτι ἐκεῖνος μενεῖ ἐν τῇ ἀγάπῃ τοῦ Χριστοῦ
ὁ ποιῶν τὰς αὐτοῦ ἐντολάς, ὅπερ οὐκ ἀνάγει εἰς ὁμοουσιότητα τοῦ υἱοῦ τοὺς
μαθητὰς δυναμένους καὶ ἀπογενέσθαι διὰ τοῦ μηκέτι θέλειν ποιεῖν τὰς αὐτοῦ
ἐντολάς, εἰ καὶ ὁ υἱὸς ἀτρέπτως ποιεῖ τὸ τῷ πατρὶ δοκοῦν ὡς θεὸς φύσει
, εἰ καὶ ὡς ἄνθρωπος πρὸς τὴν ἡμῶν ὠφέλειαν ταῦτά φησιν, ἵνα μιμησώμεθα αὐτὸν
καθὼς δυνατόν. τὸ οὖν θέλειν καὶ μὴ θέλειν οὐκ ἐν οὐσίαις καὶ φύσεως λόγῳ
νοοῦμεν, ἀλλ' ἐν προαιρέσει, ὅπερ ἐστὶν ἐπὶ τῶν μαθητῶν, οὐ μὴν ἐπὶ υἱῷ· ἐκ γὰρ
τῶν ὄντων τῷ υἱῷ φυσικῶν ἀγαθῶν νοοῦμεν, ὅτι ὁμοούσιός ἐστι τῷ πατρί, οὐ μὴν
ἐξ ἐκείνων ἅπερ ἐν ἡμῖν γίνεται καὶ ἀπογίνεται.
8 Jo 10, 39 Ἐξηγεῖται ὁ εὐαγγελιστὴς καὶ τὸ τῶν Ἰουδαίων σκληρὸν τῶν
ἐπὶ τοσούτοις ἀγαθοῖς λόγοις μὴ μαλαχθέντων, ἀλλὰ ζητούντων αὐτὸν πιάσαι καὶ
ἀποκτεῖναι καὶ τοῦ σωτῆρος τὴν ἰσχύν. ὡς θεὸς ὢν φύσει μαινομένους ἀπέφυγεν ἐν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

μέσῳ ὢν τῶν ἐκείνων χειρῶν· τοῦτο γὰρ ὡς θαυμάζων ὁ εὐαγγελιστὴς εἶπεν, δι' ὧν
σημαίνει, ὅτι εὐκόλως ἐκ μέσου τῶν φονώντων διαφεύγων ἀκινδύνως οὗτος οὐκ ἂν
ἑάλω ποτέ, εἰ μὴ ἑκών.
9 Jo 11, 1 ∆ιὰ οὖν τὴν τῶν γυναικῶν ἀγάπην ἄξιος ἦν καὶ ὁ τούτων ἀδελφὸς
ὑπὸ τοῦ Χριστοῦ ἀγαπᾶσθαι ὅπου γε καὶ αὐτὸν ἠγάπα σφόδρα.
10 Jo 11, 11–12 Πῶς οὐκ ἄλογον αὐτοὺς πιστεύειν, ὅτι διὰ τὸ ἐξυπνίσαι αὐτὸν
ἐκεῖ παραγίνεται. διὸ καὶ λέγουσιν· εἰ κεκοίμηται, σωθήσεται. οὐ περὶ ὕπνου
νομίζουσιν λέγειν αὐτὸν οὐδὲ περὶ θανάτου, ἀλλὰ αἴνιγμα νομίζουσί τι αὐτὸν
λέγειν, ἐπεί πως πέντε καὶ δέκα στάδια πορευομένου αὐτοῦ οὐκ ἂν ἔφθασεν
ἐξυπνίσαι.
11 Jo 11, 25 Ἐπεὶ οὖν ὁ Χριστὸς ἑώρα αὐτὴν οὐ νοοῦσαν σαφῶς τὰ παρ'
ἑαυτοῦ σαφηνίζων φησίν· ἐγώ εἰμι ἡ ζωὴ ἀντὶ τοῦ οὐ δεῖ σε παρὰ ἄλλου με αἰτεῖν
οὐδὲ ἐλπίζειν μετὰ χρόνους μακροὺς ἔσεσθαι τοῦ σοῦ ἀδελφοῦ τὴν ἀνάστασιν·
πάρειμι γὰρ ἐγὼ ὁ πρὸς τοῦτο καλῶν. ζωὴ δέ ἐστι κατὰ φύσιν ὁ υἱὸς ὁ πάντα πρὸς τὸ
εἶναι καλῶν καὶ ἀνιστῶν μὲν ἅπαντας ἰδίᾳ δυνάμει, ἀνιστῶν δὲ οὐχ ἁπλῶς, ἀλλὰ
πρὸς ζωήν· διὰ γὰρ τοῦτο καὶ παρέζευξε τῷ τῆς ἀναστάσεως ὀνόματι τὴν ζωήν.
σημειωτέον δέ, ὅτι ζωὴν ἑαυτὸν ὁ κύριος καλεῖ, οὐ μὴν ζῷον ὥς τινες ἀσεβῶς
ἐτόλμησαν εἰπεῖν ἐπ' αὐτοῦ τοῦτο· ἄλλη γὰρ διαφορὰ ζῴου καὶ ζωῆς· τὸ μὲν γὰρ
ζῷον δεκτικόν ἐστι ζωῆς, ἡ δὲ ζωὴ παρεκτικὴ τοῦ ζῆν ὥσπερ ἡ σοφία καὶ ὁ σοφός. ὁ
μὲν λαμβάνει, ἡ δὲ δίδωσιν.
12 Jo 11, 25 3 Ὥσπερ γὰρ οἱ πάλαι ἅγιοι οὐκέτι ἔφθησαν τῇ ἀναστάσει καὶ ταῖς
τιμαῖς, ἀλλὰ ἀνέμειναν ἡμᾶς, οὕτω καὶ τοὺς πιστοὺς δεῖ ἀναμεῖναι τὴν κοινὴν
ἀνάστασιν, ἵνα καὶ <σὺν> τοῖς πάλαι ἁγίοις ἅπαντες ἀναστῶσιν. ὡς ἐκεῖνοι ἔμειναν
δι' ἡμᾶς μὴ λαβόντες τὰς ἐποφειλομένας τιμὰς διὰ τὸ κοινῇ ἀπολαῦσαι πάντας,
οὕτω καὶ ἡμεῖς μόνοι οὐκ ἀπολαύομεν τῆς ἀναστάσεως, ἵνα σὺν τοῖς πάλαι ἁγίοις
ἀναστάντες ἅμα τύχωμεν τοῦ ἀγαθοῦ.
13 Jo 11, 33 Ἔοικε γὰρ ἐννοῆσαι τὸ ἐν ψαλμοῖς εἰρημένον· ἔντειλαι, ὁ θεός,
τῇ δυνάμει σου, δυνάμωσον, ὁ θεός, τοῦτο, ὃ κατειργάσω ἐν ἡμῖν θεοπρεπῶς
νοοῦντες τὸ ἔντειλαι, εἰ καὶ λέγεται φωναῖς ἀνθρωπίναις ἐκ τῶν καθ' ἡμᾶς ἀνάγων
ἡμᾶς εἰς ἔννοιαν τὴν ὑπὲρ ἡμᾶς.
14 Jo 11, 33 Τὴν κατὰ τοῦ θανάτου δριμείαν ἀπειλὴν ἐμβρίμησιν ἐκάλεσεν ὁ
εὐαγγελιστής.
15 Jo 11, 38 Ὥσπερ οὖν, ὅτε ὁ Χριστὸς ἐπὶ τὸ τοῦ Λαζάρου μνημεῖον
πρόεισιν, γέγραπται, ὅτι Ἰησοῦς ἐμβριμώμενος ἑαυτῷ ἔρχεται εἰς τὸ μνημεῖον,
οὕτως οὖν ἐπ' αὐτοῦ νοοῦμεν τὴν ἐμβρίμησιν τὴν οἱονεὶ μετὰ κινήσεως τῆς κατ'
ἐξουσίαν θέλησιν.
16 Jo 11, 43–44 Θεοπρεπὲς καὶ βασιλικὸν τὸ κέλευσμα. διὰ δὲ τοῦτο κράζει
παρὰ τὸ σύνηθες ἑαυτῷ ὁ σωτήρ, ἵνα διὰ τοῦ ἐπὶ Λαζάρου τύπου τὴν καθόλου
ἀνάστασιν προδιατυπώσῃ, ποίῳ μέλλει γίνεσθαι τρόπῳ τῆς σάλπιγγος μέγα τι
ἠχούσης καὶ δυνάμει θεοῦ τῶν νεκρῶν ἐγειρομένων. οὐκ εὔχεται δὲ ὧδε οὐδὲ λέγει
τάδε λέγει κύριος, ἵνα μὴ ἐν οἰκέτου τάξει νοῆται, ἀλλ' ἐξουσιαστικῶς ὡς θεὸς καλεῖ
τὸν νεκρὸν ἀπὸ τοῦ μνήματος. ὁ δὲ εὐθέως ἐξῆλθεν, ἐπείπερ θεῷ ἐπιτάττοντι
ἀντιτεῖνον οὐδέν, ἵνα δείξῃ, ὅτι ἐν ἀτόμῳ καὶ ἐν ῥιπῇ ὀφθαλμοῦ μέλλει καὶ ἡ
πάντων γενέσθαι ἀνάστασις, καθὼς καὶ ἐπὶ Λαζάρου γέγονεν. δεδεμένος δὲ ἔξεισι
πρὸς πληροφορίαν τῶν ὁρώντων ὅλον ἀποσῴζων τὸ τῶν νεκρῶν σχῆμα, ἵνα μή
τινες εἴπωσι μηδὲ ὅλως τεθνάναι τὸν Λάζαρον.
17 Jo 12, 31 Ἄφθαρτοι γεγονότες οἱ ἄνθρωποι οὕτω δόξαν τῷ θεῷ
ἀναμάρτητοι ζητοῦσιν. πῶς δεῖ νοεῖν τὸ εἰρημένον καὶ οὕτως αὐτῶν νοῒ γεγόνασιν
καὶ παθῶν ἀλλότριοι μόνοις προσανέχοντες τοῖς ἀγαθοῖς; ἐπειδὴ δὲ ἑάλω πᾶς ὁ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

κόσμος καὶ ὑπέδυ τὸν τῆς δουλείας ζυγὸν λατρεύων τῇ κτίσει καὶ ἐδεσπόζετο
τυραννούμενος ὑπὸ τοῦ σατανᾶ ταῖς τῶν παθῶν πλεονεξίαις, εἰσήγαγεν ὁ θεὸς τὸν
νόμον διὰ τῶν ἁγίων ἀνδρῶν ὀλίγον ἡμῖν τῆς θεογνωσίας ἐπιλάμψας τὸ φῶς.
ἐπειδὴ δὲ δυσήκοοι ὄντες οἱ πολλοὶ οὐκ ἐπείθοντο διὰ τὴν συνέχουσαν πλάνην, οὐ
μόνης ἐχρῄζομεν τῆς παρὰ τῶν ἁγίων συμβουλῆς, ἀλλὰ καὶ τῆς ἄνωθεν εὐμενείας
καὶ χάριτος εὐκόλως ἐπὶ τὸ ἄμεινον ἡμᾶς μετατιθείσης. ἡγοῦντο οἱ ἅγιοι ὑπὲρ τῶν
ἐθνῶν καὶ εἰδωλολατρῶν τοὺς ὑπὸ τοῦ διαβόλου ἀδικηθέντας καὶ τῆς εὐθείας ὁδοῦ
ἀποπλανηθέντας καθάπερ ἐκ βίας. ἦν γὰρ τότε δυνατὸς καὶ δυσκαταγώνιστος τοῖς
ἀνθισταμένοις θεοῦ δικάζοντος ἐλευθερωθῆναι· ὡς γὰρ δίκαιος εἶχεν ἐλεῆσαι καὶ
τούτους ὡς ἀσθενεῖς καὶ ὑπὸ τῆς ἀφορήτου πλεονεξίας συντετριμμένους καὶ τὸν
ὠμῶς ἐπελθόντα τοῖς ἀσθενεστέροις τιμωρήσασθαι. ὁ οὖν Χριστὸς ἡ τοῦ θεοῦ καὶ
πατρὸς δικαιοσύνη φανεὶς ἤγαγεν ἡμᾶς εἰς τὸ φῶς τῆς θεογνωσίας διὰ τῆς πίστεως
δικαιώσας ἡμᾶς. ὁ οὖν τῆς τοῦ Χριστοῦ παρουσίας καιρὸς παροῦσαν ἤδη τὴν ἐπὶ τοῖς
ἔθνεσι κρίσιν καὶ δίκην ἐδείκνυεν· ἔμελλον γὰρ ἀπαλλάττεσθαι τῆς τοῦ διαβόλου
πλεονεξίας δικαίως αὐτοὺς ἐλεοῦντος τοῦ Χριστοῦ τοῦ δικαίου κριτοῦ. καὶ τοῦτο
σημαίνει ὁ λόγος· οὐ γὰρ νῦν ὁ κόσμος κατακρίνεται, ὅτε τοῦ δικαιοῦσθαι παρῆν ὁ
καιρός· οὕτω γὰρ νοοῦντες κἀκείνῳ τῷ λόγῳ οὐκ ἀντιμαχόμεθα τῷ λέγοντι· οὐ γὰρ
ἀπέστειλεν ὁ θεὸς τὸν υἱὸν εἰς τὸν κόσμον, ἵνα κρίνῃ τὸν κόσμον, ἀλλ' ἵνα σωθῇ ὁ
κόσμος δι' αὐτοῦ· οὐδὲ γάρ, ὥς φασί τινες, ὧδε τοὺς σταυρώσαντας τὸν κύριον
Ἰουδαίους κόσμον καλεῖ, τοὺς ὑπὲρ τῶν εἰς Χριστὸν τολμημάτων ἀπαιτηθέντας
δίκην· ὁ γὰρ τοῦ Χριστοῦ θάνατος τὴν πᾶσαν ἀνθρωπότητα δικαιοῖ. διὸ οὕτω
μᾶλλον ἀκόλουθον νοεῖν. ἀκολούθως οὖν τῇ νοήσει ταύτῃ ἔσται ἡ κρίσις κατὰ τοῦ
τὸν κόσμον ἠδικηκότος, οὐ μὴν κατὰ τοῦ τοῦ κόσμου τὴν ἀδικίαν ὑπομείναντος. καὶ
ὁ ἐπὶ τὸ σῶσαι ἡμᾶς ἐλθὼν πρεπόντως μᾶλλον τοῦτο ἐτέλεσε τὸν ἀδικήσαντα
τιμωρησάμενος, τὸν δὲ ἀδικηθέντα ἐλεήσας· ὅρος αὐτῆς ἐνεστώσης κρίσεως τὸ
ἐκβληθῆναι ἔξω τὸν ἄρχοντα τοῦ κόσμου τούτου. ἄρχοντα δὲ αὐτὸν καλεῖ οὐ κυρίως
ὄντα τοῦτο· μόνος γὰρ τῶν ἁπάντων βασιλεὺς ὁ θεός, ἀλλ' ὅτι ὑπὸ ἀσυνεσίας οἱ
ἄνθρωποι ἄρχοντα ἑαυτῶν τοῦτον ἐπεγράψαντο. ἔσχεν οὖν τὴν προσηγορίαν ἐκ τοῦ
συμβεβηκότος· οὕτω γὰρ καὶ ἡ ἁμαρτία λέγεται τῶν ἀσεβῶν βεβασιλευκέναι καίπερ
ἀσύστατόν τι οὖσα, ἐξ ὧν πράττοντες οἱ ἁμαρτωλοὶ δοκοῦσιν ἑαυτοὺς ταύτῃ
καθυποτάττειν. ἢ τῆς ἀρχῆς οὖν ἐκβάλλεται καταρραγείσης αὐτοῦ τῆς ἰσχύος ἢ τὸ
ἔξω δηλοῖ τὴν κόλασιν καὶ τὴν εἰς ᾅδου βάδισιν· ἐκεῖ γὰρ ὁ διάβολος ἐκβληθεὶς
δικαίως ἐπείπερ οὐ μόνον τοῖς ἀνθρώποις ἐπολέμησεν, ἀλλὰ καὶ τῷ πάντων θεῷ
Ἰησοῦ. διὸ καὶ πάλαι ἄπειρος ὢν δεσμῶν καὶ κολάσεως νῦν δίκας τῶν τολμημάτων
ἀπῃτεῖτο ὡς εἰς ἄκρον ἁπάσης δυσσεβείας ἐλθών.
18 Jo 12, 37–38 Πολλὰ λέγει τὰ κατὰ τῶν Ἰουδαίων ἐγκλήματα· πολλὰ γὰρ
σημεῖα οὐκ ἐδυσώπησαν αὐτούς, ἀλλὰ τρόπον τινὰ ἄτρεπτοι ἦσαν τῇ σκληροκαρδίᾳ
οὐδενὸς ἐλλείψαντος τῶν εἰς ἐπανόρθωσιν. εἰ δὲ καὶ δυσωπηθέντες οἱ Ἰουδαῖοι
ἐπίστευσαν, ἀλλ' οὖν ἀδύνατον ἦν ψεύσασθαι τὸ πνεῦμα τὸ ἅγιον τὸ διὰ τοῦ
προφήτου Ἠσαΐου λαλῆσαν περὶ τῆς αὐτῶν ἀπιστίας· οὔτε γὰρ τοῖς προφήταις
ἐπίστευσαν οὔτε γνώριμος γέγονε τοῖς Ἰουδαίοις ὁ Χριστός, ἡ δύναμις καὶ ὁ βραχίων
τοῦ πατρός. ὑπερβολικῶς δὲ εἶπεν τὸ οὐδεὶς διὰ τὸ ὀλίγους ἐκ πολλῶν σεσῷσθαι.
θέλων δὲ αὐτῶν τὴν ἄμετρον δυστροπίαν ἐλέγξαι ὁ εὐαγγελιστὴς προσέθηκε καὶ τὸ
ἔμπροσθεν αὐτῶν δεικνύς, ὅτι οὐδὲ οἷς ἑώρων ἐπίστευον.
19 Jo 12, 38 Βραχίων τοῦ θεοῦ τοῦ πατρὸς ὁ υἱός ἐστιν.
Jo 12, 41 Ἐξ ὧν ὧδε δηλοῖ τὸν υἱὸν τοσαύτην ἔχοντα δόξαν, ἣν εἶδεν
Ἠσαΐας, δῆλον, ὅτι οὐδεμία διαφορὰ πατρὸς καὶ υἱοῦ· οὐ γάρ ἐστί τι πλεῖον
ἄνθρωπον δύνασθαι θεωρῆσαι οὐδὲ ἕτερόν τι λέγει τούτου μεῖζον ἡ γραφή· τὰ γὰρ
Σεραφίμ, ἃ εἶδεν Ἠσαΐας ὑπὸ τὸν τοῦ θεοῦ θρόνον, ὑπεράνω ἐστὶ καὶ ἐντιμότερα τῶν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

ἀγγέλων πάντων. τὸ δὲ τοῦ θρόνου σημαίνει τὴν τοῦ δεσπότου φύσιν ἢ τὸ ἐν τάξει
τῇ βασιλικῇ καθέζεσθαι παρεστώτων τῶν ἐν σχήματι δουλείας τῆς κτίσεως.
21 Jo 12, 44 Ὃ λέγει, τοιοῦτόν ἐστιν. ὁ δυνάμενος ἐπιγνῶναι τίς ὁ διὰ τῆς
σαρκὸς ἐπιδημῶν καὶ τῆς νοητῆς καὶ καλλίονος ἐπιλαμβανόμενος θεωρίας οὗτος
εὑρίσκει μὲν ἀληθῶς ἐμέ. εὑρίσκει δὲ ἐν τῇ πρὸς ἐμὲ πίστει τὴν εἰς τὸν πατέρα.
ὅμοιον οὖν, ὡσεὶ ἔλεγεν οὐκ εἰς ἄνθρωπον πιστεύει ὁ πιστεύων εἰς ἐμέ, ἀλλ' εἰς θεοῦ
δύναμιν, ἐν ᾗ <ὁ> πατὴρ γινώσκεται καὶ πιστεύεται.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

