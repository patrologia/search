Fragmenta varia
Τοῦ αὐτοῦ (Ἀθανασίου) κατὰ Οὐαλεντίνου, τὰ πολλὰ συμφώνου ὄντος
τοῖς προδηλωθεῖσιν αἱρετικοῖς.
Πῶς λέγετε Χριστὸν οὐρανίαν σάρκα περιβεβλῆσθαι, καὶ οὐκ ἀνθρωπίνην, καίτοι
τοῦ Ἀποστόλου φάσκοντος· Ὁ ἁγιάζων καὶ οἱ ἁγιαζόμενοι ἐξ ἑνὸς πάντες; ∆ι' ἣν
αἰτίαν οὐκ ἐπαισχύνεται, φησὶν, ἀδελφοὺς αὐτοὺς καλεῖν, λέγων· Ἀπαγγελῶ τὸ
ὄνομά σου τοῖς ἀδελφοῖς μου. Ἐξ ἑνὸς δὲ, τίνος; Τοῦ Ἀδὰμ δηλονότι, κατὰ τὴν τοῦ
Λουκᾶ γενεαλογίαν· καὶ πάλιν· Ἐπεὶ τὰ παιδία κεκοινώνηκε σαρκὸς καὶ αἵματος, καὶ
αὐτὸς παραπλησίως ἐπέσχε τῶν αὐτῶν. Ἀναιρῶν δὲ καὶ τὴν ὑπόληψιν τῆς οὐρανίου
σαρκὸς, ἐπήγαγεν· Οὐ γὰρ δήπου ἀγγέλων ἐπιλαμβάνεται, ἀλλὰ σπέρματος Ἀβραάμ·
ὅθεν ὤφειλε κατὰ πάντα τοῖς ἀδελφοῖς ὁμοιωθῆναι. Καὶ πολλὰ τοιαῦτα. Ἀλλ' ὁ
Κύριος, φασὶν, εἴρηκεν ἐν τοῖς Εὐαγγελίοις· Οὐδεὶς ἀναβέβηκεν εἰς τὸν οὐρανὸν, εἰ
μὴ ὁ ἐκ τοῦ οὐρανοῦ καταβὰς, ὁ Υἱὸς τοῦ ἀνθρώπου, ὁ ὢν ἐν τῷ οὐρανῷ· καὶ ὁ
Ἀπόστολος πάλιν· Ὁ πρῶτος ἄνθρωπος ἐκ γῆς χοϊκός· ὁ δεύτερος ἄνθρωπος, ὁ
Κύριος, ἐξ οὐρανοῦ οὐράνιος. Ἰδοὺ γὰρ ἀμφοτέρωθεν οὐράνιος ἄνθρωπος
ὀνομάζεται. Πρὸς οὓς ἐροῦμεν, ὅτι ἐπεὶ διπλοῦς ὢν ταῖς φύσεσιν ὁ Χριστὸς,
μοναδικός ἐστι τὴν ὑπόστασιν, ἰδιοποιεῖται τὰ τῆς ἑκατέρας φύσεως. Καὶ καθάπερ ὁ
Ἀπόστολος εἰπὼν περὶ τῶν Χριστοκτόνων ὅτι· Εἰ ἔγνωσαν, οὐκ ἂν τὸν Κύριον τῆς
δόξης ἐσταύρωσαν, οὐ τὴν θεότητα πεπονθέναι φησὶν, ἥτις ἐστὶ Κύριος τῆς δόξης,
ἀλλὰ τὸ πρόσλημμα· οὕτω καὶ ἐπὶ τῶν ῥητῶν τούτων αὐτὸς μὲν ὁ Υἱὸς καὶ Θεὸς
πεπονθέναι λέγεται, διὰ τὴν μίαν ὑπόστασιν τῆς θεότητος, ἥτις καὶ τῇ ἀνθρωπίνῃ
φύσει γέγονεν ὑπόστασις· πεπονθέναι δὲ κατὰ τὴν ἀνθρωπίνην φύσιν αὐτοῦ, καὶ ἐκ
τοῦ οὐρανοῦ μὲν καταβεβηκέναι, καὶ ἐν τῷ οὐρανῷ εἶναι, κατὰ τὴν ὑπόστασιν τῆς
θεότητος· Υἱὸν δὲ ἀνθρώπου εἶναι κατὰ τὴν ὕστερον προσληφθεῖσαν φύσιν, μετὰ
τὴν ἐξ οὐρανοῦ κάθοδον. Καὶ πάλιν ὁ αὐτὸς Κύριος διὰ τὴν μίαν ὑπόστασιν·
δεύτερος μὲν ἄνθρωπος κατὰ τὴν ἡνωμένην ἀνθρωπότητα, ἐξ οὐρανοῦ δὲ κατὰ τὴν
θεότητα· εἰ γὰρ μὴ νοήσωμεν ἑκάστην οἰκείως, ἐναντιολογίαις ὑποβαλοῦμεν τὸν
Ἀπόστολον. Πῶς γὰρ κατὰ πάντα ὅμοιος ἡμῖν τοῖς γηΐνοις; Πῶς δὲ ἀδελφὸς ἡμῶν, ὁ
σάρκα οὐρανίαν περιβεβλημένος; Καὶ ἄλλα δὲ μυρία ἄτοπα ἕψεται.

 
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

