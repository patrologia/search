Generalis elementaria introductio Fragmenta
470 ἔλεγχος ἀνδρὸς ἤθους ἡ μετὰ τῶν τοιῶνδε συνουσία. ὅτε γὰρ φαῦλος, ἐξ
ἀνάγκης τοῖς ὁμοίοις συνέσται· ὅτε αὖ πάλιν σώφρων καὶ σοφὸς, τοῖς τὰ αὐτὰ αὐτῷ
μετιοῦσιν. 471 ὃν τρόπον ἡ τῶν ὀφθαλμῶν φύσις ἔχει μὲν ἐν ἑαυτῇ τὴν ὁρατικὴν
δύναμιν, οὐχ ἱκανὴ δὲ καθ' ἑαυτὴν πέφυκεν πρὸς τὴν τῶν ὁρατικῶν κατάληψιν, μὴ
οὐχὶ ἑτέρου τινὸς ἔξωθεν φωτίζοντος καὶ συνυπουργοῦντος τῇ τῶν ὀφθαλμῶν
χρήσει καὶ τῷ συμβαλλομένῳ φωτὶ πρὸς τὴν τῶν αἰσθητῶν θέαν, οὕτως καὶ ἡ νοερὰ
καὶ λογικὴ φύσις ἔχει πρὸς τὸν ἱερὸν τοῦ θεοῦ λόγον.
472 ἡ φύσις αὕτη τὸ τῶν ἀνθρώπων κατέσπειρεν γένει τά τε τῆς ἀρετῆς καὶ
τὰ τῆς κακίας σπέρματα γινώσκειν, θατέρου τε τούτων συνεχώρησεν μετέχειν τῷ
λογισμῷ, αὐτοκράτορα καὶ κριτὴν ἡγεμόνα τε καὶ κύριον αὐτὸν ἑαυτοῦ
καταστήσασα· ὡς οἷα δὴ ὑπὸ τῆς φύσεως τὰ τῆς ἀρετῆς καὶ τὰ τῆς κακίας
προτερήματα ἔχων ἐν ἑαυτῷ οὐκ ἂν δύναιτο, κἂν εἰς αὐτὸν τῆς κακίας βυθὸν
καταπέσοι ποτὲ, μὴ οὐχὶ σεμνύειν τὴν ἀρετὴν, καταπταῖσον τὸ συνειδὸς, εἰ
προέλοιτο τὸ χεῖρον. καὶ τούτων ἀπόδειξις καὶ ἔλεγχος ἐναργὴς τὸ μηδένα τολμᾶν
τῆς ἑαυτοῦ κακίας γενέσθαι κατήγορον ἀλλ' ἐκ τῶν ἐναντίων ὡς ἡ δύναμις
ἐπικρύπτειν καὶ σκέπειν ἅπερ δὴ ἂν παρὰ τὸ καθῆκον λαθραίως ἐπιχειρῇ. καὶ αὐτὴ
δὲ ἡ ἑκάστου διάθεσις μὴ ὁμοίως διακειμένου πρός τε τοὺς εὐεργετοῦντας καὶ πρὸς
τοὺς βλάπτειν αὐτὸν ἐπιχειροῦντας αὐτομαθὴς διδάσκαλος γίνεται ἑκάστῳ τῆς περὶ
ἀγαθῶν καὶ κακῶν ἐπιστήμης, ὁ τῆς φύσεως νόμος, ταῦτά τινας δρᾶν ἑτέροις
ἀπαγορεύων, ἅπερ τις αὐτὸς παρ' ἑτέρων οὐκ ἂν ἕλοιτο παθεῖν. 473 ἐνδέχεται μὲν, ὦ
παιδίον, λέγειν μὲν εὖ, φαῦλον δὲ ὑπάρχειν· πράσσοντα δὲ εὖ φαῦλον εἶναι
ἀδύνατον.

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

