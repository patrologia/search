Τοῦ αὐτοῦ ἁγίου
Ἰουστίνον Φιλοσόφου καὶ Μάρτυρος
Ἀπολογία ὑπὲρ Χριστιανῶν
πρὸς τὴν Ῥωμαίων Σύγκλητον.
Καὶ τὰ χθὲς δὲ καὶ πρώην ἐν τῇ πόλει
ὑμῶν γενόμενα ἐπὶ Oὐρβίκου, ὦ Ῥωμαῖοι, καὶ τὰ πανταχοῦ
ὁμοίως ὑπὸ τῶν ἡγουμένων ἀλόγως πραττόμενα ἐξηνάγκασέ με
ὑπὲρ ὑμῶν, ὁμοιοπαθῶν ὄντων καὶ ἀδελφῶν, κἂν ἀγνοῆτε καὶ
μὴ θέλητε διὰ τὴν δόξαν τῶν νομιζομένων ἀξιωμάτων, τὴν
τῶνδε τῶν λόγων σύνταξιν ποιήσασθαι.
πανταχοῦ γὰρ, ὃς
ἂν σωφρονίζηται ὑπὸ πατρὸς ἢ γείτονος ἢ τέκνου ἢ φίλου ἢ
ἀδελφοῦ ἢ ἀνδρὸς ἢ γυναικὸς κατ' ἔλλειψιν, χωρὶς τῶν πεισθέν
των τοὺς ἀδίκους καὶ ἀκολάστους ἐν αἰωνίῳ πυρὶ κολασθή
σεσθαι, τοὺς δ' ἐναρέτους καὶ ὁμοίως Χριστῷ βιώσαντας ἐν
ἀπαθείᾳ συγγενέσθαι τῷ θεῷ (λέγομεν δὲ τῶν γενομένων
Χριστιανῶν), διὰ τὸ δυσμετάθετον καὶ φιλήδονον καὶ
δυσκίνητον πρὸς τὸ καλὸν ὁρμῆσαι, καὶ οἱ φαῦλοι δαίμονες,
ἐχθραίνοντες ἡμῖν καὶ τοὺς τοιούτους δικαστὰς ἔχοντες ὑποχει
ρίους καὶ λατρεύοντας, ὡς οὖν ἄρχοντας δαιμονιῶντας, φονεύειν
ἡμᾶς παρασκευάζουσιν.
ὅπως δὲ καὶ ἡ αἰτία τοῦ παντὸς
γενομένου ἐπὶ Oὐρβίκου φανερὰ ὑμῖν γένηται, τὰ πεπραγμένα
ἀπαγγελῶ.
Γυνή τις συνεβίου ἀνδρὶ ἀκολασταίνοντι, ἀκολασταί
νουσα καὶ αὐτὴ πρότερον.
ἐπεὶ δὲ τὰ τοῦ Χριστοῦ διδάγ
ματα ἔγνω, αὐτὴ ἐσωφρονίσθη καὶ τὸν ἄνδρα ὁμοίως σω
φρονεῖν πείθειν ἐπειρᾶτο, τὰ διδάγματα ἀναφέρουσα, τήν τε
μέλλουσαν τοῖς οὐ σωφρόνως καὶ μετὰ λόγου ὀρθοῦ βιοῦσιν
ἔσεσθαι ἐν αἰωνίῳ πυρὶ κόλασιν ἀπαγγέλλουσα.
ὁ δὲ ταῖς
αὐταῖς ἀσελγείαις ἐπιμένων ἀλλοτρίαν διὰ τῶν πράξεων ἐποιεῖτο
τὴν γαμετήν.
ἀσεβὲς γὰρ ἡγουμένη τὸ λοιπὸν ἡ γυνὴ συγκα
τακλίνεσθαι ἀνδρί, παρὰ τὸν τῆς φύσεως νόμον καὶ παρὰ τὸ
δίκαιον πόρους ἡδονῆς ἐκ παντὸς πειρωμένῳ ποιεῖσθαι, τῆς συ
ζυγίας χωρισθῆναι ἐβουλήθη.
καὶ ἐπειδὴ ἐξεδυσωπεῖτο ὑπὸ
τῶν αὐτῆς, ἔτι προσμένειν συμβουλευόντων, ὡς εἰς ἐλπίδα με
ταβολῆς ἥξοντός ποτε τοῦ ἀνδρός, βιαζομένη ἑαυτὴν ἐπέμενεν.
ἐπειδὴ δὲ ὁ ταύτης ἀνὴρ εἰς τὴν Ἀλεξάνδρειαν πορευθεὶς χα
λεπώτερα πράττειν ἀπηγγέλθη, ὅπως μὴ κοινωνὸς τῶν ἀδικημά
των καὶ ἀσεβημάτων γένηται, μένουσα ἐν τῇ συζυγίᾳ καὶ ὁμο
δίαιτος καὶ ὁμόκοιτος γινομένη, τὸ λεγόμενον παρ' ὑμῖν ·επού
διον δοῦσα ἐχωρίσθη.
ὁ δὲ καλὸς κἀγαθὸς ταύτης ἀνήρ, δέον
1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

αὐτὸν χαίρειν ὅτι ἃ πάλαι μετὰ τῶν ὑπηρετῶν καὶ τῶν μισθο
φόρων εὐχερῶς ἔπραττε, μέθαις χαίρουσα καὶ κακίᾳ πάσῃ, τού
των μὲν τῶν πράξεων πέπαυτο καὶ αὐτὸν τὰ αὐτὰ παύσασθαι
πράττοντα ἐβούλετο, μὴ βουλομένου ἀπαλλαγείσης κατηγορίαν
πεποίηται, λέγων αὐτὴν Χριστιανὴν εἶναι.
καὶ ἡ μὲν βιβλί
διόν σοι τῷ αὐτοκράτορι ἀνέδωκεν, πρότερον συγχωρηθῆναι αὐτῇ
διοικήσασθαι τὰ ἑαυτῆς ἀξιοῦσα, ἔπειτα ἀπολογήσασθαι περὶ
τοῦ κατηγορήματος μετὰ τὴν τῶν πραγμάτων αὐτῆς διοίκησιν·
καὶ συνεχώρησας τοῦτο.
ὁ δὲ ταύτης ποτὲ ἀνήρ, πρὸς ἐκεί
νην μὲν μὴ δυνάμενος τὰ νῦν ἔτι λέγειν, πρὸς Πτολεμαῖόν τινα,
ὃν Oὔρβικος ἐκολάσατο, διδάσκαλον ἐκείνης τῶν Χριστιανῶν
μαθημάτων γενόμενον, ἐτράπετο διὰ τοῦδε τοῦ τρόπου.
ἑκα
τόνταρχον εἰς δεσμὰ ἐμβαλόντα τὸν Πτολεμαῖον, φίλον αὐτῷ
ὑπάρχοντα, ἔπεισε λαβέσθαι τοῦ Πτολεμαίου καὶ ἀνερωτῆσαι
εἰ, αὐτὸ τοῦτο μόνον, Χριστιανός ἐστιν.
καὶ τὸν Πτολε
μαῖον, φιλαλήθη ἀλλ' οὐκ ἀπατηλὸν οὐδὲ ψευδολόγον τὴν γνώ
μην ὄντα ὁμολογήσαντα ἑαυτὸν εἶναι Χριστιανόν, ἐν δεσμοῖς
γενέσθαι ὁ ἑκατόνταρχος πεποίηκεν, καὶ ἐπὶ πολὺν χρόνον ἐν
τῷ δεσμωτηρίῳ ἐκολάσατο.
τελευταῖον δέ, ὅτε ἐπὶ Oὔρβι
κον ἤχθη ὁ ἄνθρωπος, ὁμοίως αὐτὸ τοῦτο μόνον ἐξητάσθη, εἰ
εἴη Χριστιανός.
καὶ πάλιν, τὰ καλὰ ἑαυτῷ συνεπιστάμενος
διὰ τὴν ἀπὸ τοῦ Χριστοῦ διδαχήν, τὸ διδασκαλεῖον τῆς θείας
ἀρετῆς ὡμολόγησεν.
ὁ γὰρ ἀρνούμενος ὁτιοῦν ἢ κατεγνω
κὼς τοῦ πράγματος ἔξαρνος γίνεται, ἢ ἑαυτὸν ἀνάξιον ἐπιστά
μενος καὶ ἀλλότριον τοῦ πράγματος τὴν ὁμολογίαν φεύγει· ὧν
οὐδὲν πρόσεστιν τῷ ἀληθινῷ Χριστιανῷ.
καὶ τοῦ Oὐρβίκου
κελεύσαντος αὐτὸν ἀπαχθῆναι Λούκιός τις, καὶ αὐτὸς ὢν Χριστια
νός, ὁρῶν τὴν ἀλόγως οὕτως γενομένην κρίσιν, πρὸς τὸν Oὔρ
βικον ἔφη·
Τίς ἡ αἰτία; τοῦ μήτε μοιχὸν μήτε πόρνον
μήτε ἀνδροφόνον μήτε λωποδύτην μήτε ἅρπαγα μήτε ἁπλῶς
ἀδίκημά τι πράξαντα ἐλεγχόμενον, ὀνόματος δὲ Χριστιανοῦ
προσωνυμίαν ὁμολογοῦντα τὸν ἄνθρωπον τοῦτον ἐκολάσω; οὐ
πρέποντα Eὐσεβεῖ αὐτοκράτορι οὐδὲ φιλοσόφου Καίσαρος παιδὶ
οὐδὲ τῇ ἱερᾷ συγκλήτῳ κρίνεις, ὦ Oὔρβικε.
καὶ ὃς οὐδὲν
ἄλλο ἀποκρινάμενος καὶ πρὸς τὸν Λούκιον ἔφη· ∆οκεῖς μοι καὶ
σὺ εἶναι τοιοῦτος.
καὶ Λουκίου φήσαντος· Μάλιστα, πάλιν
καὶ αὐτὸν ἀπαχθῆναι ἐκέλευσεν.
ὁ δὲ καὶ χάριν εἰδέναι
2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ὡμολόγει, πονηρῶν δεσποτῶν τῶν τοιούτων ἀπηλλάχθαι γι
νώσκων καὶ πρὸς τὸν πατέρα καὶ βασιλέα τῶν οὐρανῶν πο
ρεύεσθαι.
καὶ ἄλλος δὲ τρίτος ἐπελθὼν κολασθῆναι προσ
ετιμήθη.
Κἀγὼ οὖν προσδοκῶ ὑπό τινος τῶν
ὠνομασμένων ἐπιβουλευθῆναι καὶ ξύλῳ ἐμπαγῆναι, ἢ κἂν
ὑπὸ Κρίσκεντος τοῦ φιλοψόφου καὶ φιλοκόμπου.
οὐ γὰρ
φιλόσοφον εἰπεῖν ἄξιον τὸν ἄνδρα, ὅς γε περὶ ἡμῶν ἃ μὴ
ἐπίσταται δημοσίᾳ καταμαρτυρεῖ, ὡς ἀθέων καὶ ἀσεβῶν Χριστια
νῶν ὄντων, πρὸς χάριν καὶ ἡδονὴν τῶν πολλῶν τῶν πεπλανη
μένων ταῦτα πράττων.
εἴτε γὰρ μὴ ἐντυχὼν τοῖς τοῦ Χριστοῦ
διδάγμασι κατατρέχει ἡμῶν, παμπόνηρός ἐστι καὶ ἰδιωτῶν πολὺ
χείρων, οἳ φυλάττονται πολλάκις περὶ ὧν οὐκ ἐπίστανται διαλέ
γεσθαι καὶ ψευδομαρτυρεῖν· ἢ εἰ ἐντυχών, μὴ συνῆκε τὸ ἐν
αὐτοῖς μεγαλεῖον, ἢ συνείς, πρὸς τὸ μὴ ὑποπτευθῆναι τοιοῦτος
ταῦτα ποιεῖ, πολὺ μᾶλλον ἀγεννὴς καὶ παμπόνηρος, ἰδιωτικῆς
καὶ ἀλόγου δόξης καὶ φόβου ἐλάττων ὤν.
καὶ γὰρ προθέντα
με καὶ ἐρωτήσαντα αὐτὸν ἐρωτήσεις τινὰς τοιαύτας καὶ μαθεῖν
καὶ ἐλέγξαι, ὅτι ἀληθῶς μηδὲν ἐπίσταται, εἰδέναι ὑμᾶς βούλομαι.
καὶ ὅτι ἀληθῆ λέγω, εἰ μὴ ἀνηνέχθησαν ἡμῖν αἱ
κοινωνίαι τῶν λόγων, ἕτοιμος καὶ ἐφ' ὑμῶν κοινωνεῖν τῶν ἐρω
τήσεων πάλιν· βασιλικὸν δ' ἂν καὶ τοῦτο ἔργον εἴη.
εἰ δὲ
καὶ ἐγνώσθησαν ὑμῖν αἱ ἐρωτήσεις μου καὶ αἱ ἐκείνου ἀποκρί
σεις, φανερὸν ὑμῖν ἐστιν ὅτι οὐδὲν τῶν ἡμετέρων ἐπίσταται·
ἢ εἰ καὶ ἐπίσταται, διὰ τοὺς ἀκούοντας δὲ οὐ τολμᾷ λέγειν,
ὁμοίως Σωκράτει ὡς προέφην, οὐ φιλόσοφος ἀλλὰ φιλόδοξος
ἀνὴρ δείκνυται, ὅς γε μηδὲ τὸ Σωκρατικὸν ἀξιέραστον ὂν τιμᾷ·
Ἀλλ' οὔτι γε πρὸ τῆς ἀληθείας τιμητέος ἀνήρ.
ἀδύνατον δὲ
Κυνικῷ, ἀδιάφορον τὸ τέλος προθεμένῳ, τὸ ἀγαθὸν εἰδέναι
πλὴν ἀδιαφορίας.
Ὅπως δὲ μή τις εἴπῃ· Πάντες οὖν ἑαυ
τοὺς φονεύσαντες πορεύεσθε ἤδη παρὰ τὸν θεὸν καὶ ἡμῖν πράγ
ματα μὴ παρέχετε, –ἐρῶ δι' ἣν αἰτίαν τοῦτο οὐ πράττομεν,
καὶ δι' ἣν ἐξεταζόμενοι ἀφόβως ὁμολογοῦμεν.
οὐκ
εἰκῆ τὸν κόσμον πεποιηκέναι τὸν θεὸν δεδιδάγμεθα, ἀλλ' ἢ διὰ
τὸ ἀνθρώπειον γένος· χαίρειν τε τοῖς τὰ προσόντα αὐτῷ μι
μουμένοις προέφημεν, ἀπαρέσκεσθαι δὲ τοῖς τὰ φαῦλα ἀσπα
ζομένοις ἢ λόγῳ ἢ ἔργῳ.
εἰ οὖν πάντες ἑαυτοὺς φονεύ
σομεν, τοῦ μὴ γεννηθῆναί τινα καὶ μαθητευθῆναι εἰς τὰ θεῖα
διδάγματα, ἢ καὶ μὴ εἶναι τὸ ἀνθρώπειον γένος, ὅσον ἐφ' ἡμῖν,
αἴτιοι ἐσόμεθα, ἐναντίον τῇ τοῦ θεοῦ βουλῇ καὶ αὐτοὶ ποιοῦντες,
3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἐὰν τοῦτο πράξωμεν.
ἐξεταζόμενοι δὲ οὐκ ἀρνούμεθα διὰ τὸ
συνεπίστασθαι ἑαυτοῖς μηδὲν φαῦλον, ἀσεβὲς δὲ ἡγούμενοι μὴ
κατὰ πάντα ἀληθεύειν, ὃ καὶ φίλον τῷ θεῷ γινώσκομεν, ὑμᾶς
δὲ καὶ τῆς ἀδίκου προλήψεως ἀπαλλάξαι νῦν σπεύδοντες.
Eἰ δέ τινα ὑπέλθοι καὶ ἡ ἔννοια αὕτη ὅτι, εἰ θεὸν
ὡμολογοῦμεν βοηθόν, οὐκ ἄν, ὡς λέγομεν, ὑπὸ ἀδίκων ἐκρα
τούμεθα καὶ ἐτιμωρούμεθα, καὶ τοῦτο διαλύσω.
ὁ θεὸς τὸν
πάντα κόσμον ποιήσας καὶ τὰ ἐπίγεια ἀνθρώποις ὑποτάξας καὶ
τὰ οὐράνια στοιχεῖα εἰς αὔξησιν καρπῶν καὶ ὡρῶν μεταβολὰς
κοσμήσας καὶ θεῖον τούτοις νόμον τάξας, ἃ καὶ αὐτὰ δι' ἀν
θρώπους φαίνεται πεποιηκὼς, τὴν μὲν τῶν ἀνθρώπων καὶ τῶν
ὑπὸ τὸν οὐρανὸν πρόνοιαν ἀγγέλοις, οὓς ἐπὶ τούτοις ἔταξε,
παρέδωκεν.
οἱ δ' ἄγγελοι, παραβάντες τήνδε τὴν
τάξιν, γυναικῶν μίξεσιν ἡττήθησαν καὶ παῖδας ἐτέκνωσαν, οἵ
εἰσιν οἱ λεγόμενοι δαίμονες.
καὶ προσέτι λοιπὸν τὸ ἀνθρώ
πειον γένος ἑαυτοῖς ἐδούλωσαν· τὰ μὲν διὰ μαγικῶν γραφῶν,
τὰ δὲ διὰ φόβων καὶ τιμωριῶν, ὧν ἐπέφερον, τὰ δὲ διὰ δι
δαχῆς θυμάτων καὶ θυμιαμάτων καὶ σπονδῶν, ὧν ἐνδεεῖς γε
γόνασι μετὰ τὸ πάθεσιν ἐπιθυμιῶν δουλωθῆναι· καὶ εἰς ἀνθρώ
πους φόνους, πολέμους, μοιχείας, ἀκολασίας καὶ πᾶσαν κακίαν
ἔσπειραν.
ὅθεν καὶ ποιηταὶ καὶ μυθολόγοι, ἀγνοοῦντες
τοὺς ἀγγέλους καὶ τοὺς ἐξ αὐτῶν γεννηθέντας δαίμονας ταῦτα
πρᾶξαι εἰς ἄρρενας καὶ θηλείας καὶ πόλεις καὶ ἔθνη, ἅπερ συνέ
γραψαν, εἰς αὐτὸν τὸν θεὸν καὶ τοὺς ὡς ἀπ' αὐτοῦ σπορᾷ γε
νομένους υἱοὺς καὶ τῶν λεχθέντων ἐκείνου ἀδελφῶν καὶ τέκνων
ὁμοίως τῶν ἀπ' ἐκείνων, Ποσειδῶνος καὶ Πλούτωνος, ἀνήνεγκαν.
ὀνόματι γὰρ ἕκαστον, ὅπερ ἕκαστος ἑαυτῷ τῶν ἀγγέλων καὶ
τοῖς τέκνοις ἔθετο, προσηγόρευσαν.
Ὄνομα δὲ τῷ πάντων πατρὶ θετόν, ἀγεννήτῳ ὄντι,
οὐκ ἔστιν· ᾧ γὰρ ἂν καὶ ὄνομά τι προσαγορεύηται, πρεσβύ
τερον ἔχει τὸν θέμενον τὸ ὄνομα.
τὸ δὲ πατὴρ καὶ θεὸς
καὶ κτίστης καὶ κύριος καὶ δεσπότης οὐκ ὀνόματά ἐστιν, ἀλλ'
ἐκ τῶν εὐποιϊῶν καὶ τῶν ἔργων προσρήσεις.
ὁ δὲ υἱὸς ἐκεί
νου, ὁ μόνος λεγόμενος κυρίως υἱός, ὁ λόγος πρὸ τῶν ποιη
μάτων καὶ συνὼν καὶ γεννώμενος, ὅτε τὴν ἀρχὴν δι' αὐτοῦ πάντα
ἔκτισε καὶ ἐκόσμησε, Χριστὸς μὲν κατὰ τὸ κεχρῖσθαι καὶ κοσ
μῆσαι τὰ πάντα δι' αὐτοῦ τὸν θεὸν λέγεται, ὄνομα καὶ αὐτὸ
περιέχον ἄγνωστον σημασίαν, ὃν τρόπον καὶ τὸ θεὸς προσαγό
ρευμα οὐκ ὄνομά ἐστιν, ἀλλὰ πράγματος δυσεξηγήτου ἔμφυτος
τῇ φύσει τῶν ἀνθρώπων δόξα.
Ἰησοῦς δὲ καὶ ἀνθρώπου καὶ
σωτῆρος ὄνομα καὶ σημασίαν ἔχει.
4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

καὶ γὰρ καὶ ἄνθρωπος, ὡς
προέφημεν, γέγονε κατὰ τὴν τοῦ θεοῦ καὶ πατρὸς βουλὴν ἀπο
κυηθεὶς ὑπὲρ τῶν πιστευόντων ἀνθρώπων καὶ ἐπὶ καταλύσει
τῶν δαιμόνων· καὶ νῦν ἐκ τῶν ὑπ' ὄψιν γινομένων μαθεῖν δύ
νασθε.
δαιμονιολήπτους γὰρ πολλοὺς κατὰ πάντα τὸν κόσμον
καὶ ἐν τῇ ὑμετέρᾳ πόλει πολλοὶ τῶν ἡμετέρων ἀνθρώπων, τῶν
Χριστιανῶν, ἐπορκίζοντες κατὰ τοῦ ὀνόματος Ἰησοῦ Χριστοῦ,
τοῦ σταυρωθέντος ἐπὶ Ποντίου Πιλάτου, ὑπὸ τῶν ἄλλων πάν
των ἐπορκιστῶν καὶ ἐπᾳστῶν καὶ φαρμακευτῶν μὴ ἰαθέντας,
ἰάσαντο καὶ ἔτι νῦν ἰῶνται, καταργοῦντες καὶ ἐκδιώκοντες τοὺς
κατέχοντας τοὺς ἀνθρώπους δαίμονας.
Ὅθεν καὶ ἐπιμένει ὁ θεὸς τὴν σύγχυσιν καὶ κατά
λυσιν τοῦ παντὸς κόσμου μὴ ποιῆσαι, ἵνα καὶ οἱ φαῦλοι ἄγγελοι
καὶ δαίμονες καὶ ἄνθρωποι μηκέτι ὦσι, διὰ τὸ σπέρμα τῶν
Χριστιανῶν, ὃ γινώσκει ἐν τῇ φύσει ὅτι αἴτιόν ἐστιν.
ἐπεὶ
εἰ μὴ τοῦτο ἦν, οὐκ ἂν οὐδὲ ὑμῖν ταῦτα ἔτι ποιεῖν καὶ ἐνερ
γεῖσθαι ὑπὸ τῶν φαύλων δαιμόνων δυνατὸν ἦν, ἀλλὰ
τὸ πῦρ τὸ τῆς κρίσεως κατελθὸν ἀνέδην πάντα διέκρινεν, ὡς
καὶ πρότερον ὁ κατακλυσμὸς μηδένα λιπὼν ἀλλ' ἢ τὸν μόνον
σὺν τοῖς ἰδίοις παρ' ἡμῖν καλούμενον Νῶε, παρ' ὑμῖν δὲ ∆ευ
καλίωνα, ἐξ οὗ πάλιν οἱ τοσοῦτοι γεγόνασιν, ὧν οἱ μὲν φαῦλοι,
οἱ δὲ σπουδαῖοι.
οὕτω γὰρ ἡμεῖς τὴν ἐκπύρωσίν φαμεν γε
νήσεσθαι, ἀλλ' οὐχ, ὡς οἱ Στωϊκοί, κατὰ τὸν τῆς εἰς ἄλληλα
πάντων μεταβολῆς λόγον, ὃ αἴσχιστον ἐφάνη· ἀλλ' οὐδὲ καθ'
εἱμαρμένην πράττειν τοὺς ἀνθρώπους ἢ πάσχειν τὰ γινόμενα,
ἀλλὰ κατὰ μὲν τὴν προαίρεσιν ἕκαστον κατορθοῦν ἢ ἁμαρτάνειν,
καὶ κατὰ τὴν τῶν φαύλων δαιμόνων ἐνέργειαν τοὺς σπουδαίους,
οἷον Σωκράτην καὶ τοὺς ὁμοίους, διώκεσθαι καὶ ἐν δεσμοῖς
εἶναι, Σαρδανάπαλον δὲ καὶ Ἐπίκουρον καὶ τοὺς ὁμοίους ἐν
ἀφθονίᾳ καὶ δόξῃ δοκεῖν εὐδαιμονεῖν.
ὃ μὴ νοήσαντες οἱ
Στωϊκοὶ καθ' εἱμαρμένης ἀνάγκην πάντα γίνεσθαι ἀπεφήναντο.
ἀλλ' ὅτι αὐτεξούσιον τό τε τῶν ἀγγέλων γένος καὶ τῶν ἀν
θρώπων τὴν ἀρχὴν ἐποίησεν ὁ θεός, δικαίως ὑπὲρ ὧν ἂν πλημμε
λήσωσι τὴν τιμωρίαν ἐν αἰωνίῳ πυρὶ κομίσονται.
γεννητοῦ
δὲ παντὸς ἥδε ἡ φύσις, κακίας καὶ ἀρετῆς δεκτικὸν εἶναι· οὐ
γὰρ ἂν ἦν ἐπαινετὸν οὐδὲν αὐτῶν, εἰ οὐκ ἦν ἐπ' ἀμφότερα τρέ
πεσθαι καὶ δύναμιν εἶχε.
δεικνύουσι δὲ τοῦτο καὶ οἱ παντα
χοῦ κατὰ λόγον τὸν ὀρθὸν νομοθετήσαντες καὶ φιλο
σοφήσαντες ἄνθρωποι ἐκ τοῦ ὑπαγορεύειν τάδε μὲν πράττειν,
τῶνδε δὲ ἀπέχεσθαι.
καὶ οἱ Στωϊκοὶ φιλόσοφοι ἐν τῷ περὶ
ἠθῶν λόγῳ τὰ αὐτὰ τιμῶσι καρτερῶς, ὡς δηλοῦσθαι ἐν τῷ περὶ
ἀρχῶν καὶ ἀσωμάτων λόγῳ οὐκ εὐοδοῦν αὐτούς.
5
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

εἴτε γὰρ
καθ' εἱμαρμένην φήσουσι τὰ γινόμενα πρὸς ἀνθρώπων γίνεσθαι,
ἢ μηδὲν εἶναι θεὸν παρὰ τρεπόμενα καὶ ἀλλοιούμενα καὶ ἀνα
λυόμενα εἰς τὰ αὐτὰ ἀεί, φθαρτῶν μόνων φανήσονται κατάληψιν
ἐσχηκέναι καὶ αὐτὸν τὸν θεὸν διά τε τῶν μερῶν καὶ διὰ τοῦ
ὅλου ἐν πάσῃ κακίᾳ γινόμενον ἢ μηδὲν εἶναι κακίαν μηδ' ἀρε
τήν· ὅπερ καὶ παρὰ πᾶσαν σώφρονα ἔννοιαν καὶ λόγον καὶ
νοῦν ἐστι.
Καὶ τοὺς ἀπὸ τῶν Στωϊκῶν δὲ δογμάτων, ἐπειδὴ
κἂν τὸν ἠθικὸν λόγον κόσμιοι γεγόνασιν, ὡς καὶ ἔν τισιν οἱ
ποιηταὶ, διὰ τὸ ἔμφυτον παντὶ γένει ἀνθρώπων σπέρμα τοῦ
λόγου, μεμισῆσθαι καὶ πεφονεῦσθαι οἴδαμεν· Ἡράκλειτον μέν,
ὡς προέφημεν, καὶ Μουσώνιον δὲ ἐν τοῖς καθ' ἡμᾶς καὶ ἄλλους
οἴδαμεν.
ὡς γὰρ ἐσημάναμεν, πάντας τοὺς κἂν ὁπωσδήποτε
κατὰ λόγον βιοῦν σπουδάζοντας καὶ κακίαν φεύγειν μισεῖσθαι
ἀεὶ ἐνήργησαν οἱ δαίμονες.
οὐδὲν δὲ θαυμαστόν, εἰ τοὺς
οὐ κατὰ σπερματικοῦ λόγου μέρος, ἀλλὰ κατὰ τὴν τοῦ παντὸς
λόγου, ὅ ἐστι Χριστοῦ, γνῶσιν καὶ θεωρίαν πολὺ μᾶλλον μι
σεῖσθαι οἱ δαίμονες ἐλεγχόμενοι ἐνεργοῦσιν· οἳ τὴν ἀξίαν
κόλασιν καὶ τιμωρίαν κομίσονται ἐν αἰωνίῳ πυρὶ
ἐγκλεισθέντες.
εἰ γὰρ ὑπὸ τῶν ἀνθρώπων ἤδη διὰ τοῦ ὀνό
ματος Ἰησοῦ Χριστοῦ ἡττῶνται, δίδαγμά ἐστι τῆς καὶ μελλού
σης αὐτοῖς καὶ τοῖς λατρεύουσιν αὐτοῖς ἐσομένης ἐν πυρὶ
αἰωνίῳ κολάσεως.
οὕτως γὰρ καὶ οἱ προφῆται πάντες προε
κήρυξαν γενήσεσθαι, καὶ Ἰησοῦς ὁ ἡμέτερος διδάσκαλος ἐδί
δαξε.
Ἵνα δὲ μή τις εἴπῃ τὸ λεγόμενον ὑπὸ τῶν νομι
ζομένων φιλοσόφων, ὅτι κόμποι καὶ φόβητρά ἐστι τὰ λεγόμενα
ὑφ' ἡμῶν ὅτι κολάζονται ἐν αἰωνίῳ πυρὶ οἱ ἄδικοι, καὶ διὰ
φόβον ἀλλ' οὐ διὰ τὸ καλὸν εἶναι καὶ ἀρεστὸν ἐναρέτως βιοῦν
τοὺς ἀνθρώπους ἀξιοῦμεν, βραχυεπῶς πρὸς τοῦτο ἀποκρινοῦ
μαι, ὅτι, εἰ μὴ τοῦτό ἐστιν, οὔτε ἔστι θεός, ἤ, εἰ ἔστιν, οὐ μέλει
αὐτῷ τῶν ἀνθρώπων, καὶ οὐδέν ἐστιν ἀρετὴ οὐδὲ κακία, καί,
ὡς προέφημεν, ἀδίκως τιμωροῦσιν οἱ νομοθέται τοὺς παραβαί
νοντας τὰ διατεταγμένα καλά.
ἀλλ' ἐπεὶ οὐκ ἄδικοι ἐκεῖνοι
καὶ ὁ αὐτῶν πατήρ, τὰ αὐτὰ αὐτῷ πράττειν διὰ τοῦ λόγου
διδάσκων, οἱ τούτοις συντιθέμενοι οὐκ ἄδικοι.
ἐὰν
δέ τις τοὺς διαφόρους νόμους τῶν ἀνθρώπων προβάληται, λέ
γων ὅτι παρ' οἷς μὲν ἀνθρώποις τάδε καλά, τὰ δὲ αἰσχρὰ νενό
μισται, παρ' ἄλλοις δὲ τὰ παρ' ἐκείνοις αἰσχρὰ καλά, καὶ τὰ
καλὰ αἰσχρὰ νομίζεται, ἀκουέτω καὶ τῶν εἰς τοῦτο λεγομένων
καὶ νόμους διατάξασθαι τῇ ἑαυτῶν κακίᾳ ὁμοίους τοὺς πονη
ροὺς ἀγγέλους ἐπιστάμεθα, οἷς χαίρουσιν οἱ ὅμοιοι γενόμενοι
6
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἄνθρωποι, καὶ ὀρθὸς λόγος παρελθὼν οὐ πάσας δόξας οὐδὲ
πάντα δόγματα καλὰ ἀποδείκνυσιν, ἀλλὰ τὰ μὲν φαῦλα, τὰ δὲ
ἀγαθά· ὥστε μοι καὶ πρὸς τοὺς τοιούτους τὰ αὐτὰ καὶ τὰ ὅμοια
εἰρήσεται, καὶ λεχθήσεται διὰ πλειόνων, ἐὰν χρεία ᾖ.
τανῦν
δὲ ἐπὶ τὸ προκείμενον ἀνέρχομαι.
Μεγαλειότερα μὲν οὖν πάσης ἀνθρωπείου διδασκα
λίας φαίνεται τὰ ἡμέτερα διὰ τοῦ τὸ λογικὸν τὸ ὅλον τὸν φα
νέντα δι' ἡμᾶς Χριστὸν γεγονέναι, καὶ σῶμα καὶ λόγον καὶ
ψυχήν.
ὅσα γὰρ καλῶς ἀεὶ ἐφθέγξαντο καὶ εὗρον οἱ φιλο
σοφήσαντες ἢ νομοθετήσαντες, κατὰ λόγου μέρος δι' εὑρέσεως
καὶ θεωρίας ἐστὶ πονηθέντα αὐτοῖς.
ἐπειδὴ δὲ οὐ πάντα τὰ
τοῦ λόγου ἐγνώρισαν, ὅς ἐστι Χριστός, καὶ ἐναντία ἑαυτοῖς
πολλάκις εἶπον.
καὶ οἱ προγεγενημένοι τοῦ Χριστοῦ, κατὰ
τὸ ἀνθρώπινον λόγῳ πειραθέντες τὰ πράγματα θεωρῆσαι καὶ
ἐλέγξαι, ὡς ἀσεβεῖς καὶ περίεργοι εἰς δικαστήρια ἤχθησαν.
ὁ
πάντων δὲ αὐτῶν εὐτονώτερος πρὸς τοῦτο γενόμενος
Σωκράτης τὰ αὐτὰ ἡμῖν ἐνεκλήθη· καὶ γὰρ ἔφασαν αὐτὸν καινὰ
δαιμόνια εἰσφέρειν, καὶ οὓς ἡ πόλις νομίζει θεοὺς μὴ ἡγεῖσθαι
αὐτόν.
ὁ δὲ δαίμονας μὲν τοὺς φαύλους καὶ τοὺς πράξαντας
ἃ ἔφασαν οἱ ποιηταί, ἐκβαλὼν τῆς πολιτείας καὶ Ὅμηρον καὶ
τοὺς ἄλλους ποιητάς, παραιτεῖσθαι τοὺς ἀνθρώπους ἐδίδαξε,
πρὸς θεοῦ δὲ τοῦ ἀγνώστου αὐτοῖς διὰ λόγου ζητήσεως ἐπίγνωσιν
προὐτρέπετο, εἰπών· Τὸν δὲ πατέρα καὶ δημιουργὸν πάντων οὔθ'
εὑρεῖν ·ᾴδιον, οὔθ' εὑρόντα εἰς πάντας εἰπεῖν ἀσφαλές.
ἃὁ
ἡμέτερος Χριστὸς διὰ τῆς ἑαυτοῦ δυνάμεως ἔπραξε.
Σωκράτει
μὲν γὰρ οὐδεὶς ἐπείσθη ὑπὲρ τούτου τοῦ δόγματος ἀποθνήσκειν·
Χριστῷ δέ, τῷ καὶ ὑπὸ Σωκράτους ἀπὸ μέρους γνωσθέντι (λόγος
γὰρ ἦν καὶ ἔστιν ὁ ἐν παντὶ ὤν, καὶ διὰ τῶν προφητῶν προει
πὼν τὰ μέλλοντα γίνεσθαι καὶ δι' ἑαυτοῦ ὁμοιοπαθοῦς γενο
μένου καὶ διδάξαντος ταῦτα), οὐ φιλόσοφοι οὐδὲ φιλόλογοι
μόνον ἐπείσθησαν, ἀλλὰ καὶ χειροτέχναι καὶ παντελῶς ἰδιῶται,
καὶ δόξης καὶ φόβου καὶ θανάτου καταφρονήσαντες· ἐπειδὴ
δύναμίς ἐστι τοῦ ἀρρήτου πατρὸς καὶ οὐχὶ ἀνθρωπείου λόγου
κατασκευή.
Oὐκ ἂν δὲ οὐδὲ ἐφονευόμεθα οὐδὲ δυνατώτεροι
ἡμῶν ἦσαν οἵ τε ἄδικοι ἄνθρωποι καὶ δαίμονες, εἰ μὴ πάντως
παντὶ γεννωμένῳ ἀνθρώπῳ καὶ θανεῖν ὠφείλετο· ὅθεν καὶ τὸ
ὄφλημα ἀποδιδόντες εὐχαριστοῦμεν.
καίτοι γε καὶ
τὸ Ξενοφώντειον ἐκεῖνο νῦν πρός τε Κρίσκεντα καὶ τοὺς ὁμοίως
αὐτῷ ἀφραίνοντας καλὸν καὶ εὔκαιρον εἰπεῖν ἡγούμεθα.
7
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

τὸν
Ἡρακλέα ἐπὶ τρίοδόν τινα ἔφη ὁ Ξενοφῶν βαδίζοντα εὑρεῖν
τήν τε ἀρετὴν καὶ τὴν κακίαν, ἐν γυναικῶν μορφαῖς φαινομένας.
καὶ τὴν μὲν κακίαν, ἁβρᾷ ἐσθῆτι καὶ ἐρωτοπεποιημένῳ καὶ
ἀνθοῦντι ἐκ τῶν τοιούτων προσώπῳ, θελκτικήν τε εὐθὺς πρὸς
τὰς ὄψεις οὖσαν, εἰπεῖν πρὸς τὸν Ἡρακλέα ὅτι, ἢν αὐτῇ ἕπηται,
ἡδόμενόν τε καὶ κεκοσμημένον τῷ λαμπροτάτῳ καὶ ὁμοίῳ τῷ
περὶ αὐτὴν κόσμῳ διαιτήσειν ἀεὶ ποιήσει.
καὶ τὴν ἀρετὴν
ἐν αὐχμηρῷ μὲν τῷ προσώπῳ καὶ τῇ περιβολῇ οὖσαν εἰπεῖν·
Ἀλλ' ἢν ἐμοὶ πείθῃ, οὐ κόσμῳ οὐδὲ κάλλει τῷ ·έοντι καὶ φθει
ρομένῳ ἑαυτὸν κοσμήσεις ἀλλὰ τοῖς ἀϊδίοις καὶ καλοῖς κόσμοις.
καὶ πάνθ' ὁντινοῦν πεπείσμεθα, φεύγοντα τὰ δοκοῦντα καλά,
τὰ δὲ νομιζόμενα σκληρὰ καὶ ἄλογα μετερχόμενον, εὐδαιμονίαν
ἐκδέχεσθαι.
ἡ γὰρ κακία, πρόβλημα ἑαυτῆς τῶν πράξεων τὰ
προσόντα τῇ ἀρετῇ καὶ ὄντως ὄντα καλὰ διὰ μιμήσεως ἀφθάρ
των περιβαλλομένη (ἄφθαρτον γὰρ οὐδὲν ἔχει οὐδὲ ποιῆσαι
δύνατα), δουλαγωγεῖ τοὺς χαμαιπετεῖς τῶν ἀνθρώπων, τὰ προ
σόντα αὐτῇ φαῦλα τῇ ἀρετῇ περιθεῖσα.
οἱ δὲ νενοηκότες τὰ
προσόντα τῷ ὄντι καλὰ καὶ ἄφθαρτοι τῇ ἀρετῇ· ὃ καὶ περὶ
Χριστιανῶν καὶ τῶν ἀπὸ τοῦ ἄθλου καὶ τῶν ἀνθρώ
πων τῶν τοιαῦτα πραξάντων, ὁποῖα ἔφασαν οἱ ποιηταὶ περὶ
τῶν νομιζομένων θεῶν, ὑπολαβεῖν δεῖ πάντα νουνεχῆ, ἐκ τοῦ
καὶ τοῦ φευκτοῦ καταφρονεῖν ἡμᾶς θανάτου λογισμὸν ἕλκοντα.
Καὶ γὰρ αὐτὸς ἐγώ, τοῖς Πλάτωνος χαίρων διδάγ
μασι, διαβαλλομένους ἀκούων Χριστιανούς, ὁρῶν δὲ ἀφόβους
πρὸς θάνατον καὶ πάντα τὰ ἄλλα νομιζόμενα φοβερά, ἐνε
νόουν ἀδύνατον εἶναι ἐν κακίᾳ καὶ φιληδονίᾳ ὑπάρχειν αὐτούς.
τίς γὰρ φιλήδονος ἢ ἀκρατὴς καὶ ἀνθρωπίνων σαρκῶν βορὰν
ἀγαθὸν ἡγούμενος δύναιτο ἂν θάνατον ἀσπάζεσθαι, ὅπως
τῶν αὐτοῦ ἀγαθῶν στερηθῇ, ἀλλ' οὐκ ἐκ παντὸς ζῆν μὲν
ἀεὶ τὴν ἐνθάδε βιοτὴν καὶ λανθάνειν τοὺς ἄρχοντας ἐπειρᾶτο,
οὐχ ὅτι γε ἑαυτὸν κατήγγειλε φονευθησόμενον;
ἤδη καὶ
τοῦτο ἐνήργησαν οἱ φαῦλοι δαίμονες διά τινων πονηρῶν ἀν
θρώπων πραχθῆναι.
φονεύοντες γὰρ αὐτοί τινας ἐπὶ συκο
φαντίᾳ τῇ εἰς ἡμᾶς καὶ εἰς βασάνους εἵλκυσαν οἰκέτας τῶν
ἡμετέρων ἢ παῖδας ἢ γύναια, καὶ δι' αἰκισμῶν φοβερῶν ἐξαναγ
κάζουσι κατειπεῖν ταῦτα τὰ μυθολογούμενα, ἃ αὐτοὶ φανερῶς
πράττουσιν· ὧν ἐπειδὴ οὐδὲν πρόσεστιν ἡμῖν, οὐ φροντίζομεν,
θεὸν τὸν ἀγέννητον καὶ ἄρρητον μάρτυρα ἔχοντες τῶν τε λο
γισμῶν καὶ τῶν πράξεων.
τίνος γὰρ χάριν οὐχὶ καὶ ταῦτα
δημοσίᾳ ὡμολογοῦμεν ἀγαθὰ καὶ φιλοσοφίαν θείαν
αὐτὰ ἀπεδείκνυμεν, φάσκοντες Κρόνου μὲν μυστήρια τελεῖν ἐν
τῷ ἀνδροφονεῖν, καὶ ἐν τῷ αἵματος ἐμπίπλασθαι, ὡς λέγεται,
8
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

τὰ ἴσα τῷ παρ' ὑμῖν τιμωμένῳ εἰδώλῳ, ᾧ οὐ μόνον ἀλόγων
ζώων αἵματα προσραίνεται ἀλλὰ καὶ ἀνθρώπεια, διὰ τοῦ παρ'
ὑμῖν ἐπισημοτάτου καὶ εὐγενεστάτου ἀνδρὸς τὴν πρόσχυσιν τοῦ
τῶν φονευθέντων αἵματος ποιούμενοι, ∆ιὸς δὲ καὶ τῶν ἄλλων
θεῶν μιμηταὶ γενόμενοι ἐν τῷ ἀνδροβατεῖν καὶ γυναιξὶν ἀδεῶς
μίγνυσθαι, Ἐπικούρου μὲν καὶ τὰ τῶν ποιητῶν συγγράμματα
ἀπολογίαν φέροντες;
ἐπειδὴ δὲ ταῦτα τὰ μαθήματα καὶ τοὺς
ταῦτα πράξαντας καὶ μιμουμένους φεύγειν πείθομεν, ὡς καὶ νῦν
διὰ τῶνδε τῶν λόγων ἠγωνίσμεθα, ποικίλως πολεμούμεθα· ἀλλ'
οὐ φροντίζομεν, ἐπεὶ θεὸν τῶν πάντων ἐπόπτην δίκαιον οἴδαμεν.
εἴθε καὶ νῦν τις ἂν τραγικῇ φωνῇ ἀνεβόησεν ἐπί τι βῆμα
ὑψηλὸν ἀναβάς· Aἰδέσθητε, αἰδέσθητε ἃ φανερῶς πράττετε εἰς
ἀναιτίους ἀναφέροντες, καὶ τὰ προσόντα καὶ ἑαυτοῖς καὶ τοῖς
ὑμετέροις θεοῖς περιβάλλοντες τούτοις ὧν οὐδὲν οὐδ' ἐπὶ ποσὸν
μετουσία ἐστί.
μετάθεσθε, σωφρονίσθητε.
Καὶ γὰρ ἐγώ, μαθὼν περίβλημα πονηρὸν εἰς ἀπο
στροφὴν τῶν ἄλλων ἀνθρώπων περιτεθειμένον ὑπὸ τῶν φαύ
λων δαιμόνων τοῖς Χριστιανῶν θείοις διδάγμασι, καὶ ψευδο
λογουμένων ταῦτα καὶ τοῦ περιβλήματος κατεγέλασα
καὶ τῆς παρὰ τοῖς πολλοῖς δόξης.
Χριστιανὸς εὑρεθῆναι καὶ
εὐχόμενος καὶ παμμάχως ἀγωνιζόμενος ὁμολογῶ, οὐχ ὅτι ἀλλό
τριά ἐστι τὰ Πλάτωνος διδάγματα τοῦ Χριστοῦ, ἀλλ' ὅτι οὐκ
ἔστι πάντη ὅμοια, ὥσπερ οὐδὲ τὰ τῶν ἄλλων, Στωϊκῶν τε καὶ
ποιητῶν καὶ συγγραφέων.
ἕκαστος γάρ τις ἀπὸ μέρους τοῦ
σπερματικοῦ θείου λόγου τὸ συγγενὲς ὁρῶν καλῶς ἐφθέγξατο·
οἱ δὲ τἀναντία ἑαυτοῖς ἐν κυριωτέροις εἰρηκότες οὐκ ἐπιστήμην
τὴν ἄποπτον καὶ γνῶσιν τὴν ἀνέλεγκτον φαίνονται ἐσχηκέναι.
ὅσα οὖν παρὰ πᾶσι καλῶς εἴρηται, ἡμῶν τῶν Χριστιανῶν
ἐστι· τὸν γὰρ ἀπὸ ἀγεννήτου καὶ ἀρρήτου θεοῦ λόγον μετὰ τὸν
θεὸν προσκυνοῦμεν καὶ ἀγαπῶμεν, ἐπειδὴ καὶ δι' ἡμᾶς ἄνθρω
πος γέγονεν, ὅπως καὶ τῶν παθῶν τῶν ἡμετέρων συμμέτοχος
γενόμενος καὶ ἴασιν ποιήσηται.
οἱ γὰρ συγγραφεῖς πάντες
διὰ τῆς ἐνούσης ἐμφύτου τοῦ λόγου σπορᾶς ἀμυδρῶς ἐδύναντο
ὁρᾶν τὰ ὄντα.
ἕτερον γάρ ἐστι σπέρμα τινὸς καὶ μίμημα
κατὰ δύναμιν δοθέν, καὶ ἕτερον αὐτὸ οὗ κατὰ χάριν τὴν ἀπ'
ἐκείνου ἡ μετουσία καὶ μίμησις γίνεται.
Καὶ ὑμᾶς οὖν ἀξιοῦμεν ὑπογράψαντας τὸ ὑμῖν δοκοῦν
προθεῖναι τουτὶ τὸ βιβλίδιον, ὅπως καὶ τοῖς ἄλλοις τὰ ἡμέτερα
γνωσθῇ καὶ δύνωνται τῆς ψευδοδοξίας καὶ ἀγνοίας τῶν καλῶν
ἀπαλλαγῆναι, οἳ παρὰ τὴν ἑαυτῶν αἰτίαν ὑπεύθυνοι
ταῖς τιμωρίαις γίνονται εἰς τὸ γνωσθῆναι τοῖς ἀνθρώποις ταῦτα,
διὰ τὸ ἐν τῇ φύσει τῇ τῶν ἀνθρώπων εἶναι τὸ γνωριστικὸν
καλοῦ καὶ αἰσχροῦ, καὶ διὰ τὸ ἡμῶν, οὓς οὐκ ἐπίστανται τοιαῦτα
9
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ὁποῖα λέγουσιν αἰσχρὰ πράττειν, καταψηφίζεσθαι, καὶ διὰ τὸ
χαίρειν τοιαῦτα πράξασι θεοῖς καὶ ἔτι νῦν ἀπαιτοῦσι παρὰ ἀν
θρώπων τὰ ὅμοια, ὡς ἐκ τοῦ καὶ ἡμῖν, ὡς τοιαῦτα πράττουσι,
θάνατον ἢ δεσμὰ ἢ ἄλλο τι τοιοῦτον προστιμᾶν ἑαυτοὺς κατα
κρίνειν, ὡς μὴ δέεσθαι ἄλλων δικαστῶν.
Καὶ τοῦ ἐν τῷ ἐμῷ ἔθνει, ἀσεβοῦς καὶ πλάνου Σιμω
νιανοῦ διδάγματος κατεφρόνησα.
ἐὰν δὲ ὑμεῖς τοῦτο προ
γράψητε, ἡμεῖς τοῖς πᾶσι φανερὸν ποιήσαιμεν, ἵνα εἰ δύναιντο
μεταθῶνται· τούτου γε μόνου χάριν τούσδε τοὺς λόγους συνε
τάξαμεν.
οὐκ ἔστι δὲ ἡμῶν τὰ διδάγματα κατὰ κρίσιν σώ
φρονα αἰσχρά, ἀλλὰ πάσης μὲν φιλοσοφίας ἀνθρωπείου ὑπέρ
τερα· εἰ δὲ μὴ, κἂν Σωταδείοις καὶ Φιλαινιδείοις καὶ Ἀρχεστρα
τείοις καὶ Ἐπικουρείοις καὶ τοῖς ἄλλοις τοῖς τοιούτοις ποιητι
κοῖς διδάγμασιν οὐχ ὅμοια, οἷς ἐντυγχάνειν πᾶσι, καὶ λεγομέ
νοις καὶ γεγραμμένοις, συγκεχώρηται.
καὶ παυσόμεθα λοι
πόν, ὅσον ἐφ' ἡμῖν ἦν πράξαντες, καὶ προσεπευξάμενοι τῆς
ἀληθείας καταξιωθῆναι τοὺς πάντη πάντας ἀνθρώπους.
εἴη
οὖν καὶ ὑμᾶς ἀξίως εὐσεβείας καὶ φιλοσοφίας τὰ
δίκαια ὑπὲρ ἑαυτῶν κρῖναι.

10
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

