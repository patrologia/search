Poenae in monachos delinquentes
ΕΠΙΤΙΜΙΑ.
αʹ Εἴ τις ὑγιαίνων τῷ σώματι ἀμελοίη τῶν προσευχῶν, ἢ τῆς ἐκμαθήσεως
τῶν ψαλμῶν, προφασι ζόμενος προφάσεις ἐν ἁμαρτίαις, οὗτος ἀφοριζέσθω, ἢ
νηστευέτω ἑβδομάδα μίαν. βʹ Εἴ τις ἐλέγχοιτο ἐπὶ πταίσματι μετὰ ἀγάπης, καὶ μὴ
καταδέχοιτο τὸν ἔλεγχον, μετὰ πάσης εὐλαβείας παρακαλῶν, καὶ αἰτῶν συγγνώμην
ἐφ' οἷς ἐπλημμέλησε, καὶ αὐτὸς ἀφοριζέσθω ἑβδομάδα μίαν. γʹ Εἴ τις συγγνῷ τινι
ἁμάρτημα, καὶ μὴ διορθώσεται αὐτὸν, ἢ ἐλέγξει ἐν εἰρήνῃ μετὰ ἀγάπης τῆς ἐν
Χριστῷ, ἀλλ' ἐν λύπῃ, ὡς θριαμβεύων μᾶλλον τὸν ἀδελφὸν, ἢ θεραπεύων τὸν αἴτιον
τῆς ἁμαρ τίας, καὶ αὐτὸς συναφοριζέσθω τῷ ἐλεγχομένῳ ἐπὶ τῷ ἁμαρτήματι
ἑβδομάδας δύο, ἐπειδὴ ὡς ἐχθρὸς, καὶ οὐχ ὡς ἀδελφὸς ἤλεγξεν. δʹ Εἴ τις ὑβρίζει ἢ
ἀντιλέγει, ἐπιτασσόμενος παρὰ ἀδελφοῦ τὰ κατὰ δύναμιν, καὶ μὴ ὑπακούει μετὰ
πάσης τῆς ἐν Χριστῷ χαρᾶς, ὡς πληροφο ρῶν αὐτὸν, ὁμοίως ἀφοριζέσθω ἑβδομάδα
μίαν. εʹ Εἴ τις ἀργολογοίη ἢ εὐτραπελεύοιτο, μὴ φυλάσ σων μετὰ φόβου καὶ τρόμου
τὴν διδασκαλίαν τοῦ μακαρίου Ἀποστόλου καὶ τῶν Εὐαγγελίων, καὶ αὐτὸς
ἀφοριζέσθω ἑβδομάδα μίαν. 31.1308 ςʹ Εἴ τις πλὴν τοῦ ναὶ καὶ τοῦ οὒ, ἄλλον ὀμνύει
ὅρκον, καὶ αὐτὸς ἀφοριζέσθω ἑβδομάδα μίαν. ζʹ Εἴ τις συγγνῷ τινι ἀδελφῷ
πταίσαντι, καὶ μετὰ τὸ παρελθεῖν ὀνειδίζοι, ὡς παραδειγματίζων, καὶ αὐτὸς ὡς
ἀσυμπαθὴς καὶ ἐπιχαιρέ κακος ἀφοριζέσθω ἑβδομάδα μίαν. ηʹ Εἴ τις συγγνῷ τινι
ἁμάρτημα, καὶ ὁρᾷ αὐτὸν ἀδιαφόρως φέροντα, καὶ μὴ μετὰ συντριμμοῦ καρδίας καὶ
πολλῶν δακρύων ἐξιλεούμενον τὸν Κύριον, παρακαλείτω καὶ ὑπομιμνησκέτω
αὐτὸν μετα νοεῖν, καὶ ἐπιδείκνυσθαι ἔργα μετανοίας. Εἰ δὲ τούτων γινομένων
ἀμελοίη τῆς ἑαυτοῦ σωτηρίας, χωριζέσθω ὁ τοιοῦτος τῆς συνοδίας παντελῶς. Μικρὰ
γὰρ ζύμη ὅλον τὸ φύραμα ζυμοῖ, κατὰ τὸν εἰπόντα ἅγιον. θʹ Εἴ τις προέρχοιτο παρὰ
τὰς ἀναγκαίας χρείας, περιάγων ἀκαίρως, ἀφοριζέσθω ὁ τοιοῦτος, ἵνα ἡσυχάζωσιν
ἐν οἴκῳ οἱ πόδες αὐτοῦ, καθὼς γέγραπται. ιʹ Εἴ τις ὀργισθῇ εἰκῆ, καὶ μὴ παραχρῆμα
παρακαλέσῃ τὸν ἀδελφὸν ὃν ἐλύπησεν, ὅπως ἀφῇ αὐτῷ τὴν ἁμαρτίαν ἣν ἥμαρτεν
εἰς αὐτὸν, ἀφοριζέσθω καὶ αὐτὸς ἑβδομάδα μίαν. ιαʹ Εἴ τις ὑβρισθείη παρὰ ἀδελφοῦ,
καὶ παρακληθεὶς ὑπὸ τῆς συνοδίας, μὴ συγχωρήσῃ τὸ ἁμάρτημα, μνημονεύσας τοῦ
λέγοντος· Ἐάν τις πρός τινα ἔχῃ μομφήν· καθὼς καὶ ὁ Χριστὸς ἐχαρίσατο· καὶ αὐτὸς
ὁμοίως ἀφοριζέσθω ἑβδομάδα μίαν.

 
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

