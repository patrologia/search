In pentecosten (homilia 11)
Λεοντίου πρεσβυτέρου Κωνσταντινουπόλεως λόγος εἰς τὴν ἁγίαν
πεντηκοστήν
Ὅσοι περὶ τὴν παροῦσαν ἑορταστικὴν ἀένναον χάριν ἐπείγεσθε, τῇ πηγῇ τῶν
ἀγαθῶν Χριστῷ προσδράμωμεν. Πηγὴ περὶ ἧς ὁ προφήτης βοᾷ πρὸς τὸν πατέρα τῶν
φώτων· Ὅτι παρὰ σοὶ πηγὴ ζωῆς, ἐν τῷ φωτί σου ὀψόμεθα φῶς· τουτέστιν ἐν τῷ σῷ
μονογενεῖ υἱῷ τὸ πνεῦμα τὸ ἅγιον. Καλῶς οὖν ὁ προφήτης τὸν δεσπότην Χριστὸν πηγὴν
προσηγόρευσεν.
Ἔστι μὲν ἔστιν, ὦ φίλοι, καὶ κοσμικὴν πηγὴν ἐν διαφόροις τόποις ἀργυρόχροον
κατιδεῖν, καὶ τὰς ἐν βάθει ψηφῖδας ἐν τῷ πέλματι κειμένας τῇ καθαρότητι τοῦ ὕδατος
ἐπάνω σκοπῆσαί τινα ἐπικύπτοντα. Τοῦ γὰρ ἐμπύρου χρυσοτόξου τούτου ἡλίου ἐν
ἡμέρᾳ θερινῇ πλατύτερον τὰς ἑαυτοῦ ἀκτῖνας ἀβρόχως ἐναναπαύσαντος τῇ τοῦ ὕδατος
νωτοφορίᾳ, εὐθέως ὁ τῇ τοιαύτῃ πηγῇ προσπελάσας ὁδοιπόρος ἀζήμιος οὐ παρέρχεται·
τὰς χεῖρας γὰρ ἀντὶ κρατῆρος κοιλάνας καὶ ὅλον ἑαυτὸν ἡδέως κυρτώσας, τοῦ ὕδατος
ἀρύεται, τὴν δίψαν ἰᾶται, τὸν καύσωνα παραμυθεῖται, τὸ πρόσωπον διαθερμανθὲν
δροσίζει, τὰς αἰσθήσεις διαλυθείσας συνάγει, τὴν ψυχὴν στενοχωρουμένην πλατύνει,
τὴν καρδίαν λιθουμένην λεαίνει
, τὰ γόνατα βαδίζειν μὴ βουλόμενα παλινδρομεῖν ἀναγκάζει. Ἀλλ' αὕτη μὲν ἡ
ἀργυρόχροος πηγὴ δίψαν μὲν πολλάκις ἰᾶται, σπλῆνα δὲ βλάπτει, καὶ καύσωνα μὲν
πολλάκις παραμυθεῖται, τὸ δὲ ἧπαρ νοσεῖν παρασκευάζει, καὶ τὴν ψυχὴν μὲν πολλάκις
ἐκπηδῆσαι τοῦ σώματος βουλομένην παρακατέχει, ψύξιν δὲ τῷ θώρακι ἐνθηκιάσασα
πολυχρόνιον πάθος γεννᾷ. Ὁ δέ γε δεσπότης ἡμῶν Ἰησοῦς Χριστὸς ὁ καλῶς πηγὴ
προσαγορευθείς, οὐχ ὡς βρύων ἀργυρόχροα νάματα, ἀλλ' ὡς λιμνάζων θεόκρατα
διδάγματα, οὐ μέρος μέν τι τῶν πιστῶν θεραπεύει, μέρος δέ τι τραυματίζει· ἐξ
ὁλοκλήρου δὲ τὴν ὁλοκληρίαν δωρούμενος, ἐκεῖνα τοῖς προστρέχουσι χαρίζεται, ἅπερ
αὐτὸς παρὰ μηδενὸς δανεισάμενος κέκτηται. Οὐ γὰρ εἰς μακρὰν ὁ δεσπότης Χριστὸς
τοὺς πιστοὺς πέμπει τὸ ζῶν ὕδωρ ἀρύσασθαι, ἀλλ' ἐν τῇ κοιλίᾳ τῆς γνώσεως ἔχειν αὐτὸ
προτρέπεται· ἤκουες γὰρ αὐτοῦ ἀρτίως λέγοντος· Ἐάν τις διψᾷ, ἐρχέσθω πρός με καὶ
πινέτω· ὁ πιστεύων εἰς ἐμέ, καθὼς εἶπεν ἡ γραφή, ποταμοὶ ἐκ τῆς κοιλίας αὐτοῦ
ῥεύσουσιν ὕδατος ζῶντος. Ἔστι μέν, ὦ φίλε, καὶ τετραρόων ποταμῶν μήτηρ πηγὴ ἐν τῷ
Ἐδὲμ παραδείσῳ, ἀλλ' ἐκείνη ἐπὶ γῆς ἐκοιλάνθη, καὶ κάτωθεν ἔχει τὸ ῥεῖθρον, καὶ τῷ
παρόντι συνδιαλύεται βίῳ· τῶν βαπτιζομένων γὰρ μὴ παρόντων περιττὴ ἡ τῶν ὑδάτων
χρῆσις.
Ὁ δέ γε δεσπότης ἡμῶν Ἰησοῦς Χριστός, ἡ ἀένναος πηγή, οὐκ ἐκ τῆς γῆς
ἀναδίδοται, ἀλλ' ἐκ τῶν οὐρανῶν διαδίδοται· οὐκ ἐν μέρει περιγράφεται, ἀλλὰ
πανταχοῦ δοξάζεται· οὐ τοῖς φθαρτοῖς συμφθείρεται, ἀλλὰ τοῖς φθειρομένοις
ἀφθαρσίαν χαρίζεται. Καὶ τὴν μὲν τοῦ ἐν Ἐδὲμ παραδείσου πηγὴν τέσσαρες μόνοι
ποταμοὶ θηλάζουσιν· Γηῶν, Φισῶν, Τίγρις, Εὐφράτης. Ἀλλ' οὗτοι μὲν μερικοὶ καὶ
ὀλιγόροοι, καὶ οὐ πᾶσαν τὴν γῆν ἀρδεύοντες, καὶ ἐν χειμῶνι μὲν πλημμυροῦντες, ἐν δὲ
θέρει ὀλιγοροῦντες· τῆς δὲ τοῦ ἐκκλησιαστικοῦ τούτου παραδείσου πηγῆς οἱ
ἀποστολικοὶ ποταμοὶ ποταμοὶ πολλοὶ μὲν καὶ ἄπειροι, εἰ καὶ Ἰούδας ἐψύγη. Κατανόησον
δὲ μετὰ πάντων καὶ τοὺς τέσσαρας εὐαγγελικοὺς μεγάλους ποταμούς, Ματθαῖον,
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

Λουκᾶν, Μάρκον, Ἰωάννην, οἵτινες πᾶσαν κώμην καὶ πατρίδα καὶ πόλιν ἀρδεύουσι τοῖς
θείοις διδάγμασιν. Εἰ γὰρ ἀστὴρ ἀστέρος διαφέρει ἐν δόξῃ, κατὰ τὴν τοῦ Παύλου
διδασκαλίαν, πῶς οὐχὶ τῶν σαρκικῶν τὰ πνευματικὰ προύχει; Τί τοιοῦτον ὁ ἐν Ἐδὲμ
παράδεισος ἔχων εὑρίσκεται, οἷον ὁ ἐκκλησιαστικὸς οὗτος λειμὼν φέρων γνωρίζεται;
Ἐκεῖ ὁ Ἀδὰμ ὡς δραπέτης κρυπτόμενος, ὦδε ὁ δεσπότης Χριστὸς ὡς θεὸς
προσκυνούμενος· ἐκεῖ Εὕα πτερνιζομένη, ὧδε Μαρία δοξαζομένη· ἐκεῖ ξύλον θάνατον
ὀλυνθίζον, ὧδε ξύλον ζωὴν βλαστάνον· ἐκεῖ ὄφις ψευδολογίαν γλωσσίζων, ὧδε τὸ
ἅγιον πνεῦμα θεολογίαν σοφιστεῦον· ἐκεῖ νομοθεσία παρακουσθεῖσα, ὧδε θεογνωσία
βεβαιωθεῖσα· ἐκεῖ ῥοδωνιά, ὧδε παρθενία· ἐκεῖ κρίνον λευκόφυλλον, ὧδε νεοφώτιστος
λαμπρόψυχος· ἐκεῖ ἀναδενδράδες μεγαλορῶγοι, ὧδε ἄνδρες φιλόλογοι· ἐκεῖ φυλλάδες
ἄγριαι, ὧδε γυναῖκες ἅγιαι· ἐκεῖ πηγὴ ἄφωνος, ὧδε ποταμοὶ μεγαλόφωνοι. Ἤκουες γὰρ
ἀρτίως τοῦ ποταμοῦ Εὐφράτου, λέγω δὴ τοῦ εὐαγγελιστοῦ Ἰωάννου βοῶντος καὶ
λέγοντος· Ἐν τῇ ἐσχάτῃ ἡμέρᾳ τῇ μεγάλῃ τῆς ἑορτῆς εἱστήκει ὁ Ἰησοῦς καὶ ἔκραζε
λέγων· Ἐάν τις διψᾷ, ἐρχέσθω πρός με καὶ πινέτω. Τί λέγεις, ὦ φίλε, μᾶλλον δὲ τί
βραδύνεις; Χριστὸς καλεῖ, μηδεὶς ὀκνείτω· πηγὴ βοᾷ, μηδεὶς παραιτείσθω. Πάντες
διψῶμεν, πάντες προσέλθωμεν· ὅσον ἀρυόμεθα, τοσοῦτον περιττεύει. Βοήσωμεν, οἱ τοῦ
∆αυῒδ φίλοι, τὰ τοῦ ∆αυῒδ πρὸς τὴν ἀένναον πηγὴν λέγοντες· Ὃν τρόπον ἐπιποθεῖ ἡ
ἔλαφος ἐπὶ τὰς πηγὰς τῶν ὑδάτων, οὕτως ἐπιποθεῖ ἡ ψυχή μου πρὸς σέ, ὁ θεός. Καλῶς
οὖν ἔλεγεν ὁ εὐαγγελιστής· Ἐν τῇ ἐσχάτῃ ἡμέρᾳ τῇ μεγάλῃ τῆς ἑορτῆς. Καλῶς καὶ
ἐσχάτην ταύτην τὴν ἑορτὴν καὶ μεγάλην προσηγόρευσεν· ἔστι γὰρ ἐσχάτη καὶ μεγάλη
καὶ πρώτη. Ἐσχάτη πρὸς Ἰουδαίους· τὰ ἔσχατα γὰρ αὐτοῖς συνέβη κακὰ διὰ τὴν εἰς
Χριστὸν παρανομίαν, καὶ ἡ αἰσχύνη τοῦ προσώπου αὐτῶν ἀπεκαλύφθη. Ὅθεν ὁ κύριος
ὀνειδίζων τὴν μητρόπολιν αὐτῶν Ἱερουσαλὴμ ἔλεγεν· Ὄψις πόρνης ἐγένετό σοι,
ἀπηναισχύντησας πρὸς πάντας. Ἐσχάτη οὖν πρὸς Ἰουδαίους, πρώτη πρὸς Χριστιανούς·
ἐκείνων ἡ τρυγία, ἡμῶν ἡ καθαροποσία· ἐκείνων τὰ γίγαρτα, ἡμῶν τὰ ὑπολήνια. Καλῶς
οὖν ἐσχάτην καὶ μεγάλην ἑορτὴν ταύτην ὁ εὐαγγελιστὴς προσηγόρευσεν. Τί γὰρ
μεγαλειότερον ταύτης τῆς παρούσης ἑορτῆς; Πᾶσα ἑορτὴ δεσποτικὴ τοὺς ζῶντας μόνον
ἱλαρύνει, αὕτη δὲ ἡ τῆς πεντηκοστῆς ἑορτὴ οὐ μόνον τοὺς ζῶντας ἱλαρύνει, ἀλλὰ καὶ
τοὺς τεθνεῶτας προσαγορεύει. Αὕτη ἡ τῆς πεντηκοστῆς ἑορτὴ καὶ τὰς ἐκκλησίας
φωτίζει καὶ τὰ μνήματα ῥοδίζει· αὕτη ἡ τῆς πεντηκοστῆς ἑορτὴ καὶ τοὺς οἴκους
εὐωδιάζει καὶ τοὺς τύμβους μυρίζει. Τί γὰρ μεγαλειότερον ταύτης τῆς ἑορτῆς, ἐν ᾗ τὸ
ἅγιον πνεῦμα τὴν ἐμφάνειαν ποιησάμενον τὸν φωτισμὸν τῶν γλωσσῶν τοῖς
ἀποστόλοις ἐχαρίσατο; Σήμερον ὁ Χριστιανισμὸς ἐρριζώθη, σήμερον ὁ Ἰουδαϊσμὸς
ἐμαράνθη, σήμερον ὁ Ἑλληνισμὸς ἐκαπνίσθη, σήμερον ὁ αἱρετισμὸς ἐνεφράγη, σήμερον
ὁ θεῖος δογματισμὸς ἐπλατύνθη. Ἐροῦσι δὲ πάντως τινές· Καὶ πῶς σήμερον ὁ αἱρετισμὸς
ἐνεφράγη, ὁ δὲ θεῖος δογματισμὸς ἐπλατύνθη; Πῶς; Ἐκ τῆς τοῦ κυρίου διδασκαλίας.
Σήμερον γὰρ ὁ κύριος ἡμᾶς σαφέστερον ἐδίδαξε διὰ ὑποδείγματος ἐναργοῦς μίαν οὐσίαν
πατρὸς καὶ υἱοῦ καὶ ἁγίου πνεύματος ἐν τρισὶ διαφόροις ὑποστάσεσιν. Εἰ γὰρ πηγὴν καὶ
ὕδωρ καὶ ποταμὸν οὐδεὶς δύναται χωρίσαι, πῶς οἱ τῶν αἱρετικῶν παῖδες μερίζειν
τολμῶσι τὸν πατέρα τοῦ υἱοῦ καὶ τὸν υἱὸν τοῦ ἁγίου πνεύματος; Τὰ ἐπὶ τῆς γῆς
ἀμέριστα, καὶ τὰ πνευματικὰ μεριστά; Ἐροῦσι δὲ πάντως τινές· Καὶ τίς ἡ πηγή; Τίς δὲ τὸ
ὕδωρ; Τίς δὲ καὶ ὁ ποταμός; Τίς ἡ πηγή; Ὁ πατὴρ τῶν φώτων. Τίς δὲ τὸ ὕδωρ; Ὁ
μονογενὴς υἱός. Τίς δὲ ὁ ποταμός; Τὸ ἅγιον πνεῦμα. Καὶ πόθεν τοῦτο δῆλον; Λάμβανε
τὴν ἀπόδειξιν μὴ ἐκ τῶν ἐμῶν λόγων, ἀλλ' ἐκ τῶν θείων γραφῶν. Καὶ ὅτι πηγὴ ὁ
πατήρ, ἄκουε αὐτοῦ τοῦ πατρὸς διὰ τοῦ προφήτου πρὸς τοὺς Ἰουδαίους τοὺς ἔχοντας
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

τοὺς βλασφήμους καὶ βορβορώδεις λάκκους βοῶντος οὕτως· Ἐμὲ ἐγκατέλιπον πηγὴν
ὕδατος ζῶντος, καὶ ὤρυξαν ἑαυτοῖς λάκκους συντετριμμένους, οἳ οὐ δυνήσονται ὕδωρ
συσχεῖν. Ὅτι δὲ ὕδωρ ὁ υἱός, ἄκουε αὐτοῦ λέγοντος πρὸς τὴν Σαμαρεῖτιν· Εἰ ᾔδεις,
γύναι, τὴν δωρεὰν τοῦ θεοῦ καὶ τίς ἐστιν ὁ λέγων σοι· ∆ός μοι πιεῖν, σὺ ἂν ᾔτησας αὐτὸν
καὶ ἔδωκεν ἄν σοι ὕδωρ ζῶν.
Ὅτι δὲ ποταμὸς τὸ ἅγιον πνεῦμα, ἤκουες περὶ τούτου ἀρτίως τοῦ κυρίου
λέγοντος· Ἐάν τις διψᾷ, ἐρχέσθω πρός με καὶ πινέτω· ὁ πιστεύων εἰς ἐμέ, καθὼς εἶπεν ἡ
γραφή, ποταμοὶ ἐκ τῆς κοιλίας αὐτοῦ ῥεύσουσιν ὕδατος ζῶντος. Εἶτα προσθεὶς ὁ
εὐαγγελιστὴς ἔλεγεν, ἵνα μὴ δόξωσι περὶ κοσμικοῦ ποταμοῦ τὸν κύριον λόγον
ποιεῖσθαι· Τοῦτο δὲ ἔλεγεν περὶ τοῦ πνεύματος, οὗ ἤμελλον λαμβάνειν οἱ πιστεύοντες
εἰς αὐτόν. Εἶδες ὀρθοδοξίας ἀένναον πηγήν; Εἶδες εὐσεβείας ἀτάραχον ὕδωρ; Εἶδες
θεογνωσίας ἀθόλωτον ποταμόν; Καλῶς οὖν τὴν παροῦσαν ἑορτὴν τῆς πεντηκοστῆς
μεγάλην ὁ εὐαγγελιστὴς προσηγόρευσεν· ἔστι γὰρ αὕτη μεγάλη. Σήμερον γὰρ οἱ
ἀπόστολοι τὸ βέβαιον ἔσχον, σήμερον οἱ ἁλιεῖς σοφισταί, σήμερον οἱ ἀγράμματοι
διδάσκαλοι, σήμερον οἱ μονόγλωσσοι ποικιλόγλωσσοι, σήμερον οἱ ψελλίζοντες
ῥωμαΐζοντες, σήμερον οἱ στυγνοὶ φαιδροί, σήμερον οἱ πήλινοι πύρινοι, σήμερον οἱ
ἀγκιστρευταὶ δογματισταί, σήμερον οἱ τὴν λίμνην ἁλιεύοντες τὴν οἰκουμένην σαγη
νεύοντες, σήμερον οἱ ἰχθύας ψηλαφῶντες ἀνθρώπους κατηχοῦσιν, σήμερον οἱ θῆρας
φεύγοντες δαίμονας πατοῦσιν, σήμερον οἱ ἀνένθηκοι εὐένθηκοι, σήμερον οἱ ἄπρακτοι
ἔμπρακτοι, σήμερον οἱ πτωχοὶ δανεισταί, σήμερον οἱ διωκόμενοι παρακαλούμενοι,
σήμερον οἱ γυμνητεύοντες ὑπερβασιλεύοντες, σήμερον οἱ ἐν γωνίᾳ κρυπτόμενοι ἐπὶ τῆς
συναγωγῆς εἰσι διαλεγόμενοι. Καὶ τί ἔτι μακρολογῶ; Σήμερον οἱ ὀρφανίαν πενθοῦντες
πατρωνυμίαν βοῶσιν. Ἄκουε συνετῶς. Τοῦ γὰρ δεσπότου Χριστοῦ ἐπ' ὄψεσι τῶν
ἀποστόλων εἰς οὐρανοὺς ἀναλαμβανομένου καὶ τοῖς ἀποστόλοις ἐπαγγειλαμένου τὸν
ποταμὸν τοῦ ἁγίου πνεύματοςἔπρεπε γὰρ τῇ πηγῇ ἀναλαμβανομένῃ ποταμὸν
ἐπαγγέλλεσθαι, ἔχαιρον μὲν οἱ ἀπόστολοι καὶ τὰς ψυχὰς ὡς δεξαμενὰς εὐτρέπισαν, πότε
δὲ ὁ ποταμὸς πυρφόρος γενήσεται τὸν καιρὸν οὐκ ἐγίνωσκον. Ὁ γὰρ κύριος
ἀναλαμβανόμενος τὴν μὲν ἐπαγγελίαν τοῦ ἁγίου πνεύματος ἐσήμανεν, τὸν δὲ καιρὸν
πότε γενήσεται οὐδ' ὅλως ἐγνώρισεν. ∆ιὰ τί; Ὅτι ὁ θεὸς ὡρισμένως οὐκ ἐργάζεται, ἀλλ'
ὅτε βούλεται τότε καὶ ἐμφανίζεται. Ἔμενον τοίνυν οἱ ἀπόστολοι μετὰ τὴν ἀνάληψιν
τοῦ κυρίου τὴν μὲν ἐλπίδα τοῦ ἁγίου πνεύματος φέροντες καὶ συνσφίγγοντες ἑαυτοὺς
καὶ τὴν πρὸς ἀλλήλους παρρησίαν συνάγοντες. Ὡς δὲ εἶδον μίαν καὶ δευτέραν καὶ
τρίτην ἡμέραν ὅτι τῆς ἐπαγγελίας τὸ δῶρον εἰς πέρας οὐκ ἤγετο, ἀνθρωπίνοις λογισμοῖς
κινηθέντες οἱ ἀπόστολοι ἤρξαντο πρὸς ἑαυτοὺς ἀντιβάλλοντες λέγειν· Τί ποιήσωμεν,
ἄνδρες ἀδελφοί; Ἤδη δεκάτην ἡμέραν ταύτην διατελοῦμεν καὶ οὐδὲν τῶν λεχθέντων
παρὰ τοῦ διδασκάλου εἰς πέρας ἐλθὸν ὁρῶμεν. Τί τὸ ξένον καὶ παράδοξον τοῦτο; Ἆρα
τῆς προτεραίας ἁλείας ἐπιλαβώμεθα ἢ τὴν ἐπαγγελίαν ἔτι μικρὸν ἐκδεξώμεθα; Ταύτην
δεκάτην ἡμέραν ἄγομεν ἀργοῦντες. Τί τὸ ξένον; τί τὸ παράδοξον; Ἐλισσαῖος ὁ μαθητὴς
τοῦ Ἡλία παραυτὰ τὴν μηλωτὴν δεξάμενος ἐθαυματούργησεν, ὁ μαθητὴς τοῦ μαθητοῦ,
καὶ ἡμεῖς ἀργοῦμεν; Τί τὸ παράδοξον; Μὴ ἆρα ὁ Ἡλίας τοῦ ἡμετέρου διδασκάλου
δραστικώτερος; Ἀλλ' οὐ τολμῶμεν λέγειν· εἴδομεν γὰρ κατὰ πρόσωπον ἐν τῷ ὄρει τῆς
μεταμορφώσεως πῶς οὐ μόνον Ἡλίας, ἀλλὰ καὶ Μωϋσῆς τῷ διδασκάλῳ ἡμῶν ὡς θεῷ
προσεκύνησαν. Τί τὸ ξένον; τί τὸ παράδοξον; Ἐλισσαῖος ὁ ζευγηλάτης παραυτὰ
ἐθαυματούργησεν ἀλόγου ζῴου κεντρόνυκτον δορὰν δεξάμενοςτί γὰρ ἔπρεπε νομικῷ
ἐργάτῃ ἀλλ' ἢ τὴν μηλωτὴν κληρονομῆσαι; καὶ ἡμεῖς οἱ μὴ ὄντες μαθηταὶ μαθητοῦ ἀλλ'
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

ἀπόστολοι θεοῦ δεκάτην ἡμέραν ἀργοὶ διατελοῦμεν;
Ταῦτα τῶν ἀποστόλων πρὸς ἑαυτοὺς ἀντιβαλλόντων καὶ εἰς μακρὰν ἔτι τῆς
ἐλπίδος τὴν ἐπαγγελίαν καραδοκούντων, ἰδοὺ ἀθρόον οὐρανῶν μὲν ἀνοίγονται πύλαι,
ἀρχαγγελικῶν δὲ δυνάμεων ἔκραζον στρατιαί, τὸ δέ γε τοῦ ἀέρος πέλαγος ἐθορυβεῖτο
δειλίᾳ, τὰ δὲ τῶν νεφῶν κύμβαλα βασιλικῶν πατάγων ἦχον ἐξέπεμπον, ἅπαν δὲ τὸ
ἐπίγειον χωρίον ὡς κάλαμος ὑπὸ ἀνέμου τοῖς σεισμοῖς ἐδονεῖτο, τὸ δέ γε τῆς θαλάσσης
ἄπειρον κύτος πυρίνῃ σινδόνι κατεσκέπετο. Καὶ μαρτυρεῖ μου τῷ λόγῳ ὁ συγγραφεὺς
τῶν ἀποστολικῶν Πράξεων Λουκᾶς λέγων, καθὼς ἀρτίως ἤκουες· Ἐν τῷ
συμπληροῦσθαι τὴν ἡμέραν τῆς πεντηκοστῆς ἦσαν ἅπαντες ὁμοθυμαδὸν ἐπὶ τὸ αὐτὸ οἱ
ἀπόστολοι. Καὶ ἐγένετο ἄφνω ἐκ τοῦ οὐρανοῦ ἦχος ὥσπερ φερομένης πνοῆς βιαίας καὶ
ἐπλήρωσεν ὅλον τὸν οἶκον οὗ ἦσαν καθήμενοι, καὶ ὤφθησαν αὐτοῖς διαμεριζόμεναι
γλῶσσαι ὡσεὶ πυρός, ἐκάθισέ τε ἐφ' ἕνα ἕκαστον αὐτῶν, καὶ ἤρξαντο λαλεῖν ἑτέραις
γλώσσαις καθὼς τὸ πνεῦμα ἐδίδου αὐτοῖς ἀποφθέγγεσθαι. Καθὼς ἐδίδου, οὐ καθὼς
ἐκελεύετο. Εἶδες ἑορτῆς μέγεθος; Εἶδες ἁγίου πνεύματος δωρεάν; Εἶδες αἰσχύνην τῶν
πνευματομάχων αἱρετικῶν; Πεντηκοστὴν ἑορτάζουσι καὶ τὸ πνεῦμα τοξεύουσιν. Τίς
γλώσσας χαλκεύει, εἰ μὴ ὁ τῶν γλωσσῶν πηλοπλάστης; Τίς μέγα τορνεύει στόμα, εἰ μὴ ὁ
τὴν ὄνον ἔναρθρα λαλεῖν παρασκευάσας; Τίς τοὺς ἀγραμμάτους σοφιστεύει, εἰ μὴ μόνον
τὸ ἅγιον πνεῦμα; Ἀλλ' εὐθέως ἐροῦσιν οἱ ἐξ ἐναντίας· Καὶ πόθεν τοῦτο δῆλον, ὅτι τὸ
ἅγιον πνεῦμα δίδωσι τὴν σοφίαν καὶ τὴν εἴδησιν τῶν γλωσσῶν; Πόθεν; Ἄκουε τοῦ
κυρίου
, αἱρετικέ, λέγοντος πρὸς τοὺς ἑαυτοῦ μαθητὰς ἐκ πολλῶν τῶν χρόνων· Μὴ
μεριμνήσητε ὅταν προσάγωσιν ὑμᾶς ἐπὶ ἄρχοντας καὶ ἡγεμόνας πῶς ἢ τί λαλήσητε
, οὐ γὰρ ὑμεῖς ἐστε οἱ λαλοῦντες, ἀλλὰ τὸ πνεῦμα τοῦ πατρός μου τὸ λαλοῦν ἐν ὑμῖν. Ὢ
τῶν παραδόξων πραγμάτων. Ἐν πυρὶ κατῆλθε τὸ ἅγιον πνεῦμα, ὡς πῦρ ἐκαθέσθη ἐφ'
ἕνα ἕκαστον τῶν ἀποστόλων, καὶ πῦρ ὑπάρχον οὐκ ἔκαυσε τοὺς ἀποστόλους, ἀλλ'
ἐφώτισεν. ∆ιὰ τί; Ὅτι οὐκ ἦν καυστικόν, ἀλλὰ φωτιστικόν, μᾶλλον δὲ καὶ καυστικὸν
καὶ φωτιστικόν, τοὺς βλασφήμους καῖον, τοὺς εὐσεβεῖς φωτίζον. Ὥσπερ γὰρ τὸ
κοσμικὸν καὶ ὑλῶδες πῦρ ἀκάνθας μὲν καίει, κηρὸν δὲ τήκει, μόλιβδον δὲ λύει, πηλὸν
δὲ ξηραίνει, ἄργυρον δὲ λαμπρύνει, χρυσὸν δὲ καθαίρει, οἶκον δὲ φωτίζει, οὕτω καὶ τὸ
ἄϋλον πῦρ, τὸ ἅγιον πνεῦμα, ἁμαρτίας ἐξαλείφει, καρδίαν πλεονεκτοῦσαν μαραίνει,
ἄνθρωπον δόλιον ἐξαλείφει, ψυχὴν ἐπιστρέφουσαν καθαίρει, ἄνδρα πιστὸν λαμπρύνει,
ἐκκλησιαστικὸν οἶκον φωτίζει. Ἄλλως δέ, ἐν πυρὶ κατῆλθε τὸ ἅγιον πνεῦμα, ἵνα
γνῶμεν σαφῶς ὅτι τῆς θεϊκῆς ἐστιν οὐσίας τὸ ἅγιον πνεῦμα. Αὐτὸς γὰρ κύριος ὁ θεὸς
ἡμῶν πῦρ ἐστι κατὰ τὸ φάσκον ῥητόν· Ὁ θεὸς ἡμῶν πῦρ καταναλίσκον. Καὶ ὅπου δ' ἂν
ζητήσῃς τὸν θεὸν θεϊκὸν σημεῖον ἐπιδεικνύμενον καὶ μυστήριον παρέχοντα, ἐν πυρὶ
αὐτὸν εὑρήσεις ποιοῦντα. Οὕτω γὰρ καὶ τοὺς υἱοὺς Ἰσραὴλ ἐκ γῆς Αἰγύπτου ἐξαγαγὼν
στύλον πυρὸς αὐτοῖς παρεκατέστησεν, οὐ πυρπολοῦντα ἀλλ' ὁδηγοῦντα. Οὕτω καὶ
πρώτην ἐμφάνειαν ὁ θεὸς τῷ Μωϋσεῖ ποιούμενος διὰ φλογὸς πυρὸς βάτου τὴν ἑαυτοῦ
φωνὴν ἐξέπεμψεν
, οὐχ ἵνα ἀκούσῃ ἀλλ' ἵνα διδάξῃ. Οὕτω καὶ τῷ ∆ανιὴλ ἐμφαίνων ἑαυτὸν ὁ θεὸς ἐν
θρόνῳ πυρὸς ἑαυτὸν καθήμενον ἔδειξεν, οὐχ ἵνα ζημιώσῃ τὸν προφήτην ἀλλ' ἵνα δείξῃ
πανταχοῦ ἐν τίσι θεός
. Ἐροῦσι δὲ πάντως τινὲς ὅτι ταῦτα τῆς παλαιᾶς διαθήκης τὰ γνωρίσματα. ∆εῖξον
ἡμῖν φησιν ὅτι καὶ ἐν τῇ καινῇ διαθήκῃ ὁ θεὸς ἐν πυρὶ θαυματουργεῖ· ἐκεῖνα γὰρ τῆς
παλαιᾶς μηνύματα. Ἄκουε, φίλε· εἰ μὲν ἀγνοεῖς, μάνθανε· εἰ δὲ οἶδας, ὑπομνήσθητι.
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

Φέρεις ἐν μνήμῃ πάντως, ὦ φίλε, ὅτι ὁ θεὸς πρὸ πάντων τοὺς μάγους ὁδηγῶν, δι'
ἀστέρος αὐτοὺς ὡδήγησεν· παντί που δῆλον ὅτι πῦρ ὁ ἀστήρ. Ὁμοίως ὁ κύριος τὸ
ἑαυτοῦ σῶμα τὸ τῆς ἱερᾶς τραπέζης ἄνθρακα πυρὸς προσηγόρευσεν. Καὶ πότε τοῦτο;
Ἄκουε Ἡσαΐου ἐκ πολλῶν τῶν χρόνων βοῶντος· Ἀπεστάλη πρός με ἓν τῶν σεραφίμ,
καὶ ἐν τῇ χειρὶ αὐτοῦ εἶχεν ἄνθρακα ὃν τῇ λαβίδι ἔλαβεν ἀπὸ τοῦ θυσιαστηρίου καὶ
ἥψατο τῶν χειλέων μου καὶ εἶπέ μοι· Ἰδοὺ ἥψατο τοῦτο τῶν χειλέων σου καὶ ἀφελεῖ
τὰς ἀνομίας σου καὶ τὰς ἁμαρτίας σου περικαθαριεῖ. Ποῖος δὲ ἄνθραξ ἁμαρτίας ἀφίησιν,
εἰ μὴ ἡ δεσποτικὴ κοινωνία; Ὡσαύτως ὁ κύριος καὶ τὸ βάπτισμα τῆς ἀναστάσεως ἡμῶν
πῦρ προσηγόρευσεν. Καὶ πόθεν τοῦτο δῆλον; Ἄκουε Ἰωάννου τοῦ βαπτιστοῦ βοῶντος·
Ἔρχεταιὀπίσω μου ὁ ἰσχυρότερός μου, οὗ οὐκ εἰμὶ ἄξιος τὰ ὑποδήματα βαστάσαι· αὐτὸς
ὑμᾶς βαπτίσει ἐν πνεύματι ἁγίῳ καὶ πυρί. Τὸ δέ γε πάντων κεφαλαιωδέστερον, ὅτι ἐν
πυρὶ κατῆλθε τὸ ἅγιον πνεῦμα. Ταύτην τὴν παράδοξον ἐνέργειαν τοῦ ἁγίου πνεύματος
οἱ συνετώτεροι τῶν Ἰουδαίων κατανοήσαντες, καὶ ὅτι οἱ πρὸ τούτου μονόλαλοι
ἀπόστολοι ἄνευ ἑρμηνευτοῦ πρὸς ἕνα ἕκαστον τῶν προσιόντων αὐτοῖς ἐκ
παρηλλαγμένων πατρίδων τῇ ἐπιχωριαζούσῃ γλώσσῃ παρεῖχον τὰ διδάγματα τῆς
εὐσεβείας, καθὼς ἀρτίως ἤκουες, καταπλαγέντες οἱ συνετώτεροι τῶν Ἰουδαίων
ἤρξαντο λέγειν πρὸς ἑαυτούς· Οὐχ ἰδοὺ πάντες οὗτοί εἰσιν οἱ λαλοῦντες Γαλιλαῖοι; Καὶ
πῶς ἡμεῖς ἀκούομεν ἕκαστος τῇ ἰδίᾳ διαλέκτῳ ἡμῶν ἐν ᾗ ἐγεννήθημεν, Πάρθοι καὶ
Μῆδοι καὶ Ἐλαμῖται καὶ οἱ κατοικοῦντες τὴν Μεσοποταμίαν, Ἰουδαίαν τε καὶ
Καππαδοκίαν, Πόντον καὶ τὴν Ἀσίαν, Φρυγίαν τε καὶ Παμφυλίαν, Αἴγυπτον καὶ τὰ μέρη
τῆς Λιβύης τῆς κατὰ Κυρήνην, καὶ οἱ ἐπιδημοῦντες Ῥωμαῖοι, Ἰουδαῖοί τε καὶ
προσήλυτοι, Κρῆτες καὶ Ἄραβες, ἀκούομεν λαλούντων αὐτῶν ταῖς ἡμετέραις γλώσσαις
τὰ μεγαλεῖα τοῦ θεοῦ; Κατεπλάγησαν οἱ Ἰουδαῖοι θεωροῦντες τοὺς ἀποστόλους
ποικιλογλωττοῦντας· ὅθεν καταπλαγέντες ἔλεγον πρὸς ἑαυτούς· Οὐχ ἰδοὺ πάντες οὗτοι
οἱ λαλοῦντές εἰσι Γαλιλαῖοι, πένητες καὶ ἰδιῶται; Οὐκ εἰσὶν ἀγράμματοι καὶ
δικτυοπλόκοι; Οὐ παρὰ τούτων συχνῶς ἰχθύας ἠγοράσαμεν; Πόθεν αὐτοῖς ἡ γνῶσις
αὕτη; Μὴ ἆρα ἀντὶ ἰχθύων γλώσσας ἐσαγήνευσαν οὗτοι; Ἀλλ' ὁρῶμεν αὐτοὺς πρὸ
τούτου μονογλώσσους. Πόθεν τῆς οἰκουμενικῆς ἐνθήκης τὰς διαφόρους ὁμιλίας ὡς ἐκ
βαλλαντίου τοῦ λάρυγγος προβάλλονται; Πόθεν αὐτοῖς ἡ γνῶσις αὕτη; Νῦν ἐμάθομεν
ὅτι ἀληθῶς θεὸς ὁ τούτων διδάσκαλος· νῦν ἐμάθομεν ὅτι τὰ ἀρχαῖα παρῆλθεν, ἰδοὺ
γέγονε καινὰ τὰ πάντα. Πέτρος ῥωμαΐζει, ὅπερ Μωϋσῆς οὐκ ἴσχυσε ποιῆσαι. Οὐκ
ἠκούσαμεν αὐτοῦ λέγοντος· Ἰσχνόφωνός εἰμι καὶ βραδύγλωσσος; Καὶ παρῃ τεῖτο τὴν
ἀποστολικὴν θείαν πρόσταξιν διὰ τὸ τῆς φωνῆς σπινῶδες, καὶ διόρθωσιν οὐκ ἐδέξατο,
ἀλλ' ἔμεινεν ἔχων ὅπερ ἡ φύσις ἐχαρίσατο. Μωσέα καταλείψωμεν καὶ τούτους
μακαρίσωμεν, μᾶλλον δὲ τὸν ἐν αὐτοῖς παραδοξοποιοῦντα θεὸν δοξάσωμεν, τὸν διὰ τὴν
κοινὴν τῶν πάντων ὠφέλειαν τὸ μονοειδὲς τῶν ἁλιέων γλωττιαῖον ὄργανον πρὸς τὴν
χρείαν τοῦ προσιόντος μεταρρυθμίζοντα. Τί τοιοῦτον τῷ Ἀβραὰμ παρεσχέθη;
Ὑπερηκόντισαν οὗτοι πάντας· χρημάτων διαπτύουσιν, δόξης καταφρονοῦσιν, ἀμισθὶ
θεραπεύουσιν, δι' ὅλου νηστεύουσιν, ὡς τρυφῶντες φαιδρύνονται, τοῖς νεκροῖς
ἐπιτάττουσιν, νόσους φυγαδεύουσιν καὶ δαίμονας μαραίνουσιν, ἀγγαρευόμενοι
χαίρουσιν, ῥαπιζόμενοι ἀντιστρέφουσι καὶ τὴν ἄλλην σιαγόνα, ἄνευ ῥάβδου καὶ πήρας
καὶ διπλοῦ χιτῶνος πορεύονται, χρυσὸν καὶ ἄργυρον καὶ χαλκὸν οὐδὲ ἀκοῇ
προσδέξασθαι βούλονται.
Τούτοις προσδράμωμεν τοῖς λαλοῦσι τὰ μεγαλεῖα τοῦ
θεοῦ, τοὺς λοιποὺς ἀποστραφῶμεν τοὺς φλυαροῦντας καὶ μηδὲν ὀνοῦντας. Ἀλλὰ ταῦτα
μὲν οἱ συνετώτεροι τῶν Ἰουδαίων ἔλεγον, οἱ δὲ παράνομοι πάλιν τῶν Ἰουδαίων ἐξ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

ἀντιστρόφου κινηθέντες τῷ φθόνῳ, καὶ οὗτοι ἤρξαντο ἀντιλέγειν, καθὼς ἀρτίως
ἤκουες, ὅτι γλεύκους μεμεστωμένοι εἰσίν. Τί ἐστι γλεύκους μεμεστωμένοι εἰσίν; Ἀντὶ
τοῦ μεθύουσι σφόδρα καὶ διὰ τοῦτο ἀλλόκοτα φθέγγονται. Ταῦτα ἀκούσας ὁ μακάριος
Πέτρος καὶ φέρειν μηκέτι δυνάμενοςἐκείνει γὰρ αὐτὸν ἔνδον λοιπὸν ὁ πυρσὸς τοῦ ἁγίου
πνεύματος σταθεὶς ἐν μέσῳ, καθὼς ἀρτίως ἤκουες, ἤρξατο λέγειν· Ἄνδρες Ἰουδαῖοι καὶ
οἱ κατοικοῦντες Ἱερουσαλὴμ πάντες,τοῦτο ὑμῖν γνωστὸν ἔστω καὶ ἐνωτίσασθε τὰ
ῥήματά μου. Οὐ γὰρ ὡς ὑμεῖς ὑπολαμβάνετε οὗτοι μεθύουσιν· ἔστι γὰρ ὥρα τρίτη τῆς
ἡμέρας. Καλῶς ὁ μακάριος Πέτρος μάρτυρα τῆς νηφαλαιότητος τὴν τρίτην ὥραν
ἐκάλεσεν. Καὶ τίς ἡ τρίτη ὥρα; Τὸ ἅγιον πνεῦμα. Πρώτη ὥρα ὁ πατήρ, ὡς τὸν νόμον
βραβεύσας. ∆ευτέρα ὥρα ὁ υἱός, ὡς τὴν χάριν δωρησάμενος. Τρίτη ὥρα τὸ ἅγιον
πνεῦμα, ὡς τὰς γλώσσας ποικίλας τῶν ἀποστόλων ἐργασάμενον. Φησὶν οὖν ὁ Πέτρος·
Ἄνδρες Ἰουδαῖοι καὶ οἱ κατοικοῦντες Ἱερουσαλὴμ πάντες, τοῦτο ὑμῖν γνωστὸν ἔστω,
καὶ ἐνωτίσασθε τὰ ῥήματά μου. Οὐ γὰρ ὡς ὑμεῖς ὑπολαμβάνετε οὗτοι μεθύουσιν. Μηδεὶς
τὴν δωρεὰν τοῦ θεοῦ μέθην ἡγήσηται· ἢ ὀσφράνθητε, ἢ ἀκούσατε. Πλησιέστερον
χωρήσατε, καὶ μάθετε ἀκριβῶς. Οἶνον ὄζουσιν, ἢ χάριν πνέουσιν; Ψελλίζουσιν, ἢ
ῥητορεύουσιν; Ὡς φύλλα κινοῦνται τῇ μέθῃ, ἢ ὡς στύλοι πεπήγασι τῇ νηφαλαιότητι;
Νυστάζουσι τῇ οἰνοφλυγίᾳ, ἢ διδάσκουσι τῇ θεογνωσίᾳ; Μέθην ἡγεῖσθε τὸ πρᾶγμα, ναὶ
Ἰουδαῖοι; Μέθη τὴν γλῶσσαν δεσμεῖ, τὸ στόμα διαστρέφει, τοὺς ὀφθαλμοὺς πελιδνοῖ,
τοὺς τοίχους πλευρίζει, λοξὰ καὶ δίδυμα βαδίζειν ποιεῖ. Μέθῃ γυμνοῦταί τις καὶ οὐκ
αἰσθάνεται· παρ' ὧν ἔδει τιμᾶσθαι, γελᾶται. Μεθύουσιν οἱ ἀπόστολοι, ναὶ Ἰουδαῖοι; Ὁ
τὴν κιβωτὸν τεκτονεύσας Νῶε ἐμεθύσθη, ἢ ἡμεῖς οἱ τὸν σταυρὸν κηρύττοντες;
Μεθύουσιν οἱ ἀπόστολοι, ναὶ Ἰουδαῖοι; Ὁ πρόγονος ὑμῶν ἐμεθύσθη Λὼτ ὁ
θυγατρογαμίας ὕπνον ὑπνώσας· μὴ γὰρ ἡμεῖς οἱ τῆς ἁγνείας κήρυκες; Καλῶς μὲν εἴπατε,
ὦ Ἰουδαῖοι, ὅτι μεθύομεν. Οὐκ ἀπὸ οἴνου καθὼς νοεῖτε ὑμεῖς, ἀλλ' ἀπὸ τῆς θείας
χάριτος· περὶ ταύτης γὰρ τῆς μέθης ἐκ πολλῶν τῶν χρόνων ὁ προφήτης ἐβόα·
Μεθυσθήσονται ἀπὸ πιότητος οἴκου σου, καὶ τὸν Χειμάρρουν τῆς τρυφῆς σου ποτιεῖς
αὐτούς, ὅτι παρὰ σοὶ πηγὴ ζωῆς. Πρόσεχε συνετῶς. Καιρὸς γὰρ λοιπὸν σφραγίσαι τὸν
λόγον καὶ συντομώτερον ὑμᾶς ἀπολῦσαι σήμερον· οἷμαι γὰρ ὑμᾶς προσαγορεῦσαι τοὺς
τάφους καὶ τὴν μνήμην τῶν προκοιμηθέντων τιμῆσαι τῇ ἐπιστασίᾳ. Ἀλλ' ὅμως, ὦ φίλε,
σπεῦσον τιμῆσαι καὶ μὴ ὑβρίσαι· μὴ διαπτύσῃς αὐτοῦ ὡς τεθνεῶτος· ζῇ γὰρ ὁ τούτου
δεσπότης καὶ γνωρίζει τῷ προτελευτήσαντι τί σὺ διαπράττῃ. Ἀπέρχῃ σήμερον τὸν
προτελευτήσαντα ῥοδίσαι· καλῶς ποιεῖς, ἐπαίνων εἶ ἄξιος. Ἀλλ' ὅμως ῥόδισον αὐτὸν
εὐποιΐᾳ, μὴ οἰνοφλυγίᾳ· στεφάνωσον τὸν προτελευτήσαντα τῇ εὐσεβείᾳ, μὴ τῇ
κρεοφαγίᾳ· πρότρεψαι αὐτὸν τῇ προσφορᾷ, μὴ τῇ συμφορᾷ. Ἐὰν ἐπάνω τοῦ μνήματος
σὺ ἐν τῷ παρόντι οἰνοποτήσῃς, ἢ κρεοφαγήσῃς, τί ὠφέλησας τὸν προτελευτήσαντα;
Μετάδος εἰς λόγον αὐτοῦ ὀβολὸν τῷ πτωχῷ, κἀκεῖνος εὐφραίνεται· θυμίασον τὴν
κοιλίαν τοῦ πεινῶντος ἄρτῳ, κἀκεῖνος ἡδύνεται. Ὢ τῆς ἀνθρωπίνης ἀδιαφορίας. Τὰ
βλάπτοντα φιλοῦμεν, τὰ σῴζοντα μισοῦμεν. Ποῖον εὔλογον σήμερον ἐπάνω τοῦ τάφου
στιβάδας στρῶσαι, καὶ συμπόσιον δημόσιον συναθροῖσαι καὶ ὡς ἐν γάμῳ πλατύτερον
γέλωτα ποιεῖσθαι; Ἄλλο γάμος καὶ ἄλλο τάφος.
Τί ἐπάνω τοῦ μνήματος σήμερον ὡς ἐν γενεθλίῳ κοσμικῷ σκιρτῶσιν; Ἄλλο
γενέθλιον κοσμικὸν καὶ ἄλλο ἐπιτάφιον πενθικόν. Ἀπέρχεται πολλάκις σήμερον ἐν τῷ
τάφῳ γυνὴ ῥοδίσαι τὸν ἑαυτῆς ἄνδρα· καὶ οὐ τοσοῦτον ῥοδίζει, ὅσον θλίβει.
Παραλαβοῦσα γὰρ τὸν παρείσακτον φίλον, μετεωρισθείσης χειρὸς ἐκλάσθη κανδῆλα
ἐπάνω ἀξιωματικῶν τινων. Καὶ πρὸς τοῦτο ὁ ὁμιλητὴς φησίν· Μηδένα λυπείτω τὸ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

πραχθέν, μήτε ὁ ῥύπος τῆς ἐσθῆτος τὴν ψυχὴν κηλιδωσάτω. Ἔστι γὰρ καὶ τοῦτο
ἐνέργεια τοῦ ἁγίου πνεύματος· ἔστι γὰρ καὶ ὕδωρ καὶ ἔλαιον καὶ πῦρ. Ἡ δὲ τοιαύτη γυνὴ
ἐπάνω τοῦ ἀνδρικοῦ τάφου σκιρτῶσα καὶ μετὰ θάνατον τὸν ἄνδρα παροξύνουσα, πῶς
ἂν κλαύσῃ τὸν ἄνδρα τῷ φίλῳ προσγελῶσα; Τῆς εὐταξίας, ὦ φίλοι, φροντίσωμεν, τὴν
τοῦ ἁγίου πνεύματος ἐπιστασίαν τιμήσωμεν, τῆς γραφῆς ἀκούσωμεν, καὶ τῷ κυρίῳ
πειθαρχήσωμεν. Ἤκουες τῇ προτεραίᾳ πάντως, ὦ φίλε, τί ἐλθὼν ὁ κύριος εἰς τὸ
μνημεῖον Λαζάρου διεπράξατο. Τὰ ὅμοια καὶ σὺ διαπράττου ὡς Χριστιανός. Τί δὲ ἐλθὼν
ὁ κύριος διεπράξατο; Συμμέτρως ἐδάκρυσεν, ἢ ἀμέτρως ᾠνοπότησεν; Ἐνε βριμήσατο τῷ
πνεύματι προσευξάμενος, ἢ ἐσκίρτησε καὶ μάχην εἰργάσατο; Παντί που δῆλον ὅτι καὶ
προσηύξατο καὶ ἐδάκρυσεν καὶ ἐνεβριμήσατο τῷ πνεύματι καὶ ἐτάραξεν ἑαυτὸν
συμπαθείας ὑπογραμμὸν ἑαυτὸν ἡμῖν παρεχόμενος. Κατόπιν τοίνυν ἀκολουθήσωμεν τῷ
κυρίῳ καὶ τῆς γραφῆς λεγούσης ἀκούσωμεν· Κλαῦσον ἐπὶ νεκρῷ, ὅτι ἐξέλιπε φῶς.
Κλαῦσον εἶπεν, οὐχὶ δὲ ἑόρτασον. Ἐν ἐκκλησίᾳ ἑόρτασον, ἐν τῷ τάφῳ Κλαῦσον, καὶ
τοῦτο συμμέτρως, ὡς ὁ κύριος τὸν Λάζαρον. Ἄκουε τοῦ Ἐκκλησιαστοῦ λέγοντος· Καιρὸς
τοῦ κλαῦσαι καὶ καιρὸς τοῦ γελάσαι. Τί ἐπάνω τοῦ τάφου ποδοκρουστοῦσι σήμερον, καὶ
κύμβαλα σατανικόφωνα κινοῦσιν, καὶ ᾄδουσι μέλη θεατρικά; Μὴ γὰρ ἐχθρὸν ἐνίκησαν,
ἢ τύραννον κατέστρεψαν, ὅτι χοροὺς σατανικοὺς συστησάμενοι οὕτω διὰ τῆς ἀγορᾶς
πομπεύουσιν; Ὢ τῶν ἀτόπων πραγμάτων. Τί τῶν ἀτόπων οὐ διαπράττονται σήμερον οἱ
ταφομεθυσταί; Σήμερον τοὺς τάφους ἀρχιτεκτονεύουσιν, σήμερον οἰκοδομεῖν
ἐπαγγέλλονται τὰ μνήματα
, σήμερον φλυαροῦντες δαψιλεύονται· Ἐὰν δέῃ με τὰς τρίχας μου πωλῆσαι, οὐ μὴ
καταλείψω τὸν τάφον ἀτρίχωτον. Οὐ θέλω τὸν γείτονα διοδεύειν ἐντεῦθεν, οὔτε
χήρους παρέρχεσθαι. Καὶ μόνον ἀποστῶσι τοῦ μνημείου, εὐθέως καὶ τὴν μνήμην
ἀποδύονται. Μέθη γὰρ μνήμης ἀρίστης ἀφανίστρια, ἀσέμνων λογισμῶν ἐπινοήτρια
, θεϊκῶν διδαγμάτων καταφονήτρια. Ἐκκλίνωμεν τοίνυν τὴν μέθην, δείξωμεν
συκοφάντας τοὺς Ἰουδαίους, τοὺς ἀρτίως εἰρηκότας ὅτι γλεύκους μεμεστωμένοι εἰσὶν οἱ
ἀπόστολοι, περαιώσωμεν τὴν τοῦ κυρίου διδασκαλίαν τὴν λέγουσαν, καθὼς ἀρτίως
ἠκούσαμεν· Ἐάν τις διψᾷ, ἐρχέσθω πρός με καὶ πινέτω· ὁ πιστεύων εἰς ἐμέ
, καθὼς εἶπεν ἡ γραφή, ποταμοὶ ἐκ τῆς κοιλίας αὐτοῦ ῥεύσουσιν ὕδατος ζῶντος.
Ἄκουε συνετῶς. Περὶ ὕδατος ζῶντος ἐδίδαξεν, οὐχὶ δὲ περὶ οἴνου μεθύσκοντος.
Ἐροῦσι δὲ πάντως τινέςοὐ λείπει γὰρ ἀντιλογία μεθυστῇ· Τί οὖν; Κακὸς ὁ οἶνος, ἐπειδὴ
διαβάλλεις τοῦτον; Εἰ δὲ κακὸς ὁ οἶνος, διὰ τί ὁ δεσπότης ἡμῶν Χριστὸς τὸ ὕδωρ εἰς
οἶνον μετέβαλεν; Οὕτω φησὶ κάλλιστος ὁ οἶνος ὑπάρχει. Ἄκουε συνετῶς. Οὐ τὸν οἶνον
ὑβρίζομεν, θεοῦ γὰρ δῶρόν ἐστιν, ἀλλὰ τὴν μέθην βδελυττόμεθα, διαβόλου γὰρ ἔργον
ὑπάρχει. Ὅμως πρὸς τοῖς εἰρημένοις ἄκουε καὶ τοῦτο· ἤδη γὰρ ὁ λόγος τὴν σιωπὴν
περιβλέπεται. Μετέβαλε μὲν ὁ κύριος τὸ ὕδωρ εἰς οἶνον. Ἀλλ' ὅμως τοῦτο πεποίηκεν ἐν
Κανᾷ τῆς Γαλιλαίας ἐν τοῖς γάμοις, οὐχὶ δὲ ἐν Βηθανίᾳ τῆς Ἰουδαίας ἐπὶ Λαζάρου τοῦ
τεθνεῶτος. Μετέβαλεν ὁ κύριος τὸ ὕδωρ εἰς οἶνον, ἐπὶ τῶν γαμούντων τοῦτο
πεποιηκώς, οὐχὶ δὲ ἐπὶ τῶν πενθούντων τὸν Λάζαρον. Ὅπου γάμος, ὕδωρ εἰς οἶνον
μετήνεγκεν· ὅπου νεκρός, δάκρυον ἐχορήγησεν. Τῷ δὲ θεῷ τῷ τὰ πάντα πληροῦντι
δόξαν ἀναπέμψωμεν, νῦν καὶ ἀεὶ καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

7

