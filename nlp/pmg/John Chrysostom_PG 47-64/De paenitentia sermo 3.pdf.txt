De paenitentia (sermo 3) [Sp.]
Περὶ μετανοίας. Λόγος γʹ.
60.705
∆ιαγράψωμεν, ἀγαπητοὶ, τὰ τῆς μετανοίας φάρμακα, ἵνα τῷ τῆς σωτηρίας
λιμένι προσδράμωμεν. Ἐτρώθημεν τῇ ἁμαρτίᾳ· ἰαθῶμεν τῇ μετανοίᾳ· διὰ τῆς
ἁμαρτίας ὁ διάβολος ἔτρωσε· διὰ τῆς μετανοίας ὁ Χριστὸς ἐθεράπευσεν· ἐκεῖνος διὰ
τῆς ἁμαρτίας τὰς ῥίζας κατεβάλετο· ὁ Κύριος πρόῤῥιζον τὴν ἁμαρτίαν τῇ ἀξίνῃ τῆς
μετανοίας ἐξέτεμεν. Ἰδοὺ γὰρ, φησὶν, ἡ ἀξίνη ἐπὶ πᾶν δένδρον κεῖται. Πᾶν οὖν
δένδρον μὴ ποιοῦν καρπὸν καλὸν, ἐκκόπτεται, καὶ εἰς πῦρ βάλλεται. Ὁ διάβολος τὴν
ἁμαρτίαν ὡς ζιζάνιον ἔσπειρε· ὁ Χριστὸς τῇ δρεπάνῃ τῆς μετανοίας τὴν ζιζανιώδη
ἁμαρτίαν ἀπέκειρε. Κακὸν πάθος ἡ ἁμαρτία, ἀλλὰ καλὸν πρᾶγμα ἡ μετάνοια.
Φύγωμεν τοίνυν τὴν φοβερὰν, καὶ στέρξωμεν τὴν ποθεινήν. ∆ι' ἁμαρτίας ὁ διάβολος
εἰς τὴν γέενναν ὁδηγεῖ· διὰ μετανοίας ὁ Χριστὸς εἰς τὴν βασιλείαν χειραγωγεῖ.
Ἥμαρτες; Ἡσύχασον· οὐ γὰρ τὸ ἁμαρτεῖν τοσοῦτον δεινὸν, ἀλλὰ τὸ ἐμμεῖναι τῇ
ἁμαρτίᾳ κακόν· τὸ μὲν γὰρ ἀπολογίαν ἔχεται, τὸ δὲ ἀσύγγνωστον τιμωρίαν
ἀποφέρεται.
Ἐπεὶ καὶ ὁ Πέτρος ἀρνησάμενος ἥμαρτε, καὶ Παῦλος διώκων τὸν Χριστὸν
παρηνόμησε, καὶ ∆αυῒδ μοιχείαν καὶ φόνον ἐργασάμενος, ἄξια θανάτου ἔπραξεν.
Ἀλλ' εἰ καὶ ὡς ἄνθρωποι ἥμαρτον, ἀλλ' ὡς ἅγιοι μετενόησαν· καὶ εἰ ἀπροόπτως τοῖς
δικτύοις τῆς ἁμαρτίας περιέπεσαν, ἀλλὰ τὴν λύμην τῆς ἀπιστίας ἐκ τῶν τῆς διανοίας
ὀφθαλμῶν ἀφελόμενοι, προσέδραμον τῇ μετανοίᾳ, ἵνα δι' αὐτῆς τὴν ἄφεσιν τῶν
πλημμελημάτων ἀπενέγκωνται. ∆εῦτε οὖν, καὶ ἡμεῖς προσδράμωμεν τῇ μετανοίᾳ. Εἰ
γὰρ καὶ ἐφόρτισεν ἡμᾶς ὁ διάβολος τῇ ἁμαρτίᾳ, ἀλλὰ κουφίζει ἡμᾶς ὁ Κύριος Ἰησοῦς
Χριστὸς, ὁ γεννηθεὶς ἐκ τῆς ἁγίας Παρθένου Μαρίας, τῇ μετανοίᾳ τὸ φορτίον τῆς
ἁμαρτίας εἰς ἑαυτὸν ἀναδεχόμενος· Ἴδε γὰρ, φησὶν, ὁ ἀμνὸς τοῦ Θεοῦ ὁ αἴρων τὴν
ἁμαρτίαν τοῦ κόσμου. Ὁ διάβολος γὰρ τὴν ἁμαρτίαν τοῖς ἀνθρώποις ἐπέθηκεν· ὁ
Κύριος τὴν ἁμαρτίαν ἀπὸ τῶν ἀνθρώπων ἀνέλαβεν. Ἐκεῖνος ἔδωκε τὴν ἁμαρτίαν,
ἵνα τῆς ἁμαρτίας ἕνεκεν ἀπολέσῃ τὸν ἄνθρωπον· ὁ Κύριος ἔλαβε τὴν ἁμαρτίαν, ἵνα
τῆς ἀνθρωπίνης φύσεως ἐλευθερώσῃ τὸ ἀτόπημα. Πρὸ τῆς τοῦ Χριστοῦ παρουσίας
φοβερὸς ἦν ὁ διάβολος, πρὸ τοῦ σταυροῦ ἀφόρητος ἦν ἡ ἁμαρτία· ὁ δὲ σταυρὸς νῦν
ἔθλασε τῆς ἁμαρτίας τὰ κέντρα· φανεὶς ἐπὶ γῆς ὁ Κύριος Ἰησοῦς, ὡς ἀστραπὴν ἐπὶ τὸ
τῆς γῆς χωρίον τὸν διάβολον κατέπαυσεν· Εἶδον γὰρ, φησὶ, τὸν Σατανᾶν ὡς
ἀστραπὴν ἐκ τοῦ οὐρανοῦ πεσόντα.
Ὁ θάνατος τοῦ Χριστοῦ ἐλυμήνατο τοῦ θανάτου τὴν βασιλείαν. Εἰ γὰρ καὶ
Ἐβασίλευσεν ὁ θάνατος ἀπὸ Ἀδὰμ μέχρι Μωϋσέως, καὶ ἐπὶ τοὺς μὴ ἁμαρτήσαντας ἐν
τῷ ὁμοιώματι τῆς παραβάσεως Ἀδὰμ, ὅς ἐστι τύπος τοῦ μέλλοντος· ἀλλ' ὅμως μετὰ
τὸν θάνατον τοῦ Κυρίου παίγνιον καὶ νηπίοις γέγονεν ὁ θάνατος. ∆ιὸ πάντες μετὰ
τὴν τοῦ Χριστοῦ ἀνάστασιν ἐπικερτομοῦντες τοῦ θανάτου τὸν τῦφον, λέγομεν· Ποῦ
σου, θάνατε, τὸ κέντρον; ποῦ σου, ᾅδη, τὸ νῖκος; Πρὸ τοῦ σταυροῦ καὶ ἡ τοῦ ὄφεως
συμβουλὴ θάνατον ἔπνεε· μετὰ δὲ τὸν σταυρὸν, μετὰ τὰ πάθη τοῦ Χριστοῦ, ἰδοὺ καὶ
πρεσβῦται καὶ γυναῖκες, μείρακές τε καὶ ἔφηβοι, ὥσπερ ληνοβάται βοτρύων, οὕτω
καὶ αὐτοὶ πατοῦσιν ἐπάνω ὄφεων καὶ σκορπίων καὶ ἐπὶ πᾶσαν τὴν δύναμιν τοῦ
ἐχθροῦ. Ταῦτα ἔφην, ἵνα δείξω τῆς ἁμαρτίας τὴν πτῶσιν, καὶ τῆς μετανοίας τὴν
δύναμιν. Μὴ ὀξύνῃς οὖν καθ' ἑαυτοῦ τῆς ἁμαρτίας τὸ κέντρον, μὴ ἡ σὴ ῥᾳθυμία
ἀκόνη γένηται τῆς ἁμαρτίας. Οὐ κατακρίνῃ ὅτι ἥμαρτες, ἀλλὰ τιμωρῇ ὅτι ἁμαρτήσας
οὐ μετενόησας. Ὥσπερ γὰρ τὴν ἀσθένειαν τοῦ σώματός σου γινώσκων ὁ Κύριος, τὴν
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἰατρικὴν ἐπιστήμην τοῖς ἀνθρώποις ἐπέτρεψε, καὶ πόας πρὸς τὴν ἰατρείαν
ἁρμοζούσας τὴν γῆν βλαστάνειν ἐποίησεν· οὕτω καὶ τῆς ψυχῆς εἰδὼς τὸ εὐόλισθον,
καὶ τῆς ἁμαρτίας τὰ τραύματα, ὡς ἀλεξητήριον ψυχῶν, τὴν μετάνοιαν ἔδωκε, καὶ
ταῖς θείαις καὶ 60.706 πνευματικαῖς βίβλοις τὰς τῆς μετανοίας ἀφορμὰς ὥσπερ
βοτάνας ἐνέθηκεν, ἵνα ἕκαστος ἡμῶν ἐντεῦθεν ἔχων τῆς ἰατρείας τὸ ἔμπλαστρον,
φύγῃ τὸ πάθος, ἐπιθεὶς ἑαυτῷ διὰ τῆς πίστεως. Ἀλλὰ μὴ μέχρι ῥημάτων στῇ ἡμῶν ἡ
μετάνοια· ἄτοπον γὰρ διὰ ῥημάτων μὲν ὑφαίνεσθαι τὴν μετάνοιαν, διὰ πραγμάτων
δὲ μὴ οἰκοδομεῖσθαι τὴν Ἐκκλησίαν Ἰησοῦ Χριστοῦ τοῦ Κυρίου ἡμῶν. Ὁμολόγησόν
σου τὰ πλημμελήματα, ἄνθρωπε, ἐξαγόρευσόν σου τὴν ἁμαρτίαν, ἵνα λάβῃς σου τὴν
ἄφεσιν τῆς ἁμαρτίας. Ἐξαγορεύων, τῆς ἀφέσεως τῶν πλημμελημάτων δέχῃ τὴν
δωρεάν. Μάρτυς ἱκέσθω ∆αυῒδ ὁ λέγων· Εἶπα, Ἐξαγορεύσω κατ' ἐμοῦ τὴν ἀνομίαν
μου τῷ Κυρίῳ, καὶ σὺ ἀφῆκας τὴν ἀσέβειαν τῆς καρδίας μου.
Νῦν ἐὰν εἴπῃς σου τὸ πλημμέλημα, εὑρίσκεις σου τὴν συγχώρησιν·
φιλανθρωπίας γὰρ ὁ παρὼν καιρός· ἐν δὲ τῷ μέλλοντι αἰῶνι καὶ θρηνῶν ἀνόνητα
θρηνήσεις, καὶ μεταμελόμενος οὐκ ἀπολαύσεις τῆς συγχωρήσεως. Ἐν γὰρ τῷ ᾅδῃ τίς
ἐξομολογήσεταί σοι; Εἰ δὲ καὶ ἐξομολογήσῃ τὸ πλημμέλημα τότε, ὠφελήσεις σαυτὸν
οὐδέν. Καὶ γὰρ ὁ πλούσιος ὁρῶν τοῦ Λαζάρου τὴν ἔνδειαν εἰς εὐθηνίαν
μεταβληθεῖσαν, καὶ τὴν ἑαυτοῦ περιουσίαν εἰς ἄπειρον πτωχείαν καταπεσοῦσαν,
ἔστενε μὲν, οὐκ ἠλεεῖτο δέ· ἐθρήνει καὶ ὠδύρετο, ἀλλ' οὐκ ἀπήλαυσε παρακλήσεως·
ἐζήτει ἔλεον, ἀλλ' οὐκ ἔλαβεν, ἐπειδὴ καὶ αὐτὸς αἰτούμενος, μεταδοῦναι οὐκ
ἤθελεν. Ἐπλούτησεν ὧδε, ἀλλ' ἐπτώχευσεν ἐκεῖ· ὧδε μεγάλα ἐφύσα ἐπὶ πλούτῳ,
ἐκεῖ ἐλεεινὰ ἐφθέγγετο ἐπὶ πενίᾳ· ὧδε οἴνων εἶχε χειμάῤῥους καὶ μύρων πηγὰς, ἐκεῖ
καὶ σταγόνα ὕδατος ζητῶν οὐκ ἐλάμβανεν, ἀλλ' ὧδε κἀκεῖσε περιέῤῥει, ὑπὸ τοῦ
ἀΰλου πυρὸς κατατηκόμενος. Προσδράμωμεν οὖν καὶ ἡμεῖς τῇ μετανοίᾳ· μετανοίας
γὰρ καιρός· σβέσωμεν τῆς ἁμαρτίας τὴν φλόγα τῇ δρόσῳ τῆς μετανοίας· μὴ τῇ
μακροθυμίᾳ τοῦ Θεοῦ προσχὼν, ἀμεταμέλητον κτήσῃ καρδίαν· μή σου σκληρύνῃ τὴν
γνώμην ἡ τοῦ Θεοῦ χρηστότης. Τὸ γὰρ χρηστὸν τοῦ Θεοῦ εἰς μετάνοιάν σε ἀγείρει
καὶ ἄγει. Ὅρα οὖν μὴ κατὰ τὴν σκληρότητά σου καὶ ἀμεταμέλητον καρδίαν
θησαυρίσῃς σεαυτῷ ὀργὴν ἐν ἡμέρᾳ ὀργῆς καὶ ἀποκαλύψεως καὶ δικαιοκρισίας τοῦ
Θεοῦ, ὃς ἀποδώσει ἑκάστῳ κατὰ τὰ ἔργα αὐτοῦ. Ἥμαρτες; Μετανόησον· μὴ διὰ τῆς
ῥᾳθυμίας ἀνίατον σεαυτῷ τὸ πάθος ποιήσῃς. Σκότος ἀλλοιοῦται, φωτὸς φανέντος·
καὶ ἁμαρτία ἀφανίζεται, μετανοίας ὀφθείσης. Γεωργὸς ἑαυτοῦ γένου· ὡς ζιζάνιον
ἔκτιλον τὴν ἁμαρτίαν, ὡς ἄκανθαν θέρισον τὴν παρανομίαν, ὡς ἀγριελαίου κλάδον,
ὡς ἀνήμερον καὶ ἄκαρπον φυτὸν ἔκκοψον τὴν ἁμαρτίαν. Χαίρουσι δαίμονες
ἁμαρτίαν ὁρῶντες, ἀγάλλονται ἄγγελοι μετάνοιαν καθορῶντες. Εὐφραίνονται γὰρ,
φησὶν, οἱ ἄγγελοι τοῦ Θεοῦ, κατὰ τὴν κυριακὴν φωνὴν, ἐπὶ ἑνὶ ἁμαρτωλῷ
μετανοοῦντι.
Μὴ γυμνώσῃς σαυτὸν, ἄνθρωπε, μὴ ῥίπτῃς τοῦ ἁγίου Πνεύματος τὴν
παντευχίαν, ἵνα μὴ εὐάλωτος γένῃ τοῖς πολεμίοις· τῷ τῆς μετανοίας θυρεῷ δέχου
καὶ τίνασσε τῆς ἐπιθυμίας τὰ βέλη. Ἡδεῖα ἡ πορνεία, ἀλλ' ἐπιβλαβής· τραχεῖα ἡ
σωφροσύνη, ἀλλ' ἐπωφελής. Ἡ πορνεία συμπράττουσαν ἔχει τοῦ σώματος τὴν
φύσιν· ἡ σωφροσύνη συνεργοῦσαν ἔχει τοῦ Πνεύματος τὴν χάριν. Ὅπου τοίνυν
πορνεία, ἐκεῖ ὁ διάβολος κατοικεῖ ὅπου δὲ σωφροσύνη, ἐκεῖ ὁ Κύριος Ἰησοῦς Χριστὸς
ἐπαναπαύεται. ∆ιὰ τοῦτο ὁ μακάριος Παῦλος λέγει· Εἴ τις τὸν ναὸν τοῦ Θεοῦ φθείρει,
φθείρει τοῦτον ὁ Θεός· καὶ πάλιν φησίν· Ἄρας οὖν τὰ μέλη τοῦ Χριστοῦ ποιήσω
πόρνης μέλη; Μὴ γένοιτο. Στήσωμεν οὖν τῆς ἁμαρτίας τὴν ῥύμην τῇ ἀγκύρᾳ τῆς
μετανοίας, ἵνα μὴ ἡ ἐπάλληλος τῶν ἐπιθυμιῶν ἀντίπνοια ναυάγιον ἡμᾶς ὑπομεῖναι
παρασκευάσῃ. Ἀλλ' οἱ μὲν πλωτῆρες ναυάγιον ἐν ὕδασιν ὑπομένουσιν, οἱ δὲ
ἁμαρτωλοὶ εἰς τὸν ποταμὸν τοῦ πυρὸς τὸν ῥέοντα ἔμπροσθεν τοῦ βήματος τοῦ
 
 

2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

Χριστοῦ, ὡσεὶ μόλιβδος καταποντίζονται, οὐ σὺν 60.707 ἅρμασι καὶ ἵπποις, ἀλλὰ σὺν
λογισμοῖς καὶ γνώμαις. Αἰδεσθῶμεν οὖν τὴν τοῦ Χριστοῦ ἀγαθότητα, ἐντράπωμεν
αὐτοῦ τὰ πάθη, μὴ ματαιώσωμεν αὐτοῦ τοὺς μώλωπας, μὴ τὴν αἰσχρὰν ἡδονὴν τῆς
καλῆς αὐτοῦ προκρίνωμεν ἡδονῆς καὶ ἐντολῆς, μὴ κακοῖς τὸν εὐεργέτην
ἀμειψώμεθα, μὴ λάβωμεν αὐτοῦ τὰ μέλη, καὶ ποιήσωμεν πόρνης μέλη· οὐκ ἐσμὲν
γὰρ ἑαυτῶν κύριοι· τιμῆς ἠγοράσθημεν. Τίς δὲ ἡμᾶς ἀπέδοτο, ἢ τίς ἡμᾶς ὠνήσατο; Ὁ
διάβολος ἡμᾶς ἀπέδοτο διὰ τῆς ἁμαρτίας, ὁ Χριστὸς ἡμᾶς ἐξηγόρασε διὰ τῆς
δικαιοσύνης. Πῶς δὲ ἡμᾶς ἠγόρασε; χρυσίον, ἢ ἀργύριον δούς; Οὐχὶ, ἀλλὰ τὸ οἰκεῖον
αἷμα τὸ ὑπὲρ χρυσίον καὶ λίθον τίμιον πολύ· πάσας γὰρ ἡμῶν τὰς ἁμαρτίας ἠγόρασε·
πολλαὶ δέ εἰσι, καὶ ποικίλως καὶ διαφόρως ἐνοχλοῦσαι ἡμῶν τὴν σωτηρίαν.
Ποίας ἁμαρτίας; Τὰς τοῦ νόμου· ἀλλ' οὐχ ὅτι ὁ νόμος ἁμαρτία, μὴ γένοιτο·
ἀλλ' ὅτι ἐκτὸς νόμου ἁμαρτίαν 60.708 οὐκ ᾔδειν. Ἐπεὶ οὖν ὁ νόμος ὡς παραβάτην με
ἔνοχον κατάρας ἐποίει, διὰ τοῦτο ἐλθὼν ὁ Χριστὸς Ἰησοῦς ἐξηγόρασέ με ἐκ τῆς
κατάρας τοῦ νόμου, γενόμενος ὑπὲρ ἡμῶν κατάρα. Μὴ τὴν πάνδημον οὖν πρόνοιαν
τοῦ διαβόλου προκρίνωμεν τῆς οἰκονομίας τοῦ Χριστοῦ· μὴ πρὸς χάριν τῆς ἁμαρτίας
παραδράμωμεν τῆς μετανοίας τὸ κάλλος. Εἰ φοβῇ τὴν ἀπειλὴν, κατάφυγε ἐπὶ τὴν
ἐντολήν· εἰ μὲν μέλλουσαν κρίσιν ἀποδρᾶσαι θέλεις, ἔκκλινον ἀπὸ κακοῦ, καὶ
ποίησον ἀγαθόν· τουτέστιν, ἀπόθου τὴν ἁμαρτίαν, καὶ ἐργάτης γένου τῆς μετανοίας,
ἵνα ὁ μὴ βουλόμενος τὸν θάνατον τῶν ἁμαρτωλῶν ὡς τὸ ἐπιστρέψαι καὶ ζῇν
αὐτοὺς, ἀποδεξάμενός σου τῆς ψυχῆς τὴν προθυμίαν, χαρίσηταί σοι τὴν ἄφεσιν τῆς
ἁμαρτίας· ἡμεῖς δὲ ὑπὲρ τῶν εἰρημένων αἶνον καὶ δόξαν ἀναπέμπωμεν τῷ Πατρὶ καὶ
τῷ Υἱῷ καὶ τῷ ἁγίῳ Πνεύματι, νῦν καὶ ἀεὶ, καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

