#!/bin/bash

text="Historiae
ΑΓΑΘΙΟΥ ΣΧΟΛΑΣΤΙΚΟΥ ΙΣΤΟΡΙΩΝ
Α ΠΡΟΟΙΜΙΟΝ
Καλὸν μέν τι χρῆμα καὶ εὔδαιμον νῖκαι πολέμων καὶ τρόπαια πόλεών τε
ἀνοικισμοὶ καὶ ἀγλαΐσματα καὶ ἅπαντα ὁπόσα μεγάλα τε καὶ ἀξιά γαστα ἔργα. ταῦτα
δὲ καὶ τὰ τοιάδε δόξαν μέν τινα καὶ ἡδονὴν τοῖς κτησαμένοις ἐπάγει, ἀποβιοῦσι δὲ
αὐτοῖς καὶ ἐκεῖσε οἰχομένοις οὔτι μάλα ἐθέλουσιν ἕπεσθαι, ἀλλὰ καὶ λήθη
παρεμπεσοῦσα ἐπικαλύπτει καὶ παρα τρέπει τὰς ἀληθεῖς τῶν πράξεων ἀποβάσεις·
ἤδη δὲ καὶ τῶν ἐπισταμένων ἀποβιούντων οἴχεται καὶ διαδιδράσκει ἡ γνῶσις σὺν
αὐτοῖς σβεννυμένη. 2 οὕτως ἄρα μνήμη γυμνὴ ἀνόνητόν τι καὶ οὐ μόνιμον οὐδὲ τῷ
μακρῷ συνεκτείνεσθαι πέφυκε χρόνῳ. καὶ οὐκ ἂν οἶμαι ἢ πατρίδος προκινδυνεύειν
ἔνιοι ἔγνωσαν ἢ ἄλλους ἀναδέχεσθαι πόνους, εὖ εἰδότες, ὡς, εἰ καὶ σφόδρα μέγιστα
δράσαιεν, συναπολεῖται τὸ κλέος αὐτοῖς καὶ διαρρυήσεται, μόνῳ τῷ βίῳ αὐτῶν
ἐκμεμετρημένον, εἰ μή τις, ὡς ἔοικε, θεία προμήθεια τὸ ἀσθενὲς τῆς φύσεως
ἀναρρωννῦσα τὰ ἐκ τῆς ἱστορίας ἐπεισήγαγεν ἀγαθὰ καὶ τὰς ἐνθένδε ἐλπίδας. 3 οὐ
γὰρ οἶμαι κοτίνου γε ἕνεκα καὶ σελίνου οἱ Ὀλυμπιονῖκαι καὶ Νεμεονῖκαι ἐν ταῖς
κονίστραις ἐναπεδύοντο, οὐδ' αὖ οἱ ἀγαθοὶ τῶν πολέμων ἀγωνισταὶ λαφύρων γε
μόνον καὶ τοῦ παραυ τίκα κερδαλέου ἐφιέμενοι ἐς προὖπτόν τε καὶ διαφανῆ
κίνδυνον σφᾶς αὐτοὺς ἀφιᾶσιν· ἀλλὰ δόξης ἀμφότεροι ἕκατι βεβαίας τε καὶ
ἀκηράτου, 4 ἣν οὐχ οἷόν τε ἄλλως καρπώσασθαι ἢ τῆς ἱστορίας αὐτοὺς
ἀπαθανατιζού σης, οὐχ οἶα τὰ Ζαμόλξιδος νόμιμα καὶ ἡ Γετικὴ παραφροσύνη, ἀλλ'
ὡς ἀληθῶς τρόπῳ τινὶ θείῳ τε καὶ ἀθανάτῳ καὶ ᾧ μόνῳ δύναται τὰ θνητὰ ἐς ἀεὶ
διαβιώσκειν. 4 ῥᾴδιον μὲν οὖν ἥκιστα ἂν εἴη ἅπαντα διεξιέναι καὶ ἀπαριθμεῖσθαι,
ὁπόσων ἀγαθῶν ἡ ἱστορία τὸν βίον ἐμπίπ λησι τὸν ἀνθρώπειον· ὡς δὲ συλλήβδην
εἰπεῖν, οἶμαί γε αὐτὴν φιλοσοφίας τῆς πολιτικῆς οὐ μάλα μειονεκτεῖσθαι, εἰ μή τι
καὶ μᾶλλον ὀνίνησιν.
sex,city,age
male,london,32
male,manchester,32
male,oxford,64
female,oxford,23
female,london,33
male,oxford,45
maale,omamaxford,45
ὁ κουβικουλάριος καὶ σπαθάριος ἐξελθὼν λαθραίως δι' ἑαυτοῦ καὶ τῶν
ἀνθρώπων ὥρμησαν εἰς τὸ Ἱππικόν, 
ὁ μὲν Ναρσῆς διὰ τῶν θυρῶν, 
Ναρσῆς ὁ δὲ υἱὸς Μούνδου


ὁ μὲν fff Ναρσῆς διὰ τῶν θυρῶν,
ὁ Ναρσῆς διὰ τῶν θυρῶν

"
 

function Set  { 
  var=`echo $1 | sed 's@\[\|\]@@g'`
  eval $var
   
}

function Get {
  var=`echo $1 | sed 's@\[\|\]@@g'`
  eval echo \$$var
   
}

function wordTokenise {
	text="$1"
echo "$text"  | tr ' ' '\n' | sort | uniq 
#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}




function findNames {
text="$1"
#find word that begins with capital and has pattern 
#echo "$text" | sed -rn '/ὁ/ { /ὁ\s[^\s]*\sΝαρσῆς/ p; }'
#echo "$text" | sed -rn '/(ma){2}/p'

# pattern
# ὁ word{0,2} [Α|Β|...|Ω].*

###echo "$text" | sed -rn '/ὁ/ { /ὁ(\s[^\s]*){0,2}\s[Α|Β|Ν][^\s]*/ p; }'

#ἐπὶ Ῥάβενναν
#ἐς Ῥάβενναν
#ἐν Ῥάβενναν
#εἰς τὴν
#Patterns
# Finds possible names and get the list of names
# ὁ word{0,2} [Α|Β|...|Ω].*
#(ὁ ([space or tab][not space,not tab]*){0,2} [space or tab][Α|Β|Ν][not space,not tab]*)[space or tab](.*$)

echo "$text" | sed -rn 's@(.*)(ὁ([ |\t][^ \t]*){0,2}[ |\t][Α|Β|Ν][^ \t]*)[ |\t](.*$)@<name>\2</name>@gp'




#finds properties of names get the list of names
# [Α|Β|...|Ω].* ὁ word{0,2}                                              

echo "$text" | sed -rn 's@(.*)([Α|Β|Ν][^ \t]*)([ |\t]ὁ([ |\t][^ \t]*){0,2}[ |\t])(.*$)@<name>\2\3</name>@gp'


#Check the word 'word' maybe noun or proposition
#there is list of propositions so check them and erase them
#ἐν Ῥαβέννῃ


#echo "$text" | sed -rn 's@(.*)(ὁ(\s[^\s]*){0,2}\s[Α|Β|Ν][^\s]*)\s(.*$)@&@gp'


#echo "$text" | sed -rn '/ὁ/ { /ὁ(\s[^\s]*){0,2}\sΝαρσῆς/ p; }'



#ὁ μὲν Ναρσῆς διὰ τῶν θυρῶν,
}







currentDir=`dirname $0`
echo $currentDir
sedfile=$currentDir"/patrologia.sed"
#cat "$file" | tr ' ' '\n' | sort | uniq | sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile




#boundary of word
res=`echo "$text" | sed -r 's@\sμ[^\s]*\s@4444@g'`






#extract verbs
#res=`echo "$text" | sed -r 's@\s[^\s]*ουσιν\s@4444@g'`


 
#res=`echo "$text" | sed  -rf "$sedfile"` 


#extract contitioned (if there is male print male and oxford)
res=`echo "$text" | sed -n '/male/ { /oxford/ p; }'`
res=`echo "$text" |sed -n '/\<male\>/ { /\<oxford\>/ p; }'`
echo "===================="
#res=`echo "$text" | sed -rn "$sedfile"`



res=`echo "$text" | sed -n '/ὁ/ { /ὁ\s[^\s]*\sΝαρσῆς/ p; }'`
#ὁ μὲν Ναρσῆς διὰ τῶν θυρῶν,
res=`echo "$text" | sed -n '/[Α|Ν]/p'`
#ΑΓΑΘΙΟΥ ΣΧΟΛΑΣΤΙΚΟΥ ΙΣΤΟΡΙΩΝ
#Α ΠΡΟΟΙΜΙΟΝ
#γὰρ οἶμαι κοτίνου γε ἕνεκα καὶ σελίνου οἱ Ὀλυμπιονῖκαι καὶ Νεμεονῖκαι 
#ἐν ταῖς
#ὁ μὲν Ναρσῆς διὰ τῶν θυρῶν,








res=`echo "$text" | awk -F, '/\<male\>/ && /\<oxford\>/'`
 
echo "$res"
#https://www.thegeekstuff.com/2010/01/8-powerful-awk-built-in-variables-fs-ofs-rs-ors-nr-nf-filename-fnr/?ref=binfind.com/web
#https://www.thegeekstuff.com/2010/01/awk-introduction-tutorial-7-awk-print-examples/
res=`echo "POP3_SERVER_NAME = localhost" |  awk '/POP3_SERVER_NAME/{print $NF}'`
#localhost


echo "$res"

echo "$text" | awk '\
BEGIN  { 
	print "hello\tGeorge" 
	} 
/^Να.*ς/ {
	 print $1, "\t", $2
	 }
END {
	 print " - DONE -" 
	 }
'







echo "==============================="
findNames "$text"

wordTokenise "$text" | sed -rn '/[Α|Β|Ν]/p'




exit



