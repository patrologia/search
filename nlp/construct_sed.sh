#!/bin/bash

function format {
x="$1"
echo "<b><font size=6>""$x""<\/font><\/b>"

}



#echo "2314325i5372946346735" | sed 's@2@'`format \1`'@g'




#exit


currentDir=`dirname $0`
echo $currentDir
wordsFolder="words"


function checkVerb {
or_verb="εικάζω"
or_verb="λύω"

or_stem=`echo "$or_verb" | sed -r 's@(^.*)ω$@\1@g'`
 

check_synfono1=`echo "$or_stem" | sed -r 's@^(λ|μ).*$@1@g'`
echo "$check_synfono1"
check_synfono2=`echo "$or_stem" | sed -r 's@^(αι|ει|α|ε).*$@2@g'`
echo "$check_synfono2"

if [ "$check_synfono1" = "1" ]
then 
par_stem=`echo "$or_stem" |sed -r '/^(λ|μ)/s/^(λ|μ)/έ\1/g'`
echo "$par_stem"
fi
if [ "$check_synfono2" = "2" ]
then
par_stem=`echo "$or_stem" |sed -r '/^(αι|ει|α|ε)/s/^(αι|ει|α|ε)/ή/g'`
echo "$par_stem"
fi


}


#checkVerb


function checkNoun {
IFS=' '

num=`echo  '
στρατιώτης
στρατιώτου' | sed -r 's@(^.*)(ης|ου$)@\1-\2@g' | wc -l`
echo $num
if [ $num -gt 1 ]
then

:

fi



}
#checkNoun
#exit

function compineSed {
file="$1"
count=0
str=''
#cat "$file" | sed -rn '/^[^#].*$/ p'
#remove the comments #something
for word in `cat "$file" | sed -rn '/^[^#].*$/ p'`
do
count=`expr $count + 1`
if [ $count -le 1 ]
then
str="$word""$str"
else
str="$word""|""$str"
fi
done
echo $str
}


function prepareSed {
IFS='
'
file="$1"
count=0
str=''
#cat "$file" | sed -rn '/^[^#].*$/ p'
#remove the comments #something
for line in `cat "$file" | sed -rn '/^[^#].*$/ p'`
do

group=`echo "$line" |sed 's@,@\n@g'`

str="$group"'
'"$str"



done
#Patern for 'ἀλλὰ μήν'
str2words=`echo "$str"  |sed -r 's@^ @@g'| sort | uniq | sed -rn '/[^ ]* [^ ]*$/p'`
#Patern for 'ἀλλὰ'
str1words=`echo "$str"  |sed -r 's@^ @@g'| sort | uniq | sed -rn '/^[^ ]*$/p'`
str="$str2words""$str1words"
#echo "$str"

sed='000'

for word in `echo "$str"`
do

sed="$sed""|"" $word "


done

echo "$sed"



}








function prepareSed2 {
IFS='
'
file="$1"
count=0
str=' '
#cat "$file" | sed -rn '/^[^#].*$/ p'
#remove the comments #something
for line in `cat "$file" | sed -rn '/^[^#].*$/ p'`
do

count=`expr $count + 1`
if [ $count -le 1 ]
then
str="$str""""$line"
else
str="$str"",""$line"
fi
done
echo "$str" | sed -r 's@,@\|@g'

}
function wordTokeniserNosort {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	
	
	
echo "$INPUT"  | tr ' ' '\n' | sed -r 's@[,|.|·]@@g' 
#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}
function wordTokeniser {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	
filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\
#more “ ” ‘ ˉχˉυ ʹ #ϛφπθʹ - Ϡπηʹ Ακουε :
echo "$INPUT"  | tr ' ' '\n' | sort | uniq|\
sed -r 's@[,|.|·|;]@@g' |\
sed -r 's@('"$filter"')@@g'|\
sed -r 's@[^[:print:]]@@g' |\
sed -r '/^[ \t]*$/d'\


#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}
function paragraphTokeniser {
	
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
# removes \n, \r and cut to points (\.|·)
	echo "$INPUT"  | sed -r ':a;N;$!ba;s/\n/ /g'| sed -r 's/\r//g' | sed -r 's@(\.|·)@\1\n@g'

	
}
function checkCapitalNames {
	if [ ! -t 0 ] 
    then
        INPUT=`cat`
    else
        INPUT=""
    fi 
	
IFS='
'
count=0
echo '' > "$currentDir""/""resutl.html"
for word in `cat "$currentDir""/""CapitalNames.txt"`
do


	count=`expr $count + 1`
	if [ $count -gt 10 ]
	then
	break 
	fi
#echo "$INPUT" | sed -rn 's@(.*)(ὁ([ |\t][^ \t]*){0,2}[ |\t][Α|Β|Ν][^ \t]*)[ |\t](.*$)@<name>\2</name>@gp'
#find "$currentDir""/""pmg" -type f -name '*.txt' -exec cat {} \;
#find . -name \*.xml | while read i; do grep '<?xml' "$i" >/dev/null; [ $? == 0 ] && echo $i; done

#find "$currentDir""/""pmg" -type f -name '*.txt'  -print0 | xargs -0 cat 

#<font size="16">This is some text!</font>

#cat `find "$currentDir""/""pmg" -type f -name '*.txt'`|paragraphTokeniser  |\
sed -rn 's@'"(.*)(ὁ|τοῦ|τῷ|τὸν|ὦ)([ |\t])($word)(.*)"'@\1<b><font size=6>\2\3\4<\/b><\/font>\5<br><br>@gp' >> "$currentDir""/""resutl.html"

pattern="(.*)([ |\t])(ὁ|τοῦ|τῷ|τὸν|ὦ)([ |\t])($word)([ |\t])(.*)"

cat `find "$currentDir""/""pmg" -type f -name '*.txt'`|paragraphTokeniser  |\
sed -rn 's@'"$pattern"'@\
\1\2'`format '\3\4\5'`'\6\7<br><br>\
@gp' >> "$currentDir""/""resutl.html"





echo "$word"
done	
ls
}

#file="text.txt"
#cat "$currentDir""/""$file" | checkCapitalNames
#exit



function writeCapitalNames {
	IFS='
	'
	echo "======"
	ECHO ''> "$currentDir""/""CapitalNames.txt"
for filePath in `find "$currentDir""/""pmg" -type f -name '*.txt'`
do 
 

echo $filePath
cat $filePath|wordTokeniser |findCapitalNames >> "$currentDir""/""CapitalNames.txt"

done
	
}




function findCapitalNames {
	 if [ ! -t 0 ] 
    then
        INPUT=`cat`
    else
        INPUT=""
    fi 
    capLetters="Α|Ἅ|Ἀ|Ἄ|Β|Γ|Δ|Ε|Ἔ|Ἑ|Ἐ|Ζ|Η|Ἦ|Θ|Ι|Ἰ|Ἱ|Κ|Λ|Μ|Ν|Ξ|Ο|Ὁ|Ὅ|Π|Ρ|Ῥ|Ῥ|Σ|T|Υ|Ὑ|Φ|Χ|Ψ|Ω|Ὠ|Ὢ|Ὧ|Ὡ"
   #echo "$INPUT" |sed -rn '/^[Α|Β|Ν].*$/ p'
    echo "$INPUT" |sed -rn '/^['"$capLetters"'].*$/ p'
    

}
#file="text.txt"
#cat "$currentDir""/""$file"|wordTokeniser |findCapitalNames
#writeCapitalNames








function sentenceTokeniser {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
    
#Parataktikoi syndesmoi    

file="syndesmoi.md"
syndesmoi=`prepareSed "$currentDir""/""$file"`
   
echo "$syndesmoi"
#syndesmoi=' εἰ καὶ | καὶ |,'
#syndesmoi=' εἰ καί | ,'
#εἰ καὶ

echo "$INPUT"  |sed -r 's@('"$syndesmoi"')@\n===\1===\n@g'
echo 's@('"$syndesmoi"')@\n===\1===\n@g'	
}
#sentenceTokeniser
#exit

function findCategoryWord {
	
file="text.txt"
cat "$currentDir""/""$file" |\
paragraphTokeniser |\
#sentenceTokeniser|\

wordTokeniserNosort|\

#λοχίας
#ας|ου|ᾳ|αν|α
sed -r  's@(ας|ου|ᾳ|αν|α)(<[^>]*>)*$@&<nM>@g'|\

#ποιητ-ής
#ής|οῦ|ῇ|ήν|ά
sed -r  's@(ής|οῦ|ῇ|ήν|ά)(<[^>]*>)*$@&<nM>@g'|\
#στρατιώτης
#ης|ου|ῃ|ην|α ή η
sed -r  's@(ης|ου|ῃ|ην|α|η)(<[^>]*>)*$@&<nM>@g'|\

#πολιτεί-α
#α|ας|ης|ᾳ|ῃ|αν|α
sed -r  's@(α|ας|ης|ᾳ|ῃ|αν|α)(<[^>]*>)*$@&<nF>@g'|\


# κώμ-η
#η|ης|ῃ|ην|η
sed -r  's@(η|ης|ῃ|ην|η)(<[^>]*>)*$@&<nF>@g'|\

# ψυχ-ή
#ή|ῆς|ῇ|ήν|ή
sed -r  's@(ή|ῆς|ῇ|ήν|ή)(<[^>]*>)*$@&<nF>@g'|\

#Plural Male and Female
#αι|ῶν|αις|ας|αι

sed -r  's@(αι|ῶν|αις|ας|αι)(<[^>]*>)*$@&<pnM><pnF>@g'


}

function createWordFiles {
IFS='
'
#wordsFolder="words"



#echo 's@('"$filter"')@@g'


#;'|'\\('|'\)\\

#echo "$currentDir"





rm -r "$currentDir""/""$wordsFolder"
mkdir "$currentDir""/""$wordsFolder"

cat `find "$currentDir""/""pmg" -type f -name '*.txt'`|\
wordTokeniser| sort | uniq >"$currentDir""/""$wordsFolder""/list4.html"


linux=`echo $SHELL`
#echo $linux
if [ "$linux" = "/bin/zsh" -o "$linux" = "/bin/bash" ]
then
#echo 'local'
echo '
ἀναγκαζόμενος
ἀναγκαστέον
ἀνάγκη
ἀνάγκη
ἄπαγε
ἀπαύ
ἄπιστον
ἀπίτωσαν
Ἀπόχρη
ἆρα
αὐτίκα
ἀφεστήκασιν
ἀγνοίας
'>"$currentDir""/""$wordsFolder""/list.html"
for word in `cat "$currentDir""/""$wordsFolder""/list.html"`
do

echo ''> "$currentDir""/""$wordsFolder""/""$word"
#echo $file
:
done




else


for word in `cat "$currentDir""/""$wordsFolder""/list.html"`
do

echo ''> "$currentDir""/""$wordsFolder""/""$word"

:
done
#echo 'gitlab'
:
fi



cat "$currentDir""/""$wordsFolder""/list.html"




#xargs -I % touch  "$currentDir""/""$wordsFolder""/"% 

 
#ls nlp/words
#head -35 nlp/words/list.html
}

function cutLength {
IFS='
'
if [ ! -t 0 ] 
    then
        INPUT=`cat`
    else
        INPUT=""
    fi
tmplist=''
for word in `echo "$INPUT"`
do
#echo "-------"
#echo $word
length=`echo -n $word | wc -c`
#echo $length

if [ "$length" -lt 8 ]
then


continue
else

tmplist=$word'
'$tmplist



fi
done
echo "$tmplist"




}




function main {
#cat nlp/allLIST.txt |wc -l

 
#exit

#file="text.txt"
#cat "$currentDir""/""$file" | paragraphTokeniser |sentenceTokeniser|wordTokeniser
#cat "$currentDir""/""$file" | sentenceTokeniser
#findCategoryWord

IFS='
'
list=`createWordFiles`

list=`echo "$list"|cutLength`

for word in `echo "$list"`
do

echo $word
cat `find "$currentDir""/""pmg" -type f -name '*.txt'`|\
paragraphTokeniser | sed -n '/'" $word "'/p' > "$currentDir""/""$wordsFolder""/"$word

done

#createWordFiles returns list.html which is used for search the patterns and fill the files
 

}
main
exit

file=epirimata.txt
#epirimata=`compineSed "$currentDir""/""$file"`
echo "========================"

file=protheseis.txt
protheseis=`compineSed "$currentDir""/""$file"`
echo "========================"

file=rimata-energitiki.txt
#rimata_energitiki=`compineSed "$currentDir""/""$file"`
echo "========================"

file=rimata-mesi.txt
#rimata_mesi=`compineSed "$currentDir""/""$file"`
echo "========================"

file=rimata-pathitiki.txt
#rimata_pathitiki=`compineSed "$currentDir""/""$file"`
echo "========================"


#<vb></vn>
#<nn></nn>
#<pr></pr>

file=text.txt
cat "$currentDir""/""$file" |wordTokeniser |findCapitalNames

cat "$currentDir""/""$file" | wordTokeniser  |\
sed -r 's@^('"$protheseis"')$@'"<vb>"'\1'"</vb>"'@'




#ls -A | tee >(grep ^[.])


exit
 
exit
exit
