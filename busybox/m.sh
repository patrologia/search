#!/bin/bash

currentDir=`dirname $0`
echo $currentDir
wordsFolder="words"
subfolder='Basilius Caesariensis_PG 29-32'
subfolder='Agathias Scholasticus Myrinaeus,Constantinopolitanus_PG 88'


subfolder=[t-t]*
subfolder='test'

find "$currentDir""/""pmg""/"$subfolder
 
 

function format {
x="$1"
echo "<b><font size=6>""$x""<\/font><\/b>"

}
function createdirectories {
#htmlFolder="html"

#mkdir "htmlFolder"
files=`find "$currentDir""/""$wordsFolder" -type f -name '*'`
#echo "$files"
for folderfile in `echo "$files"`
do


#echo "$currentDir""/""$wordsFolder""/"`basename "$folderfile"`


mv "$folderfile" "$folderfile"".html" 
mkdir "$folderfile"
mv "$folderfile"".html" "$folderfile""/""index.html" 
#echo    "$folderfile"".html"
#exit
 

done
	
	
}






function insertTemplate {

files=`find "$currentDir""/""$wordsFolder" -type f -name '*'`
#echo "$files"
for file in `echo "$files"`
do
#echo "$file"
sed -i '1i {{}}' "$file"
sed -i '$a \<script src="../../js/js.js"\>\<\/script\>' "$file"
done


#cp -r js word/js



}






function cutLength {
IFS='
'
if [ ! -t 0 ] 
    then
        INPUT=`cat`
    else
        INPUT=""
    fi
tmplist=''
for word in `echo "$INPUT"`
do
#echo "-------"
#echo $word
length=`echo -n $word | wc -c`
#echo $length

if [ "$length" -lt 8 ]
then


continue
else

tmplist=$word'
'$tmplist

fi
done
echo "$tmplist"




}

function paragraphTokeniser {
	
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
    
filter="Ερευνητικό έργο:|Εργαστήριο ∆ιαχείρισης|Χρηµατοδότηση|Πανεπιστήµιο Αιγαίου|Επιτρέπεται"

# removes filter text
   INPUT=`echo "$INPUT" | sed -r 's@^('"$filter"').*$@@g'`
    
# removes \n, \r and cut to points (\.|·)
	echo "$INPUT"  | sed -r ':a;N;$!ba;s/\n/ /g'| sed -r 's/\r//g' | sed -r 's@(\.|·)@\1\n@g'

	
}


function wordTokeniser {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	
filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\
#more “ ” ‘ ˉχˉυ ʹ #ϛφπθʹ - Ϡπηʹ Ακουε :
echo "$INPUT"  | tr ' ' '\n' | sort | uniq|\
sed -r 's@[,|.|·|;]@@g' |\
sed -r 's@('"$filter"')@@g'|\
sed -r 's@[^[:print:]]@@g' |\
sed -r '/^[ \t]*$/d'\


#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}

function wordTokeniserOnly {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	
filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\

filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\'|'#'|'“'|'”'|'-'|'‘'|':


#more “ ” ‘ ˉχˉυ ʹ #ϛφπθʹ - Ϡπηʹ Ακουε :
echo "$INPUT"  | tr ' ' '\n' |\
sed -r 's@[,|.|·|;]@@g' |\
sed -r 's@('"$filter"')@@g'|\
sed -r 's@[^[:print:]]@@g' |\
sed -r '/^[ \t]*$/d'\


#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}

#Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
count=0
function main {
 
 
 
 
#exit

 
rm -r "$currentDir""/""$wordsFolder"
mkdir "$currentDir""/""$wordsFolder"

IFS='
'
files=`find "$currentDir""/""pmg""/"$subfolder -type f -name '*.txt'`

for file in `echo "$files"`
do

echo "$file"
#==========

#cat "$file"|paragraphTokeniser | sed = |sed  '1i 00000' | sed 'N;s/\n/-->\t/'
cat "$file"|paragraphTokeniser|\
sed  '$a 99999' |sed =|sed  '1i 00000'  | sed 'N;s@\n@\t [PARAGRAPH=@'
#exit

#===========
for paragraph in `cat "$file"|paragraphTokeniser|\
sed  '$a 99999' |sed =|sed  '1i 00000'  | sed 'N;s@\n@\t [PARAGRAPH=@'`

do

#echo "$paragraph"

#listWords=`echo "$paragraph"|wordTokeniserOnly|cutLength`

listWords=`echo "$paragraph"|wordTokeniserOnly|sed -r 's@PARAGRAPH=@@g'`
echo "$listWords"
for word in `echo "$listWords"`
do

echo "$paragraph AUTHOR=$file]" >> "$currentDir""/""$wordsFolder""/"$word
#echo "$currentDir""/""$wordsFolder""/""$word"
done



done

done
#numbering methods
#https://linuxcommando.blogspot.com/2008/06/how-to-number-each-line-in-text-file-on.html
du -sh "$currentDir"

insertTemplate


createdirectories

}
main












